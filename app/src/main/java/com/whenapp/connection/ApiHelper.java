package com.whenapp.connection;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonObject;
import com.whenapp.model.Event;
import com.whenapp.model.Invitation;
import com.whenapp.model.InviteTermin;
import com.whenapp.model.SessionManager;
import com.whenapp.model.Termin;
import com.whenapp.model.User;
import com.whenapp.model.UsersGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tomislav on 02/05/15.
 */
public class ApiHelper {

    public static final String BASE_PATH = "http://api.whenapp.eu";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String USERNAME = "username";

    private static final String ACCESS_TOKEN = "access_token";

    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String LOCATION = "location";

    private static final String IS_SINGLE_DAY = "is_single_day";
    private static final String IS_ALL_DAY = "is_all_day";

    private static final String INVITATIONS = "invitations";
    private static final String INVITED_EMAIL = "invited_email";
    private static final String FRIENDS_EMAILS = "friends_emails";
    private static final String LIST_NAME = "list_name";

    private static final String TIME_SUGGESTIONS = "time_suggestions";
    private static final String CAN_INVITE = "can_invite";
    private static final String CAN_SUGGEST_TIME = "can_suggest";
    private static final String IMAGE_B64 = "base64_encoded_image";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";

    private static final String ACCEPT = "accept";

    private static final String FRIENDS = "friends";
    private static final String FRIEND_EMAIL = "friend_email";

    private static final String START_TIME = "start_time";
    private static final String END_TIME = "end_time";

    private static final String ID = "id";

    private static final String INVITATION_STATUS = "invitation_status";
    private static final String ADD_TIME_SUGGESTIONS = "add_time_suggestions";
    private static final String VOTE_FOR_TIME_SUGGESTION = "vote_for_time_suggestions";
    private static final String UNVOTE_FOR_TIME_SUGGESTION = "unvote_for_time_suggestions";
    private static final String FACEBOOK_ACCESS_TOKEN = "facebook_access_token";

    private static ApiHelper instance;
    private static RequestQueue requestQueue;
    private SessionManager session;

    public static ApiHelper getInstance(Context context) {
        if (instance == null) instance = new ApiHelper(context);
        return instance;
    }

    private ApiHelper(Context context) {
        requestQueue = Volley.newRequestQueue(context);
        session = SessionManager.getInstance(context);
    }

    public void login(String email, String password, final OnLoginListener onLoginListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(EMAIL, email);
        params.put(PASSWORD, password);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/login", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    String accessToken = response.optJSONObject("data").optString(ACCESS_TOKEN);
                    User user = new User(response.optJSONObject("data"));
                    onLoginListener.onLogin(user, accessToken);
                }else {
                    onLoginListener.onLoginFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onLoginListener.onLoginFail(getErrorMessage(error));
            }
        });

        requestQueue.add(request);
    }


    public void facebookLogin(String fb_access_token, final OnLoginListener onLoginListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(FACEBOOK_ACCESS_TOKEN, fb_access_token);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/connect_with_facebook", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {

                    String accessToken = response.optJSONObject("data").optString(ACCESS_TOKEN);
                    User user = new User(response.optJSONObject("data"));
                    onLoginListener.onLogin(user, accessToken);
                }else {
                    onLoginListener.onLoginFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onLoginListener.onLoginFail(getErrorMessage(error));
            }
        });

        requestQueue.add(request);
    }

    public interface OnLoginListener {
        void onLogin(User user, String accessToken);

        void onLoginFail(String errorMessage);
    }


    public void register(String name, String email, String password, final OnRegisterListener onRegisterListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(EMAIL, email);
        params.put(PASSWORD, password);
        params.put(USERNAME, name);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/register", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    User user = new User(response.optJSONObject("data"));
                    onRegisterListener.onRegister(user);
                } else {
                    onRegisterListener.onRegisterFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onRegisterListener.onRegisterFail(getErrorMessage(error));
            }
        });

        requestQueue.add(request);
    }

    public interface OnRegisterListener {
        void onRegister(User user);

        void onRegisterFail(String error);
    }

    public void logout(final OnLogoutListener onLogoutListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/register", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    User user = new User(response.optJSONObject("data"));
                    onLogoutListener.onLogout();
                } else {
                    onLogoutListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onLogoutListener.onFail(getErrorMessage(error));
            }
        });

        requestQueue.add(request);
    }

    public interface OnLogoutListener {
        void onLogout();
        void onFail(String errorMessage);
    }

    public void createEvent(String name, String description, String locationName, double latitude, double longitude, boolean isSingleDay, String b64Image, List<String> emails, Event.CanInvite canInvite, Event.CanSuggestTime canSuggestTime, List<Termin> termins, final OnCreateEventListener onCreateEventListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());
        params.put(NAME, name);
        params.put(DESCRIPTION, description);

        JSONArray invitations = new JSONArray();
        for (String email : emails) {
            JSONObject invite = new JSONObject();
            invite.put(INVITED_EMAIL, email);
            invitations.put(invite);
        }
        params.put(INVITATIONS, invitations);
        params.put(CAN_INVITE, canInvite.toString());
        params.put(CAN_SUGGEST_TIME, canSuggestTime.toString());

        JSONObject location = new JSONObject();
        location.put(NAME, locationName);
        if (latitude != 0)
            location.put(LATITUDE, latitude);
        if (longitude != 0)
            location.put(LONGITUDE, longitude);
        params.put(LOCATION, location);


        JSONArray timeSuggestions = new JSONArray();
        for (Termin t : termins) {
            JSONObject ts = new JSONObject();
            ts.put(START_TIME, t.getStart().getTimeInMillis() / 1000);
            ts.put(END_TIME, t.getEnd().getTimeInMillis() / 1000);
            if (t.isAllDay())
                ts.put(IS_ALL_DAY, true);
            timeSuggestions.put(ts);
        }
        params.put(TIME_SUGGESTIONS, timeSuggestions);

        params.put(IS_SINGLE_DAY, isSingleDay);

        params.put(IMAGE_B64, b64Image);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/events", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onCreateEventListener.onEventCreated(new Event(data));
                } else {
                    onCreateEventListener.onCreateFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String errorMessage = getErrorMessage(error);
                onCreateEventListener.onCreateFail(errorMessage);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(20000, 1, 1.2f));
        requestQueue.add(request);
    }

    public interface OnCreateEventListener {
        void onEventCreated(Event event);

        void onCreateFail(String error);
    }


    public interface OnGetEventListener {
        void onEvent(Event event);

        void onFail(String error);
    }


    public void getEvents(final OnGetEventsListener onGetEventsListener) {
        RequestParams params = new RequestParams();
        params.put(ACCESS_TOKEN, session.getAuthToken());
        long userId = session.getUser().getId();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, BASE_PATH + "/users/" + userId + params.getUrlParams(), params.getUrlParams(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        List<Event> events = new ArrayList<>();
                        JSONObject data = response.optJSONObject("data");
                        JSONArray invitationsJ = data.optJSONArray("actual_events");
                        if(invitationsJ != null)
                            for(int i = 0; i<invitationsJ.length(); i++){
                                events.add(new Event(invitationsJ.optJSONObject(i)));
                            }
                        onGetEventsListener.onEvents(events);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                onGetEventsListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }

    public interface OnGetEventsListener {
        void onEvents(List<Event> events);

        void onFail(String error);
    }



    public void getEvent(long eventId, final OnGetEventListener onGetEventListener) {
        RequestParams params = new RequestParams();
        params.put(ACCESS_TOKEN, session.getAuthToken());


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, BASE_PATH + "/events/" + eventId + params.getUrlParams(), params.getUrlParams(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        onGetEventListener.onEvent(new Event(response.optJSONObject("data")));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onGetEventListener.onFail(getErrorMessage(error));
            }
        });

        requestQueue.add(request);
    }

    public void getInvitation(long eventId, long invitationId, final OnGetInvitationListener onGetInvitationListener) {
        RequestParams params = new RequestParams();
        params.put(ACCESS_TOKEN, session.getAuthToken());


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, BASE_PATH + "/events/" + eventId + "/invitations/" + invitationId + params.getUrlParams(), params.getUrlParams(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        onGetInvitationListener.onInvitation(new Invitation(response.optJSONObject("data")));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onGetInvitationListener.onFail(getErrorMessage(error));
            }
        });

        requestQueue.add(request);
    }

    public interface OnGetInvitationListener {
        void onInvitation(Invitation invitation);
        void onFail(String error);
    }

    public void voteForTermin(long eventId, long terminId, final OnVoteTerminListener onVoteTerminListener) throws JSONException {
        RequestParams params = new RequestParams();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/events/" + eventId + "/time_suggestions/" + terminId + "/vote" + params.getUrlParams(), params.getUrlParams(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    InviteTermin it = new InviteTermin(response.optJSONObject("data"));
                    onVoteTerminListener.onVoted(it);
                } else {
                    onVoteTerminListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    if (error != null && error.getCause() != null) {
                        Crashlytics.logException(error.getCause());

                    }
                }catch (Exception e){}

                onVoteTerminListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }

    public void deleteVoteForTermin(long eventId, long terminId, final OnVoteTerminListener onVoteTerminListener) throws JSONException {
        RequestParams params = new RequestParams();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, BASE_PATH + "/events/" + eventId + "/time_suggestions/" + terminId + "/vote" + params.getUrlParams(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    InviteTermin it = new InviteTermin(response.optJSONObject("data"));
                    onVoteTerminListener.onVoted(it);
                } else {
                    onVoteTerminListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onVoteTerminListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }

    public interface OnVoteTerminListener {
        void onVoted(InviteTermin inviteTermin);

        void onFail(String errorMessage);
    }


    public void addTerminsToEvent(long eventId, List<Termin> termins, final OnTerminsAddedListener onTerminsAddedListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JSONArray timeSuggestions = new JSONArray();
        for (Termin t : termins) {
            JSONObject ts = new JSONObject();
            ts.put(START_TIME, t.getStart().getTimeInMillis() / 1000);
            ts.put(END_TIME, t.getEnd().getTimeInMillis() / 1000);
            if (t.isAllDay())
                ts.put(IS_ALL_DAY, true);
            timeSuggestions.put(ts);
        }
        params.put(TIME_SUGGESTIONS, timeSuggestions);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/events/" + eventId + "/time_suggestions", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onTerminsAddedListener.onTerminsAdded();
                } else {
                    onTerminsAddedListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onTerminsAddedListener.onFail(getErrorMessage(error));
            }

        });

        request.setRetryPolicy(new DefaultRetryPolicy(20000, 1, 1.2f));
        requestQueue.add(request);
    }

    public interface OnTerminsAddedListener {
        void onTerminsAdded();

        void onFail(String errorMessage);
    }

    public void inviteUserToEvento(long eventId, String email, final OnInviteUserToEventListener onInviteUserToEvent) throws JSONException {

        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());
        params.put(INVITED_EMAIL, email);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/events/" + eventId + "/invitations", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onInviteUserToEvent.onInvite(new Invitation(data));
                } else {
                    onInviteUserToEvent.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onInviteUserToEvent.onFail(getErrorMessage(error));
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(20000, 1, 1.2f));
        requestQueue.add(request);
    }

    public interface OnInviteUserToEventListener {
        void onInvite(Invitation invitation);
        void onFail(String errorMessage);
    }


    public void removeInvitationFromEvent(long eventId, final Invitation invitation, final OnRemoveInvitationListener onRemoveInvitationListener) {
        RequestParams params = new RequestParams();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, BASE_PATH + "/events/" + eventId + "/invitations/" + invitation.getId() + params.getUrlParams(), params.getUrlParams(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //JSONObject data = response.optJSONObject("data");
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    onRemoveInvitationListener.onInvitationRemoved(invitation);
                } else {
                    onRemoveInvitationListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onRemoveInvitationListener.onFail(getErrorMessage(error));
            }

        });

        request.setRetryPolicy(new DefaultRetryPolicy(20000, 1, 1.2f));
        requestQueue.add(request);
    }

    public interface OnRemoveInvitationListener {
        void onInvitationRemoved(Invitation invitation);

        void onFail(String errorMessage);
    }

    public void getUser(long id, final OnGetUserListener onGetUserListener) {
        RequestParams params = new RequestParams();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, BASE_PATH + "/users/" + id + params.getUrlParams(), params.getUrlParams(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONObject data = response.optJSONObject("data");

                onGetUserListener.onGetUser(new User(data));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onGetUserListener.onFail(getErrorMessage(error));
            }
        });


        request.setRetryPolicy(new DefaultRetryPolicy(2500, 2, 1.5f));
        requestQueue.add(request);
    }

    public interface OnGetUserListener {
        void onGetUser(User me);

        void onFail(String errorMessage);
    }


    public void exitEvent(long eventId, final OnEventExitListener onEventExitListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/events/" + eventId + "/cancel?" + ACCESS_TOKEN + "=" + session.getAuthToken(), params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onEventExitListener.onEventExit();
                } else {
                    onEventExitListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onEventExitListener.onFail(getErrorMessage(error));
            }
        });

        requestQueue.add(request);
    }

    public interface OnEventExitListener {
        void onEventExit();

        void onFail(String errorText);
    }


    public void createGroup(String name, List<String> userEmails, final OnCreateGroupListener onCreateGroupListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());
        params.put(LIST_NAME, name);

        long userId = session.getUser().getId();

        JSONArray userEmailsJ = new JSONArray();
        for (String ue : userEmails) {
            userEmailsJ.put(ue);
        }

        params.put(FRIENDS_EMAILS, userEmailsJ);


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/users/" + userId + "/custom_lists/friend_list", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onCreateGroupListener.onGroupCreated(new UsersGroup(data));
                } else {
                    onCreateGroupListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                onCreateGroupListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }

    public interface OnCreateGroupListener {
        void onGroupCreated(UsersGroup usersGroup);
        void onFail(String errorMessage);
    }

    public void addFriend(long userId, String email, final OnAddFriendListener onAddFriendListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());
        params.put(FRIEND_EMAIL, email);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/users/" + userId + "/friends", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onAddFriendListener.onFriendAdded();
                } else {
                    onAddFriendListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                onAddFriendListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }


    public interface OnAddFriendListener {
        void onFriendAdded();

        void onFail(String errorMessage);
    }


    public void removeFriend(long userId, long deletedUserId, final OnRemoveFriendListener onRemoveFriendListener) throws JSONException {
        RequestParams params = new RequestParams();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, BASE_PATH + "/users/" + userId + "/friends/" + deletedUserId  + params.getUrlParams(), params.getUrlParams(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onRemoveFriendListener.onFriendRemoved();
                } else {
                    onRemoveFriendListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                onRemoveFriendListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }

    public interface OnRemoveFriendListener {
        void onFriendRemoved();

        void onFail(String errorMessage);
    }

    public void deleteEvent(long eventId, final OnDeleteEventListener onDeleteEventListener) {
        RequestParams params = new RequestParams();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, BASE_PATH + "/events/" + eventId + params.getUrlParams(), params.getUrlParams(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");

                    onDeleteEventListener.onEventDeleted();
                } else {
                    onDeleteEventListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onDeleteEventListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }

    public interface OnDeleteEventListener {
        void onEventDeleted();

        void onFail(String errorMessage);
    }


    public void changeProfileImage(long userId, String base64Image, final OnChangeImageListener onChangeImageListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());
        params.put(IMAGE_B64, base64Image);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, BASE_PATH + "/users/" + userId + "/image", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onChangeImageListener.onImageChanged(new User(data));
                } else {
                    onChangeImageListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onChangeImageListener.onFail(getErrorMessage(error));
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(20000, 2, 1.5f));
        requestQueue.add(request);
    }



    public interface OnChangeImageListener {
        void onImageChanged(User me);

        void onFail(String errorMessage);
    }

    public void changeEventImage(long eventId, String base64Image, final OnChangeEventImageListener onChangeImageListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());
        params.put(IMAGE_B64, base64Image);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, BASE_PATH + "/events/" + eventId, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onChangeImageListener.onImageChanged(new Event(data));
                } else {
                    onChangeImageListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onChangeImageListener.onFail(getErrorMessage(error));
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(20000, 2, 1.5f));
        requestQueue.add(request);
    }

    public interface OnChangeEventImageListener {
        void onImageChanged(Event event);
        void onFail(String errorMessage);
    }

    public void removeUserFromGroup(long groupId, long userId, final OnRemoveUserFromGroupListener onRemoveUserFromGroupListener) throws JSONException {
        RequestParams params = new RequestParams();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, BASE_PATH + "/users/" + session.getUser().getId() + "/custom_lists/" + groupId + "/friends/" + userId + params.getUrlParams(), params.getUrlParams(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    onRemoveUserFromGroupListener.onUserRemoved();
                } else {
                    onRemoveUserFromGroupListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onRemoveUserFromGroupListener.onFail(getErrorMessage(error));
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(20000, 2, 1.5f));
        requestQueue.add(request);
    }

    public interface OnRemoveUserFromGroupListener {
        void onUserRemoved();
        void onFail(String errorMessage);
    }


    public void deleteGroup(long groupId, final OnDeleteGroupListener onDeleteGroupListener) throws JSONException {
        RequestParams params = new RequestParams();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, BASE_PATH + "/users/" + session.getUser().getId() + "/custom_lists/" + groupId + params.getUrlParams(), params.getUrlParams(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    onDeleteGroupListener.onGroupRemoved();
                } else {
                    onDeleteGroupListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onDeleteGroupListener.onFail(getErrorMessage(error));
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(20000, 2, 1.5f));
        requestQueue.add(request);
    }

    public interface OnDeleteGroupListener {
        void onGroupRemoved();
        void onFail(String errorMessage);
    }

    public void addUserToGroup(long groupId, final User user, final OnAddUserToGroup onAddUserToGroup) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());
        params.put(FRIEND_EMAIL, user.getEmail());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/users/" + session.getUser().getId() + "/custom_lists/" + groupId + "/friends", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    onAddUserToGroup.onUserAdded(user);
                } else {
                    onAddUserToGroup.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onAddUserToGroup.onFail(getErrorMessage(error));
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(20000, 2, 1.5f));
        requestQueue.add(request);
    }

    public interface OnAddUserToGroup {
        void onUserAdded(User user);
        void onFail(String errorMessage);
    }
    public void respondToInvitation(long eventId, long invitationId, boolean accept, @Nullable List<Termin> addedTermins, @Nullable List<InviteTermin> existingTerminsVoted, @Nullable List<InviteTermin> existingTerminsUnvoted, @Nullable List<User> userInvitations, final OnEventRespondListener onEventRespondListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        JSONObject resolveInvitation = new JSONObject();
        resolveInvitation.put("invitation_id", invitationId);
        resolveInvitation.put(ACCEPT, accept);

        params.put("resolve_invitation", resolveInvitation);
        params.put("event_id", eventId);

        if(accept){

            JSONArray addedTimeSuggestions = new JSONArray();
            if (addedTermins != null && !addedTermins.isEmpty()) {
                for(Termin t : addedTermins) {
                    JSONObject tJ = new JSONObject();
                    tJ.put(START_TIME, t.getStart().getTimeInMillis() / 1000);
                    tJ.put(END_TIME, t.getEnd().getTimeInMillis() / 1000);
                    addedTimeSuggestions.put(tJ);
                }
                params.put(ADD_TIME_SUGGESTIONS, addedTimeSuggestions);
            }


            JSONArray terminsVoted = new JSONArray();
            if (existingTerminsVoted != null && !existingTerminsVoted.isEmpty()) {
                for (InviteTermin it : existingTerminsVoted) {
                    terminsVoted.put(it.getId());
                }
                params.put(VOTE_FOR_TIME_SUGGESTION, terminsVoted);
            }

            JSONArray terminsUnvoted = new JSONArray();
            if (existingTerminsUnvoted != null && !existingTerminsUnvoted.isEmpty()) {
                for (InviteTermin it : existingTerminsUnvoted) {
                    terminsUnvoted.put(it.getId());
                }
                params.put(UNVOTE_FOR_TIME_SUGGESTION, terminsUnvoted);
            }

            JSONArray usersAdded = new JSONArray();
            if (userInvitations != null && !userInvitations.isEmpty()) {
                for (User u : userInvitations) {
                    JSONObject invitedEmail = new JSONObject();
                    invitedEmail.put(INVITED_EMAIL, u.getEmail());
                    usersAdded.put(invitedEmail);
                }
                params.put(INVITATIONS, usersAdded);
            }
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/events/" + eventId + "/complex", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onEventRespondListener.onResponded(new Event(data));
                } else {
                    onEventRespondListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onEventRespondListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }

    public void editEvent(long eventId, @Nullable List<Termin> addedTermins, @Nullable List<InviteTermin> existingTerminsVoted, @Nullable List<InviteTermin> existingTerminsUnvoted, @Nullable List<User> userInvitations, final OnEventRespondListener onEventRespondListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());

        params.put("event_id", eventId);

            JSONArray addedTimeSuggestions = new JSONArray();
            if (addedTermins != null && !addedTermins.isEmpty()) {
                for(Termin t : addedTermins) {
                    JSONObject tJ = new JSONObject();
                    tJ.put(START_TIME, t.getStart().getTimeInMillis() / 1000);
                    tJ.put(END_TIME, t.getEnd().getTimeInMillis() / 1000);
                    addedTimeSuggestions.put(tJ);
                }
                params.put(ADD_TIME_SUGGESTIONS, addedTimeSuggestions);
            }


            JSONArray terminsVoted = new JSONArray();
            if (existingTerminsVoted != null && !existingTerminsVoted.isEmpty()) {
                for (InviteTermin it : existingTerminsVoted) {
                    terminsVoted.put(it.getId());
                }
                params.put(VOTE_FOR_TIME_SUGGESTION, terminsVoted);

            }

            JSONArray terminsUnvoted = new JSONArray();
            if (existingTerminsUnvoted != null && !existingTerminsUnvoted.isEmpty()) {
                for (InviteTermin it : existingTerminsUnvoted) {
                    terminsUnvoted.put(it.getId());
                }
                params.put(UNVOTE_FOR_TIME_SUGGESTION, terminsUnvoted);

            }

            JSONArray usersAdded = new JSONArray();
            if (userInvitations != null && !userInvitations.isEmpty()) {
                for (User u : userInvitations) {
                    JSONObject invitedEmail = new JSONObject();
                    invitedEmail.put(INVITED_EMAIL, u.getEmail());
                    usersAdded.put(invitedEmail);
                }
                params.put(INVITATIONS, usersAdded);

            }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/events/" + eventId + "/complex", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onEventRespondListener.onResponded(new Event(data));
                } else {
                    onEventRespondListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onEventRespondListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }

    public interface OnEventRespondListener {
        void onResponded(Event event);
        void onFail(String errorMessage);
    }

    public void respondToInvitatione(long eventId, long invitationId, boolean accept, final OnEventRespondListener onEventRespondListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());
        params.put(ACCEPT, accept);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, BASE_PATH + "/events/" + eventId + "/invitations/" + invitationId, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int statusCode = response.optInt("status");
                if (statusCode == 200) {
                    JSONObject data = response.optJSONObject("data");
                    onEventRespondListener.onResponded(new Event());
                } else {
                    onEventRespondListener.onFail(getErrorMessage(response));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onEventRespondListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }

    public interface OnInvitationRespondListener {
        void onResponded();
        void onFail(String errorMessage);
    }


    public static final String REGISTRATION_ID = "registration_id";

    public void pushRegister(String regId, final OnPushRegisterListener onPushRegisterListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());
        params.put(REGISTRATION_ID, regId);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/push/register" , params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONObject data = response.optJSONObject("data");
                onPushRegisterListener.onRegistered();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onPushRegisterListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }


    public void pushUnRegister(String regId, final OnPushRegisterListener onPushRegisterListener) throws JSONException {
        JSONObject params = new JSONObject();
        params.put(ACCESS_TOKEN, session.getAuthToken());
        params.put(REGISTRATION_ID, regId);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, BASE_PATH + "/push/unregister" , params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONObject data = response.optJSONObject("data");
                onPushRegisterListener.onRegistered();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onPushRegisterListener.onFail(getErrorMessage(error));
            }
        });
        requestQueue.add(request);
    }


    public interface OnPushRegisterListener {
        void onRegistered();
        void onFail(String errorMessage);
    }

    private static class RequestParams {
        private Map<String, Object> map = new HashMap<>();

        public RequestParams() {

        }

        public RequestParams(String key, Object value) {
            map.put(key, value);
        }

        public void put(String key, Object value) {
            map.put(key, value);
        }

        public String getUrlParams() {
            String p = "?";
            for (String key : map.keySet()) {
                p += key + "=" + String.valueOf(map.get(key)) + "&";
            }
            return p;
        }
    }




    private static String getErrorMessage(VolleyError error) {
        String errorMessage = "Unexpected error occured.";
        if (error.networkResponse != null) {
            String jsonString;
            try {
                jsonString = new String(error.networkResponse.data,
                        HttpHeaderParser.parseCharset(error.networkResponse.headers));
                Response<JSONObject> responseJson = Response.success(new JSONObject(jsonString),
                        HttpHeaderParser.parseCacheHeaders(error.networkResponse));
                Log.i("API", responseJson.result.toString());
                JSONObject resJson = responseJson.result;
                JSONObject data = resJson.optJSONObject("data");
                if (data != null) {
                    errorMessage = data.optString("status");
                }

            } catch (UnsupportedEncodingException | JSONException e) {
                e.printStackTrace();
            }
        }

        //responseHandler.onFailure(error.networkResponse.statusCode, new Header[]{}, error.getCause(), new JSONObject(error.getMessage()));
        VolleyLog.e("Error: ", error.getMessage());
        return errorMessage;
    }


    private static String getErrorMessage(JSONObject response){
        String errorMessage = "Unexpected error occured.";
        if(response != null) {
            JSONObject data = response.optJSONObject("data");
            if (data != null) {
                errorMessage = data.optString("status");
            }
        }
        return errorMessage;
    }

}
