package com.whenapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.analytics.HitBuilders;
import com.whenapp.adapters.UsersAdapter;
import com.whenapp.adapters.UsersDeletableAdapter;
import com.whenapp.connection.ApiHelper;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.UserGroupsDataLoader;
import com.whenapp.database.UsersGroupsTable;
import com.whenapp.model.SessionManager;
import com.whenapp.model.User;
import com.whenapp.model.UsersGroup;
import com.whenapp.views.DialogBuilder;

import org.json.JSONException;

import java.util.List;


public class UsersGroupActivity extends BaseActivity {

    public static final String USERS_GROUP = "users_group";
    UsersGroup usersGroup;
    UsersDeletableAdapter usersAdapter;

    UserGroupsDataLoader userGroupsDataLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_group);

        setResult(false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        userGroupsDataLoader = new UserGroupsDataLoader(this, new UsersGroupsTable(CalendarDatabaseHelper.getInstance(this).getWritableDatabase()), null, null, null, null, null);


        if(getIntent().hasExtra(USERS_GROUP)){
            usersGroup = getIntent().getParcelableExtra(USERS_GROUP);
        }

        RecyclerView userRecyclerView = (RecyclerView) findViewById(R.id.users_recycler_view);
        usersAdapter = new UsersDeletableAdapter(usersGroup.getUsers(), new UsersDeletableAdapter.OnUserClickedListener() {
            @Override
            public void onUserClicked(User user) {
                Intent uIntent = new Intent(UsersGroupActivity.this, ProfileActivity.class);
                uIntent.putExtra("user", user);
                startActivity(uIntent);
            }

            @Override
            public void onUserDeleted(final User user) {
                DialogBuilder.showYesNoDialog(UsersGroupActivity.this, getString(R.string.are_sure_delete_user, user.getName()), new DialogBuilder.OnYesNoDialogResponse() {
                    @Override
                    public void onResponse(boolean pressedYes) {
                        if(pressedYes) deleteUser(user, new ApiHelper.OnRemoveUserFromGroupListener() {
                            @Override
                            public void onUserRemoved() {
                                getGATracker().send(new HitBuilders.EventBuilder("UserGroup", "UserRemoved").build());
                                usersAdapter.removeUser(user);
                                usersGroup.getUsers().remove(user);
                                userGroupsDataLoader.update(usersGroup);
                                setResult(true);
                            }

                            @Override
                            public void onFail(String errorMessage) {
                                Toast.makeText(UsersGroupActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        });

        usersAdapter.setUsers(usersGroup.getUsers());
        userRecyclerView.setAdapter(usersAdapter);
        userRecyclerView.setLayoutManager(new LinearLayoutManager(this){

            @Override
            public int getPaddingTop() {
                return (int) (16 * getResources().getDisplayMetrics().density);
            }

            @Override
            public int getPaddingBottom() {
                return (int) (64 * getResources().getDisplayMetrics().density);
            }

        });
        userRecyclerView.setItemAnimator(new DefaultItemAnimator());

        TextView groupName = (TextView) findViewById(R.id.group_name);
        TextView groupUsersCount = (TextView) findViewById(R.id.group_users_count);

        groupName.setText(usersGroup.getName());
        groupUsersCount.setText(getString(R.string.n_friends, usersGroup.getUsers().size()));

        FloatingActionButton edit = (FloatingActionButton) findViewById(R.id.group_edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usersAdapter.setEditable(!usersAdapter.isEditable());
            }
        });

        FloatingActionButton addUser = (FloatingActionButton) findViewById(R.id.group_add_user);
        addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addUser = new Intent(UsersGroupActivity.this, AddFriendsActivity.class);
                startActivityForResult(addUser, 333);
            }
        });

    }

    private void deleteUser(User user, ApiHelper.OnRemoveUserFromGroupListener onRemoveUserFromGroupListener) {
        try {
            ApiHelper.getInstance(this).removeUserFromGroup(usersGroup.getId(), user.getId(), onRemoveUserFromGroupListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_users_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == android.R.id.home){
            supportFinishAfterTransition();
        }else if (id == R.id.action_delete_group) {
            deleteGroup();
        } else if (id == R.id.action_single_day) {
            Intent createSingle = new Intent(this, CreateEventActivity.class);
            createSingle.putExtra(CreateEventActivity.IS_SINGLE_DAY, true);
            createSingle.putExtra(CreateEventActivity.USERS_GROUP, usersGroup);
            startActivity(createSingle);
        } else if (id == R.id.action_multi_day) {
            Intent createSingle = new Intent(this, CreateEventActivity.class);
            createSingle.putExtra(CreateEventActivity.IS_SINGLE_DAY, false);
            createSingle.putExtra(CreateEventActivity.USERS_GROUP, usersGroup);
            startActivity(createSingle);
        }


        return super.onOptionsItemSelected(item);
    }

    private void deleteGroup() {
        DialogBuilder.showYesNoDialog(this, getString(R.string.sure_delete_group), new DialogBuilder.OnYesNoDialogResponse() {
            @Override
            public void onResponse(boolean pressedYes) {
                if (pressedYes) {
                    try {
                        ApiHelper.getInstance(UsersGroupActivity.this).deleteGroup(usersGroup.getId(), new ApiHelper.OnDeleteGroupListener() {
                            @Override
                            public void onGroupRemoved() {
                                getGATracker().send(new HitBuilders.EventBuilder("UserGroup", "GroupRemoved").build());
                                userGroupsDataLoader.delete(usersGroup);
                                setResult(true);
                                supportFinishAfterTransition();
                            }

                            @Override
                            public void onFail(String errorMessage) {
                                Toast.makeText(UsersGroupActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }


    private void setResult(boolean ok){
        if(ok) {
            if (getParent() != null) {
                getParent().setResult(RESULT_OK);
            }else{
                setResult(RESULT_OK);
            }
        }else{
            if (getParent() != null) {
                getParent().setResult(RESULT_CANCELED);
            }else{
                setResult(RESULT_CANCELED);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 333) {
            if (resultCode == Activity.RESULT_OK) {
                final List<User> users =  data.getParcelableArrayListExtra("contacts");

                for (User u : users) {
                    if(usersGroup.getUsers().contains(u)) continue;
                    try {
                        getGATracker().send(new HitBuilders.EventBuilder("UserGroup", "UserAdded").setLabel(u.getEmail()).build());
                        ApiHelper.getInstance(this).addUserToGroup(usersGroup.getId(), u, new ApiHelper.OnAddUserToGroup() {
                            @Override
                            public void onUserAdded(User user) {
                                usersGroup.getUsers().add(user);
                                usersAdapter.addUser(user);
                                userGroupsDataLoader.update(usersGroup);
                                setResult(true);
                            }

                            @Override
                            public void onFail(String errorMessage) {
                                Toast.makeText(UsersGroupActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
