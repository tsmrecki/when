package com.whenapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.whenapp.connection.ApiHelper;
import com.whenapp.model.ReminderManager;


/**
 * Created by tomislav on 26/10/15.
 */
public class OnReminderReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        String title = intent.getStringExtra("title");
        String imageUrl = intent.getStringExtra("image");
        Long eventId = intent.getLongExtra("event_id", 0);
        Intent event = new Intent(context, EventActivity.class);
        event.putExtra(EventActivity.EVENT_ID, eventId);
        PendingIntent pi = PendingIntent.getActivity(context, eventId.intValue(), event, PendingIntent.FLAG_CANCEL_CURRENT);


        Bitmap largeIcon;
        if(imageUrl != null) {
            ImageSize imageSize = new ImageSize(android.R.dimen.notification_large_icon_width, android.R.dimen.notification_large_icon_height);
            largeIcon = ImageLoader.getInstance().loadImageSync(ApiHelper.BASE_PATH + imageUrl, imageSize, new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).build());
        }
        else {
            largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        }

        String description = ReminderManager.getReminderTextPretty(ReminderManager.getInstance(context).remindsInFront(eventId), context);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_stat_notify)
                        .setLargeIcon(largeIcon)
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(description))
                        .setAutoCancel(true)
                        .setContentText(description);

        if(imageUrl != null) mBuilder.setColor(context.getResources().getColor(R.color.prominent_green));
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setLights(context.getResources().getColor(R.color.prominent_green), 1000, 800);
        mBuilder.setContentIntent(pi);

        NotificationManager mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(eventId.intValue(), mBuilder.build());

        ReminderManager.getInstance(context).setReminderForEvent(eventId, false, null);
    }

}
