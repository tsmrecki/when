package com.whenapp.syncadapter;

/**
 * Created by tomislav on 21/04/15.
 */

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.RemoteException;

import com.whenapp.connection.ApiHelper;
import com.whenapp.database.EventsContentProvider;
import com.whenapp.database.EventsTable;
import com.whenapp.database.InvitationsTable;
import com.whenapp.database.TerminsTable;
import com.whenapp.database.UsersTable;
import com.whenapp.model.Event;
import com.whenapp.model.Invitation;
import com.whenapp.model.InviteTermin;
import com.whenapp.model.User;

import java.util.List;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    // Global variables
    // Define a variable to contain a content resolver instance
    ContentResolver mContentResolver;

    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();

    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, final ContentProviderClient provider, SyncResult syncResult) {

        try {
            provider.delete(EventsContentProvider.CONTENT_URI, null, null);
            provider.delete(EventsContentProvider.CONTENT_URI_TERMINS, null, null);
            provider.delete(EventsContentProvider.CONTENT_URI_USERS, null, null);
            provider.delete(EventsContentProvider.CONTENT_URI_INVITATIONS, null, null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


        ApiHelper.getInstance(getContext()).getEvents(new ApiHelper.OnGetEventsListener() {
            @Override
            public void onEvents(List<Event> events) {
                try {
                    for (Event event : events) {

                        provider.insert(EventsContentProvider.CONTENT_URI_USERS, UsersTable.generateContentValuesFromObject(event.getInvitee()));

                        for (User u : event.getPeople()) {
                            provider.insert(EventsContentProvider.CONTENT_URI_USERS, UsersTable.generateContentValuesFromObject(u));
                        }
                        for (InviteTermin i : event.getInviteTerminList()) {
                            provider.insert(EventsContentProvider.CONTENT_URI_TERMINS, TerminsTable.generateContentValuesFromObject(i));
                        }

                        for (Invitation i : event.getInvitations()) {
                            provider.insert(EventsContentProvider.CONTENT_URI_INVITATIONS, InvitationsTable.generateContentValuesFromObject(i));
                        }

                        provider.insert(EventsContentProvider.CONTENT_URI, EventsTable.generateContentValuesFromObject(event));


                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(String error) {
            }
        });


    }




}