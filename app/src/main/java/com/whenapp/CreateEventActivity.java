package com.whenapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.TransitionInflater;
import android.transition.TransitionSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.whenapp.adapters.PlaceAutocompleteAdapter;
import com.whenapp.adapters.TerminsAdapter;
import com.whenapp.adapters.TerminsMultiDayAdapter;
import com.whenapp.adapters.TerminsSingleDayAdapter;
import com.whenapp.common.ImageChooser;
import com.whenapp.common.ImageCoder;
import com.whenapp.common.Utils;
import com.whenapp.connection.ApiHelper;
import com.whenapp.fragments.ContactsFragment;
import com.whenapp.model.AllDayTermin;
import com.whenapp.model.ContactToken;
import com.whenapp.model.Event;
import com.whenapp.model.GroupContactToken;
import com.whenapp.model.Termin;
import com.whenapp.model.User;
import com.whenapp.model.UsersGroup;
import com.whenapp.views.AutoWrapLinearLayoutManager;
import com.whenapp.views.DateTimePickerDialog;
import com.whenapp.views.DialogBuilder;
import com.whenapp.views.DividerItemDecoration;
import com.whenapp.views.PersonCompletionView;
import com.whenapp.views.SwipeDismissRecyclerViewTouchListener;
import com.whenapp.views.TimePickerDialog;

import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class CreateEventActivity extends BaseActivity {

    public static final String IS_SINGLE_DAY = "singleDay";
    public static final String USERS_GROUP = "users_group";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        if (getParent() == null) {
            setResult(Activity.RESULT_CANCELED);
        } else {
            getParent().setResult(Activity.RESULT_CANCELED);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        boolean singleDay = false;
        if (getIntent() != null) {
            singleDay = getIntent().getBooleanExtra(IS_SINGLE_DAY, true);
        }


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, PlaceholderFragment.newInstance(singleDay))
                    .commit();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_event_act, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {


        public static PlaceholderFragment newInstance(boolean singleDayEvent) {
            PlaceholderFragment placeholderFragment = new PlaceholderFragment();
            Bundle arguments = new Bundle();
            arguments.putBoolean(IS_SINGLE_DAY, singleDayEvent);
            placeholderFragment.setArguments(arguments);
            return placeholderFragment;
        }

        public PlaceholderFragment() {
        }

        private static final int CHOOSE_IMAGE_REQ_CODE = 200;

        private GoogleApiClient mGoogleApiClient;
        private PlaceAutocompleteAdapter mPlacesAdapter;

        private AutoCompleteTextView locationAutocomplete;
        private ImageChooser imageChooser;
        private Uri imageUri = null;
        PersonCompletionView userCompletionView;
        private boolean singleDayEvent = false;

        TerminsAdapter terminAdapter;

        private EditText name;
        private EditText description;
        private CheckBox canAddPeople;
        private CheckBox canAddTime;
        private View poweredByGoogle;


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            if (getArguments() != null)
                singleDayEvent = getArguments().getBoolean(IS_SINGLE_DAY, true);


            setHasOptionsMenu(true);
            setRetainInstance(true);


            if (singleDayEvent) terminAdapter = new TerminsSingleDayAdapter();
            else terminAdapter = new TerminsMultiDayAdapter();


            mGoogleApiClient = new GoogleApiClient
                    .Builder(getActivity())
                    .addApi(Places.GEO_DATA_API)
                    .addApi(LocationServices.API)

                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            imageChooser = new ImageChooser(getActivity(), CHOOSE_IMAGE_REQ_CODE);
        }

        @Override
        public void onStart() {
            super.onStart();
            mGoogleApiClient.connect();

        }

        @Override
        public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                                 Bundle savedInstanceState) {
            setRetainInstance(true);
            View rootView = inflater.inflate(R.layout.fragment_create_event, container, false);

            Button addImage = (Button) rootView.findViewById(R.id.add_photo);
            addImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivityForResult(imageChooser.buildChooseImageIntent(), CHOOSE_IMAGE_REQ_CODE);
                }
            });
            locationAutocomplete = (AutoCompleteTextView) rootView.findViewById(R.id.create_event_location);

            final RecyclerView terminsRecyclerView = (RecyclerView) rootView.findViewById(R.id.event_termins_recycler_view);

            terminsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), RecyclerView.VERTICAL));

            terminAdapter.setOnTerminClickedListener(new TerminsAdapter.OnTerminClickedListener() {
                @Override
                public void onStartClicked(final Termin termin, final int position, View view) {
                    if (getChildFragmentManager().findFragmentByTag("calendar_view") != null)
                        return;

                    DateTimePickerDialog dialog = new DateTimePickerDialog();
                    dialog.setOnDateTimeSelectedListener(new DateTimePickerDialog.OnDateTimeSelectedListener() {
                        @Override
                        public void onDateTimeSelected(boolean allDay, int year, int monthOfYear, int dayOfMonth, int hourOfDay, int minute) {
                            termin.setYear(year);
                            termin.setMonth(monthOfYear);
                            termin.setDay(dayOfMonth);
                            termin.setHour(hourOfDay);
                            termin.setMinute(minute);
                            terminAdapter.updateTermin(position, termin);
                        }
                    }, new DateTimePickerDialog.OnDatesSelectedListener() {
                        @Override
                        public void onDatesSelected(List<Date> selectedDates) {

                        }
                    });
                    if (!singleDayEvent) dialog.setAllDaySwitchEnabled(false);

                    dialog.show(getChildFragmentManager(), "calendar_view");

                }

                @Override
                public void onEndClicked(final Termin termin, final int position, View view) {

                    if (!singleDayEvent) {
                        if (getChildFragmentManager().findFragmentByTag("calendar_view") != null)
                            return;

                        DateTimePickerDialog dialog = new DateTimePickerDialog();
                        dialog.setOnDateTimeSelectedListener(new DateTimePickerDialog.OnDateTimeSelectedListener() {
                            @Override
                            public void onDateTimeSelected(boolean allDay, int year, int monthOfYear, int dayOfMonth, int hourOfDay, int minute) {
                                Calendar cNew = Calendar.getInstance();
                                cNew.set(Calendar.YEAR, year);
                                cNew.set(Calendar.MONTH, monthOfYear);
                                cNew.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                cNew.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                cNew.set(Calendar.MINUTE, minute);

                                if (cNew.before(termin.getStart())) {
                                    Toast.makeText(getActivity(), getString(R.string.cant_set_end_time_before_start), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                termin.setEndYear(year);
                                termin.setEndMonth(monthOfYear);
                                termin.setEndDay(dayOfMonth);
                                termin.setEndHour(hourOfDay);
                                termin.setEndMinute(minute);
                                terminAdapter.updateTermin(position, termin);
                            }
                        }, new DateTimePickerDialog.OnDatesSelectedListener() {
                            @Override
                            public void onDatesSelected(List<Date> selectedDates) {

                            }
                        });

                        dialog.setSelectableAfter(termin.getStart().getTime());
                        if (!singleDayEvent) dialog.setAllDaySwitchEnabled(false);
                        dialog.show(getChildFragmentManager(), "calendar_view");
                    } else {
                        TimePickerDialog dialog = new TimePickerDialog();
                        dialog.setOnTimePickedListener(new TimePickerDialog.OnTimePickedListener() {
                            @Override
                            public void onTimePicked(int hourOfDay, int minute) {
                                if (hourOfDay < termin.getHour() || (hourOfDay == termin.getHour() && minute < termin.getMinute())) {
                                    Toast.makeText(getActivity(), getString(R.string.cant_set_end_time_before_start), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                termin.setEndHour(hourOfDay);
                                termin.setEndMinute(minute);
                                terminAdapter.updateTermin(position, termin);
                            }
                        });
                        dialog.show(getChildFragmentManager(), "time_picker_view");
                    }
                }
            });

            terminsRecyclerView.setLayoutManager(new AutoWrapLinearLayoutManager(getActivity(), AutoWrapLinearLayoutManager.VERTICAL, false));
            terminsRecyclerView.setAdapter(terminAdapter);
            terminsRecyclerView.setItemAnimator(new DefaultItemAnimator());
            Button addDateTime = (Button) rootView.findViewById(R.id.add_termin);
            addDateTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getChildFragmentManager().findFragmentByTag("calendar_view") != null) return;

                    DateTimePickerDialog dialog = new DateTimePickerDialog();
                    dialog.setOnDateTimeSelectedListener(new DateTimePickerDialog.OnDateTimeSelectedListener() {
                        @Override
                        public void onDateTimeSelected(boolean allDay, int year, int monthOfYear, int dayOfMonth, int hourOfDay, int minute) {
                            terminAdapter.addTermin(new Termin(year, monthOfYear, dayOfMonth, hourOfDay, minute));
                        }
                    }, new DateTimePickerDialog.OnDatesSelectedListener() {
                        @Override
                        public void onDatesSelected(List<Date> selectedDates) {
                            Calendar c = Calendar.getInstance();
                            for (Date d : selectedDates) {
                                c.setTime(d);
                                terminAdapter.addTermin(new AllDayTermin(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)));
                            }
                        }
                    });

                    if (!singleDayEvent) dialog.setAllDaySwitchEnabled(false);

                    dialog.show(getChildFragmentManager(), "calendar_view");

                }
            });

            poweredByGoogle = rootView.findViewById(R.id.powered_by_google);

            SwipeDismissRecyclerViewTouchListener swipeListener = new SwipeDismissRecyclerViewTouchListener(terminsRecyclerView, new SwipeDismissRecyclerViewTouchListener.DismissCallbacks() {
                @Override
                public boolean canDismiss(int position) {
                    return terminAdapter.isDismisable(position);
                }

                @Override
                public void onDismiss(RecyclerView recyclerView, int[] reverseSortedPositions) {
                    for (int position : reverseSortedPositions) {
                        terminAdapter.removeTermin(position);
                    }
                }
            });

            terminsRecyclerView.setOnTouchListener(swipeListener);
            terminsRecyclerView.setOnScrollListener(swipeListener.makeScrollListener());


            final List<User> users = new ArrayList<>();

            ArrayAdapter<User> userAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, users);
            userCompletionView = (PersonCompletionView) rootView.findViewById(R.id.person_completion_view);
            char[] splitChar = {',', ';', ' '};
            userCompletionView.setSplitChar(splitChar);
            userCompletionView.setAdapter(userAdapter);
            userCompletionView.allowDuplicates(false);

            final List<UsersGroup> usersGroups = new ArrayList<>();
            if (getActivity().getIntent() != null && getActivity().getIntent().hasExtra(USERS_GROUP)) {
                UsersGroup ug = (UsersGroup) getActivity().getIntent().getParcelableExtra(USERS_GROUP);
                userCompletionView.addObject(new GroupContactToken(ug, ug.getName(), ""));
            }

            userCompletionView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final ContactsFragment contacts = ContactsFragment.newInstance(users, usersGroups, userCompletionView.getObjects());

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        contacts.setSharedElementEnterTransition(TransitionInflater.from(v.getContext()).inflateTransition(R.transition.shared_elements_transform));
                        TransitionSet enter = new TransitionSet();
                        enter.addTransition(new Fade(Fade.IN));
                        enter.addTransition(new Slide(Gravity.BOTTOM));
                        enter.excludeTarget(userCompletionView, true);
                        enter.setOrdering(TransitionSet.ORDERING_SEQUENTIAL);


                        contacts.setEnterTransition(enter);
                        setSharedElementEnterTransition(TransitionInflater.from(v.getContext()).inflateTransition(R.transition.shared_elements_transform));
                        //contacts.setExitTransition(TransitionInflater.from(v.getContext()).inflateTransition(R.transition.window_transition));
                        //contacts.setReturnTransition(TransitionInflater.from(v.getContext()).inflateTransition(R.transition.window_transition));
                        setExitTransition(new Explode());
                    }

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, contacts, "contacts_chooser")
                            .addSharedElement(userCompletionView, "contactCompletionView")
                            .addToBackStack("contacts_chooser")
                            .commit();

                    getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                        @Override
                        public void onBackStackChanged() {
                            if (contactsFragmentVisible) {
                                userCompletionView.clear();
                                for (ContactToken ct : contacts.getSelectedContacts())
                                    userCompletionView.addObject(ct);
                                contactsFragmentVisible = false;
                                getActivity().getSupportFragmentManager().removeOnBackStackChangedListener(this);
                            } else
                                contactsFragmentVisible = true;
                        }
                    });

                }
            });

            name = (EditText) rootView.findViewById(R.id.name);
            description = (EditText) rootView.findViewById(R.id.description);
            canAddPeople = (CheckBox) rootView.findViewById(R.id.all_can_add_friends);
            canAddTime = (CheckBox) rootView.findViewById(R.id.all_can_add_termins);

            Utils.hideKeyboardOnFocusLost(name, description, locationAutocomplete);

            return rootView;
        }

        private boolean contactsFragmentVisible = false;


        public void createEvent() throws JSONException {


            name.setError(null);
            View focusView;

            String mName = name.getText().toString().trim();
            String mDescription = description.getText().toString().trim();
            boolean morePeople = canAddPeople.isChecked();
            boolean moreTermins = canAddTime.isChecked();



            if (TextUtils.isEmpty(mName)) {
                name.setError(getResources().getString(R.string.error_field_required));
                name.requestFocus();
                return;
            }

            Event.CanInvite canInvite = Event.CanInvite.ALL;
            Event.CanSuggestTime canSuggestTime = Event.CanSuggestTime.ALL;

            if(!morePeople) canInvite = Event.CanInvite.ONLY_AUTHOR;
            if(!moreTermins) canSuggestTime = Event.CanSuggestTime.ONLY_AUTHOR;

            List<Termin> termins = terminAdapter.getTermins();
            if(termins.isEmpty()){
                Toast.makeText(getActivity(), getString(R.string.you_didnt_add_any_termins), Toast.LENGTH_SHORT).show();
                return;
            }


            List<String> emails = new ArrayList<>();
            for(Object o : userCompletionView.getObjects()){
                for(User u : ((ContactToken) o).getUsers()) {
                    if (!emails.contains(u.getEmail()) && Utils.isValidEmail(u.getEmail()))
                        emails.add(u.getEmail());
                }
            }
            if(emails.isEmpty()){
                Toast.makeText(getActivity(), getString(R.string.you_havent_invited_anyone), Toast.LENGTH_SHORT).show();
                return;
            }


            String image = "";

            if(imageUri != null)
            try {
                image = new ImageCoder.EncodeAsync(imageUri, getActivity()).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            double latitude = 0;
            double longitude = 0;

            if (lastPlaceChoosen != null && lastPlaceString.equals(locationAutocomplete.getText().toString().trim())) {
                latitude = lastPlaceChoosen.getLatLng().latitude;
                longitude = lastPlaceChoosen.getLatLng().longitude;
            }


            final Dialog loader = DialogBuilder.showLoadingDialog(getActivity(), getString(R.string.wait_till_we_save));
            ApiHelper.getInstance(getActivity()).createEvent(mName, mDescription, locationAutocomplete.getText().toString(), latitude, longitude, singleDayEvent, image, emails, canInvite, canSuggestTime, termins, new ApiHelper.OnCreateEventListener() {
                @Override
                public void onEventCreated(Event event) {
                    ((BaseActivity) getActivity()).getGATracker().send(new HitBuilders.EventBuilder("Event", "Create").setLabel(event.getName()).build());
                    loader.dismiss();
                    finishWithResult();
                }


                @Override
                public void onCreateFail(String error) {
                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                    loader.dismiss();
                }
            });

        }

        private void finishWithResult(){
            Intent result = new Intent();
            if (getActivity().getParent() == null) {
                getActivity().setResult(Activity.RESULT_OK, result);
            } else {
                getActivity().getParent().setResult(Activity.RESULT_OK, result);
            }
            getActivity().supportFinishAfterTransition();
        }


        @Override
        public void onStop() {
            mGoogleApiClient.disconnect();
            super.onStop();
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putParcelableArrayList("termins", (ArrayList<? extends Parcelable>) terminAdapter.getTermins());
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);
            inflater.inflate(R.menu.menu_create_event, menu);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {

            int id = item.getItemId();

            if(id == R.id.action_done){
                try {
                    createEvent();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            imageUri = imageChooser.onActivityResult(requestCode, resultCode, data);
            if (imageUri != null) {
                Toast.makeText(getActivity(), "Image added.", Toast.LENGTH_LONG).show();
            }
        }

        /**
         * Listener that handles selections from suggestions from the AutoCompleteTextView that
         * displays Place suggestions.
         * Gets the place id of the selected item and issues a request to the Places Geo Data API
         * to retrieve more details about the place.
         *
         * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
         * String...)
         */
        private AdapterView.OnItemClickListener mAutocompleteClickListener
                = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a PlaceAutocomplete object from which we
             read the place ID.
              */
                final PlaceAutocompleteAdapter.PlaceAutocomplete item = mPlacesAdapter.getItem(position);
                final String placeId = String.valueOf(item.placeId);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
              details about the place.
              */
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            }
        };

        /**
         * Callback for results from a Places Geo Data API query that shows the first place result in
         * the details view on screen.
         */
        private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
                = new ResultCallback<PlaceBuffer>() {
            @Override
            public void onResult(PlaceBuffer places) {
                if (!places.getStatus().isSuccess()) {
                    // Request did not complete successfully
                    lastPlaceString = "";
                    lastPlaceChoosen = null;
                    return;
                }
                // Get the Place object from the buffer.
                final Place place = places.get(0);

                lastPlaceChoosen = place;

                // Format details of the place for display and show it in a TextView.

                Spanned text = formatPlaceDetails(getResources(), place.getName(),
                        place.getId(), place.getAddress(), place.getPhoneNumber(),
                        place.getWebsiteUri());
                lastPlaceString = locationAutocomplete.getText().toString().trim();

                //locationAutocomplete.setText(text);

            }
        };

        private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                                  CharSequence address, CharSequence phoneNumber, Uri websiteUri) {

            return Html.fromHtml(res.getString(R.string.place_details, name));

        }


        private String lastPlaceString = "";
        private Place lastPlaceChoosen = null;


        /**
         * Called when the Activity could not connect to Google Play services and the auto manager
         * could resolve the error automatically.
         * In this case the API is not available and notify the user.
         *
         * @param connectionResult can be inspected to determine the cause of the failure
         */
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            mPlacesAdapter = new PlaceAutocompleteAdapter(getActivity(), android.R.layout.simple_list_item_1,
                    new LatLngBounds(new LatLng(-80D, -180D), new LatLng(80D, 180D)), null);

            // Disable API access in the adapter because the client was not initialised correctly.
            mPlacesAdapter.setGoogleApiClient(null);

        }


        @Override
        public void onConnected(Bundle bundle) {
            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            double lat = 0;
            double lon = 0;
            if (mLastLocation != null) {
                lat = mLastLocation.getLatitude();
                lon = mLastLocation.getLongitude();
            }
            // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
            // the entire world.
            mPlacesAdapter = new PlaceAutocompleteAdapter(getActivity(), android.R.layout.simple_list_item_1,
                    new LatLngBounds(new LatLng(lat - 0.6f, lon - 0.6f), new LatLng(lat + 0.6f, lon + 0.6f)), null);

            // Successfully connected to the API client. Pass it to the adapter to enable API access.
            mPlacesAdapter.setGoogleApiClient(mGoogleApiClient);

            locationAutocomplete.setAdapter(mPlacesAdapter);

            locationAutocomplete.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().isEmpty()) {
                        poweredByGoogle.setVisibility(View.VISIBLE);
                    }


                }
            });

            locationAutocomplete.setOnItemClickListener(mAutocompleteClickListener);

        }

        @Override
        public void onConnectionSuspended(int i) {
            // Connection to the API client has been suspended. Disable API access in the client.
            mPlacesAdapter.setGoogleApiClient(null);
        }

    }


}
