package com.whenapp;

import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.viewpagerindicator.CirclePageIndicator;
import com.whenapp.model.SessionManager;


public class TutorialActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        final Button next = (Button) findViewById(R.id.next);

        final ViewPager tutorialPager = (ViewPager) findViewById(R.id.tutorial_pager);
        tutorialPager.setAdapter(new TutorialPagerAdapter());

        CirclePageIndicator titleIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        titleIndicator.setViewPager(tutorialPager);

        titleIndicator.setFillColor(getResources().getColor(R.color.color_accent));
        titleIndicator.setPageColor(getResources().getColor(R.color.color_primary));
        titleIndicator.setStrokeColor(Color.TRANSPARENT);

        titleIndicator.setRadius(getResources().getDisplayMetrics().density * 4);


        titleIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 4) {
                    next.setText(getString(R.string.got_it));
                } else {
                    next.setText(getString(R.string.next));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pagerPosition = tutorialPager.getCurrentItem();
                if (pagerPosition < 4) {
                    tutorialPager.setCurrentItem(pagerPosition + 1, true);
                } else {
                    getGATracker().send(new HitBuilders.EventBuilder("Tutorial", "Finish").build());
                    SessionManager.getInstance(getApplicationContext()).setPassedTutorial(true);
                    finish();
                }
            }
        });

        Button skip = (Button) findViewById(R.id.skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getGATracker().send(new HitBuilders.EventBuilder("Tutorial", "Skip").build());
                SessionManager.getInstance(getApplicationContext()).setPassedTutorial(true);
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tutorial, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class TutorialPagerAdapter extends PagerAdapter {

        public Object instantiateItem(ViewGroup collection, int position) {

            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.layout.fragment_tut_frag1;
                    break;
                case 1:
                    resId =  R.layout.fragment_tut_frag2;
                    break;
                case 2:
                    resId =  R.layout.fragment_tut_frag3;
                    break;
                case 3:
                    resId =  R.layout.fragment_tut_frag4;
                    break;
                case 4:
                    resId = R.layout.fragment_tut_frag5;
                    break;
            }
            View view = LayoutInflater.from(collection.getContext()).inflate(resId, collection, false);

            if(position == 2){
                ((TextView) view.findViewById(R.id.tutorial_text)).setText(Html.fromHtml(getString(R.string.choose_single_day_event)));
            }else if (position == 3) {
                ((TextView) view.findViewById(R.id.tutorial_text)).setText(Html.fromHtml(getString(R.string.choose_multi_day_event)));
            }
            ((ViewPager) collection).addView(view, 0);
            return view;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == ((View) arg1);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object view) {
            (container).removeView((View) view);
        }
    }
}
