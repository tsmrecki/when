package com.whenapp;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionInflater;
import android.transition.TransitionSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.whenapp.connection.ApiHelper;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.EventsDataLoader;
import com.whenapp.database.EventsTable;
import com.whenapp.database.FacebookContactsLoader;
import com.whenapp.database.FacebookContactsTable;
import com.whenapp.database.FriendsDataLoader;
import com.whenapp.database.FriendsTable;
import com.whenapp.database.InvitesEventsDataLoader;
import com.whenapp.database.InvitesEventsTable;
import com.whenapp.database.MyInvitationsDataLoader;
import com.whenapp.database.MyInvitationsTable;
import com.whenapp.database.UserGroupsDataLoader;
import com.whenapp.database.UsersGroupsTable;
import com.whenapp.fragments.EventsFragment;
import com.whenapp.fragments.FriendsFragment;
import com.whenapp.fragments.HistoryFragment;
import com.whenapp.fragments.InvitesFragment;
import com.whenapp.fragments.NavigationDrawerFragment;
import com.whenapp.model.Event;
import com.whenapp.model.Invitation;
import com.whenapp.model.Push;
import com.whenapp.model.SessionManager;
import com.whenapp.model.User;
import com.whenapp.views.DialogBuilder;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


public class MainActivity extends BaseActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks{


    public static final int GLOBAL_REFRESH = 10;

    public static final String REFRESH = "com.whenapp.RefreshData";

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        sessionManager =  SessionManager.getInstance(this);

        if (!sessionManager.isLoggedIn()) {
            Intent login = new Intent(this, LoginActivity.class);
            startActivity(login);
            finish();
            return;
        }

        if(!sessionManager.isPassedTutorial()){
            Intent tutorial = new Intent(MainActivity.this, TutorialActivity.class);
            startActivity(tutorial);
        }



        if (!sessionManager.hasRatedApp()) {

            if (sessionManager.getLastTimeAskedForRating() == 0L) {
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(System.currentTimeMillis());
                c.add(Calendar.HOUR_OF_DAY, 5 * 24);
                sessionManager.setLastTimeAskedForRating(c.getTimeInMillis());

            }
            checkRatingDialog();
        }

        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(GcmIntentService.NOTIFICATION_ID);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionSet transitionSet = new TransitionSet();
            transitionSet.addTransition(TransitionInflater.from(this).inflateTransition(android.R.transition.move));

            transitionSet.setDuration(600);
            getWindow().setSharedElementEnterTransition(transitionSet);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        View toolbarShadow = findViewById(R.id.toolbar_shadow);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                drawerLayout, toolbar, toolbarShadow);


        // Check device for Play Services APK. If check succeeds, proceed with
        //  GCM registration.
        if (checkPlayServices() && sessionManager.isLoggedIn()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);

            if (regid.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }

        Handler h = new Handler(Looper.getMainLooper());
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                setupUserLoader();
            }
        }, 800);
    }

    FriendsDataLoader udl;
    MyInvitationsDataLoader midl;
    InvitesEventsDataLoader edl;
    EventsDataLoader eventsDataLoader;
    FacebookContactsLoader facebookContactsLoader;

    public void setupUserLoader() {

        final SQLiteDatabase database = CalendarDatabaseHelper.getInstance(this).getWritableDatabase();
        FriendsTable usersTable = new FriendsTable(database);
        MyInvitationsTable invitationsTable = new MyInvitationsTable(database);
        final InvitesEventsTable eventsTable = new InvitesEventsTable(database);
        EventsTable myEventsTable = new EventsTable(database);
        final UserGroupsDataLoader userGroupsDataLoader = new UserGroupsDataLoader(this, new UsersGroupsTable(database), null, null, null, null, null);

        udl = new FriendsDataLoader(this, usersTable, null, null, null, null, null);
        midl = new MyInvitationsDataLoader(this, invitationsTable, null, null, null, null, null);
        edl  = new InvitesEventsDataLoader(this, eventsTable, null, null, null, null, null);
        eventsDataLoader = new EventsDataLoader(this, myEventsTable, null, null, null, null, null);
        facebookContactsLoader = new FacebookContactsLoader(this, new FacebookContactsTable(database), null, null, null, null, null);

        ApiHelper.getInstance(this).getUser(sessionManager.getUser().getId(), new ApiHelper.OnGetUserListener() {
            @Override
            public void onGetUser(User me) {
                if(me.getId() != 0)
                    sessionManager.setUser(me);
                else logout();



                eventsDataLoader.insert(me.getEvents());

                //midl.deleteAll();
                midl.insert(me.getInvitations());

                for(Invitation i : me.getInvitations()){
                    if(i.getStatus() == Invitation.Status.ACCEPTED || i.getStatus() == Invitation.Status.REJECTED) continue;

                    Event e = i.getEvent();
                    ApiHelper.getInstance(context).getEvent(e.getId(), new ApiHelper.OnGetEventListener() {
                        @Override
                        public void onEvent(Event event) {
                            edl.insert(event);
                        }

                        @Override
                        public void onFail(String error) {

                        }
                    });
                }


                int numInvites = 0;
                for(Invitation i : me.getInvitations()) if(i.getStatus() == Invitation.Status.WAITING) numInvites++;

                sessionManager.setNumOfInvitations(numInvites);

                List<User> users = new ArrayList<>();
                for (User u : me.getFriends()) {
                    u.setIsFriend(true);
                    users.add(u);
                }
                udl.insert(users);
                userGroupsDataLoader.insert(me.getGroups());

                facebookContactsLoader.insert(me.getFacebookFriends());

            }

            @Override
            public void onFail(String errorMessage) {
            }
        });

        //refreshEvents();
    }




    private void refreshChildren() {

        for (Fragment f : getSupportFragmentManager().getFragments())
            f.onActivityResult(10, RESULT_OK, null);
    }


    BroadcastReceiver refreshReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.hasExtra(Push.PUSH)) {
                Push p = intent.getParcelableExtra(Push.PUSH);

            }
            refreshChildren();
        }
    };

    int o = 0;


    @Override
    protected void onResume() {
        super.onResume();
        if(sessionManager.isLoggedIn())
            registerReceiver(refreshReceiver, new IntentFilter(REFRESH));
 }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (sessionManager.isLoggedIn())
                unregisterReceiver(refreshReceiver);
        } catch (Exception e) {

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment f : getSupportFragmentManager().getFragments()) {
            f.onActivityResult(requestCode, resultCode, data);
        }
    }

    public NavigationDrawerFragment getNavigationDrawerFragment() {
        return mNavigationDrawerFragment;
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;
        switch (position) {

            case 0:
                fragment = EventsFragment.newInstance(position + 1);
                break;
            case 1:
                fragment = InvitesFragment.newInstance(position + 1);
                break;
            case 2:
                fragment = FriendsFragment.newInstance(position + 1);
                break;
            case 3:
                fragment = HistoryFragment.newInstance(position + 1);
                break;
            case 4:
                logout();
                return;
            default:
                fragment = EventsFragment.newInstance(position + 1);
                break;
        }
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commitAllowingStateLoss();
    }

    private void logout() {

        getGATracker().send(new HitBuilders.EventBuilder("User", "Logout").setLabel(SessionManager.getInstance(this).getUser().getEmail()).build());

        try {
            unregisterReceiver(refreshReceiver);
        } catch (Exception e) {

        }

        CalendarDatabaseHelper.getInstance(this).deleteDatabase();

        try {
            ApiHelper.getInstance(this).pushUnRegister(getRegistrationId(this), new ApiHelper.OnPushRegisterListener() {
                @Override
                public void onRegistered() {
                }

                @Override
                public void onFail(String errorMessage) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            ApiHelper.getInstance(this).logout(new ApiHelper.OnLogoutListener() {
                @Override
                public void onLogout() {

                }

                @Override
                public void onFail(String errorMessage) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        getGCMPreferences(this).edit().clear().apply();

        sessionManager.logout();



        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    public void onSectionAttached(int number) {
        switch (number) {

            case 1:
                mTitle = getString(R.string.title_events);
                break;
            case 2:
                mTitle = getString(R.string.title_invitations);
                break;
            case 3:
                mTitle = getResources().getString(R.string.contacts);
                break;
            case 4:
                mTitle = getString(R.string.history);
                break;
            case 5:
                mTitle = "Logout";
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    // GCM stuff

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    /**
     * Tag used on log messages.
     */
    static final String TAG = "GCMStuff";

    TextView mDisplay;
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    SharedPreferences prefs;
    Context context;

    String regid;


    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    /**
     * Gets the current registration ID for application on GCM service.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }


    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the registration ID in your app is up to you.
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }


    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }


    private void registerInBackground(){
        new RegisterInBackground().execute();
    }

    public class RegisterInBackground extends AsyncTask<Void, Void, String> {

        public RegisterInBackground() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            String msg = "";
            try {
                final SharedPreferences prefs = getGCMPreferences(context);
                String oldRegistrationId = prefs.getString(PROPERTY_REG_ID, "");

                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(context);
                }
                regid = gcm.register(getString(R.string.google_app_id));

                // You should send the registration ID to your server over HTTP,
                // so it can use GCM/HTTP or CCS to send messages to your app.
                // The request to your server should be authenticated if your
                // app
                // is using accounts.

                //success = sendRegistrationIdToBackend(regid, oldRegistrationId);

                // Persist the regID - no need to register again.
                //if (success)
                //   storeRegistrationId(context, regid);
            } catch (IOException ex) {
                msg = "Error :" + ex.getMessage();
                // If there is an error, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off.
            }
            return regid;
        }

        protected void onPostExecute(String regId) {
            try {
                sendRegistrationIdToBackend(regid);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            /*
            Log.i("GCM", "onPostExecute " + msg);
            if (success)
                appSession.setShoudRegisterDevice(false);
                */
            // if (pDialog.isShowing()) pDialog.dismiss();
        }
    }




    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend(final String regId) throws JSONException{

        ApiHelper.getInstance(this).pushRegister(regId, new ApiHelper.OnPushRegisterListener() {
            @Override
            public void onRegistered() {
                storeRegistrationId(context, regId);

            }

            @Override
            public void onFail(String errorMessage) {

            }
        });
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private void checkRatingDialog() {
        long lastChecked = sessionManager.getLastTimeAskedForRating();
        if (lastChecked < System.currentTimeMillis()) {
            DialogBuilder.showRatingDialog(this, sessionManager);
        }
    }
}
