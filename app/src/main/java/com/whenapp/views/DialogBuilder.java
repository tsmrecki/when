package com.whenapp.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.whenapp.R;
import com.whenapp.adapters.UsersAdapter;
import com.whenapp.common.Utils;
import com.whenapp.model.ReminderManager;
import com.whenapp.model.SessionManager;
import com.whenapp.model.User;

import java.util.Calendar;
import java.util.List;

/**
 * Created by tomislav on 04/05/15.
 */
public class DialogBuilder {


    public static void  showYesNoDialog(Activity activity, String text, final OnYesNoDialogResponse onYesNoDialogResponse){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cant_attend_layout);

        TextView textV;
        textV = (TextView) dialog.findViewById(R.id.text);
        textV.setText(text);


        Button no = (Button) dialog.findViewById(R.id.no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onYesNoDialogResponse.onResponse(false);
                dialog.dismiss();
            }
        });


        Button yes = (Button) dialog.findViewById(R.id.yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onYesNoDialogResponse.onResponse(true);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public interface OnYesNoDialogResponse {
        void onResponse(boolean pressedYes);
    }


    public static Dialog showLoadingDialog(Activity activity, String text){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loader_dialog);

        dialog.setCancelable(false);

        TextView textV;
        textV = (TextView) dialog.findViewById(R.id.text);
        textV.setText(text);


        dialog.show();

        return dialog;
    }


    public static void showPeopleListDialog(Activity activity, List<User> users) {
        final Dialog dialog = new Dialog(activity, R.style.AppTheme_Clean);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.termin_people);

        RecyclerView usersRecycler = (RecyclerView) dialog.findViewById(R.id.users_recycler_view);
        usersRecycler.setLayoutManager(new LinearLayoutManager(activity));

        UsersAdapter usersAdapter = new UsersAdapter(users, new UsersAdapter.OnUserClickedListener() {
            @Override
            public void onUserClicked(User user) {

            }
        });

        usersRecycler.setAdapter(usersAdapter);

        ImageButton close = (ImageButton) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showRatingDialog(final Activity activity, final SessionManager sessionManager) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.ask_rating_layout);
        Button rateYes = (Button) dialog.findViewById(R.id.rate_dialog_yes);

        Button rateLater = (Button) dialog
                .findViewById(R.id.rate_dialog_later);
        Button rateNo = (Button) dialog.findViewById(R.id.rate_dialog_no);

        rateYes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sessionManager.setHasRatedApp();
                Uri uri = Uri.parse("market://details?id="
                        + activity.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    activity.startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    activity.startActivity(new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id="
                                    + activity.getPackageName())));
                }
                dialog.dismiss();
            }
        });

        rateLater.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                // Add defined amount of days to the date
                calendar.add(Calendar.DAY_OF_MONTH, 7);
                sessionManager.setLastTimeAskedForRating(calendar.getTimeInMillis());
                dialog.dismiss();
            }
        });

        rateNo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sessionManager.setHasRatedApp();
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public static void showRemindInFrontDialog(final Activity activity, long eventTime, final OnRemindInFrontChoosenListener onRemindInFrontChoosenListener) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.remind_in_front_layout);

        RadioButton off = (RadioButton) dialog.findViewById(R.id.off);
        off.setTag(ReminderManager.ReminderInFront.off);

        RadioButton tenMin = (RadioButton) dialog.findViewById(R.id.ten_mins);
        tenMin.setTag(ReminderManager.ReminderInFront.ten_minutes);

        RadioButton halfHour = (RadioButton) dialog.findViewById(R.id.half_hour);
        halfHour.setTag(ReminderManager.ReminderInFront.half_hour);

        RadioButton hour = (RadioButton) dialog.findViewById(R.id.hour);
        hour.setTag(ReminderManager.ReminderInFront.hour);

        RadioButton twoHours = (RadioButton) dialog.findViewById(R.id.two_hours);
        twoHours.setTag(ReminderManager.ReminderInFront.two_hours);

        RadioButton day = (RadioButton) dialog.findViewById(R.id.one_day);
        day.setTag(ReminderManager.ReminderInFront.day);


        long currentTime = System.currentTimeMillis();

        if(currentTime > eventTime - Utils.getMillis(ReminderManager.ReminderInFront.day)) day.setVisibility(View.GONE);
        if(currentTime > eventTime - Utils.getMillis(ReminderManager.ReminderInFront.ten_minutes)) tenMin.setVisibility(View.GONE);
        if(currentTime > eventTime - Utils.getMillis(ReminderManager.ReminderInFront.half_hour)) halfHour.setVisibility(View.GONE);
        if(currentTime > eventTime - Utils.getMillis(ReminderManager.ReminderInFront.two_hours)) twoHours.setVisibility(View.GONE);
        if(currentTime > eventTime - Utils.getMillis(ReminderManager.ReminderInFront.hour)) hour.setVisibility(View.GONE);


        CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    onRemindInFrontChoosenListener.onRemindInFrontChoosen((ReminderManager.ReminderInFront) buttonView.getTag());
                    dialog.dismiss();
                }
            }
        };

        off.setOnCheckedChangeListener(onCheckedChangeListener);
        tenMin.setOnCheckedChangeListener(onCheckedChangeListener);
        halfHour.setOnCheckedChangeListener(onCheckedChangeListener);
        hour.setOnCheckedChangeListener(onCheckedChangeListener);
        twoHours.setOnCheckedChangeListener(onCheckedChangeListener);
        day.setOnCheckedChangeListener(onCheckedChangeListener);

        dialog.show();
    }

    public interface OnRemindInFrontChoosenListener {
        void onRemindInFrontChoosen(ReminderManager.ReminderInFront inFront);
    }
}
