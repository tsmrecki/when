package com.whenapp.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SwitchCompat;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.TimePicker;

import com.whenapp.R;
import com.whenapp.fragments.CalendarFragment;
import com.whenapp.fragments.MonthFragment;
import com.whenapp.fragments.MultiSelectionCalendarFragment;
import com.whenapp.fragments.MultiSelectionMonthFragment;
import com.whenapp.fragments.SingleSelectionCalendarFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by Tomislav on 26.3.2015..
 */
public class DateTimePickerDialog extends DialogFragment {

    private List<Date> selectedDates;
    private OnDateTimeSelectedListener onDateTimeSelectedListener;
    private OnDatesSelectedListener onDatesTimeSelectedListener;
    private boolean allDaySwitchEnabled = true;
    private Date selectableAfterDate = new Date();

    public void setSelectableAfter(Date selectableAfter) {
        selectableAfterDate = selectableAfter;
    }

    public DateTimePickerDialog() {
        super();
        selectedDates = new ArrayList<>();

    }

    public void setOnDateTimeSelectedListener(OnDateTimeSelectedListener onDateTimeSelectedListener, OnDatesSelectedListener onDatesSelectedListener) {
        this.onDateTimeSelectedListener = onDateTimeSelectedListener;
        this.onDatesTimeSelectedListener = onDatesSelectedListener;
    }

    public void setAllDaySwitchEnabled(boolean enabled) {
        allDaySwitchEnabled = enabled;
    }

    FrameLayout calendarView;
    TimePicker timePicker;
    Button done;
    SwitchCompat allDaySwitch;
    FragmentTransaction fragmentTransaction;
    CalendarFragment calendarFragment;

    MonthFragment.OnDateClickedListener onDateClickedListener = new MonthFragment.OnDateClickedListener() {
        @Override
        public void onDateClicked(Date date) {
            if (!allDaySwitch.isChecked()) {
                calendarView.setVisibility(View.GONE);
                timePicker.setVisibility(View.VISIBLE);
                done.setVisibility(View.VISIBLE);
                allDaySwitch.setVisibility(View.GONE);
                selectedDates.add(date);
            } else {
                selectedDates.add(date);
            }
        }
    };

    MultiSelectionMonthFragment.OnDatesSelectedListener onDatesSelectedListener = new MultiSelectionMonthFragment.OnDatesSelectedListener() {
        @Override
        public void onDatesSelected(List<Date> datesSelected) {
            selectedDates = datesSelected;
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View dialog = inflater.inflate(R.layout.date_time_picker, container, false);

        calendarView = (FrameLayout) dialog.findViewById(R.id.calendar_popup);
        timePicker = (TimePicker) dialog.findViewById(R.id.time_picker);
        done = (Button) dialog.findViewById(R.id.done);
        allDaySwitch = (SwitchCompat) dialog.findViewById(R.id.all_day_switch);
        done.setVisibility(View.VISIBLE);

        if (allDaySwitchEnabled) allDaySwitch.setVisibility(View.VISIBLE);
        else allDaySwitch.setVisibility(View.GONE);

        if (!allDaySwitch.isChecked())
            done.setVisibility(View.GONE);

        allDaySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectedDates.clear();

                if (isChecked) {
                    done.setVisibility(View.VISIBLE);
                    calendarFragment = MultiSelectionCalendarFragment.newInstance(onDateClickedListener, null, onDatesSelectedListener);
                    calendarFragment.setSelectableAfterDate(selectableAfterDate);
                    fragmentTransaction = getChildFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.calendar_popup, calendarFragment);
                    fragmentTransaction.commit();
                } else {
                    done.setVisibility(View.GONE);
                    calendarFragment = CalendarFragment.newInstance(allDaySwitch.isChecked(), null, onDateClickedListener);
                    calendarFragment.setSelectableAfterDate(selectableAfterDate);
                    fragmentTransaction = getChildFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.calendar_popup, calendarFragment);
                    fragmentTransaction.commit();
                }
            }
        });


        calendarFragment = CalendarFragment.newInstance(allDaySwitch.isChecked(), null, onDateClickedListener);
        calendarFragment.setSelectableAfterDate(selectableAfterDate);
        fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.calendar_popup, calendarFragment);
        fragmentTransaction.commit();

        timePicker.setIs24HourView(DateFormat.is24HourFormat(getActivity()));
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!allDaySwitch.isChecked()) {
                    Calendar date = Calendar.getInstance();
                    date.setTime(((SingleSelectionCalendarFragment) calendarFragment).getSelectedDate());
                    onDateTimeSelectedListener.onDateTimeSelected(false, date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), timePicker.getCurrentHour(), timePicker.getCurrentMinute());
                    fragmentTransaction.remove(calendarFragment);
                    dismiss();
                } else {
                    onDatesTimeSelectedListener.onDatesSelected(((MultiSelectionCalendarFragment) calendarFragment).getSelectedDates());
                    fragmentTransaction.remove(calendarFragment);
                    dismiss();
                }
            }
        });


        return dialog;
    }

    public interface OnDateTimeSelectedListener {
        public void onDateTimeSelected(boolean allDay, int year, int monthOfYear, int dayOfMonth, int hourOfDay, int minute);
    }

    public interface OnDatesSelectedListener {
        public void onDatesSelected(List<Date> selectedDates);
    }

}
