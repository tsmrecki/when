package com.whenapp.views;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tokenautocomplete.TokenCompleteTextView;
import com.whenapp.R;
import com.whenapp.common.Utils;
import com.whenapp.model.ContactToken;
import com.whenapp.model.User;
import com.whenapp.model.UserContactToken;

import java.util.List;

/**
 * Created by Tomislav on 25.3.2015..
 */
public class PersonCompletionView extends TokenCompleteTextView<ContactToken> {


    public PersonCompletionView(Context context) {
        super(context);
    }

    public PersonCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PersonCompletionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected View getViewForObject(ContactToken o) {
        ContactToken p = (ContactToken) o;
        LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if(o instanceof UserContactToken) {
            if (!Utils.isValidEmail(((UserContactToken) o).getTokenEmail())) {
                LinearLayout  invalidView = (LinearLayout) l.inflate(R.layout.person_token_invalid, (ViewGroup) PersonCompletionView.this.getParent(), false);
                ((TextView) invalidView.findViewById(R.id.name)).setText(p.getTokenName());
                Toast.makeText(getContext(), "Email '" + ((UserContactToken) o).getTokenEmail() + "' is invalid.", Toast.LENGTH_LONG).show();
                return invalidView;
            }
        }

        int onEmptyImage;
        if(p instanceof UserContactToken) onEmptyImage = R.drawable.profile_icon;
        else onEmptyImage = R.drawable.ic_group_black_24dp;

        DisplayImageOptions imageOptions = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).showImageForEmptyUri(onEmptyImage).showImageOnFail(R.drawable.profile_icon).build();
        LinearLayout view = (LinearLayout) l.inflate(R.layout.person_token, (ViewGroup) PersonCompletionView.this.getParent(), false);
        ImageLoader.getInstance().displayImage(p.getImage(), (ImageView) view.findViewById(R.id.user_image), imageOptions, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
        ((TextView) view.findViewById(R.id.name)).setText(p.getTokenName());
        return view;
    }

    @Override
    protected ContactToken defaultObject(String completionText) {
        //Stupid simple example of guessing if we have an email or not
        User u = new User(completionText.trim(), "");
        u.setEmail(completionText.trim());
        boolean validEmail = Utils.isValidEmail(completionText.trim());
        if (validEmail) {
            return new UserContactToken(u, completionText.trim(), "");
            //return new User(completionText, completionText.replace(" ", "") + "@example.com");
        } else {
            return new UserContactToken(u, completionText.trim(), "");
            //return new User(completionText.substring(0, index), completionText);
        }
    }


    public static class CompletionAdapter extends ArrayAdapter {

        public CompletionAdapter(Context context, int resource, List<ContactToken> objects) {
            super(context, resource, objects);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.completion_layout, parent, false);
            }

            TextView email = (TextView) convertView.findViewById(R.id.user_email);
            TextView name = (TextView) convertView.findViewById(R.id.user_name);
            ImageView userImage = (ImageView) convertView.findViewById(R.id.user_image);

            name.setText(((ContactToken) getItem(position)).getTokenName());
            if(getItem(position) instanceof UserContactToken){
                email.setText(((UserContactToken) getItem(position)).getTokenEmail());
            }
            ImageLoader.getInstance().displayImage(((ContactToken) getItem(position)).getImage(), userImage);

            return convertView;
        }

    }
}
