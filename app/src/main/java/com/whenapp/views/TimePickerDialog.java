package com.whenapp.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TimePicker;

import com.whenapp.R;


/**
 * Created by Tomislav on 26.3.2015..
 */
public class TimePickerDialog extends DialogFragment {

    private OnTimePickedListener onTimePickedListener;

    public TimePickerDialog() {
        super();
    }

    public void setOnTimePickedListener(OnTimePickedListener onTimePickedListener) {
        this.onTimePickedListener = onTimePickedListener;
    }

    TimePicker timePicker;
    Button done;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View dialog = inflater.inflate(R.layout.time_picker, container, false);

        timePicker = (TimePicker) dialog.findViewById(R.id.time_picker);
        done = (Button) dialog.findViewById(R.id.done);
        done.setVisibility(View.VISIBLE);

        timePicker.setIs24HourView(DateFormat.is24HourFormat(getActivity()));
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTimePickedListener.onTimePicked(timePicker.getCurrentHour(), timePicker.getCurrentMinute());
                dismiss();
            }
        });

        return dialog;
    }

    public interface OnTimePickedListener {
        public void onTimePicked(int hourOfDay, int minute);
    }


}
