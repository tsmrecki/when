package com.whenapp.adapters;

import com.whenapp.model.Day;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tomislav on 16/04/15.
 */
public class MultipleSelectionMonthAdapter extends MonthAdapter {
    private List<Integer> selectedDays;
    private Map<Integer, Date> selectedDates;

    public MultipleSelectionMonthAdapter(int month, int year, DateViewHolder.OnDateViewHolderClickedListener onDateClickedListener) {
        super(month, year, onDateClickedListener);
        selectedDays = new ArrayList<>();
        selectedDates = new HashMap<>();
    }


    @Override
    public DateViewHolder.OnDateViewHolderClickedListener getOnDateClickedListener(final DateViewHolder.OnDateViewHolderClickedListener onDateClickedListener) {
        return new DateViewHolder.OnDateViewHolderClickedListener() {
            @Override
            public void onDateViewHolderClicked(int position, Date date) {
                if (selectableAfterDate.before(getDateOnPosition(position))) {
                    onDateClickedListener.onDateViewHolderClicked(dates.get(position), getDateOnPosition(position));
                    if(!selectedDays.contains(position))
                        selectedDays.add(position);
                    else {
                        int index = selectedDays.indexOf(position);
                        selectedDays.remove(index);
                    }

                    if(!selectedDates.containsKey(position))
                        selectedDates.put(position, getDateOnPosition(position));
                    else selectedDates.remove(position);

                    notifyItemChanged(position);
                }
            }
        };
    }


    public List<Date> getSelectedDates() {
        return (List<Date>) selectedDates.values();
    }

    public void setSelectedDates(List<Date> selectedDates) {
        for (Date date : selectedDates) {
            helperCalendar.setTime(date);

            int month = helperCalendar.get(Calendar.MONTH);
            int dayOfMonth = helperCalendar.get(Calendar.DAY_OF_MONTH);

            int position = -1;
            if (month == thisMonth) {
                position = previousMonthDays + dayOfMonth - 1;
            } else if ((month == previousMonth) && (dayOfMonth > previousMonthMax - previousMonthDays)) {
                position = dayOfMonth - (previousMonthMax - previousMonthDays) - 1;
            } else if (month == nextMonth && (dayOfMonth <= nextMonthDays)) { //if month=nextMonth && dayOfMonth < nextMonthDays
                position = previousMonthDays + thisMonthDays + dayOfMonth - 1;
            } else {
                continue;
            }
            notifyItemChanged(position);
        }
    }


    @Override
    public int getItemViewType(int position) {
        int acceptedCount = 0;
        int pendingCount = 0;
        if (daysMap.containsKey(position)) {
            Day d = daysMap.get(position);
            acceptedCount = d.getApprovedEventsCount();
            pendingCount = d.getPendingEventsCount();
        }

        if (getDateOnPosition(position).before(selectableAfterDate))
            return NON_SELECTABLE_VIEW_HOLDER;

        if (position + 1 - previousMonthDays == today && acceptedCount == 0 && pendingCount == 0)
            return TODAY_VIEW_HOLDER;
        else if (position + 1 - previousMonthDays == today && (acceptedCount > 0 || pendingCount > 0))
            return TODAY_BUSY_ALL_HOLDER;

        else if (selectedDays.contains(position) && acceptedCount == 0 && pendingCount == 0)
            return SELECTED_DAY_VIEW_HOLDER;
        else if (selectedDays.contains(position) && (acceptedCount > 0 || pendingCount > 0))
            return SELECTED_BUSY_ALL_HOLDER;

        else if ((position < previousMonthDays || position >= previousMonthDays + thisMonthDays) && acceptedCount == 0 && pendingCount == 0)
            return NOT_THIS_MONTH_VIEW_HOLDER;
        else if ((position < previousMonthDays || position >= previousMonthDays + thisMonthDays) && (acceptedCount > 0 || pendingCount > 0))
            return NOT_THIS_MONTH_BUSY_ALL_HOLDER;

        else if (acceptedCount == 0 && pendingCount == 0) return REGULAR_DAY_VIEW_HOLDER;
        else if (acceptedCount > 0 || pendingCount > 0) return REGULAR_BUSY_ALL_HOLDER;

        else return REGULAR_DAY_VIEW_HOLDER;
    }
}
