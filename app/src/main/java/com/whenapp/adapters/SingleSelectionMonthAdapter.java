package com.whenapp.adapters;

import com.whenapp.model.Day;

import java.util.Date;

/**
 * Created by tomislav on 16/04/15.
 */
public class SingleSelectionMonthAdapter extends MonthAdapter {

    private int selectedDay = -8;

    public SingleSelectionMonthAdapter(int month, int year, DateViewHolder.OnDateViewHolderClickedListener onDateClickedListener) {
        super(month, year, onDateClickedListener);
    }

    @Override
    public DateViewHolder.OnDateViewHolderClickedListener getOnDateClickedListener(final DateViewHolder.OnDateViewHolderClickedListener onDateClickedListener) {
        return new DateViewHolder.OnDateViewHolderClickedListener() {
            @Override
            public void onDateViewHolderClicked(int position, Date date) {
                if (selectableAfterDate.before(getDateOnPosition(position))) {
                    onDateClickedListener.onDateViewHolderClicked(dates.get(position), getDateOnPosition(position));
                    selectedDay = position;
                    notifyItemChanged(position);
                }
            }
        };
    }

    public Date getSelectedDate() {
        return getDateOnPosition(selectedDay);
    }

    @Override
    public int getItemViewType(int position) {
        int acceptedCount = 0;
        int pendingCount = 0;
        if (daysMap.containsKey(position)) {
            Day d = daysMap.get(position);
            acceptedCount = d.getApprovedEventsCount();
            pendingCount = d.getPendingEventsCount();
        }

        if (getDateOnPosition(position).before(selectableAfterDate))
            return NON_SELECTABLE_VIEW_HOLDER;

        if (position + 1 - previousMonthDays == today && acceptedCount == 0 && pendingCount == 0)
            return TODAY_VIEW_HOLDER;
        else if (position + 1 - previousMonthDays == today) return TODAY_BUSY_ALL_HOLDER;

        else if (selectedDay == position && acceptedCount == 0 && pendingCount == 0)
            return SELECTED_DAY_VIEW_HOLDER;
        else if (selectedDay == position) return SELECTED_BUSY_ALL_HOLDER;

        else if ((position < previousMonthDays || position >= previousMonthDays + thisMonthDays) && acceptedCount == 0 && pendingCount == 0)
            return NOT_THIS_MONTH_VIEW_HOLDER;
        else if ((position < previousMonthDays || position >= previousMonthDays + thisMonthDays))
            return NOT_THIS_MONTH_BUSY_ALL_HOLDER;

        else if (acceptedCount == 0 && pendingCount == 0) return REGULAR_DAY_VIEW_HOLDER;
        else if (acceptedCount > 0 || pendingCount > 0) return REGULAR_BUSY_ALL_HOLDER;

        else return REGULAR_DAY_VIEW_HOLDER;
    }
}
