package com.whenapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whenapp.R;
import com.whenapp.model.UsersGroup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by Tomislav on 18.3.2015..
 */
public class UserGroupsAdapter extends RecyclerView.Adapter<UserGroupsAdapter.ViewHolder> {

    private List<UsersGroup> usersGroups = new ArrayList<>();
    private List<UsersGroup> allGroups = new ArrayList<>();

    private OnUserGroupClickedListener onUserGroupClickedListener;

    public UserGroupsAdapter(List<UsersGroup> usersGroups, OnUserGroupClickedListener onUserGroupClickedListener) {
        if(usersGroups == null) usersGroups = new ArrayList<>();
        this.usersGroups = usersGroups;
        if(allGroups == null) allGroups = new ArrayList<>();

        allGroups.addAll(usersGroups);
        this.onUserGroupClickedListener = onUserGroupClickedListener;
    }

    @Override
    public UserGroupsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_group_layout, parent, false), new ViewHolder.OnItemClickedListener() {
            @Override
            public void onItemClicked(int position) {
                if (onUserGroupClickedListener != null)
                    onUserGroupClickedListener.onUserGroupClicked(usersGroups.get(position));
            }
        });
    }

    public void setGroups(List<UsersGroup> groups) {
        this.allGroups = groups;
        this.usersGroups.clear();
        this.usersGroups.addAll(groups);
        notifyDataSetChanged();
    }

    public void setFilter(String query) {
        Set<UsersGroup> oldVisible = new HashSet<>();
        oldVisible.addAll(usersGroups);
        if (query == null) query = "";
        query = query.trim();
        query = query.toLowerCase(Locale.getDefault());
        int size = usersGroups.size();
        usersGroups.clear();

        if (query.isEmpty()) {
            usersGroups.addAll(allGroups);
            notifyDataSetChanged();
        } else {
            List<UsersGroup> visibles = new ArrayList<>();
            for (UsersGroup user : allGroups) {
                if (user.getName().toLowerCase(Locale.getDefault()).contains(query)) {
                    visibles.add(user);
                }
            }

            Set<UsersGroup> visibleNew = new HashSet<>();
            usersGroups.addAll(visibles);
            visibleNew.addAll(visibles);
            if (!visibleNew.containsAll(oldVisible) || !oldVisible.containsAll(visibleNew))
                notifyDataSetChanged();
        }

    }

    @Override
    public void onBindViewHolder(final UserGroupsAdapter.ViewHolder holder, int position) {
        UsersGroup usersGroup = usersGroups.get(position);
        holder.name.setText(usersGroup.getName());
        holder.users.setText(usersGroup.getUserNames());
    }

    @Override
    public int getItemCount() {
        return usersGroups.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView users;
        private TextView name;

        public ViewHolder(View itemView, final OnItemClickedListener onItemClickedListener) {
            super(itemView);

            users = (TextView) itemView.findViewById(R.id.users);
            name = (TextView) itemView.findViewById(R.id.name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickedListener.onItemClicked(getLayoutPosition());
                }
            });
        }

        public interface OnItemClickedListener {
            public void onItemClicked(int position);
        }
    }

    public interface OnUserGroupClickedListener {
        public void onUserGroupClicked(UsersGroup usersGroup);
    }
}
