package com.whenapp.adapters;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.whenapp.R;
import com.whenapp.model.NavigationItem;

import java.util.List;

/**
 * Created by tomislav on 18/04/15.
 */
public class NavigationAdapter extends BaseAdapter {

    List<NavigationItem> navigationItems;
    private int checkedPosition = -1;

    public NavigationAdapter(List<NavigationItem> navigationItemList) {
        this.navigationItems = navigationItemList;

    }


    @Override
    public int getCount() {
        return navigationItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navigationItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.navigation_item, parent, false);
        }

        ImageView icon = (ImageView) convertView.findViewById(R.id.navigation_item_icon);
        TextView text = (TextView) convertView.findViewById(R.id.navigation_item_text);


        if (getItemViewType(position) == CHECKED_VIEW) {
            text.setText(Html.fromHtml("<b>" + navigationItems.get(position).text + "</b>"));
            icon.setImageResource(navigationItems.get(position).selectedResourceId);
        } else {
            text.setText(navigationItems.get(position).text);
            icon.setImageResource(navigationItems.get(position).resourceId);
        }

        return convertView;
    }


    private final static int CHECKED_VIEW = 0;
    private final static int NORMAL_VIEW = 1;

    @Override
    public int getItemViewType(int position) {
        if (position == checkedPosition)
            return CHECKED_VIEW;
        else return NORMAL_VIEW;
    }

    public void setCheckedItem(int position) {
        this.checkedPosition = position;
        notifyDataSetChanged();
    }
}
