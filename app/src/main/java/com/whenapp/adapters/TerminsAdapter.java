package com.whenapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whenapp.R;
import com.whenapp.model.Termin;
import com.whenapp.model.TerminComparator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Tomislav on 25.3.2015..
 */
public abstract class TerminsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface OnTerminClickedListener {
        public void onStartClicked(Termin termin, int position, View view);
        public void onEndClicked(Termin termin, int position, View view);
    }

    private OnTerminClickedListener onTerminClickedListener;

    private List<Termin> termins = new ArrayList<>();

    private List<Integer> subsPositions = new ArrayList<>();

    public TerminsAdapter() {
    }

    public void setOnTerminClickedListener(OnTerminClickedListener onTerminClickedListener) {
        this.onTerminClickedListener = onTerminClickedListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == REGULAR_VIEW) {
            return getRegularTerminViewHolder(parent, onTerminClickedListener);
        }
        else if(viewType == SUBTITLE_VIEW){
            return new SubtitleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.termin_subtitle_layout, parent, false));
        }
        else return getAllDayTerminViewHolder(parent, onTerminClickedListener);
    }

    protected abstract RecyclerView.ViewHolder getAllDayTerminViewHolder(ViewGroup parent, OnTerminClickedListener onTerminClickedListener);

    protected abstract RecyclerView.ViewHolder getRegularTerminViewHolder(ViewGroup parent, OnTerminClickedListener onTerminClickedListener);


    @Override
    public int getItemCount() {
        return termins.size() + additionalSubs;
    }

    private static final int ALL_DAY_VIEW = 0;
    private static final int REGULAR_VIEW = 1;
    private static final int SUBTITLE_VIEW = 2;


    @Override
    public int getItemViewType(int position) {

        if(subsPositions.contains(position)) return SUBTITLE_VIEW;
        else if (termins.get(position - getShift(position)).isAllDay()) return ALL_DAY_VIEW;
        else return REGULAR_VIEW;
    }

    int additionalSubs = 0;

    public void addTermin(Termin termin) {
        if(!termins.contains(termin)) {
            termins.add(termin);
            Collections.sort(this.termins, new TerminComparator());

            calculateSubs();

            notifyDataSetChanged();
        }
    }

    protected Map<Integer, String> subsTitles = new HashMap<>();

    synchronized void calculateSubs(){

        additionalSubs = 0;
        subsPositions.clear();
        subsTitles.clear();

        int subShift = 0;
        int lastMonth = 0;
        for(Termin t : termins){
            if(t.getMonth() != lastMonth){
                additionalSubs++;
                lastMonth = t.getMonth();
                int index = termins.indexOf(t);
                subsPositions.add(index + subShift);
                String month = new SimpleDateFormat("MMMM, yyyy", Locale.getDefault()).format(t.getStart().getTime());
                subsTitles.put(index + subShift, month);
                subShift++;
            }
        }

    }

    public int getShift(int position) {
        int s = 0;
        for(int i : subsPositions){
            if(position > i) s++;
            else break;
        }
        return s;
    }


    public void updateTermin(int position, Termin termin) {
        termins.remove(position - getShift(position));
        termins.add(position - getShift(position), termin);

        notifyItemChanged(position);
    }

    public void removeTermin(int position) {
        termins.remove(position - getShift(position));

        calculateSubs();

        notifyDataSetChanged();
    }

    public boolean isDismisable(int position) {
        return !subsPositions.contains(position);
    }

    public Termin getTermin(int position) {
        return termins.get(position - getShift(position));
    }

    public List<Termin> getTermins() {
        return termins;
    }

    protected static class SubtitleViewHolder extends RecyclerView.ViewHolder {

        TextView subtitle;

        public SubtitleViewHolder(View itemView) {
            super(itemView);
            subtitle = (TextView) itemView.findViewById(R.id.subtitle);
        }
    }
}
