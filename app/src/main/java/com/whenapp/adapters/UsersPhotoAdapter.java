package com.whenapp.adapters;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.whenapp.R;
import com.whenapp.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomislav on 18.3.2015..
 */
public class UsersPhotoAdapter extends RecyclerView.Adapter<UsersPhotoAdapter.ViewHolder> {

    private List<User> users = new ArrayList<>();
    DisplayImageOptions imageOptions;

    public UsersPhotoAdapter(List<User> users) {
        this.users = users;
        imageOptions = new DisplayImageOptions.Builder().displayer(new FadeInBitmapDisplayer(100, false, false, false)).cacheOnDisk(true).cacheInMemory(true).showImageForEmptyUri(R.drawable.profile_icon).showImageOnFail(R.drawable.profile_icon).build();
    }

    @Override
    public UsersPhotoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_image_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(final UsersPhotoAdapter.ViewHolder holder, int position) {
        ImageLoader.getInstance().displayImage(users.get(position).getImage(), holder.image, imageOptions);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.user_image);

            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0.1f);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            image.setColorFilter(filter);
        }
    }
}
