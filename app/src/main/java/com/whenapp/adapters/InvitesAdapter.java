package com.whenapp.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.whenapp.R;
import com.whenapp.model.Invitation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tomislav on 18.3.2015..
 */
public class InvitesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Invitation> invitations = new ArrayList<>();
    Context context;
    private InviteViewHolder.OnInviteOptionsClicked onInviteOptionsClicked;

    public InvitesAdapter(Context context, InviteViewHolder.OnInviteOptionsClicked onInviteOptionsClicked) {
        this.context = context;
        this.onInviteOptionsClicked = onInviteOptionsClicked;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        if (type == TYPE_INVITE_PHOTO_ACCEPTED)
            return new InvitePhotoViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.invite_layout, viewGroup, false), context, onInviteOptionsClicked);
        else
            return new InviteNoPhotoViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.invite_no_photo_layout, viewGroup, false), context, onInviteOptionsClicked);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof InviteViewHolder) {
            ((InviteViewHolder) viewHolder).name.setText(invitations.get(position).getEvent().getName());
            ((InviteViewHolder) viewHolder).peopleRV.setAdapter(new UsersPhotoAdapter(invitations.get(position).getEvent().getPeople()));
            ((InviteViewHolder) viewHolder).invitee.setText(String.format(context.getString(R.string.invited_by), invitations.get(position).getCaller().getName()));
            ((InviteViewHolder) viewHolder).personInfo.setText(invitations.get(position).getEvent().getParticipants().size() + "/" + invitations.get(position).getEvent().getPeople().size());
            if (viewHolder instanceof InvitePhotoViewHolder) {
                ImageLoader.getInstance().displayImage(invitations.get(position).getEvent().getImageUrl(), ((InvitePhotoViewHolder) viewHolder).eventPhoto);
            }
        }

    }

    @Override
    public int getItemCount() {
        return invitations.size();
    }

    public void removeInvitation(int position) {
        invitations.remove(position);
        notifyItemRemoved(position);
    }


    private static final int TYPE_FOOTER = -1;
    private static final int TYPE_INVITE_PHOTO_ACCEPTED = 0;
    private static final int TYPE_INVITE_NO_PHOTO_ACCEPTED = 1;

    @Override
    public int getItemViewType(int position) {
        if (invitations.get(position).getEvent().getImageUrl() != null)
            return TYPE_INVITE_PHOTO_ACCEPTED;
        else
            return TYPE_INVITE_NO_PHOTO_ACCEPTED;
    }

    public void setInvitations(List<Invitation> invitations) {
        this.invitations = invitations;
        notifyDataSetChanged();
    }

    public Invitation getInvitation(int position) {
        return invitations.get(position);
    }


    public static class InviteViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView invitee;
        TextView personInfo;
        RecyclerView peopleRV;
        Button accept;
        Button notGoing;

        public InviteViewHolder(final View itemView, Context context, final OnInviteOptionsClicked onInviteOptionsClicked) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.event_name);
            peopleRV = (RecyclerView) itemView.findViewById(R.id.people_recycler_view);
            peopleRV.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
            accept = (Button) itemView.findViewById(R.id.accept);
            personInfo = (TextView) itemView.findViewById(R.id.event_time_info);

            notGoing = (Button) itemView.findViewById(R.id.not_going);
            invitee = (TextView) itemView.findViewById(R.id.event_invitee);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onInviteOptionsClicked.onInviteResponded(itemView, getLayoutPosition(), true);
                }
            });

            notGoing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onInviteOptionsClicked.onInviteResponded(itemView, getLayoutPosition(), false);
                }
            });
        }

        public interface OnInviteOptionsClicked {
            public void onInviteResponded(View view, int position, boolean accepted);
        }
    }

    public static class InvitePhotoViewHolder extends InviteViewHolder {
        ImageView eventPhoto;

        public InvitePhotoViewHolder(View itemView, Context context, OnInviteOptionsClicked onInviteOptionsClicked) {
            super(itemView, context, onInviteOptionsClicked);
            eventPhoto = (ImageView) itemView.findViewById(R.id.event_photo);
        }

    }

    public class InviteNoPhotoViewHolder extends InviteViewHolder {
        public InviteNoPhotoViewHolder(View itemView, Context context, OnInviteOptionsClicked onInviteOptionsClicked) {
            super(itemView, context, onInviteOptionsClicked);
        }

    }

}
