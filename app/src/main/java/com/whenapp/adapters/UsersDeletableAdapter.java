package com.whenapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.whenapp.R;
import com.whenapp.model.User;
import com.whenapp.model.UserComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by Tomislav on 18.3.2015..
 */
public class UsersDeletableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<User> allUsers = new ArrayList<>();
    private List<User> visibleUsers = new ArrayList<>();
    DisplayImageOptions imageOptions;
    private UserViewHolder.OnItemClickedListener onItemClickedListener;
    private boolean editable = false;

    public UsersDeletableAdapter(final List<User> users, final OnUserClickedListener onUserClickedListener) {
        this.allUsers = users;
        Collections.sort(this.allUsers, new UserComparator());
        this.visibleUsers.addAll(users);
        imageOptions = new DisplayImageOptions.Builder().displayer(new FadeInBitmapDisplayer(50, true, true, false)).cacheInMemory(true).cacheOnDisk(true).resetViewBeforeLoading(true).showImageForEmptyUri(R.drawable.profile_icon).showImageOnFail(R.drawable.profile_icon).build();
        onItemClickedListener = new UserViewHolder.OnItemClickedListener() {
            @Override
            public void onItemClicked(int position) {
                onUserClickedListener.onUserClicked(visibleUsers.get(position));
            }

            @Override
            public void onItemDeleted(int position) {
                onUserClickedListener.onUserDeleted(visibleUsers.get(position));
            }
        };
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
        notifyItemRangeChanged(0, visibleUsers.size());
    }

    public boolean isEditable() {
        return editable;
    }

    public void removeUser(User user) {
        allUsers.remove(user);
        int loc = visibleUsers.indexOf(user);
        if (loc != -1) {
            visibleUsers.remove(user);
            notifyItemRemoved(loc);
        }
    }

    public void addUser(User user) {
        allUsers.add(user);
        Collections.sort(allUsers, new UserComparator());
        this.visibleUsers.add(user);
        Collections.sort(visibleUsers, new UserComparator());
        int loc = this.visibleUsers.indexOf(user);
        notifyItemInserted(loc);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_deletable_layout, parent, false), editable, onItemClickedListener);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        User user = visibleUsers.get(position);

        if(holder instanceof UserViewHolder) {
            ((UserViewHolder)holder).name.setText(user.getName());
            if(user.getName() == null || user.getName().isEmpty()) ((UserViewHolder) holder).name.setText(user.getEmail());
            ((UserViewHolder) holder).email.setText(user.getEmail());
            ImageLoader.getInstance().displayImage(user.getImage(), ((UserViewHolder)holder).image, imageOptions);

        }
    }

    @Override
    public int getItemCount() {
        return visibleUsers.size();
    }

    public void setUsers(List<User> users){
        this.allUsers = users;
        Collections.sort(this.allUsers, new UserComparator());
        this.visibleUsers.clear();
        this.visibleUsers.addAll(users);
        notifyDataSetChanged();
    }

    public void addUsers(List<User> users) {
        for (User u : users) {
            if(!allUsers.contains(u)) {
                allUsers.add(u);
                this.visibleUsers.add(u);
            }
        }

        Collections.sort(this.visibleUsers, new UserComparator());
        notifyDataSetChanged();
    }

    public List<User> getUsers() {
        return this.allUsers;
    }

    public void setFilter(String query) {
        Set<User> oldVisible = new HashSet<>();
        oldVisible.addAll(visibleUsers);
        if (query == null) query = "";
        query = query.trim();
        query = query.toLowerCase(Locale.getDefault());
        int size = visibleUsers.size();
        visibleUsers.clear();

        if (query.isEmpty()) {
            visibleUsers.addAll(allUsers);
            notifyDataSetChanged();
        } else {
            List<User> visibles = new ArrayList<>();
            for (User user : allUsers) {
                if (user.getName().toLowerCase(Locale.getDefault()).contains(query) || user.getEmail().toLowerCase(Locale.getDefault()).contains(query)) {
                    visibles.add(user);
                }
            }

            Set<User> visibleNew = new HashSet<>();
            visibleUsers.addAll(visibles);
            visibleNew.addAll(visibles);
            if (!visibleNew.containsAll(oldVisible) || !oldVisible.containsAll(visibleNew))
                notifyDataSetChanged();
        }
    }



    public static class UserViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView name;
        private TextView email;
        private ImageButton delete;

        public UserViewHolder(View itemView, boolean editable, final OnItemClickedListener onItemClickedListener) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.user_image);
            name = (TextView) itemView.findViewById(R.id.user_name);
            email = (TextView) itemView.findViewById(R.id.user_email);
            delete = (ImageButton) itemView.findViewById(R.id.user_delete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickedListener.onItemClicked(getAdapterPosition());
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickedListener.onItemDeleted(getAdapterPosition());
                }
            });

            if(editable) delete.setVisibility(View.VISIBLE);
            else delete.setVisibility(View.GONE);
        }

        protected interface OnItemClickedListener {
            void onItemClicked(int position);
            void onItemDeleted(int position);
        }
    }


    public interface OnUserClickedListener {
        void onUserClicked(User user);
        void onUserDeleted(User user);
    }

    Filter myFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            List<User> tempList = new ArrayList<>();
            //constraint is the result from text you want to filter against.
            //objects is your data set you will filter from
            if (constraint != null && visibleUsers != null) {
                int length = visibleUsers.size();
                int i = 0;
                while (i < length) {
                    User item = visibleUsers.get(i);
                    //do whatever you wanna do here
                    //adding result set output array
                    if (item.getName().toLowerCase().contains(constraint.toString().toLowerCase()) || item.getEmail().toLowerCase().contains(constraint.toString().toLowerCase()))
                        tempList.add(item);
                    i++;
                }
                //following two lines is very important
                //as publish result can only take FilterResults objects
                filterResults.values = tempList;
                filterResults.count = tempList.size();
            }
            return filterResults;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence contraint, FilterResults results) {
            visibleUsers = (ArrayList<User>) results.values;
            notifyDataSetChanged();
        }
    };




}
