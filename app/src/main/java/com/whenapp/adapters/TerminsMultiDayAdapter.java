package com.whenapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whenapp.R;
import com.whenapp.model.Termin;

/**
 * Created by tomislav on 20/04/15.
 */
public class TerminsMultiDayAdapter extends TerminsAdapter {


    @Override
    protected RecyclerView.ViewHolder getAllDayTerminViewHolder(ViewGroup parent, final OnTerminClickedListener onTerminClickedListener) {
        return new TerminAllDayViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.termin_multi_day_layout, parent, false), new TerminViewHolder.OnHolderClickedListener() {
            @Override
            public void onStartClicked(int position, View view) {
                if (onTerminClickedListener != null)
                    onTerminClickedListener.onStartClicked(getTermin(position), position, view);
            }

            @Override
            public void onEndClicked(int position, View view) {
                if (onTerminClickedListener != null)
                    onTerminClickedListener.onEndClicked(getTermin(position), position, view);
            }
        });
    }

    @Override
    protected RecyclerView.ViewHolder getRegularTerminViewHolder(final ViewGroup parent, final OnTerminClickedListener onTerminClickedListener) {
        return new TerminViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.termin_multi_day_layout, parent, false), new TerminViewHolder.OnHolderClickedListener() {
            @Override
            public void onStartClicked(int position, View view) {
                if (onTerminClickedListener != null)
                    onTerminClickedListener.onStartClicked(getTermin(position), position, view);
            }

            @Override
            public void onEndClicked(int position, View view) {
                if (onTerminClickedListener != null)
                    onTerminClickedListener.onEndClicked(getTermin(position), position, view);
            }
        });
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder inHolder, int position) {
        if (inHolder instanceof TerminViewHolder) {
            Termin termin = getTermin(position);

            TerminViewHolder holder = (TerminViewHolder) inHolder;
            holder.startTime.setText(termin.getStartTime());
            holder.endTime.setText(termin.getEndTime());
            holder.startMonthDay.setText(String.valueOf(termin.getDay()));
            holder.endMonthDay.setText(String.valueOf(termin.getEndDay()));
            holder.startWeekDay.setText(termin.getStartWeekDay());
            holder.endWeekDay.setText(termin.getEndWeekDay());

            long milisDiff = termin.getEnd().getTimeInMillis() - termin.getStart().getTimeInMillis();
            long secs = milisDiff / 1000;
            long hours = secs / 60 / 60;
            long days = hours / 24;


            holder.duration.setText(days + " DAYS");

        }else if (inHolder instanceof SubtitleViewHolder) {
            ((SubtitleViewHolder) inHolder).subtitle.setText(subsTitles.get(position));
        }
    }


    public static class TerminViewHolder extends RecyclerView.ViewHolder {
        TextView startTime;
        TextView endTime;
        TextView startWeekDay;
        TextView startMonthDay;
        TextView endWeekDay;
        TextView endMonthDay;
        TextView duration;
        View endDate;
        View endTimeEditable;

        public TerminViewHolder(View itemView, final OnHolderClickedListener onHolderClickedListener) {
            super(itemView);

            startTime = (TextView) itemView.findViewById(R.id.termin_start_time);
            endTime = (TextView) itemView.findViewById(R.id.termin_end_time);
            startWeekDay = (TextView) itemView.findViewById(R.id.termin_start_week_day);
            startMonthDay = (TextView) itemView.findViewById(R.id.termin_start_month_day);
            endDate = itemView.findViewById(R.id.termin_end_date);
            endWeekDay = (TextView) itemView.findViewById(R.id.termin_end_week_day);
            endMonthDay = (TextView) itemView.findViewById(R.id.termin_end_month_day);
            duration = (TextView) itemView.findViewById(R.id.termin_duration);
            endTimeEditable = itemView.findViewById(R.id.termin_end_layout);

            startTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onHolderClickedListener.onStartClicked(getLayoutPosition(), v);
                }
            });

            endTimeEditable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onHolderClickedListener.onEndClicked(getLayoutPosition(), v);
                }
            });
        }

        public interface OnHolderClickedListener {
            public void onStartClicked(int position, View view);

            public void onEndClicked(int position, View view);
        }
    }


    public static class TerminAllDayViewHolder extends TerminViewHolder {

        public TerminAllDayViewHolder(View itemView, final TerminAllDayViewHolder.OnHolderClickedListener onHolderClickedListener) {
            super(itemView, onHolderClickedListener);
            startTime.setVisibility(View.GONE);
            endTime.setVisibility(View.GONE);
            endTimeEditable.setVisibility(View.GONE);
        }

    }

}
