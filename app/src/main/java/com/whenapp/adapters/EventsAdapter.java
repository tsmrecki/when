package com.whenapp.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.whenapp.R;
import com.whenapp.model.Event;
import com.whenapp.model.EventComparator;
import com.whenapp.model.InviteTermin;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tomislav on 18.3.2015..
 */
public class EventsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Event> events = new ArrayList<>();
    List<Event> allEvents = new ArrayList<>();
    boolean history = false;
    Context context;

    public EventsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        if (type == TYPE_FOOTER)
            return new FooterViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.footer, viewGroup, false));
        else if (type == TYPE_EVENT_ACCEPTED)
            return new EventAcceptedViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_layout, viewGroup, false));
        else if (type == TYPE_EVENT_NOT_ACCEPTED)
            return new EventNotAcceptedViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_not_accepted_layout, viewGroup, false));
        else if (type == TYPE_EVENT_ACCEPTED_NO_PHOTO)
            return new EventAcceptedNoPhotoViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_no_photo_layout, viewGroup, false));
        else
            return new EventNotAcceptedNoPhotoViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_not_accepted_no_photo_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof EventViewHolder) {
            Event event = events.get(position);
            ((EventViewHolder) viewHolder).name.setText(event.getName());
            ((EventViewHolder) viewHolder).peopleRV.setAdapter(new UsersPhotoAdapter(event.getPeople()));

            if (viewHolder instanceof EventAcceptedViewHolder) {
                if(!event.getProminentTermin().getTermin().isAllDay())
                ((EventAcceptedViewHolder) viewHolder).time.setText(event.getProminentTermin().getTermin().getStartTime());
                else ((EventAcceptedViewHolder) viewHolder).time.setText("ALL DAY");

                ImageLoader.getInstance().displayImage(event.getImageUrl(), ((EventAcceptedViewHolder) viewHolder).eventPhoto);
                String summary = event.getProminentDate();
                if(!event.getPlaceName().isEmpty()) summary += " | " + event.getPlaceName();
                ((EventAcceptedViewHolder) viewHolder).summary.setText(summary);
            }
            else if (viewHolder instanceof EventNotAcceptedViewHolder) {
                ((EventNotAcceptedViewHolder) viewHolder).time.setText(event.getNumUsersResponded() + "/" + event.getTotalNumUsers());
                ImageLoader.getInstance().displayImage(event.getImageUrl(), ((EventNotAcceptedViewHolder) viewHolder).eventPhoto);
                ((EventNotAcceptedViewHolder) viewHolder).summary.setText(event.getProminentDate());
            }
            else if (viewHolder instanceof EventAcceptedNoPhotoViewHolder) {
                if(!event.getProminentTermin().getTermin().isAllDay())
                    ((EventAcceptedNoPhotoViewHolder) viewHolder).time.setText(event.getProminentTermin().getTermin().getStartTime());
                else ((EventAcceptedNoPhotoViewHolder) viewHolder).time.setText("ALL DAY");
                String summary = event.getProminentDate();
                if(!event.getPlaceName().isEmpty()) summary += " | " + event.getPlaceName();
                ((EventAcceptedNoPhotoViewHolder) viewHolder).summary.setText(summary);
            }
            if (viewHolder instanceof EventNotAcceptedNoPhotoViewHolder) {
                ((EventNotAcceptedNoPhotoViewHolder) viewHolder).time.setText(event.getNumUsersResponded() + "/" + event.getTotalNumUsers());
                ((EventNotAcceptedNoPhotoViewHolder) viewHolder).summary.setText(event.getProminentDate());
            }
        }
    }

    @Override
    public int getItemCount() {
        return events.size() + 1;
    }

    public Event getEvent(int position) {
        return events.get(position);
    }


    private static final int TYPE_FOOTER = -1;
    private static final int TYPE_EVENT_ACCEPTED = 0;
    private static final int TYPE_EVENT_NOT_ACCEPTED = 1;
    private static final int TYPE_EVENT_ACCEPTED_NO_PHOTO = 2;
    private static final int TYPE_EVENT_NOT_ACCEPTED_NO_PHOTO = 3;

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) return TYPE_FOOTER;
        else if (events.get(position).isAccepted() && events.get(position).getImageUrl() != null)
            return TYPE_EVENT_ACCEPTED;
        else if (events.get(position).isAccepted() && events.get(position).getImageUrl() == null)
            return TYPE_EVENT_ACCEPTED_NO_PHOTO;
        else if (!events.get(position).isAccepted() && events.get(position).getImageUrl() != null)
            return TYPE_EVENT_NOT_ACCEPTED;
        else return TYPE_EVENT_NOT_ACCEPTED_NO_PHOTO;
    }

    public void setHistory(boolean history){
        this.history = history;
    }

    public void setEvents(List<Event> events, String query) {
        Collections.sort(events, new EventComparator(history));
        this.allEvents = events;
        this.events.clear();

        Calendar c = Calendar.getInstance();

        if((query == null || query.isEmpty()) && !history) {
            for(Event e : allEvents)
            if(e.getProminentTermin() != null) {
                if (e.isAccepted()){
                    if(e.getProminentTermin().getTermin().getStart().after(c))
                        this.events.add(e);
                }
                else{
                    boolean isOld = true;
                    for (InviteTermin it : e.getInviteTerminList()) {
                        if(it.getTermin().getStart().after(c)){
                            isOld = false;
                            break;
                        }
                    }
                    if(!isOld) this.events.add(e);
                }
            }else{
                this.events.add(e);
            }
        }
        else if(history){
            for(Event e : allEvents)
                if(e.getProminentTermin() != null) {
                    if (e.getProminentTermin().getTermin().getStart().before(c)) this.events.add(e);
                }else{
                    this.events.add(e);
                }
        }
        else {
            for (Event e : allEvents) if (e.getDateQuery().contains(query)) this.events.add(e);
        }

        notifyDataSetChanged();

        /*

        List<Event> eventsCopy = new ArrayList<>();
        eventsCopy.addAll(this.events);
        for(Event e : eventsCopy){
            if(!events.contains(e)){
                int index = this.events.indexOf(e);
                this.events.remove(e);
                notifyItemRemoved(index);
            }
        }
        
        for(Event e : events){
            if(this.events.contains(e)){

                int index = this.events.indexOf(e);
                    this.events.remove(e);
                    this.events.add(index, e);
                    notifyItemChanged(index);
            }
            else{
                this.events.add(e);
                Collections.sort(this.events, new EventComparator());
                int newIndex = this.events.indexOf(e);
                notifyItemInserted(newIndex);
            }
        }
        */


    }

    public class EventViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView summary;
        TextView time;
        RecyclerView peopleRV;

        public EventViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.event_name);
            summary = (TextView) itemView.findViewById(R.id.event_summary);
            time = (TextView) itemView.findViewById(R.id.event_time_info);
            peopleRV = (RecyclerView) itemView.findViewById(R.id.people_recycler_view);
            peopleRV.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onEventClickedListener.onEventClicked(v, getLayoutPosition());
                }
            });
        }
    }

    public class EventAcceptedViewHolder extends EventViewHolder {

        ImageView eventPhoto;

        public EventAcceptedViewHolder(View itemView) {
            super(itemView);
            eventPhoto = (ImageView) itemView.findViewById(R.id.event_photo);
        }

    }

    public class EventNotAcceptedViewHolder extends EventViewHolder {

        ImageView eventPhoto;

        public EventNotAcceptedViewHolder(View itemView) {
            super(itemView);
            eventPhoto = (ImageView) itemView.findViewById(R.id.event_photo);
        }

    }

    public class EventAcceptedNoPhotoViewHolder extends EventViewHolder {

        public EventAcceptedNoPhotoViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class EventNotAcceptedNoPhotoViewHolder extends EventViewHolder {

        public EventNotAcceptedNoPhotoViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        public FooterViewHolder(View itemView) {
            super(itemView);
        }
    }


    public interface OnEventClickedListener {
        public void onEventClicked(View view, int position);
    }

    private OnEventClickedListener onEventClickedListener;

    public void setOnEventClickedListener(OnEventClickedListener onEventClickedListener) {
        this.onEventClickedListener = onEventClickedListener;
    }


    public void setQuery(String query) {
        this.events.clear();

        Calendar c = Calendar.getInstance();

        if((query == null || query.isEmpty()) && !history) {
            for(Event e : allEvents)
                if(e.getProminentTermin() != null) {
                    if (e.isAccepted()){
                        if(e.getProminentTermin().getTermin().getStart().after(c))
                            this.events.add(e);
                    }
                    else{
                        boolean isOld = true;
                        for (InviteTermin it : e.getInviteTerminList()) {
                            if(it.getTermin().getStart().after(c)){
                                isOld = false;
                                break;
                            }
                        }
                        if(!isOld) this.events.add(e);
                    }
                }else{
                    this.events.add(e);
                }
        }
        else if(history){
            for(Event e : allEvents)
                if(e.getProminentTermin() != null) {
                    if (e.getProminentTermin().getTermin().getStart().before(c)) this.events.add(e);
                }else{
                    this.events.add(e);
                }
        }
        else {
            for (Event e : allEvents) if (e.getDateQuery().contains(query)) this.events.add(e);
        }

        notifyDataSetChanged();
    }

    public List<Event> getEvents() {
        return allEvents;
    }

    public int getVisibleCount() {
        return events.size();
    }
}
