package com.whenapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.whenapp.R;
import com.whenapp.model.Event;
import com.whenapp.model.Invitation;
import com.whenapp.model.User;
import com.whenapp.model.UserComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Tomislav on 18.3.2015..
 */
public class UsersInvitesAdapter extends RecyclerView.Adapter<UsersInvitesAdapter.UserViewHolder> {

    private List<User> visibleUsers = new ArrayList<>();
    private List<User> usersResponded = new ArrayList<>();
    private Map<User, Invitation> invitationsMap = new HashMap<>();
    private List<User> myAddedUsers = new ArrayList<>();


    DisplayImageOptions imageOptions;
    private UserViewHolder.OnItemClickedListener onItemClickedListener;
    boolean editable = true;

    public UsersInvitesAdapter(boolean editable, final List<User> users, List<Invitation> invitations, final OnUserClickedListener onUserClickedListener) {
        Set<User> usersSet = new HashSet<>();
        usersSet.addAll(users);

        this.editable = editable;
        this.visibleUsers.addAll(usersSet);
        Collections.sort(visibleUsers, new UserComparator());

        for(Invitation i : invitations){
            invitationsMap.put(i.getCallee(), i);
        }
        imageOptions = new DisplayImageOptions.Builder().displayer(new FadeInBitmapDisplayer(100, true, false, false)).resetViewBeforeLoading(true).showImageForEmptyUri(R.drawable.profile_icon).showImageOnFail(R.drawable.profile_icon).build();
        onItemClickedListener = new UserViewHolder.OnItemClickedListener() {
            @Override
            public void onItemClicked(int position) {
                onUserClickedListener.onUserClicked(visibleUsers.get(position));
            }

            @Override
            public void onItemDeleted(int position) {
                onUserClickedListener.onUserRemoved(invitationsMap.get(visibleUsers.get(position)));
            }
        };
    }

    @Override
    public UsersInvitesAdapter.UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_invite_layout, parent, false), onItemClickedListener);
    }

    @Override
    public void onBindViewHolder(final UsersInvitesAdapter.UserViewHolder holder, int position) {
        User user = visibleUsers.get(position);
        if(usersResponded.contains(user)) {
            holder.image.setAlpha(1f);
            holder.name.setAlpha(1);
            holder.removeUser.setVisibility(View.GONE);
        }
        else {
            if(editable)
                holder.removeUser.setVisibility(View.VISIBLE);
            else
                holder.removeUser.setVisibility(View.GONE);
            holder.image.setAlpha(0.6f);
            holder.name.setAlpha(0.6f);
        }

        holder.name.setText(user.getName());
        ImageLoader.getInstance().displayImage(user.getImage(), holder.image, imageOptions);
    }



    @Override
    public int getItemCount() {
        return visibleUsers.size();
    }



    public void setUsersResponded(List<User> users){
        this.usersResponded = users;
        notifyDataSetChanged();
    }

    public void addInvitation(Invitation invitation){
        if(!visibleUsers.contains(invitation.getCallee())) {
            invitationsMap.put(invitation.getCallee(), invitation);
            visibleUsers.add(invitation.getCallee());
            myAddedUsers.add(invitation.getCallee());
            Collections.sort(visibleUsers, new UserComparator());
            notifyItemInserted(visibleUsers.indexOf(invitation.getCallee()));
        }
    }

    public void addUser(User user) {
        Invitation invitation = new Invitation(0, new Event(), new User("", ""), user);
        if(!visibleUsers.contains(user)) {
            invitationsMap.put(invitation.getCallee(), invitation);
            visibleUsers.add(invitation.getCallee());
            myAddedUsers.add(invitation.getCallee());
            Collections.sort(visibleUsers, new UserComparator());
            notifyItemInserted(visibleUsers.indexOf(invitation.getCallee()));
        }
    }

    public void removeUser(User user){
        if(invitationsMap.containsKey(user)) invitationsMap.remove(user);
        myAddedUsers.remove(user);
        int index = visibleUsers.indexOf(user);
        visibleUsers.remove(user);
        notifyItemRemoved(index);
    }

    public List<User> getMyAddedUsers() {
        return myAddedUsers;
    }

    public List<User> getUsers() {
        return visibleUsers;
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView name;
        RelativeLayout removeUser;

        public UserViewHolder(View itemView, final OnItemClickedListener onItemClickedListener) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.user_image);
            name = (TextView) itemView.findViewById(R.id.name);
            removeUser = (RelativeLayout) itemView.findViewById(R.id.remove_user);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickedListener.onItemClicked(getLayoutPosition());
                }
            });

            removeUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickedListener.onItemDeleted(getLayoutPosition());
                }
            });
        }

        protected interface OnItemClickedListener {
            void onItemClicked(int position);
            void onItemDeleted(int position);
        }
    }

    public interface OnUserClickedListener {
        public void onUserClicked(User user);
        public void onUserRemoved(Invitation invitation);
    }


}
