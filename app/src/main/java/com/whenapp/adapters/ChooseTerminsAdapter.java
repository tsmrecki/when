package com.whenapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whenapp.R;
import com.whenapp.model.InviteTermin;
import com.whenapp.model.InviteTerminComparator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Created by Tomislav on 25.3.2015..
 */
public abstract class ChooseTerminsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<InviteTermin> termins = new ArrayList<>();
    protected HashMap<InviteTermin, Boolean> terminSelected = new HashMap<>();
    private Set<InviteTermin> myTermins = new HashSet<>();
    private List<Integer> subsPositions = new ArrayList<>();
    private HashMap<InviteTermin, Boolean> originalInviteStates = new HashMap<>();


    public interface OnTerminClickedListener {
        public void onTerminClicked(int position, InviteTermin termin, View view, boolean editable);
        public void onTerminChecked(int position, InviteTermin termin, boolean isChecked, boolean editable);
        public void onTerminEndEdited(int position, InviteTermin termin, View view, boolean editable);
    }

    private OnTerminClickedListener onTerminClickedListener;
    public void setOnTerminClickedListener(OnTerminClickedListener onTerminClickedListener) {
        this.onTerminClickedListener = onTerminClickedListener;
    }

    public ChooseTerminsAdapter(List<InviteTermin> inviteTermins, List<InviteTermin> selectedTermins) {
        this.termins = inviteTermins;
        for (InviteTermin termin : termins) {
            terminSelected.put(termin, false);
            originalInviteStates.put(termin, false);
        }
        for(InviteTermin it : selectedTermins){
            terminSelected.put(it, true);
            originalInviteStates.put(it, true);
        }

        Collections.sort(this.termins, new InviteTerminComparator());

        calculateSubs();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == SUBTITLE_VIEW) {
            return new SubtitleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.termin_subtitle_layout, parent, false));
        }
        else if (viewType == TYPE_TERMIN) {
            return getRegularTerminView(parent, onTerminClickedListener);
        } else if (viewType == EDITABLE_TERMIN) {
            return getEditableTerminView(parent, onTerminClickedListener);
        } else if (viewType == TYPE_ALL_DAY_TERMINS) {
            return getAllDayTerminView(parent, onTerminClickedListener);
        } else {
            return getAllDayEditableTerminView(parent, onTerminClickedListener);
        }
    }

    public abstract RecyclerView.ViewHolder getRegularTerminView(ViewGroup parent, OnTerminClickedListener onTerminClickedListener);

    public abstract RecyclerView.ViewHolder getAllDayTerminView(ViewGroup parent, OnTerminClickedListener onTerminClickedListener);

    public abstract RecyclerView.ViewHolder getEditableTerminView(ViewGroup parent, OnTerminClickedListener onTerminClickedListener);

    public abstract RecyclerView.ViewHolder getAllDayEditableTerminView(ViewGroup parent, OnTerminClickedListener onTerminClickedListener);


    @Override
    public int getItemCount() {
        return termins.size() + additionalSubs;
    }

    private static final int TYPE_TERMIN = 1;
    private static final int EDITABLE_TERMIN = 2;
    private static final int TYPE_ALL_DAY_TERMINS = 3;
    private static final int TYPE_ALL_DAY_EDITABLE_TERMINS = 4;
    private static final int SUBTITLE_VIEW = 5;

    @Override
    public int getItemViewType(int position) {
        if(subsPositions.contains(position)) return SUBTITLE_VIEW;

        else if (myTermins.contains(termins.get(position - getShift(position)))) {
            if (!termins.get(position - getShift(position)).getTermin().isAllDay())
                return EDITABLE_TERMIN;
            else return TYPE_ALL_DAY_EDITABLE_TERMINS;
        } else if (!termins.get(position - getShift(position)).getTermin().isAllDay())
            return TYPE_TERMIN;
        else return TYPE_ALL_DAY_TERMINS;
    }

    public InviteTermin getTermin(int position) {
        return termins.get(position - getShift(position));
    }

    public void addTermin(InviteTermin termin) {
        termins.add(termin);
        Collections.sort(this.termins, new InviteTerminComparator());
        terminSelected.put(termin, true);
        originalInviteStates.put(termin, true);
        myTermins.add(termin);

        calculateSubs();

        notifyDataSetChanged();
    }

    public List<InviteTermin> getMyTermins(){
        List<InviteTermin> myT = new ArrayList<>();
        myT.addAll(myTermins);
        return myT;
    }

    public void removeTermin(int position) {
        InviteTermin t = termins.get(position - getShift(position));
        if(terminSelected.containsKey(t)) terminSelected.remove(t);
        if(originalInviteStates.containsKey(t)) originalInviteStates.remove(t);
        if(myTermins.contains(t)) myTermins.remove(t);
        termins.remove(position - getShift(position));

        calculateSubs();

        notifyDataSetChanged();
    }


    public void setTermins(List<InviteTermin> termins) {
        this.termins = termins;
        Collections.sort(this.termins, new InviteTerminComparator());
        for (InviteTermin termin : termins) {
            terminSelected.put(termin, false);
        }

        calculateSubs();

        notifyDataSetChanged();
    }


    public List<InviteTermin> getSelectedTermins() {
        List<InviteTermin> terminsSelected = new ArrayList<>();
        for (InviteTermin it : terminSelected.keySet()) {
            if(terminSelected.get(it) && !myTermins.contains(it)) terminsSelected.add(it);
        }
        return terminsSelected;
    }

    public List<InviteTermin> getNewlySelectedTermins() {
        List<InviteTermin> terminsSelected = new ArrayList<>();
        for (InviteTermin it : terminSelected.keySet()) {
            if(terminSelected.get(it) && !myTermins.contains(it) && !originalInviteStates.get(it)) terminsSelected.add(it);
        }

        return terminsSelected;
    }

    public List<InviteTermin> getUnselectedTermins() {
        List<InviteTermin> terminsUnSelected = new ArrayList<>();
        for (InviteTermin it : terminSelected.keySet()) {
            if(!terminSelected.get(it)) terminsUnSelected.add(it);
        }
        return terminsUnSelected;
    }

    public List<InviteTermin> getNewlyUnselectedTermins() {
        List<InviteTermin> terminsUnselected = new ArrayList<>();
        for (InviteTermin it : terminSelected.keySet()) {
            if(!terminSelected.get(it) && originalInviteStates.get(it)) terminsUnselected.add(it);
        }

        return terminsUnselected;
    }

    public void updateTermin(int position, InviteTermin termin){
        InviteTermin oldTermin = termins.get(position - getShift(position));
        boolean wasChecked = terminSelected.get(oldTermin);

        termins.remove(position - getShift(position));
        terminSelected.remove(oldTermin);

        termins.add(position - getShift(position), termin);
        terminSelected.put(termin, wasChecked);


        notifyItemChanged(position);

    }

    public boolean isTerminRemovable(InviteTermin termin)
    {
        return myTermins.contains(termin);
    }


    int additionalSubs = 0;
    protected Map<Integer, String> subsTitles = new HashMap<>();

    synchronized void calculateSubs(){

        additionalSubs = 0;
        subsPositions.clear();
        subsTitles.clear();

        int subShift = 0;
        int lastMonth = 0;
        for(InviteTermin t : termins){
            if(t.getTermin().getMonth() != lastMonth){
                additionalSubs++;
                lastMonth = t.getTermin().getMonth();
                int index = termins.indexOf(t);
                subsPositions.add(index + subShift);
                String month = new SimpleDateFormat("MMMM, yyyy", Locale.getDefault()).format(t.getTermin().getStart().getTime());
                subsTitles.put(index + subShift, month);
                subShift++;
            }
        }

    }


    public int getShift(int position) {
        int s = 0;
        for(int i : subsPositions){
            if(position > i) s++;
            else break;
        }
        return s;
    }


    protected static class SubtitleViewHolder extends RecyclerView.ViewHolder {

        TextView subtitle;

        public SubtitleViewHolder(View itemView) {
            super(itemView);
            subtitle = (TextView) itemView.findViewById(R.id.subtitle);
        }
    }
}
