package com.whenapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;

import com.whenapp.R;
import com.whenapp.model.InviteTermin;
import com.whenapp.model.Termin;

import java.util.List;

/**
 * Created by tomislav on 20/04/15.
 */
public class ChooseSingleDayTerminsAdapter extends ChooseTerminsAdapter {
    public ChooseSingleDayTerminsAdapter(List<InviteTermin> inviteTermins, List<InviteTermin> selectedTermins) {
        super(inviteTermins, selectedTermins);
    }

    @Override
    public RecyclerView.ViewHolder getRegularTerminView(final ViewGroup parent, final OnTerminClickedListener onTerminClickedListener) {

        return new TerminViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.confirm_single_day_termin_layout, parent, false), new TerminViewHolder.OnTerminCheckedListener() {
            @Override
            public void onTerminChecked(int position, boolean isChecked) {
                onTerminClickedListener.onTerminChecked(position, getTermin(position), isChecked, false);
                terminSelected.put(getTermin(position), isChecked);
            }
        }, new EditableTerminViewHolder.OnTerminClickedListener() {
            @Override
            public void onTerminClicked(int position, View view) {
                onTerminClickedListener.onTerminClicked(position, getTermin(position), view, false);
            }
        });
    }


    @Override
    public RecyclerView.ViewHolder getAllDayTerminView(ViewGroup parent, final OnTerminClickedListener onTerminClickedListener) {
        return new AllDayTerminViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.confirm_single_day_termin_layout, parent, false), new TerminViewHolder.OnTerminCheckedListener() {
            @Override
            public void onTerminChecked(int position, boolean isChecked) {
                onTerminClickedListener.onTerminChecked(position, getTermin(position), isChecked, false);
                terminSelected.put(getTermin(position), isChecked);
            }
        }, new EditableTerminViewHolder.OnTerminClickedListener() {
            @Override
            public void onTerminClicked(int position, View view) {
                onTerminClickedListener.onTerminClicked(position, getTermin(position), view, false);
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder getEditableTerminView(ViewGroup parent, final OnTerminClickedListener onTerminClickedListener) {
        return new EditableTerminViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.confirm_single_day_edit_termin_layout, parent, false), new EditableTerminViewHolder.OnTerminCheckedListener() {
            @Override
            public void onTerminChecked(int position, boolean isChecked) {
                terminSelected.put(getTermin(position), isChecked);
                if (!isChecked) {
                    removeTermin(position);
                }
            }
        }, new EditableTerminViewHolder.OnTerminClickedListener() {
            @Override
            public void onTerminClicked(int position, View view) {
                onTerminClickedListener.onTerminClicked(position, getTermin(position), view, true);
            }
        }, new EditableTerminViewHolder.OnTerminEndClickedListener() {
            @Override
            public void onTerminEndClicked(int position, View view) {
                onTerminClickedListener.onTerminEndEdited(position, getTermin(position), view, true);
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder getAllDayEditableTerminView(ViewGroup parent, final OnTerminClickedListener onTerminClickedListener) {
        return new AllDayEditableTerminView(LayoutInflater.from(parent.getContext()).inflate(R.layout.confirm_single_day_edit_termin_layout, parent, false), new TerminViewHolder.OnTerminCheckedListener() {
            @Override
            public void onTerminChecked(int position, boolean isChecked) {
                terminSelected.put(getTermin(position), isChecked);
                if (!isChecked) {
                    removeTermin(position);
                }
            }
        }, new EditableTerminViewHolder.OnTerminClickedListener() {
            @Override
            public void onTerminClicked(int position, View view) {
                onTerminClickedListener.onTerminClicked(position, getTermin(position), view, true);
            }
        }, new EditableTerminViewHolder.OnTerminEndClickedListener() {
            @Override
            public void onTerminEndClicked(int position, View view) {
                onTerminClickedListener.onTerminEndEdited(position, getTermin(position), view, true);

            }
        });
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder inHolder, int position) {

        if (inHolder instanceof TerminViewHolder) {
            InviteTermin inviteTermin = getTermin(position);
            TerminViewHolder holder = (TerminViewHolder) inHolder;
            holder.startTime.setText(inviteTermin.getTermin().getStartTime());
            holder.endTime.setText(inviteTermin.getTermin().getEndTime());
            holder.startMonthDay.setText(String.valueOf(inviteTermin.getTermin().getDay()));
            holder.startWeekDay.setText(inviteTermin.getTermin().getStartWeekDay());
            holder.personsAccepted.setText(String.valueOf(inviteTermin.getUsersAccepted().size()));
            holder.terminChecked.setChecked(terminSelected.get(inviteTermin));

            Termin t = inviteTermin.getTermin();
            long milisDiff = t.getEnd().getTimeInMillis() - t.getStart().getTimeInMillis();
            long secs = milisDiff / 1000;
            long hours = secs / 60 / 60;

            if (inHolder instanceof AllDayTerminViewHolder) {
                holder.duration.setText("ALL DAY");
            } else {
                holder.duration.setText(String.valueOf(hours) + " H");
            }

        }else if (inHolder instanceof SubtitleViewHolder) {
            ((SubtitleViewHolder) inHolder).subtitle.setText(subsTitles.get(position));
        }
    }


    public static class TerminViewHolder extends RecyclerView.ViewHolder {
        TextView startTime;
        TextView endTime;
        TextView startWeekDay;
        TextView startMonthDay;
        TextView duration;
        TextView personsAccepted;
        CheckBox terminChecked;
        ImageButton terminPeopleMore;
        View endTimeView;

        public TerminViewHolder(View itemView, final OnTerminCheckedListener onTerminCheckedListener, final OnTerminClickedListener onTerminClickedListener) {
            super(itemView);

            startTime = (TextView) itemView.findViewById(R.id.termin_start_time);
            endTime = (TextView) itemView.findViewById(R.id.termin_end_time);
            startWeekDay = (TextView) itemView.findViewById(R.id.termin_start_week_day);
            startMonthDay = (TextView) itemView.findViewById(R.id.termin_start_month_day);
            duration = (TextView) itemView.findViewById(R.id.termin_all_day_duration);
            endTimeView = itemView.findViewById(R.id.termin_end_layout);
            personsAccepted = (TextView) itemView.findViewById(R.id.termin_persons_accepted);
            terminPeopleMore = (ImageButton) itemView.findViewById(R.id.termin_people_more);

            terminChecked = (CheckBox) itemView.findViewById(R.id.termin_checked);
            terminChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    onTerminCheckedListener.onTerminChecked(getLayoutPosition(), isChecked);
                }
            });

            terminPeopleMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onTerminClickedListener.onTerminClicked(getLayoutPosition(), v);
                }
            });

        }

        protected interface OnTerminCheckedListener {
            void onTerminChecked(int position, boolean isChecked);
        }

        protected interface OnTerminClickedListener {
            void onTerminClicked(int position, View view);
        }
    }

    public static class EditableTerminViewHolder extends TerminViewHolder {

        public EditableTerminViewHolder(View itemView, final OnTerminCheckedListener onTerminCheckedListener, final OnTerminClickedListener onTerminClickedListener, final OnTerminEndClickedListener onTerminEndClickedListener) {
            super(itemView, onTerminCheckedListener, onTerminClickedListener);

            endTimeView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onTerminEndClickedListener.onTerminEndClicked(getLayoutPosition(), v);
                }
            });
        }

        protected interface OnTerminEndClickedListener {
            void onTerminEndClicked(int position, View view);
        }
    }

    public static class AllDayTerminViewHolder extends TerminViewHolder {

        public AllDayTerminViewHolder(View itemView, final OnTerminCheckedListener onTerminCheckedListener, final OnTerminClickedListener onTerminClickedListener) {
            super(itemView, onTerminCheckedListener, onTerminClickedListener);
            duration.setVisibility(View.VISIBLE);
            startTime.setVisibility(View.GONE);

            endTimeView.setVisibility(View.GONE);
        }
    }

    public static class AllDayEditableTerminView extends AllDayTerminViewHolder {

        public AllDayEditableTerminView(View itemView, OnTerminCheckedListener onTerminCheckedListener, final OnTerminClickedListener onTerminClickedListener, final EditableTerminViewHolder.OnTerminEndClickedListener onTerminEndClickedListener) {
            super(itemView, onTerminCheckedListener, onTerminClickedListener);

            endTimeView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onTerminEndClickedListener.onTerminEndClicked(getLayoutPosition(), v);
                }
            });

        }
    }


}
