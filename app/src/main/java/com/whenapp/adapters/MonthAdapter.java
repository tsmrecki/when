package com.whenapp.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whenapp.R;
import com.whenapp.model.Day;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Tomislav on 20.3.2015..
 */
public abstract class MonthAdapter extends RecyclerView.Adapter {

    Calendar calendar;
    Calendar previousMonthCalendar;
    Calendar nextMonthCalendar;
    int previousMonth;
    int previousMonthMax;
    int nextMonth;
    int thisMonth;
    int thisYear;

    Date selectableAfterDate = new Date(0);

    protected List<Integer> dates = new ArrayList<>();
    protected Map<Integer, Day> daysMap = new HashMap<>();

    protected DateViewHolder.OnDateViewHolderClickedListener onDateClickedListener;

    protected int today = -8;

    protected int previousMonthDays = 0;
    protected int nextMonthDays = 0;
    protected int thisMonthDays = 0;

    public MonthAdapter(int month, int year, final DateViewHolder.OnDateViewHolderClickedListener onDateClickedListener) {


        calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH);
        int currentYear = calendar.get(Calendar.YEAR);
        thisMonth = month;
        thisYear = year;

        if (thisMonth == currentMonth && thisYear == currentYear)
            today = calendar.get(Calendar.DAY_OF_MONTH);
        else calendar.set(Calendar.DAY_OF_MONTH, 1);

        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        int max = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int min = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        thisMonthDays = max;
        for (int i = min; i <= max; i++) {
            dates.add(i);
            notifyItemInserted(i - min);
        }

        //1
        int firstDayOfMonth = firstDayOfFirstWeek(calendar);

        previousMonthDays = (7 - (calendar.getFirstDayOfWeek() - firstDayOfMonth)) % 7;

        previousMonthCalendar = (Calendar) calendar.clone();
        previousMonthCalendar.add(Calendar.MONTH, -1);
        previousMonth = previousMonthCalendar.get(Calendar.MONTH);
        previousMonthMax = previousMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < previousMonthDays; i++) {
            dates.add(0, previousMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH) - i);
            notifyItemInserted(0);
        }

        nextMonthDays = 7 - (dates.size() % 7);
        nextMonthCalendar = (Calendar) calendar.clone();
        nextMonthCalendar.add(Calendar.MONTH, 1);
        nextMonth = nextMonthCalendar.get(Calendar.MONTH);
        for (int i = 0; i < nextMonthDays; i++) {
            dates.add(nextMonthCalendar.getActualMinimum(Calendar.DAY_OF_MONTH) + i);
            notifyItemInserted(dates.size() - 1);
        }

        this.onDateClickedListener = getOnDateClickedListener(onDateClickedListener);

    }

    public void setSelectableAfterDate(Date selectableAfterDate) {
        this.selectableAfterDate = selectableAfterDate;
        Calendar c = Calendar.getInstance();
        c.setTime(selectableAfterDate);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND, 0);
        this.selectableAfterDate = c.getTime();
    }

    Calendar helperCalendar = Calendar.getInstance();

    public void setDays(List<Day> days) {
        daysMap.clear();

        for (Day day : days) {
            helperCalendar.setTime(day.getDate());

            int year = helperCalendar.get(Calendar.YEAR);
            int month = helperCalendar.get(Calendar.MONTH);
            int dayOfMonth = helperCalendar.get(Calendar.DAY_OF_MONTH);

            int position = -1;
            if(year == thisYear) {
                if (month == thisMonth) {
                    position = previousMonthDays + dayOfMonth - 1;
                } else if ((month == previousMonth) && (dayOfMonth > previousMonthMax - previousMonthDays)) {
                    position = dayOfMonth - (previousMonthMax - previousMonthDays) - 1;
                } else if (month == nextMonth && (dayOfMonth <= nextMonthDays)) { //if month=nextMonth && dayOfMonth < nextMonthDays
                    position = previousMonthDays + thisMonthDays + dayOfMonth - 1;
                } else {
                    continue;
                }

                if(daysMap.containsKey(position)){
                    Day oldDay = daysMap.get(position);
                    oldDay.addApprovedEvents(day.getApprovedEvents());
                    oldDay.addPendingEvents(day.getPendingEvents());
                }
                else
                    daysMap.put(position, day.copy());


                notifyItemChanged(position);
            }
        }


    }

    public abstract DateViewHolder.OnDateViewHolderClickedListener getOnDateClickedListener(DateViewHolder.OnDateViewHolderClickedListener onDateClickedListener);

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case REGULAR_DAY_VIEW_HOLDER:
                return new DateViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_date_view, parent, false), onDateClickedListener);
            case SELECTED_DAY_VIEW_HOLDER:
                return new SelectedDayViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_date_view, parent, false), onDateClickedListener);
            case TODAY_VIEW_HOLDER:
                return new TodayViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_date_view, parent, false), onDateClickedListener);
            case NOT_THIS_MONTH_VIEW_HOLDER:
                return new NotThisMonthDayViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_date_view, parent, false), onDateClickedListener);
            case REGULAR_BUSY_ALL_HOLDER:
                return new BusyAllRegularViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_busy_date_view, parent, false), onDateClickedListener);
            case SELECTED_BUSY_ALL_HOLDER:
                return new BusyAllSelectedViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_busy_date_view, parent, false), onDateClickedListener);
            case TODAY_BUSY_ALL_HOLDER:
                return new BusyAllTodayViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_busy_date_view, parent, false), onDateClickedListener);
            case NOT_THIS_MONTH_BUSY_ALL_HOLDER:
                return new BusyAllNotThisMonthHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_busy_date_view, parent, false), onDateClickedListener);
            case NON_SELECTABLE_VIEW_HOLDER:
                return new NonSelectableViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_date_non_selectable_view, parent, false), onDateClickedListener);
            default:
                return new DateViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_date_view, parent, false), onDateClickedListener);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof DateViewHolder) {
            ((DateViewHolder) holder).day.setText("" + dates.get(position));

            if (holder instanceof BusyAllDayViewHolder) {
                int acceptedCount = 0;
                int pendingCount = 0;
                if (daysMap.containsKey(position)) {
                    Day d = daysMap.get(position);
                    acceptedCount = d.getApprovedEventsCount();
                    pendingCount = d.getPendingEventsCount();
                }
                if (acceptedCount > 0) {
                    ((BusyAllDayViewHolder) holder).acceptedCount.setVisibility(View.VISIBLE);
                    ((BusyAllDayViewHolder) holder).acceptedCount.setText(String.valueOf(acceptedCount));
                } else ((BusyAllDayViewHolder) holder).acceptedCount.setVisibility(View.INVISIBLE);

                if (pendingCount > 0) {
                    ((BusyAllDayViewHolder) holder).pendingCount.setText(String.valueOf(pendingCount));
                    ((BusyAllDayViewHolder) holder).pendingCount.setVisibility(View.VISIBLE);
                } else ((BusyAllDayViewHolder) holder).pendingCount.setVisibility(View.INVISIBLE);
            }
        }
    }


    protected Date getDateOnPosition(int position) {
        Calendar c = (Calendar) calendar.clone();
        if (position >= previousMonthDays && position < thisMonthDays + previousMonthDays) {
            c.set(Calendar.DAY_OF_MONTH, dates.get(position));
        } else if (position < previousMonthDays) {
            c = (Calendar) previousMonthCalendar.clone();
            c.set(Calendar.DAY_OF_MONTH, dates.get(position));
        } else if (position >= thisMonthDays + previousMonthDays) {
            c = (Calendar) nextMonthCalendar.clone();
            c.set(Calendar.DAY_OF_MONTH, dates.get(position));
        }
        c.set(Calendar.MILLISECOND, 1);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.HOUR_OF_DAY, 0);
        return c.getTime();
    }


    @Override
    public int getItemCount() {
        return dates.size();
    }

    protected static final int TODAY_VIEW_HOLDER = 0;
    protected static final int REGULAR_DAY_VIEW_HOLDER = 1;
    protected static final int SELECTED_DAY_VIEW_HOLDER = 2;
    protected static final int TODAY_BUSY_ALL_HOLDER = 8;
    protected static final int REGULAR_BUSY_ALL_HOLDER = 9;
    protected static final int SELECTED_BUSY_ALL_HOLDER = 10;
    protected static final int NOT_THIS_MONTH_VIEW_HOLDER = 11;
    protected static final int NOT_THIS_MONTH_BUSY_ALL_HOLDER = 12;
    protected static final int NON_SELECTABLE_VIEW_HOLDER = 13;

    public static class DateViewHolder extends RecyclerView.ViewHolder {

        protected TextView day;

        public DateViewHolder(View itemView, final OnDateViewHolderClickedListener onDateClickedListener) {
            super(itemView);
            day = (TextView) itemView.findViewById(R.id.day_of_month);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onDateClickedListener.onDateViewHolderClicked(getLayoutPosition(), new Date());
                }
            });
        }

        public interface OnDateViewHolderClickedListener {
            public void onDateViewHolderClicked(int position, Date date);
        }
    }

    public static class NotThisMonthDayViewHolder extends DateViewHolder {

        public NotThisMonthDayViewHolder(View itemView, OnDateViewHolderClickedListener onDateClickedListener) {
            super(itemView, onDateClickedListener);
            day.setTextColor(Color.parseColor("#80FFFFFF"));
        }
    }

    public static class NonSelectableViewHolder extends DateViewHolder {

        public NonSelectableViewHolder(View itemView, OnDateViewHolderClickedListener onDateClickedListener) {
            super(itemView, onDateClickedListener);
        }
    }

    public static class TodayViewHolder extends DateViewHolder {

        public TodayViewHolder(View itemView, OnDateViewHolderClickedListener onDateClickedListener) {
            super(itemView, onDateClickedListener);
            day.setBackgroundResource(R.drawable.current_day_background);
            day.setTextColor(itemView.getContext().getResources().getColor(R.color.prominent_green));
        }
    }

    public static class SelectedDayViewHolder extends DateViewHolder {

        public SelectedDayViewHolder(View itemView, OnDateViewHolderClickedListener onDateClickedListener) {
            super(itemView, onDateClickedListener);
            day.setBackgroundResource(R.drawable.selected_day_background);
            day.setTextColor(Color.WHITE);
        }
    }


    public static class BusyAllDayViewHolder extends DateViewHolder {

        protected TextView acceptedCount;
        protected TextView pendingCount;

        public BusyAllDayViewHolder(View itemView, OnDateViewHolderClickedListener onDateClickedListener) {
            super(itemView, onDateClickedListener);
            this.acceptedCount = (TextView) itemView.findViewById(R.id.accepted_badge_count);
            this.pendingCount = (TextView) itemView.findViewById(R.id.pending_badge_count);
        }
    }

    public static class BusyAllNotThisMonthHolder extends BusyAllDayViewHolder {

        public BusyAllNotThisMonthHolder(View itemView, OnDateViewHolderClickedListener onDateClickedListener) {
            super(itemView, onDateClickedListener);
            this.day.setTextColor(Color.parseColor("#80FFFFFF"));
        }
    }

    public static class BusyAllTodayViewHolder extends BusyAllDayViewHolder {

        public BusyAllTodayViewHolder(View itemView, OnDateViewHolderClickedListener onDateClickedListener) {
            super(itemView, onDateClickedListener);
            this.day.setTextColor(itemView.getResources().getColor(R.color.prominent_green));
            this.day.setBackgroundResource(R.drawable.current_day_background);
        }
    }

    public static class BusyAllSelectedViewHolder extends BusyAllDayViewHolder {

        public BusyAllSelectedViewHolder(View itemView, OnDateViewHolderClickedListener onDateClickedListener) {
            super(itemView, onDateClickedListener);
            day.setBackgroundResource(R.drawable.selected_day_background);
            day.setTextColor(Color.WHITE);
        }
    }

    public static class BusyAllRegularViewHolder extends BusyAllDayViewHolder {

        public BusyAllRegularViewHolder(View itemView, OnDateViewHolderClickedListener onDateClickedListener) {
            super(itemView, onDateClickedListener);
        }
    }


    private int firstDayOfFirstWeek(Calendar calendar) {
        Calendar start = (Calendar) calendar.clone();
        start.set(Calendar.DAY_OF_MONTH, 1);
        return start.get(Calendar.DAY_OF_WEEK);
    }

}
