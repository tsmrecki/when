package com.whenapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whenapp.R;

import java.util.Calendar;

/**
 * Created by Tomislav on 20.3.2015..
 */
public class WeekDaysAdapter extends RecyclerView.Adapter {

    Calendar calendar = Calendar.getInstance();

    public WeekDaysAdapter() {

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WeekDayHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.calendar_week_day_view, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof WeekDayHolder) {
            int day = (position + calendar.getFirstDayOfWeek() - 1) % 7;
            ((WeekDayHolder) holder).day.setText(holder.itemView.getContext().getResources().getStringArray(R.array.week_days)[day]);
        }
    }

    @Override
    public int getItemCount() {
        return 7;
    }

    public static class WeekDayHolder extends RecyclerView.ViewHolder {

        TextView day;

        public WeekDayHolder(View itemView) {
            super(itemView);
            day = (TextView) itemView.findViewById(R.id.day_of_week);
        }
    }
}
