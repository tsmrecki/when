package com.whenapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.whenapp.R;
import com.whenapp.layer.MessageView;
import com.whenapp.model.ChatMessage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomislav on 18/02/16.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MessageViewHolder> {

    private List<ChatMessage> messages = new ArrayList<>();

    public ChatAdapter(){}

    public ChatAdapter(List<ChatMessage> messages){
        this.messages = messages;
    }

    public void clear(){
        this.messages.clear();
        notifyDataSetChanged();
    }

    public void add(ChatMessage chatMessage){
        messages.add(chatMessage);
        notifyItemInserted(messages.indexOf(chatMessage));
    }

    public void update(int position, ChatMessage chatMessage){
        messages.remove(position);
        messages.add(position, chatMessage);
        notifyItemChanged(position);
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_SENT){
            return new MessageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sent_message, parent, false));
        }else{
            return new MessageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.received_message, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        ChatMessage message = messages.get(position);
        holder.sender.setText(message.sender.getName());
        holder.message.setText(MessageView.craftMsgText(message.message));
        holder.status.setImageResource(message.messageStatusResource);
        //Add the timestamp
        if (message.message.getSentAt() != null) {
            holder.meta.setText(SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.SHORT, DateFormat.SHORT).format(message.message.getReceivedAt()));
        }

        ImageLoader.getInstance().displayImage(message.sender.getImage(), holder.userImage);

        //holder.meta.setText(MessageView.);
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    private static final int TYPE_SENT = 0;
    private static final int TYPE_RECEIVED = 1;

    @Override
    public int getItemViewType(int position) {
        if(messages.get(position).isSent) return TYPE_SENT;
        else return TYPE_RECEIVED;
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {

        private TextView sender;
        private TextView message;
        private TextView meta;
        private ImageView status;
        private ImageView userImage;

        public MessageViewHolder(View itemView) {
            super(itemView);

            sender = (TextView) itemView.findViewById(R.id.message_sender);
            message = (TextView) itemView.findViewById(R.id.message);
            meta = (TextView) itemView.findViewById(R.id.message_meta);
            status = (ImageView) itemView.findViewById(R.id.message_status);
            userImage = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}
