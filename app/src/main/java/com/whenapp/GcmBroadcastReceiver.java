package com.whenapp;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.whenapp.model.SessionManager;

/**
 * Created by tomislav on 02/05/15.
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {


        //Intent mServiceIntent = new Intent(context, RefreshService.class);
        //context.startService(mServiceIntent);


        // Explicitly specify that GcmIntentService will handle the intent.
        ComponentName comp = new ComponentName(context.getPackageName(),
                GcmIntentService.class.getName());


        // Start the service, keeping the device awake while it is launching if the user is logged in
        if(SessionManager.getInstance(context).isLoggedIn())
            startWakefulService(context, (intent.setComponent(comp)));

        setResultCode(Activity.RESULT_OK);
    }
}
