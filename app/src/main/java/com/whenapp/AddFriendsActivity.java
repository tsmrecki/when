package com.whenapp;

import android.*;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.provider.ContactsContract;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.appevents.AppEventsLogger;
import com.whenapp.adapters.UsersAdapter;
import com.whenapp.common.Utils;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.UsersDataLoader;
import com.whenapp.database.UsersTable;
import com.whenapp.model.ContactToken;
import com.whenapp.model.GroupContactToken;
import com.whenapp.model.User;
import com.whenapp.model.UserContactToken;
import com.whenapp.model.UsersGroup;
import com.whenapp.views.PersonCompletionView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class AddFriendsActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<List<User>> {


    PersonCompletionView completionView;
    List<ContactToken> completion = new ArrayList<>();
    UsersAdapter usersAdapter;

    private List<User> phoneContacts = new ArrayList<>();

    LoadPhoneContacts loadPhoneContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friends);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("contact_users")) {
                phoneContacts = savedInstanceState.getParcelableArrayList("contact_users");
            }
        }
        setResult(RESULT_CANCELED);

        getSupportLoaderManager().initLoader(12, null, this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final List<User> users = new ArrayList<>();


        for (User phoneUser : phoneContacts) {
            if(!users.contains(phoneUser)) users.add(phoneUser);
        }


        for(User user : users){
            completion.add(new UserContactToken(user, user.getName(), user.getImage()));
        }

        PersonCompletionView.CompletionAdapter userAdapter = new PersonCompletionView.CompletionAdapter(this, R.layout.user_layout, completion);
        completionView = (PersonCompletionView) findViewById(R.id.person_completion_view);
        char[] splitChar = {',', ';', ' '};
        completionView.setSplitChar(splitChar);
        completionView.setAdapter(userAdapter);
        completionView.allowDuplicates(false);


        RecyclerView usersRecyclerView = (RecyclerView) findViewById(R.id.users_recycler_view);
        usersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        usersAdapter = new UsersAdapter(users, new UsersAdapter.OnUserClickedListener() {
            @Override
            public void onUserClicked(User user) {
                ContactToken ct = new UserContactToken(user, user.getName(), user.getImage());
                completionView.addObject(ct);
            }
        });
        usersRecyclerView.setAdapter(usersAdapter);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        loadPhoneContacts = new LoadPhoneContacts();
        if(ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED )
        loadPhoneContacts.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_friends, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == android.R.id.home){
            finish();
            return true;
        }else if(id == R.id.action_done){
            completionView.performCompletion();
            completionView.performValidation();
            finishWithResult();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void finishWithResult(){
        Intent result = new Intent();
        List<ContactToken> ctList = completionView.getObjects();


        List<User> usersList = new ArrayList<>();

        for (ContactToken token : ctList) {
            for(User u : token.getUsers()) {
                if (Utils.isValidEmail(u.getEmail())) {
                    usersList.add(u);
                }
            }
        }

        result.putParcelableArrayListExtra("contacts", (ArrayList<? extends Parcelable>) usersList);

        if (getParent() == null) {
            setResult(Activity.RESULT_OK, result);
        } else {
            getParent().setResult(Activity.RESULT_OK, result);
        }
        finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    SQLiteDatabase database;
    @Override
    public Loader<List<User>> onCreateLoader(int id, Bundle args) {
        database = CalendarDatabaseHelper.getInstance(this).getWritableDatabase();
        UsersTable usersTable = new UsersTable(database);
        return new UsersDataLoader(this, usersTable, null, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<List<User>> loader, List<User> data) {
        for (User phoneUser : phoneContacts) {
            if(!data.contains(phoneUser)) data.add(phoneUser);
        }

        List<ContactToken> completion = new ArrayList<>();

        for(User u : data){
            completion.add(new UserContactToken(u, u.getName(), u.getImage()));
        }

        PersonCompletionView.CompletionAdapter userAdapter = new PersonCompletionView.CompletionAdapter(this, R.layout.user_layout, completion);
        completionView.setAdapter(userAdapter);
        this.usersAdapter.setUsers(data);
    }

    @Override
    public void onLoaderReset(Loader<List<User>> loader) {

    }


    @Override
    protected void onDestroy() {
        if(loadPhoneContacts != null) loadPhoneContacts.cancel(true);
        super.onDestroy();
    }


    public class LoadPhoneContacts extends AsyncTask<Void, Void, List<User>>{

        @Override
        protected List<User> doInBackground(Void... params) {
            return getPhoneContacts(AddFriendsActivity.this);
        }

        @Override
        protected void onPostExecute(List<User> users) {
            phoneContacts = users;
            List<User> oldUsers = usersAdapter.getUsers();
            for (User u : users) {
                if(!oldUsers.contains(u)) oldUsers.add(u);
            }
            usersAdapter.setUsers(oldUsers);

            for(User user : users){
                completion.add(new UserContactToken(user, user.getName(), user.getImage()));
            }

            PersonCompletionView.CompletionAdapter completionAdapter = new PersonCompletionView.CompletionAdapter(getApplicationContext(), R.layout.user_layout, completion);
            completionView.setAdapter(completionAdapter);
        }
    }

    public static ArrayList<User> getPhoneContacts(Context context){
        ArrayList<User> users = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
        if (cur != null && cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                Cursor cur1 = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                if(cur1 != null) {
                    while (cur1.moveToNext()) {
                        //to get the contact names
                        String name = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY));
                        String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

                        if (email != null) {
                            User u = new User(name, "");
                            u.setEmail(email);
                            users.add(u);
                        }
                    }
                    cur1.close();
                }
            }
            cur.close();
        }
        return users;
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        outState.putParcelableArrayList("contact_users", (ArrayList<? extends Parcelable>) phoneContacts);
    }
}
