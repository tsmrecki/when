package com.whenapp.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import com.whenapp.model.User;
import com.whenapp.model.UsersGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomislav on 21/04/15.
 */
public class UsersGroupsTable extends Table<UsersGroup> {

    // Database table
    public static final String TABLE_NAME = "user_groups";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_USERS_IDS = "user_ids";

    // Database creation SQL statement
    private static final String CREATE_TABLE = "create table "
            + TABLE_NAME
            + "("
            + COLUMN_ID + " integer primary key, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_USERS_IDS + " text not null"
            + ");";

    private static final String CREATE_INDEX = "create index user_groups_idx ON " + TABLE_NAME + "(" + COLUMN_ID + ");";


    private FriendsTable usersTable;

    public UsersGroupsTable(SQLiteDatabase database) {
        super(database);
        usersTable = new FriendsTable(database);
    }

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE);
        database.execSQL(CREATE_INDEX);
    }

    @Override
    public boolean insert(UsersGroup entity) {
        if (entity == null) {
            return false;
        }
        try {
            long result = mDatabase.insertWithOnConflict(TABLE_NAME, null,
                    generateContentValuesFromObject(entity), SQLiteDatabase.CONFLICT_REPLACE);
            return result != -1;
        }catch (SQLiteConstraintException ex){
            return false;
        }
    }

    @Override
    public boolean delete(UsersGroup entity) {
        if (entity == null) {
            return false;
        }
        int result = mDatabase.delete(TABLE_NAME,
                COLUMN_ID + " = " + entity.getId(), null);
        return result != 0;
    }

    @Override
    public boolean update(UsersGroup entity) {
        if (entity == null) {
            return false;
        }
        ContentValues values = generateContentValuesFromObject(entity);
        values.remove(COLUMN_ID);
        int result = mDatabase.update(TABLE_NAME,
                values, COLUMN_ID + " = "
                        + entity.getId(), null);
        return result != 0;
    }

    @Override
    public List read() {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), null,
                null, null, null, null);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor, usersTable));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    @Override
    public List read(String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), selection,
                selectionArgs, groupBy, having, orderBy);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor, usersTable));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    public String[] getAllColumns() {
        return new String[]{COLUMN_ID, COLUMN_NAME, COLUMN_USERS_IDS};
    }

    public static UsersGroup generateObjectFromCursor(Cursor cursor, FriendsTable usersTable) {
        if (cursor == null) {
            return null;
        }

        String usersIds = cursor.getString(cursor.getColumnIndex(COLUMN_USERS_IDS));
        String[] uIds = usersIds.split(",");


        List<User> users = new ArrayList<>();
        for (String uId : uIds) {

            List<User> uout = usersTable.read(UsersTable.COLUMN_ID + "=?", new String[]{uId}, null, null, null);

            if(!uout.isEmpty())
                users.add(uout.get(0));
        }


        UsersGroup userG = new UsersGroup(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)), users);
        userG.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));


        return userG;
    }

    public static ContentValues generateContentValuesFromObject(UsersGroup entity) {
        if (entity == null) {
            return null;
        }
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, entity.getName());
        values.put(COLUMN_ID, entity.getId());

        StringBuilder userIds = new StringBuilder();
        for(User u : entity.getUsers()){
            userIds.append(u.getId()).append(",");
        }

        String uIdsString = userIds.toString();
        if(!uIdsString.isEmpty()){
            values.put(COLUMN_USERS_IDS, uIdsString.substring(0, uIdsString.length() -1));
        }else{
            values.put(COLUMN_USERS_IDS, "");
        }

        return values;
    }
}
