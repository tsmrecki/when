package com.whenapp.database;

import android.os.AsyncTask;
import android.support.v4.content.Loader;

/**
 * Created by tomislav on 22/04/15.
 */
public abstract class ContentAdapterTask<T1, T2, T3> extends
        AsyncTask<T1, T2, T3> {
    private Loader<?> loader = null;

    ContentAdapterTask(Loader<?> loader) {
        this.loader = loader;
    }

    @Override
    protected void onPostExecute(T3 param) {
        loader.onContentChanged();
    }
}
