package com.whenapp.database;

import android.content.Context;

import com.whenapp.model.UsersGroup;

import java.util.List;

/**
 * Created by tomislav on 22/04/15.
 */
public class UserGroupsDataLoader extends AbstractDataLoader<List<UsersGroup>> {
    private Table<UsersGroup> mDataSource;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mGroupBy;
    private String mHaving;
    private String mOrderBy;

    public UserGroupsDataLoader(Context context, Table dataSource, String selection, String[] selectionArgs,
                                 String groupBy, String having, String orderBy) {
        super(context);
        mDataSource = dataSource;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mGroupBy = groupBy;
        mHaving = having;
        mOrderBy = orderBy;
    }

    @Override
    protected List<UsersGroup> buildList() {
        List<UsersGroup> testList = mDataSource.read(mSelection, mSelectionArgs, mGroupBy, mHaving,
                mOrderBy);
        return testList;
    }

    public void insert(UsersGroup entity) {
        new InsertTask(this).execute(entity);
    }

    public void insert(List<UsersGroup> events){

        new InsertMultiTask(this).execute(events);
    }

    public void update(UsersGroup entity) {
        new UpdateTask(this).execute(entity);
    }

    public void delete(UsersGroup entity) {
        new DeleteTask(this).execute(entity);
    }

    public void deleteAll() {
        mDataSource.mDatabase.delete(UsersGroupsTable.TABLE_NAME, null, null);
    }


    private class InsertTask extends ContentAdapterTask<UsersGroup, Void, Void> {
        InsertTask(UserGroupsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(UsersGroup... params) {
            for(UsersGroup e : params) {
                boolean succ = mDataSource.insert(e);
                if(!succ){
                    boolean s2 = mDataSource.update(e);
                }
            }
            return (null);
        }
    }

    private class InsertMultiTask extends ContentAdapterTask<List<UsersGroup>, Void, Void> {
        InsertMultiTask(UserGroupsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(List<UsersGroup>... params) {
            List<UsersGroup> list = params[0];
            for(UsersGroup e : list){
                boolean succ = mDataSource.insert(e);
                if(!succ){
                    boolean s2 = mDataSource.update(e);
                }
            }
            return (null);
        }
    }

    private class UpdateTask extends ContentAdapterTask<UsersGroup, Void, Void> {
        UpdateTask(UserGroupsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(UsersGroup... params) {
            mDataSource.update(params[0]);
            return (null);
        }
    }

    private class DeleteTask extends ContentAdapterTask<UsersGroup, Void, Void> {
        DeleteTask(UserGroupsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(UsersGroup... params) {
            mDataSource.delete(params[0]);
            return (null);
        }
    }
}