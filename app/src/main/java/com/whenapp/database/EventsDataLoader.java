package com.whenapp.database;

import android.content.Context;

import com.whenapp.model.Event;

import java.util.List;

/**
 * Created by tomislav on 22/04/15.
 */
public class EventsDataLoader extends AbstractDataLoader<List<Event>> {
    private Table<Event> mDataSource;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mGroupBy;
    private String mHaving;
    private String mOrderBy;
    private int year = 0;
    private int month = 0;
    private int day = 0;
    private boolean customLoader = false;


    public EventsDataLoader(Context context, Table dataSource, String selection, String[] selectionArgs,
                             String groupBy, String having, String orderBy) {
        super(context);
        mDataSource = dataSource;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mGroupBy = groupBy;
        mHaving = having;
        mOrderBy = orderBy;
    }

    public EventsDataLoader(Context context, Table dataSource, String selection, String[] selectionArgs,
                            String groupBy, String having, String orderBy, int afterYear, int afterMonth, int afterDay) {
        super(context);
        mDataSource = dataSource;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mGroupBy = groupBy;
        mHaving = having;
        mOrderBy = orderBy;
        customLoader = true;
        this.year = afterYear;
        this.month = afterMonth;
        this.day = afterDay;
    }

    @Override
    protected List<Event> buildList() {
        List<Event> testList;
        if (!customLoader) {
            testList = mDataSource.read(mSelection, mSelectionArgs, mGroupBy, mHaving,
                    mOrderBy);
        } else {
            testList = ((EventsTable)mDataSource).read(mSelection, mSelectionArgs, mGroupBy, mHaving, mOrderBy, year, month, day);
        }
        return testList;
    }

    public void insert(Event entity) {
        new InsertTask(this).execute(entity);
    }

    public void insert(List<Event> events){

        new InsertMultiTask(this).execute(events);
    }

    public void update(Event entity) {
        new UpdateTask(this).execute(entity);
    }

    public void delete(Event entity) {
        new DeleteTask(this).execute(entity);
    }

    public void deleteAll() {
        mDataSource.mDatabase.delete(EventsTable.TABLE_NAME, null, null);
    }


    private class InsertTask extends ContentAdapterTask<Event, Void, Void> {
        InsertTask(EventsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(Event... params) {
            for(Event e : params) {
                boolean succ = mDataSource.insert(e);
                if(!succ){
                    boolean s2 = mDataSource.update(e);
                }
            }
            return (null);
        }
    }

    private class InsertMultiTask extends ContentAdapterTask<List<Event>, Void, Void> {
        InsertMultiTask(EventsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(List<Event>... params) {
            List<Event> list = params[0];
            mDataSource.mDatabase.beginTransaction();
            for(Event e : list){
                boolean succ = mDataSource.insert(e);
                if(!succ){
                    boolean s2 = mDataSource.update(e);
                }
            }
            mDataSource.mDatabase.setTransactionSuccessful();
            mDataSource.mDatabase.endTransaction();
            return (null);
        }
    }

    private class UpdateTask extends ContentAdapterTask<Event, Void, Void> {
        UpdateTask(EventsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(Event... params) {
            mDataSource.update(params[0]);
            return (null);
        }
    }

    private class DeleteTask extends ContentAdapterTask<Event, Void, Void> {
        DeleteTask(EventsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(Event... params) {
            mDataSource.delete(params[0]);
            return (null);
        }
    }
}