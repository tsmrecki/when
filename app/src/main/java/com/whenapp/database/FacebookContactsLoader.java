package com.whenapp.database;

import android.content.Context;

import com.whenapp.model.User;

import java.util.List;

/**
 * Created by tomislav on 22/04/15.
 */
public class FacebookContactsLoader extends AbstractDataLoader<List<User>> {
    private Table<User> mDataSource;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mGroupBy;
    private String mHaving;
    private String mOrderBy;

    public FacebookContactsLoader(Context context, Table dataSource, String selection, String[] selectionArgs,
                                  String groupBy, String having, String orderBy) {
        super(context);
        mDataSource = dataSource;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mGroupBy = groupBy;
        mHaving = having;
        mOrderBy = orderBy;
    }

    @Override
    protected List<User> buildList() {
        List<User> testList = mDataSource.read(mSelection, mSelectionArgs, mGroupBy, mHaving,
                mOrderBy);
        return testList;
    }

    public void insert(User entity) {
        new InsertTask(this).execute(entity);
    }

    public void insert(List<User> events){

        new InsertMultiTask(this).execute(events);
    }

    public void update(User entity) {
        new UpdateTask(this).execute(entity);
    }

    public void delete(User entity) {
        new DeleteTask(this).execute(entity);
    }

    public void deleteAll() {
        mDataSource.mDatabase.delete(FacebookContactsTable.TABLE_NAME, null, null);
    }


    private class InsertTask extends ContentAdapterTask<User, Void, Void> {
        InsertTask(FacebookContactsLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(User... params) {
            for(User e : params) {
                boolean succ = mDataSource.insert(e);
                if(!succ){
                    boolean s2 = mDataSource.update(e);
                }
            }
            return (null);
        }
    }

    private class InsertMultiTask extends ContentAdapterTask<List<User>, Void, Void> {
        InsertMultiTask(FacebookContactsLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(List<User>... params) {
            List<User> list = params[0];
            for(User e : list){
                boolean succ = mDataSource.insert(e);
                if(!succ){
                    boolean s2 = mDataSource.update(e);
                }
            }
            return (null);
        }
    }

    private class UpdateTask extends ContentAdapterTask<User, Void, Void> {
        UpdateTask(FacebookContactsLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(User... params) {
            mDataSource.update(params[0]);
            return (null);
        }
    }

    private class DeleteTask extends ContentAdapterTask<User, Void, Void> {
        DeleteTask(FacebookContactsLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(User... params) {
            mDataSource.delete(params[0]);
            return (null);
        }
    }
}