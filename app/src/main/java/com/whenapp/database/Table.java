package com.whenapp.database;

import android.database.sqlite.SQLiteDatabase;

import java.util.List;

/**
 * Created by tomislav on 22/04/15.
 */
public abstract class Table<T> {

    protected SQLiteDatabase mDatabase;

    public Table(SQLiteDatabase database) {
        mDatabase = database;
    }

    public abstract boolean insert(T entity);

    public abstract boolean delete(T entity);

    public abstract boolean update(T entity);

    public abstract List read();

    public abstract List read(String selection, String[] selectionArgs,
                              String groupBy, String having, String orderBy);

}
