package com.whenapp.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import com.whenapp.model.Event;
import com.whenapp.model.Invitation;
import com.whenapp.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomislav on 21/04/15.
 */
public class MyInvitationsTable extends Table<Invitation> {

    // Database table
    public static final String TABLE_NAME = "my_invitations";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_EVENT_ID = "event_id";
    public static final String COLUMN_CALLER_ID = "caller_id";
    public static final String COLUMN_CALLEE_ID = "callee_id";
    public static final String COLUMN_STATUS = "status";

    // Database creation SQL statement
    private static final String CREATE_TABLE = "create table "
            + TABLE_NAME
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_EVENT_ID + " integer not null, "
            + COLUMN_CALLER_ID + " integer not null, "
            + COLUMN_STATUS + " text, "
            + COLUMN_CALLEE_ID + " integer not null"
            + ");";

    private static final String CREATE_INDEX = "create index my_invites_idx ON " + TABLE_NAME + "(" + COLUMN_ID + ");";

    private UsersTable usersTable;
    private InvitesEventsTable eventsTable;

    public MyInvitationsTable(SQLiteDatabase database) {
        super(database);
        usersTable = new UsersTable(database);
        eventsTable = new InvitesEventsTable(database);
    }

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE);
        database.execSQL(CREATE_INDEX);
    }

    @Override
    public boolean insert(Invitation entity) {
        if (entity == null) {
            return false;
        }

            usersTable.insert(entity.getCallee());
            usersTable.insert(entity.getCaller());
            //eventsTable.insert(entity.getEvent());

        try{
        long result = mDatabase.insertWithOnConflict(TABLE_NAME, null,
                generateContentValuesFromObject(entity), SQLiteDatabase.CONFLICT_REPLACE);

        return result != -1;

    }catch (SQLiteConstraintException ex){
        return false;
    }
    }

    @Override
    public boolean delete(Invitation entity) {
        if (entity == null) {
            return false;
        }
        int result = mDatabase.delete(TABLE_NAME,
                COLUMN_ID + " = " + entity.getId(), null);
        return result != 0;

    }

    @Override
    public boolean update(Invitation entity) {
        if (entity == null) {
            return false;
        }
        ContentValues values = generateContentValuesFromObject(entity);
        values.remove(COLUMN_ID);
        int result = mDatabase.update(TABLE_NAME,
                values, COLUMN_ID + " = "
                        + entity.getId(), null);
        return result != 0;
    }

    @Override
    public List read() {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), null,
                null, null, null, null);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor, usersTable, eventsTable));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    @Override
    public List read(String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), selection,
                selectionArgs, groupBy, having, orderBy);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor, usersTable, eventsTable));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    public static String[] getAllColumns() {
        return new String[]{COLUMN_ID, COLUMN_EVENT_ID, COLUMN_CALLER_ID, COLUMN_CALLEE_ID, COLUMN_STATUS};
    }

    public static Invitation generateObjectFromCursor(Cursor cursor, UsersTable usersTable, InvitesEventsTable eventsTable) {
        if (cursor == null) {
            return null;
        }

        long eventId = cursor.getLong(cursor.getColumnIndex(COLUMN_EVENT_ID));
        List<Event> events = eventsTable.read(EventsTable.COLUMN_ID + "=?", new String[]{String.valueOf(eventId)}, null, null, null);
        Event event;
        if(!events.isEmpty())
            event = events.get(0);
        else
            event = new Event("Invitation");


        User caller = new User("", "");
        long callerId = cursor.getLong(cursor.getColumnIndex(COLUMN_CALLER_ID));
        List<User> callerList =  usersTable.read(UsersTable.COLUMN_ID + "=?", new String[]{String.valueOf(callerId)}, null, null, null);
        if(!callerList.isEmpty())
            caller = callerList.get(0);

        long calleId = cursor.getLong(cursor.getColumnIndex(COLUMN_CALLEE_ID));
        User callee = new User("", "");
        List<User> calleeList = usersTable.read(UsersTable.COLUMN_ID + "=?", new String[]{String.valueOf(calleId)}, null, null, null);
        if(!calleeList.isEmpty())callee = calleeList.get(0);

        Invitation invitation = new Invitation(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)), event, caller, callee);

        String status = cursor.getString(cursor.getColumnIndex(COLUMN_STATUS));
        if (!status.isEmpty()) {
            invitation.setStatus(Invitation.Status.valueOf(status));
        }

        return invitation;
    }

    public static ContentValues generateContentValuesFromObject(Invitation entity) {
        if (entity == null) {
            return null;
        }
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, entity.getId());
        values.put(COLUMN_EVENT_ID, entity.getEvent().getId());
        values.put(COLUMN_CALLER_ID, entity.getCaller().getId());
        values.put(COLUMN_CALLEE_ID, entity.getCallee().getId());
        values.put(COLUMN_STATUS, entity.getStatus().toString());

        return values;
    }
}
