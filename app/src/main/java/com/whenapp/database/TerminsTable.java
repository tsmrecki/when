package com.whenapp.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import com.whenapp.model.AllDayTermin;
import com.whenapp.model.InviteTermin;
import com.whenapp.model.Termin;
import com.whenapp.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomislav on 21/04/15.
 */
public class TerminsTable extends Table<InviteTermin> {

    // Database table
    public static final String TABLE_NAME = "termins";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_START_YEAR = "start_year";
    public static final String COLUMN_START_MONTH = "start_month";
    public static final String COLUMN_START_DAY = "start_day";
    public static final String COLUMN_START_HOUR = "start_hour";
    public static final String COLUMN_START_MINUTE = "start_minute";

    public static final String COLUMN_END_YEAR = "end_year";
    public static final String COLUMN_END_MONTH = "end_month";
    public static final String COLUMN_END_DAY = "end_day";
    public static final String COLUMN_END_HOUR = "end_hour";
    public static final String COLUMN_END_MINUTE = "end_minute";

    public static final String COLUMN_IS_ALL_DAY = "is_all_day";
    public static final String COLUMN_USER_AUTHOR = "author_id";
    public static final String COLUMN_USERS_ACCEPTED = "users_accepted";

    // Database creation SQL statement
    private static final String CREATE_TABLE = "create table "
            + TABLE_NAME
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_START_YEAR + " integer not null, "
            + COLUMN_START_MONTH + " integer not null, "
            + COLUMN_START_DAY + " integer not null, "
            + COLUMN_START_HOUR + " integer not null, "
            + COLUMN_START_MINUTE + " integer not null, "
            + COLUMN_END_YEAR + " integer not null, "
            + COLUMN_END_MONTH + " integer not null, "
            + COLUMN_END_DAY + " integer not null, "
            + COLUMN_END_HOUR + " integer not null, "
            + COLUMN_END_MINUTE + " integer not null, "
            + COLUMN_USER_AUTHOR + " integer not null, "
            + COLUMN_USERS_ACCEPTED + " text, "
            + COLUMN_IS_ALL_DAY + " integer default 0"
            + ");";

    private static final String CREATE_INDEX = "create index termin_idx ON " + TABLE_NAME + "(" + COLUMN_ID + ");";


    private UsersTable usersTable;

    public TerminsTable(SQLiteDatabase database) {
        super(database);
        usersTable = new UsersTable(database);
    }


    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE);
        database.execSQL(CREATE_INDEX);
    }

    @Override
    public boolean insert(InviteTermin entity) {
        if (entity == null) {
            return false;
        }

        for (User u : entity.getUsersAccepted()) {
            //usersTable.insert(u);
        }

        try {
            long result = mDatabase.insertWithOnConflict(TABLE_NAME, null,
                    generateContentValuesFromObject(entity), SQLiteDatabase.CONFLICT_REPLACE);
            return result != -1;
        } catch (SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean delete(InviteTermin entity) {
        if (entity == null) {
            return false;
        }
        int result = mDatabase.delete(TABLE_NAME,
                COLUMN_ID + " = " + entity.getId(), null);
        return result != 0;
    }

    @Override
    public boolean update(InviteTermin entity) {
        if (entity == null) {
            return false;
        }
        ContentValues values = generateContentValuesFromObject(entity);
        values.remove(COLUMN_ID);
        int result = mDatabase.update(TABLE_NAME,
                values, COLUMN_ID + " = "
                        + entity.getId(), null);
        return result != 0;
    }

    @Override
    public List read() {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), null,
                null, null, null, null);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor, usersTable));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    @Override
    public List read(String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), selection,
                selectionArgs, groupBy, having, orderBy);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor, usersTable));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }


    public String[] getAllColumns() {
        return new String[]{COLUMN_ID, COLUMN_START_YEAR, COLUMN_START_MONTH, COLUMN_START_DAY, COLUMN_START_HOUR, COLUMN_START_MINUTE, COLUMN_END_YEAR, COLUMN_END_MONTH, COLUMN_END_DAY, COLUMN_END_HOUR, COLUMN_END_MINUTE, COLUMN_IS_ALL_DAY, COLUMN_USER_AUTHOR, COLUMN_USERS_ACCEPTED};
    }

    public static InviteTermin generateObjectFromCursor(Cursor cursor, UsersTable usersTable) {
        if (cursor == null) {
            return null;
        }

        boolean terminAllDay = cursor.getInt(cursor.getColumnIndex(COLUMN_IS_ALL_DAY)) == 1;
        Termin termin;
        if (!terminAllDay) {
            termin = new Termin(cursor.getInt(cursor.getColumnIndex(COLUMN_START_YEAR)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_START_MONTH)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_START_DAY)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_START_HOUR)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_START_MINUTE)));

            termin.setEndTime(cursor.getInt(cursor.getColumnIndex(COLUMN_END_YEAR)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_END_MONTH)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_END_DAY)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_END_HOUR)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_END_MINUTE)));
        } else {
            termin = new AllDayTermin(cursor.getInt(cursor.getColumnIndex(COLUMN_START_YEAR)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_START_MONTH)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_START_DAY)));
        }

        InviteTermin inviteTermin = new InviteTermin(termin, new ArrayList<User>());
        inviteTermin.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));

        String[] userIds = cursor.getString(cursor.getColumnIndex(COLUMN_USERS_ACCEPTED)).split(",");
        List<User> usersList = new ArrayList<>();
        for (String uId : userIds) {
            if (!uId.isEmpty()) {
                List<User> uL = (List<User>) usersTable.read(UsersTable.COLUMN_ID + "=?", new String[]{uId}, null, null, null);
                if (!uL.isEmpty())
                    usersList.add(uL.get(0));
            }
        }

        long authorId = cursor.getLong(cursor.getColumnIndex(COLUMN_USER_AUTHOR));
        if (authorId >= 0) {
            List<User> uL = usersTable.read(UsersTable.COLUMN_ID + "=?", new String[]{String.valueOf(authorId)}, null, null, null);
            if (!uL.isEmpty()) {
                User author = uL.get(0);
                inviteTermin.setAuthor(author);
            } else {
                inviteTermin.setAuthor(new User("", ""));
            }

        }
        inviteTermin.setUsersAccepted(usersList);

        return inviteTermin;
    }

    public static ContentValues generateContentValuesFromObject(InviteTermin entity) {
        if (entity == null) {
            return null;
        }
        Termin termin = entity.getTermin();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, entity.getId());
        values.put(COLUMN_START_YEAR, termin.getYear());
        values.put(COLUMN_START_MONTH, termin.getMonth());
        values.put(COLUMN_START_DAY, termin.getDay());
        values.put(COLUMN_START_HOUR, termin.getHour());
        values.put(COLUMN_START_MINUTE, termin.getMinute());

        values.put(COLUMN_END_YEAR, termin.getEndYear());
        values.put(COLUMN_END_MONTH, termin.getEndMonth());
        values.put(COLUMN_END_DAY, termin.getEndDay());
        values.put(COLUMN_END_HOUR, termin.getEndHour());
        values.put(COLUMN_END_MINUTE, termin.getEndMinute());

        values.put(COLUMN_IS_ALL_DAY, termin.isAllDay() ? 1 : 0);

        StringBuilder userIds = new StringBuilder();
        for (User u : entity.getUsersAccepted())
            userIds.append(u.getId()).append(',');

        String uIds = userIds.toString();
        if (!uIds.isEmpty())
            values.put(COLUMN_USERS_ACCEPTED, userIds.toString().substring(0, userIds.toString().length() - 1));
        else
            values.put(COLUMN_USERS_ACCEPTED, "");

        values.put(COLUMN_USER_AUTHOR, entity.getAuthor().getId());
        return values;
    }
}
