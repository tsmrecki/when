package com.whenapp.database;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by tomislav on 21/04/15.
 */
public class EventsContentProvider extends ContentProvider {

    // database
    private CalendarDatabaseHelper database;

    // used for the UriMacher
    private static final int EVENTS = 10;
    private static final int EVENT_ID = 11;

    private static final int USERS = 20;
    private static final int USER_ID = 21;

    private static final int TERMINS = 30;
    private static final int TERMIN_ID = 31;

    private static final int INVITATIONS = 40;
    private static final int INVITATION_ID = 41;

    public static final String AUTHORITY = "com.whenapp.events.contentprovider";

    private static final String BASE_PATH = "events";
    private static final String USERS_PATH = "users";
    private static final String TERMINS_PATH = "termins";
    private static final String INVITATIONS_PATH = "invitations";


    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH);

    public static final Uri CONTENT_URI_USERS = Uri.parse("content://" + AUTHORITY
            + "/" + USERS_PATH);

    public static final Uri CONTENT_URI_TERMINS = Uri.parse("content://" + AUTHORITY
            + "/" + TERMINS_PATH);

    public static final Uri CONTENT_URI_INVITATIONS = Uri.parse("content://" + AUTHORITY
            + "/" + INVITATIONS_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/events";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/event";

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, EVENTS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", EVENT_ID);
        sURIMatcher.addURI(AUTHORITY, USERS_PATH, USERS);
        sURIMatcher.addURI(AUTHORITY, USERS_PATH + "/#", USER_ID);
        sURIMatcher.addURI(AUTHORITY,  TERMINS_PATH, TERMINS);
        sURIMatcher.addURI(AUTHORITY,  TERMINS_PATH + "/#", TERMIN_ID);
        sURIMatcher.addURI(AUTHORITY,  INVITATIONS_PATH, INVITATIONS);
        sURIMatcher.addURI(AUTHORITY,  INVITATIONS_PATH + "/#", INVITATION_ID);
    }

    EventsTable eventsTable;
    UsersTable usersTable;
    TerminsTable terminsTable;
    InvitationsTable invitationsTable;

    @Override
    public boolean onCreate() {
        database = CalendarDatabaseHelper.getInstance(getContext());
        eventsTable = new EventsTable(database.getWritableDatabase());
        usersTable = new UsersTable(database.getWritableDatabase());
        terminsTable = new TerminsTable(database.getWritableDatabase());
        invitationsTable = new InvitationsTable(database.getWritableDatabase());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        // Uisng SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // check if the caller has requested a column which does not exists
        //checkColumns(projection);

        int uriType = sURIMatcher.match(uri);

        // Set the table
        if (uriType == EVENTS || uriType == EVENT_ID)
            queryBuilder.setTables(EventsTable.TABLE_NAME);
        else if (uriType == TERMINS || uriType == TERMIN_ID)
            queryBuilder.setTables(TerminsTable.TABLE_NAME);
        else if (uriType == USERS || uriType == USER_ID)
            queryBuilder.setTables(UsersTable.TABLE_NAME);
        else if (uriType == INVITATIONS || uriType == INVITATION_ID)
            queryBuilder.setTables(InvitationsTable.TABLE_NAME);

        switch (uriType) {
            case EVENTS:
                break;
            case EVENT_ID:
                // adding the ID to the original query
                queryBuilder.appendWhere(EventsTable.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                break;
            case TERMIN_ID:
                queryBuilder.appendWhere(TerminsTable.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            case USER_ID:
                queryBuilder.appendWhere(UsersTable.COLUMN_ID + "=" + uri.getLastPathSegment());
            case INVITATION_ID:
                queryBuilder.appendWhere(InvitationsTable.COLUMN_ID + "=" + uri.getLastPathSegment());
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = database.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;
        long id = 0;
        switch (uriType) {
            case EVENTS:
                id = sqlDB.insert(EventsTable.TABLE_NAME, null, values);
                getContext().getContentResolver().notifyChange(uri, null);
                return Uri.parse(BASE_PATH + "/" + id);

               // break;
            case TERMINS:
                id = sqlDB.insert(TerminsTable.TABLE_NAME, null, values);
                return Uri.parse(TERMINS_PATH + "/" + id);

                //break;
            case USERS:
                id = sqlDB.insert(UsersTable.TABLE_NAME, null, values);
                return Uri.parse(USERS_PATH + "/" + id);

                //break;
            case INVITATIONS:
                id = sqlDB.insert(InvitationsTable.TABLE_NAME, null, values);
                return Uri.parse(INVITATIONS_PATH + "/" + id);

                //break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsDeleted = 0;
        String id;
        switch (uriType) {
            case EVENTS:
                rowsDeleted = sqlDB.delete(EventsTable.TABLE_NAME, selection,
                        selectionArgs);
                break;
            case EVENT_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(EventsTable.TABLE_NAME,
                            EventsTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(EventsTable.TABLE_NAME,
                            EventsTable.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            case USERS:
                rowsDeleted = sqlDB.delete(UsersTable.TABLE_NAME, selection,
                        selectionArgs);
                break;
            case USER_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(UsersTable.TABLE_NAME,
                            UsersTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(UsersTable.TABLE_NAME,
                            UsersTable.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            case TERMINS:
                rowsDeleted = sqlDB.delete(TerminsTable.TABLE_NAME, selection,
                        selectionArgs);
                break;
            case TERMIN_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(TerminsTable.TABLE_NAME,
                            TerminsTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(TerminsTable.TABLE_NAME,
                            TerminsTable.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            case INVITATIONS:
                rowsDeleted = sqlDB.delete(InvitationsTable.TABLE_NAME, selection,
                        selectionArgs);
                break;
            case INVITATION_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(InvitationsTable.TABLE_NAME,
                            TerminsTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(TerminsTable.TABLE_NAME,
                            TerminsTable.COLUMN_ID + "=" + id
                                    + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }


        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = database.getWritableDatabase();
        int rowsUpdated = 0;
        String id;
        switch (uriType) {
            case EVENTS:
                rowsUpdated = sqlDB.update(EventsTable.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
                break;
            case EVENT_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(EventsTable.TABLE_NAME,
                            values,
                            EventsTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(EventsTable.TABLE_NAME,
                            values,
                            EventsTable.COLUMN_ID + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            case USERS:
                rowsUpdated = sqlDB.update(UsersTable.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
                break;
            case USER_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(UsersTable.TABLE_NAME,
                            values,
                            UsersTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(UsersTable.TABLE_NAME,
                            values,
                            UsersTable.COLUMN_ID + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            case TERMINS:
                rowsUpdated = sqlDB.update(TerminsTable.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
                break;
            case TERMIN_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(TerminsTable.TABLE_NAME,
                            values,
                            TerminsTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(TerminsTable.TABLE_NAME,
                            values,
                            TerminsTable.COLUMN_ID + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            case INVITATIONS:
                rowsUpdated = sqlDB.update(InvitationsTable.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
                break;
            case INVITATION_ID:
                id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(InvitationsTable.TABLE_NAME,
                            values,
                            TerminsTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(InvitationsTable.TABLE_NAME,
                            values,
                            TerminsTable.COLUMN_ID + "=" + id
                                    + " and "
                                    + selection,
                            selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }


    private void checkColumns(String[] projection) {
        String[] available = {EventsTable.COLUMN_INVITEE,
                EventsTable.COLUMN_NAME, EventsTable.COLUMN_DESCRIPTION,
                EventsTable.COLUMN_ID};
        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
            // check if all columns which are requested are available
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }
}
