package com.whenapp.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import com.whenapp.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomislav on 21/04/15.
 */
public class UsersTable extends Table<User> {

    // Database table
    public static final String TABLE_NAME = "users";
    public static final String COLUMN_ID = "id";

    public static final String COLUMN_IMAGE = "photo";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_NAME = "name";

    // Database creation SQL statement
    private static final String CREATE_TABLE = "create table "
            + TABLE_NAME
            + "("
            + COLUMN_ID + " integer, "
            + COLUMN_IMAGE + " text, "
            + COLUMN_EMAIL + " text primary key not null, "
            + COLUMN_NAME + " text not null"
            + ");";

    private static final String CREATE_INDEX = "create index user_mail_idx ON " + TABLE_NAME + "(" + COLUMN_EMAIL + ");";
    private static final String CREATE_ID_INDEX = "create index user_id_idx ON " + TABLE_NAME + "(" + COLUMN_ID + ");";

    public UsersTable(SQLiteDatabase database) {
        super(database);
    }

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE);
        database.execSQL(CREATE_INDEX);
        database.execSQL(CREATE_ID_INDEX);
    }

    @Override
    public boolean insert(User entity) {
        if (entity == null) {
            return false;
        }
        try {
            long result = mDatabase.insertWithOnConflict(TABLE_NAME, null,
                    generateContentValuesFromObject(entity), SQLiteDatabase.CONFLICT_REPLACE);
            return result != -1;
        }catch (SQLiteConstraintException ex){
            return false;
        }
    }

    @Override
    public boolean delete(User entity) {
        if (entity == null) {
            return false;
        }
        int result = mDatabase.delete(TABLE_NAME,
                COLUMN_EMAIL + " =?", new String[]{entity.getEmail()});
        return result != 0;
    }

    @Override
    public boolean update(User entity) {
        if (entity == null) {
            return false;
        }
        ContentValues values = generateContentValuesFromObject(entity);
        if(values!=null)
        values.remove(COLUMN_EMAIL);
        int result = mDatabase.update(TABLE_NAME,
                values, COLUMN_EMAIL + " =?", new String[]{entity.getEmail()});
        return result != 0;
    }

    @Override
    public List read() {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), null,
                null, null, null, null);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    @Override
    public List read(String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), selection,
                selectionArgs, groupBy, having, orderBy);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    public String[] getAllColumns() {
        return new String[]{COLUMN_ID, COLUMN_NAME, COLUMN_EMAIL, COLUMN_IMAGE};
    }

    public static User generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        User user = new User(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)), cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE)));
        user.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
        user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL)));
        return user;
    }

    public static ContentValues generateContentValuesFromObject(User entity) {
        if (entity == null) {
            return null;
        }
        ContentValues values = new ContentValues();

        values.put(COLUMN_NAME, entity.getName());
        values.put(COLUMN_ID, entity.getId());
        values.put(COLUMN_EMAIL, entity.getEmail());
        values.put(COLUMN_IMAGE, entity.getImage());
        return values;
    }
}
