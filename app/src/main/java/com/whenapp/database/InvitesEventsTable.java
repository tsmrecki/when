package com.whenapp.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import com.whenapp.model.Event;
import com.whenapp.model.Invitation;
import com.whenapp.model.InviteTermin;
import com.whenapp.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomislav on 21/04/15.
 */
public class InvitesEventsTable extends Table<Event> {

    // Database table
    public static final String TABLE_NAME = "invites_events";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_INVITEE = "invitee";
    public static final String COLUMN_IS_ACCEPTED = "is_accepted";
    public static final String COLUMN_PLACE_NAME = "place_name";
    public static final String COLUMN_PLACE_LONGITUDE = "place_longitude";
    public static final String COLUMN_PLACE_LATITUDE = "place_latitude";
    public static final String COLUMN_IS_SINGLE_DAY = "is_single_day";
    private static final String COLUMN_TERMINS_IDS = "termin_ids";
    private static final String COLUMN_PARTICIPANTS_IDS = "participants_ids";
    private static final String COLUMN_INVITES_IDS = "invites_ids";
    private static final String COLUMN_CAN_INVITE = "can_invite";
    private static final String COLUMN_CAN_SUGGEST_TIME = "can_suggest_time";


    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_DATE = "date";

    // Database creation SQL statement
    private static final String CREATE_TABLE = "create table "
            + TABLE_NAME
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_INVITEE + " long not null, "
            + COLUMN_IS_SINGLE_DAY + " integer, "
            + COLUMN_IS_ACCEPTED + " integer default 0, "
            + COLUMN_IMAGE + " text, "
            + COLUMN_DATE + " text, "
            + COLUMN_PLACE_NAME + " text, "
            + COLUMN_PLACE_LONGITUDE + " decimal, "
            + COLUMN_PLACE_LATITUDE + " decimal, "
            + COLUMN_TERMINS_IDS + " text not null, "
            + COLUMN_PARTICIPANTS_IDS + " text not null, "
            + COLUMN_INVITES_IDS + " text not null, "
            + COLUMN_CAN_INVITE + " text not null, "
            + COLUMN_CAN_SUGGEST_TIME + " text not null, "
            + COLUMN_DESCRIPTION + " text "
            + ");";


    private static final String CREATE_INDEX = "create index invite_event_date_idx ON " + TABLE_NAME + "(" + COLUMN_DATE + ");";

    private InvitationsTable invitationsTable;
    private UsersTable usersTable;
    private TerminsTable terminsTable;

    public InvitesEventsTable(SQLiteDatabase database) {
        super(database);
        usersTable = new UsersTable(database);
        terminsTable = new TerminsTable(database);
        invitationsTable = new InvitationsTable(database);
    }

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_TABLE);
        database.execSQL(CREATE_INDEX);
    }

    @Override
    public boolean insert(Event entity) {
        if (entity == null) {
            return false;
        }

        for (User u : entity.getPeople()) {
            boolean succ = usersTable.insert(u);
            if(!succ){
                boolean s2 = usersTable.update(u);
            }
        }

        for (Invitation u : entity.getInvitations()) {
            boolean succ = invitationsTable.insert(u);
            if(!succ){
                boolean s2 = invitationsTable.update(u);
            }
        }

        for (InviteTermin i : entity.getInviteTerminList()) {
            boolean succ = terminsTable.insert(i);
            if(!succ){
                boolean s2 = terminsTable.update(i);
            }
        }

        try{

        long result = mDatabase.insertWithOnConflict(TABLE_NAME, null,
                generateContentValuesFromObject(entity), SQLiteDatabase.CONFLICT_REPLACE);
        return result != -1;

    }catch (SQLiteConstraintException ex){
        return false;
    }

    }

    @Override
    public boolean delete(Event entity) {
        if (entity == null) {
            return false;
        }
        int result = mDatabase.delete(TABLE_NAME,
                COLUMN_ID + " = " + entity.getId(), null);
        return result != 0;
    }

    @Override
    public boolean update(Event entity) {
        if (entity == null) {
            return false;
        }
        ContentValues values = generateContentValuesFromObject(entity);
        values.remove(COLUMN_ID);
        int result = mDatabase.update(TABLE_NAME,
                values, COLUMN_ID + " = "
                        + entity.getId(), null);
        return result != 0;
    }

    @Override
    public List read() {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), null,
                null, null, null, null);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor, usersTable, terminsTable, invitationsTable));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    @Override
    public List read(String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), selection,
                selectionArgs, groupBy, having, orderBy);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor, usersTable, terminsTable, invitationsTable));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    public static String[] getAllColumns() {
        return new String[]{COLUMN_ID, COLUMN_NAME, COLUMN_DESCRIPTION, COLUMN_INVITEE, COLUMN_CAN_INVITE, COLUMN_CAN_SUGGEST_TIME, COLUMN_IS_ACCEPTED, COLUMN_IS_SINGLE_DAY, COLUMN_PLACE_LATITUDE, COLUMN_PLACE_LONGITUDE, COLUMN_PLACE_NAME, COLUMN_IMAGE, COLUMN_PARTICIPANTS_IDS, COLUMN_INVITES_IDS, COLUMN_TERMINS_IDS, COLUMN_DATE};
    }

    public static Event generateObjectFromCursor(Cursor cursor, UsersTable usersTable, TerminsTable terminsTable, InvitationsTable invitationsTable) {
        if (cursor == null) {
            return null;
        }

        Event event = new Event(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
        event.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
        event.setDescription(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));
        event.setImageUrl(cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE)));
        event.setPlaceName(cursor.getString(cursor.getColumnIndex(COLUMN_PLACE_NAME)));
        event.setLongitude(cursor.getDouble(cursor.getColumnIndex(COLUMN_PLACE_LONGITUDE)));
        event.setLatitude(cursor.getDouble(cursor.getColumnIndex(COLUMN_PLACE_LATITUDE)));
        event.setSingleDay(cursor.getInt(cursor.getColumnIndex(COLUMN_IS_SINGLE_DAY)) == 1);
        event.setAccepted(cursor.getInt(cursor.getColumnIndex(COLUMN_IS_ACCEPTED)) == 1);
        event.setCanInvite(Event.CanInvite.valueOf(cursor.getString(cursor.getColumnIndex(COLUMN_CAN_INVITE))));
        event.setCanSuggestTime(Event.CanSuggestTime.valueOf(cursor.getString(cursor.getColumnIndex(COLUMN_CAN_SUGGEST_TIME))));
        String inviteeId = cursor.getString(cursor.getColumnIndex(COLUMN_INVITEE));


        List invitee = usersTable.read(UsersTable.COLUMN_ID + "=?", new String[]{inviteeId}, null, null, null);
        if (!invitee.isEmpty())
            event.setInvitee((User) invitee.get(0));

        int cursorIndex = cursor.getColumnIndex(COLUMN_PARTICIPANTS_IDS);

        List<User> usersList = new ArrayList<>();

        String[] userIds = cursor.getString(cursorIndex).split(",");

        for (String uId : userIds) {
            List value = usersTable.read(UsersTable.COLUMN_ID + "=?", new String[]{uId}, null, null, null);
            if (!value.isEmpty())
                usersList.add((User) value.get(0));
        }


        event.setParticipants(usersList);


        int invitesIdx = cursor.getColumnIndex(COLUMN_INVITES_IDS);

        List<Invitation> invitations = new ArrayList<>();

        String[] invUsersIds = cursor.getString(invitesIdx).split(",");

        for (String uId : invUsersIds) {
            List value = invitationsTable.read(UsersTable.COLUMN_ID + "=?", new String[]{uId}, null, null, null);
            if (!value.isEmpty()) {
                Invitation invitation = (Invitation) value.get(0);
                invitation.setEvent(event);
                invitations.add(invitation);
            }
        }


        event.setInvitations(invitations);



        List<InviteTermin> terminList = new ArrayList<>();
        int terminsId = cursor.getColumnIndex(COLUMN_TERMINS_IDS);

        String[] terminsIds = cursor.getString(terminsId).split(",");

        for (String tId : terminsIds) {
            List value = terminsTable.read(TerminsTable.COLUMN_ID + "=?", new String[]{tId}, null, null, null);
            if (!value.isEmpty())
                terminList.add((InviteTermin) value.get(0));
        }

        event.setInviteTerminList(terminList);

        return event;
    }

    public static ContentValues generateContentValuesFromObject(Event entity) {
        if (entity == null) {
            return null;
        }
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, entity.getName());
        values.put(COLUMN_ID, entity.getId());
        values.put(COLUMN_IMAGE, entity.getImageUrl());
        values.put(COLUMN_DESCRIPTION, entity.getDescription());
        values.put(COLUMN_INVITEE, entity.getInvitee().getId());
        values.put(COLUMN_PLACE_NAME, entity.getPlaceName());
        values.put(COLUMN_PLACE_LONGITUDE, entity.getLongitude());
        values.put(COLUMN_PLACE_LATITUDE, entity.getLatitude());
        values.put(COLUMN_DATE, entity.getDate());
        values.put(COLUMN_IS_SINGLE_DAY, entity.isSingleDay() ? 1 : 0);
        values.put(COLUMN_IS_ACCEPTED, entity.isAccepted() ? 1 : 0);
        values.put(COLUMN_CAN_INVITE, entity.getCanInvite().toString());
        values.put(COLUMN_CAN_SUGGEST_TIME, entity.getCanSuggestTime().toString());
        StringBuilder userIds = new StringBuilder();
        for (User u : entity.getParticipants()) {
            userIds.append(u.getId()).append(',');
        }
        String uIds = userIds.toString();

        if (!uIds.isEmpty())
            values.put(COLUMN_PARTICIPANTS_IDS, uIds.substring(0, uIds.length() - 1));
        else
            values.put(COLUMN_PARTICIPANTS_IDS, "");


        StringBuilder invitedUsersIds = new StringBuilder();
        for (Invitation u : entity.getInvitations()) {
            invitedUsersIds.append(u.getId()).append(',');
        }
        String invUserIds = invitedUsersIds.toString();

        if (!invUserIds.isEmpty())
            values.put(COLUMN_INVITES_IDS, invUserIds.substring(0, invUserIds.length() - 1));
        else
            values.put(COLUMN_INVITES_IDS, "");



        StringBuilder terminIds = new StringBuilder();
        for (InviteTermin i : entity.getInviteTerminList()) {
            terminIds.append(i.getId()).append(',');
        }
        String tIds = terminIds.toString();
        if (!tIds.isEmpty())
            values.put(COLUMN_TERMINS_IDS, tIds.substring(0, tIds.length() - 1));
        else
            values.put(COLUMN_TERMINS_IDS, "");

        return values;
    }
}
