package com.whenapp.database;

import android.content.Context;

import com.whenapp.model.Invitation;

import java.util.List;

/**
 * Created by tomislav on 22/04/15.
 */
public class MyInvitationsDataLoader extends AbstractDataLoader<List<Invitation>> {
    private Table<Invitation> mDataSource;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mGroupBy;
    private String mHaving;
    private String mOrderBy;

    public MyInvitationsDataLoader(Context context, Table dataSource, String selection, String[] selectionArgs,
                                   String groupBy, String having, String orderBy) {
        super(context);
        mDataSource = dataSource;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mGroupBy = groupBy;
        mHaving = having;
        mOrderBy = orderBy;
    }

    @Override
    protected List<Invitation> buildList() {
        List<Invitation> testList = mDataSource.read(mSelection, mSelectionArgs, mGroupBy, mHaving,
                mOrderBy);
        return testList;
    }

    public void insert(Invitation entity) {
        new InsertTask(this).execute(entity);
    }

    public void insert(List<Invitation> events){

        new InsertMultiTask(this).execute(events);
    }

    public void update(Invitation entity) {
        new UpdateTask(this).execute(entity);
    }

    public void delete(Invitation entity) {
        new DeleteTask(this).execute(entity);
    }

    public void deleteAll() {
        mDataSource.mDatabase.delete(MyInvitationsTable.TABLE_NAME, null, null);
    }


    private class InsertTask extends ContentAdapterTask<Invitation, Void, Void> {
        InsertTask(MyInvitationsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(Invitation... params) {
            for(Invitation e : params) {
                boolean succ = mDataSource.insert(e);
                if(!succ){
                    boolean s2 = mDataSource.update(e);
                }
            }
            return (null);
        }
    }

    private class InsertMultiTask extends ContentAdapterTask<List<Invitation>, Void, Void> {
        InsertMultiTask(MyInvitationsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(List<Invitation>... params) {
            List<Invitation> list = params[0];
            mDataSource.mDatabase.beginTransaction();
            for(Invitation e : list){
                boolean succ = mDataSource.insert(e);
                if(!succ){
                    boolean s2 = mDataSource.update(e);
                }
            }
            mDataSource.mDatabase.setTransactionSuccessful();
            mDataSource.mDatabase.endTransaction();
            return (null);
        }
    }

    private class UpdateTask extends ContentAdapterTask<Invitation, Void, Void> {
        UpdateTask(MyInvitationsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(Invitation... params) {
            mDataSource.update(params[0]);
            return (null);
        }
    }

    private class DeleteTask extends ContentAdapterTask<Invitation, Void, Void> {
        DeleteTask(MyInvitationsDataLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(Invitation... params) {
            mDataSource.delete(params[0]);
            return (null);
        }
    }
}