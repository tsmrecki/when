package com.whenapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by tomislav on 21/04/15.
 */
public class CalendarDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "when_calendar.db";
    private static final int DATABASE_VERSION = 24;

    private static CalendarDatabaseHelper instance = null;

    public static CalendarDatabaseHelper getInstance(Context context) {
        if(instance == null) instance = new CalendarDatabaseHelper(context);
        return instance;
    }

    private CalendarDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    public void deleteDatabase() {
        instance.getWritableDatabase().delete(EventsTable.TABLE_NAME, null, null);
        instance.getWritableDatabase().delete(TerminsTable.TABLE_NAME, null, null);
        instance.getWritableDatabase().delete(UsersTable.TABLE_NAME, null, null);
        instance.getWritableDatabase().delete(InvitationsTable.TABLE_NAME, null, null);
        instance.getWritableDatabase().delete(UsersGroupsTable.TABLE_NAME, null, null);
        instance.getWritableDatabase().delete(FriendsTable.TABLE_NAME, null, null);
        instance.getWritableDatabase().delete(MyInvitationsTable.TABLE_NAME, null, null);
        instance.getWritableDatabase().delete(InvitesEventsTable.TABLE_NAME, null, null);
        instance.getWritableDatabase().delete(FacebookContactsTable.TABLE_NAME, null, null);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        EventsTable.onCreate(db);
        TerminsTable.onCreate(db);
        UsersTable.onCreate(db);
        InvitationsTable.onCreate(db);
        UsersGroupsTable.onCreate(db);
        FriendsTable.onCreate(db);
        MyInvitationsTable.onCreate(db);
        InvitesEventsTable.onCreate(db);
        FacebookContactsTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + EventsTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + UsersTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TerminsTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + InvitationsTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + UsersGroupsTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + FriendsTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + MyInvitationsTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + InvitesEventsTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + FacebookContactsTable.TABLE_NAME);

        onCreate(db);
    }
}
