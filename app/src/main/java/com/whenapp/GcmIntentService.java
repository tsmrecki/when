package com.whenapp;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.whenapp.connection.ApiHelper;
import com.whenapp.model.Push;
import com.whenapp.model.SessionManager;

/**
 * Created by tomislav on 02/05/15.
 */
public class GcmIntentService extends IntentService {
    private static final String TAG = "GcmIntentService";

    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("", "Send error: " + extras.toString(), null, null, -1, -1);
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("", "Deleted messages on server: " +
                        extras.toString(), null, null, -1, -1);
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                String message = extras.getString("message", "Something exciting is happening.");
                String title = extras.getString("title", "When");
                String image = extras.getString("image", null);
                String pushType = extras.getString("type", null);

                Push.Type type = null;
                if(pushType != null){
                    if(pushType.equals("OnlyInvitationWaiting") || pushType.equals("InvitationReceived"))
                        type = Push.Type.INVITATION;
                    else if(pushType.equals("EventTimeDecided") || pushType.equals("BestTimeSuggestionChanged") || pushType.equals("EventTimeSuggestionAdded"))
                        type = Push.Type.EVENT;
                }

                long id = -1;
                long eventId = -1;

                if (pushType != null && pushType.equals("InvitationReceived")) {
                    SessionManager s = SessionManager.getInstance(this);
                    s.setNumOfInvitations(s.getNumOfInvitations()+1);
                }


                if (type != null) {
                    if (extras.containsKey("invitation_id") && extras.containsKey("event_id")) {
                        String idS = extras.getString("invitation_id", null);
                        if (idS == null) {
                            id = extras.getLong("invitation_id", -1);
                        } else {
                            id = Long.parseLong(idS);
                        }

                        String eventIds = extras.getString("event_id", null);
                        if (eventIds == null) {
                            eventId = extras.getLong("invitation_id", -1);
                        } else {
                            eventId = Long.parseLong(eventIds);
                        }

                        Intent mServiceIntent = new Intent(this, RefreshService.class);
                        mServiceIntent.putExtra(RefreshService.REFRESH_TYPE, RefreshService.RefreshType.invitation.toString());
                        mServiceIntent.putExtra("invitationId", id);
                        mServiceIntent.putExtra("eventId", eventId);
                        startService(mServiceIntent);
                    }
                    else if(extras.containsKey("event_id"))
                    {
                        String idS = extras.getString("event_id", null);
                        if (idS == null) {
                            id = extras.getLong("event_id", -1);
                        } else {
                            id = Long.parseLong(idS);
                        }

                        Intent mServiceIntent = new Intent(this, RefreshService.class);
                        mServiceIntent.putExtra(RefreshService.REFRESH_TYPE, RefreshService.RefreshType.event.toString());
                        mServiceIntent.putExtra(RefreshService.SUBTYPE, pushType);
                        mServiceIntent.putExtra("eventId", id);
                        startService(mServiceIntent);
                    }
                }

                sendNotification(title, message, image, type, id, eventId);
                Log.i(TAG, "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String title, String msg, String image, Push.Type type, long id, long eventId) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent;

        if(type == null) {
            contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, MainActivity.class), 0);
        }else if (type == Push.Type.INVITATION) {
            Intent data = new Intent(this, EventActivity.class);
            Bundle b = new Bundle();
            b.putLong(EventActivity.INVITATION_ID, id);
            b.putLong(EventActivity.EVENT_ID, eventId);
            b.putBoolean(EventActivity.OPENED_FROM_INVITES, true);
            data.putExtras(b);
            contentIntent = PendingIntent.getActivity(this, ((Long) eventId).intValue(), data, PendingIntent.FLAG_CANCEL_CURRENT);
        } else {
            Intent data = new Intent(this, EventActivity.class);
            Bundle b = new Bundle();
            b.putLong(EventActivity.EVENT_ID, id);
            data.putExtras(b);

            contentIntent = PendingIntent.getActivity(this, ((Long) eventId).intValue(), data, PendingIntent.FLAG_CANCEL_CURRENT);
        }

        Bitmap largeIcon;
        if(image != null) {
            ImageSize imageSize = new ImageSize(android.R.dimen.notification_large_icon_width, android.R.dimen.notification_large_icon_height);
            largeIcon = ImageLoader.getInstance().loadImageSync(ApiHelper.BASE_PATH + image, imageSize, new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).build());
        }
        else {
                largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        }

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_stat_notify)
                        .setLargeIcon(largeIcon)
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setAutoCancel(true)
                        .setContentText(msg);

        Intent notify = new Intent(MainActivity.REFRESH);
        sendBroadcast(notify);

        if(image != null) mBuilder.setColor(getResources().getColor(R.color.prominent_green));
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);
        mBuilder.setLights(getResources().getColor(R.color.prominent_green), 1000, 800);
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(((Long) eventId).intValue(), mBuilder.build());
    }
}
