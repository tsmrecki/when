package com.whenapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.Tracker;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;

/**
 * Created by tomislav on 22/10/15.
 */
public class BaseActivity extends AppCompatActivity {

    protected SimpleFacebook facebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
        facebook = SimpleFacebook.getInstance(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            facebook.onActivityResult(requestCode, resultCode, data);
        } catch (Exception ex) {
            Toast t = Toast.makeText(this, "There's a rare problem using Facebook login on your phone, please try to login via e-mail.", Toast.LENGTH_LONG);
            t.show();
            Crashlytics.logException(ex);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(this);
    }


    public Tracker getGATracker() {
        return ((App)getApplication()).getGATracker();
    }
}
