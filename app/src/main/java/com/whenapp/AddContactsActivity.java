package com.whenapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.facebook.appevents.AppEventsLogger;
import com.whenapp.fragments.ContactsFragment;
import com.whenapp.model.ContactToken;
import com.whenapp.model.User;
import com.whenapp.model.UsersGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class AddContactsActivity extends BaseActivity {


    ContactsFragment contactsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contacts);

        setResult(RESULT_CANCELED);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final List<User> users = new ArrayList<>();
        final List<UsersGroup> usersGroups = new ArrayList<>();


        if (savedInstanceState == null) {
            contactsFragment = ContactsFragment.newInstance(users, usersGroups, new ArrayList<ContactToken>());
            contactsFragment.setHasOptionsMenu(false);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, contactsFragment)
                    .commit();
            contactsFragment.setHasOptionsMenu(false);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == android.R.id.home){
            finish();
            return true;
        }else if(id == R.id.action_done){
            finishWithResult();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishWithResult();
    }

    private void finishWithResult(){
        Intent result = new Intent();

        ContactsFragment contactsFragment = (ContactsFragment) getSupportFragmentManager().getFragments().get(0);

        List<ContactToken> ctList = contactsFragment.getSelectedContacts();

        List<User> usersList = new ArrayList<>();

        for (ContactToken token : ctList) {
            usersList.addAll(token.getUsers());
        }

        result.putParcelableArrayListExtra("contacts", (ArrayList<? extends Parcelable>) usersList);

        if (getParent() == null) {
            setResult(Activity.RESULT_OK, result);
        } else {
            getParent().setResult(Activity.RESULT_OK, result);
        }
        supportFinishAfterTransition();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
