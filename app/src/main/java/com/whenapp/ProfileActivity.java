package com.whenapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.whenapp.common.ImageChooser;
import com.whenapp.common.ImageCoder;
import com.whenapp.connection.ApiHelper;
import com.whenapp.fragments.ProfileEventsStatsFragment;
import com.whenapp.model.SessionManager;
import com.whenapp.model.User;

import org.json.JSONException;

import java.util.List;
import java.util.concurrent.ExecutionException;


public class ProfileActivity extends BaseActivity {

    User user;
    ImageChooser imageChooser;
    ImageView profileImage;
    SessionManager sessionManager;
    ProgressBar imageLoader;
    View tab;
    ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        sessionManager = SessionManager.getInstance(this);
        User me = sessionManager.getUser();

        if (getIntent().hasExtra("user")) {
            user = getIntent().getParcelableExtra("user");
        } else if (getIntent().hasExtra("user_id")) {
            //API
        } else {
            user = me;
        }

        if(savedInstanceState == null)
            imageChooser = new ImageChooser(this, 200);
        else
            imageChooser = new ImageChooser(this, savedInstanceState);


        imageLoader = (ProgressBar) findViewById(R.id.image_loader);
        tab = findViewById(R.id.tab);
        scrollView = (ScrollView) findViewById(R.id.scroll_view);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setupToolbar(toolbar);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            setEnterSharedElementCallback(new android.app.SharedElementCallback() {
                @Override
                public void onSharedElementEnd(List<String> sharedElementNames, List<View> sharedElements, List<View> sharedElementSnapshots) {
                    super.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots);
                    getSupportActionBar().show();
                }

                @Override
                public void onRejectSharedElements(List<View> rejectedSharedElements) {
                    super.onRejectSharedElements(rejectedSharedElements);
                    getSupportActionBar().show();
                }

                @Override
                public void onSharedElementStart(List<String> sharedElementNames, List<View> sharedElements, List<View> sharedElementSnapshots) {
                    getSupportActionBar().hide();
                    super.onSharedElementStart(sharedElementNames, sharedElements, sharedElementSnapshots);
                }
            });
        } else {
            getSupportActionBar().show();
        }


        TextView name = (TextView) findViewById(R.id.user_name);
        name.setText(Html.fromHtml("<b>" + user.getName() + "</b><br>" + user.getEmail()));


       profileImage = (ImageView) findViewById(R.id.user_image);
        ImageLoader.getInstance().displayImage(user.getImage(), profileImage);


        if (user != null && user.equals(me)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.stats_fragment, ProfileEventsStatsFragment.newInstance(user), "user_stats").commit();

        } else {
            tab.setVisibility(View.GONE);
            scrollView.setVisibility(View.GONE);
            ViewGroup.LayoutParams lp = profileImage.getLayoutParams();
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            profileImage.setLayoutParams(lp);
            profileImage.requestLayout();
            ViewGroup.LayoutParams nameP = name.getLayoutParams();
            nameP.height = ViewGroup.LayoutParams.MATCH_PARENT;
            name.setLayoutParams(nameP);
            name.requestLayout();
        }

    }

    private void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        User me = sessionManager.getUser();
        if(user.equals(me))
        getMenuInflater().inflate(R.menu.menu_my_profile, menu);
        else
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }else if(id == R.id.action_image){
            startActivityForResult(imageChooser.buildChooseImageIntent(), 200);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 200) {
            if (resultCode == RESULT_OK) {
                final Uri imageUri = imageChooser.onActivityResult(requestCode, resultCode, data);

                imageLoader.setVisibility(View.VISIBLE);
                try {
                    String base64 = new ImageCoder.EncodeAsync(imageUri, this).execute().get();
                    try {
                        ApiHelper.getInstance(this).changeProfileImage(sessionManager.getUser().getId(), base64, new ApiHelper.OnChangeImageListener() {
                            @Override
                            public void onImageChanged(User me) {
                                sessionManager.setUser(me);
                                ImageLoader.getInstance().displayImage(me.getImage(), profileImage);
                                imageLoader.setVisibility(View.GONE);
                            }

                            @Override
                            public void onFail(String errorMessage) {
                                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                imageLoader.setVisibility(View.GONE);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        imageChooser.saveInstance(outState);
    }
}
