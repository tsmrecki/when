package com.whenapp.layer;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.layer.sdk.LayerClient;
import com.layer.sdk.changes.LayerChange;
import com.layer.sdk.changes.LayerChangeEvent;
import com.layer.sdk.exceptions.LayerException;
import com.layer.sdk.listeners.LayerChangeEventListener;
import com.layer.sdk.listeners.LayerSyncListener;
import com.layer.sdk.listeners.LayerTypingIndicatorListener;
import com.layer.sdk.messaging.Conversation;
import com.layer.sdk.messaging.LayerObject;
import com.layer.sdk.messaging.Message;
import com.layer.sdk.messaging.MessageOptions;
import com.layer.sdk.messaging.MessagePart;
import com.layer.sdk.messaging.Metadata;
import com.layer.sdk.query.Predicate;
import com.layer.sdk.query.Query;
import com.layer.sdk.query.SortDescriptor;
import com.whenapp.R;
import com.whenapp.adapters.ChatAdapter;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.EventsDataLoader;
import com.whenapp.database.EventsTable;
import com.whenapp.database.InvitesEventsDataLoader;
import com.whenapp.database.InvitesEventsTable;
import com.whenapp.model.ChatMessage;
import com.whenapp.model.Event;
import com.whenapp.model.SessionManager;
import com.whenapp.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


/**
 * Handles the conversation between the pre-defined participants (Device, Emulator) and displays
 * messages in the GUI.
 */
public class ConversationViewController implements View.OnClickListener, LayerChangeEventListener
        , TextWatcher, LayerTypingIndicatorListener, LayerSyncListener, LoaderManager.LoaderCallbacks<List<Event>> {

    private static final String TAG = ConversationViewController.class.getSimpleName();

    private LayerClient layerClient;

    //GUI elements
    private ImageButton sendButton;
    private LinearLayout topBar;
    private EditText userInput;
    //private ScrollView conversationScroll;
    private RecyclerView conversationRV;
    private ChatAdapter chatAdapter;
    private TextView typingIndicator;

    private List<User> participants;

    //List of all users currently typing
    private ArrayList<User> typingUsers;

    //Current conversation
    private Conversation activeConversation;

    //All messages
   private List<String> allMessages;

    private ChatActivity chatActivity;

    String eventId = "0";

    Uri conversationId = null;

    EventsDataLoader eventsDataLoader;
    InvitesEventsDataLoader invitesEventsDataLoader;

    private User currentUser;

    public ConversationViewController(final ChatActivity ma, List<User> participants, String chatName, Event event, LayerClient client) {
        this.participants = participants;

        //Cache off LayerClient
        layerClient = client;

        currentUser = SessionManager.getInstance(ma).getUser();

        this.chatActivity = ma;

        if(ma.getIntent().hasExtra("layer-conversation-id"))
            conversationId = ma.getIntent().getParcelableExtra("layer-conversation-id");

        //List of users that are typing which is used with LayerTypingIndicatorListener
        typingUsers = new ArrayList<>();

        //Change the layout
        ma.setContentView(R.layout.activity_chat);

        Toolbar toolbar = (Toolbar) ma.findViewById(R.id.toolbar);
        ma.setSupportActionBar(toolbar);
        ma.setToolbar();
        ma.getSupportActionBar().setTitle(chatName);

        //Cache off gui objects
        sendButton = (ImageButton) ma.findViewById(R.id.send);
        topBar = (LinearLayout) ma.findViewById(R.id.topbar);
        userInput = (EditText) ma.findViewById(R.id.input);
        //conversationScroll = (ScrollView) ma.findViewById(R.id.scrollView);
        conversationRV = (RecyclerView) ma.findViewById(R.id.conversation_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ma){
            @Override
            public int getPaddingBottom() {
                return (int) (32 * ma.getResources().getDisplayMetrics().density);
            }
        };

        layoutManager.setStackFromEnd(true);

        conversationRV.setLayoutManager(layoutManager);
        conversationRV.setItemAnimator(new DefaultItemAnimator());
        chatAdapter = new ChatAdapter();
        conversationRV.setAdapter(chatAdapter);

        typingIndicator = (TextView) ma.findViewById(R.id.typingIndicator);

        //Capture user input
        sendButton.setOnClickListener(this);
        topBar.setOnClickListener(this);
        userInput.addTextChangedListener(this);

        ma.setToolbar();


        //If there is an active conversation, cache it
        activeConversation = getConversation(conversationId);

        System.out.println("event je "+ eventId + " evet " + event);

        if(activeConversation != null){
            if(event != null){
                setEventIdMetadata(String.valueOf(event.getId()), event.getName());
            }

            if(this.participants.isEmpty()){
                for(String p : activeConversation.getParticipants()){
                    this.participants.add(new User(p, p));
                }
            }
        }

        //When conversations/messages change, capture them
        layerClient.registerEventListener(this);

        System.out.println("preload event id je " + eventId + " evet " + event);

        eventsDataLoader = (EventsDataLoader) ma.getSupportLoaderManager().initLoader(10, null, this);
        invitesEventsDataLoader = (InvitesEventsDataLoader) ma.getSupportLoaderManager().initLoader(11, null, this);

        //If there is an active conversation, draw it
        drawConversation();

        if (activeConversation != null)
            getTopBarMetaData();
    }

    //Create a new message and send it
    private void sendButtonClicked() {
        if(userInput.length() == 0) return;

        //Check to see if there is an active conversation between the pre-defined participants
        if (activeConversation == null) {
            activeConversation = getConversation(conversationId);

            //If there isn't, create a new conversation with those participants
            if (activeConversation == null) {
                activeConversation = layerClient.newConversation(ChatActivity.getAllParticipants(participants));
            }
        }

        sendMessage(userInput.getText().toString());

        //Clears the text input field
        userInput.setText("");
    }

    private void sendMessage(String text) {

        //Put the user's text into a message part, which has a MIME type of "text/plain" by default
        MessagePart messagePart = layerClient.newMessagePart(text);

        //Formats the push notification that the other participants will receive
        MessageOptions options = new MessageOptions();
        options.pushNotificationMessage(getUserById(ChatActivity.getUserID(chatActivity)).getName() + ": " + text);

        //Creates and returns a new message object with the given conversation and array of
        // message parts
        Message message = layerClient.newMessage(options, Arrays.asList(messagePart));

        //Sends the message
        if (activeConversation != null)
            activeConversation.send(message);

        conversationRV.smoothScrollToPosition(chatAdapter.getItemCount());

        /*
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.message = message;
        chatMessage.messageStatusResource = MessageView.getStatusImage(message, participants, chatActivity);
        chatMessage.isSent = true;
        chatMessage.sender = currentUser;
        chatAdapter.add(chatMessage);
        */
    }

    private User getUserById(String userId) {
        for(User u : participants)
            if(u.getId() == Long.parseLong(userId)) return u;

        return new User(userId, "");
    }

    //Create a random color and apply it to the Layer logo bar
    private void topBarClicked() {

        Random r = new Random();
        float red = r.nextFloat();
        float green = r.nextFloat();
        float blue = r.nextFloat();

        setTopBarMetaData(red, green, blue);
        setTopBarColor(red, green, blue);
    }

    //Checks to see if there is already a conversation between the device and emulator
    private Conversation getConversation(Uri conversationId) {

        if (activeConversation == null && conversationId == null) {
            Query query = Query.builder(Conversation.class)
                    .predicate(new Predicate(Conversation.Property.PARTICIPANTS, Predicate
                            .Operator.EQUAL_TO, ChatActivity.getAllParticipants(participants)))
                    .sortDescriptor(new SortDescriptor(Conversation.Property.CREATED_AT,
                            SortDescriptor.Order.DESCENDING)).build();

            List<Conversation> results = layerClient.executeQuery(query, Query.ResultType.OBJECTS);
            if (results != null && results.size() > 0) {
                Conversation conversation = results.get(0);
                if(conversation.getMetadata().containsKey("event")){
                    Metadata event = (Metadata) conversation.getMetadata().get("event");
                    eventId = (String) event.get("id");
                }
                Log.d("returning conversation ", results.get(0).getParticipants().toString() + ", eventid " + eventId);
                this.conversationId = results.get(0).getId();
                return results.get(0);
            }
        }

        if (activeConversation == null && conversationId != null) {

            Query query = Query.builder(Conversation.class)
                    .predicate(new Predicate(Conversation.Property.ID, Predicate
                            .Operator.EQUAL_TO, conversationId.toString()))
                    .sortDescriptor(new SortDescriptor(Conversation.Property.CREATED_AT,
                            SortDescriptor.Order.DESCENDING)).build();

            List<Conversation> results = layerClient.executeQuery(query, Query.ResultType.OBJECTS);
            if (results != null && results.size() > 0) {
                Conversation conversation = results.get(0);
                if(conversation.getMetadata().containsKey("event")){
                    Metadata event = (Metadata) conversation.getMetadata().get("event");
                    eventId = (String) event.get("id");
                }
                Log.d("returning conversatio o", results.get(0).getParticipants().toString() + ", eventid " + eventId);

                return results.get(0);
            }
        }

        Log.d("returning conversation ", "nullla");

        //Returns the active conversation (which is null by default)
        return activeConversation;
    }

    //Redraws the conversation window in the GUI
    private void drawConversation() {

        //Only proceed if there is a valid conversation
        if (activeConversation != null) {

            //Clear the GUI first and empty the list of stored messages

            chatAdapter.clear();
            allMessages = new ArrayList<>();

            //Grab all the messages from the conversation and add them to the GUI
            List<Message> allMsgs = layerClient.getMessages(activeConversation);
            for (int i = 0; i < allMsgs.size(); i++) {
                addMessageToView(allMsgs.get(i));
            }

            conversationRV.smoothScrollToPosition(chatAdapter.getItemCount());


        }
    }

    //Creates a GUI element (header and body) for each Message
    private void addMessageToView(Message msg) {

        //Make sure the message is valid
        if (msg == null || msg.getSender() == null || msg.getSender().getUserId() == null)
            return;

        //Once the message has been displayed, we mark it as read
        //NOTE: the sender of a message CANNOT mark their own message as read
        if (!msg.getSender().getUserId().equalsIgnoreCase(layerClient.getAuthenticatedUserId()))
            msg.markAsRead();

        //Grab the message id
        String msgId = msg.getId().toString();

        //If we have already added this message to the GUI, skip it
        if (!allMessages.contains(msgId)) {
            ChatMessage message = new ChatMessage();
            message.message = msg;
            message.sender = getUserById(msg.getSender().getUserId());
            message.isSent = Long.parseLong(msg.getSender().getUserId()) == currentUser.getId();
            message.messageStatusResource = MessageView.getStatusImage(msg, participants, chatActivity);

            chatAdapter.add(message);
            //Build the GUI element and save it
            //MessageView msgView = new MessageView(conversationView, msg, participants, Long.parseLong(msg.getSender().getUserId()) == currentUser.getId());
           allMessages.add(msgId);
       }
    }


    private void setEventIdMetadata(String eventId, String chatName) {
        if (activeConversation != null) {

            System.out.println("setting event metadaa");

            Metadata metadata = Metadata.newInstance();

            Metadata event = Metadata.newInstance();
            event.put("id", eventId);
            event.put("name", chatName);

            metadata.put("event", event);

            //Merge this new information with the existing metadata (passing in false will replace
            // the existing Map, passing in true ensures existing key/values are preserved)
            activeConversation.putMetadata(metadata, true);
        }
    }


    //Stores RGB values in the conversation's metadata
    private void setTopBarMetaData(float red, float green, float blue) {
        if (activeConversation != null) {

            Metadata metadata = Metadata.newInstance();

            Metadata colors = Metadata.newInstance();
            colors.put("red", Float.toString(red));
            colors.put("green", Float.toString(green));
            colors.put("blue", Float.toString(blue));

            metadata.put("backgroundColor", colors);

            //Merge this new information with the existing metadata (passing in false will replace
            // the existing Map, passing in true ensures existing key/values are preserved)
            activeConversation.putMetadata(metadata, true);
        }
    }

    //Check the conversation's metadata for RGB values
    private void getTopBarMetaData() {
        if (activeConversation != null) {

            Metadata current = activeConversation.getMetadata();
            if (current.containsKey("backgroundColor")) {

                Metadata colors = (Metadata) current.get("backgroundColor");

                if (colors != null) {

                    float red = Float.parseFloat((String) colors.get("red"));
                    float green = Float.parseFloat((String) colors.get("green"));
                    float blue = Float.parseFloat((String) colors.get("blue"));

                    setTopBarColor(red, green, blue);
                }
            }
        }
    }

    //Takes RGB values and sets the top bar color
    private void setTopBarColor(float red, float green, float blue) {
        if (topBar != null) {
            topBar.setBackgroundColor(Color.argb(255, (int) (255.0f * red), (int) (255.0f *
                    green), (int) (255.0f * blue)));
        }
    }


    //================================================================================
    // View.OnClickListener methods
    //================================================================================

    public void onClick(View v) {
        //When the "send" button is clicked, grab the ongoing conversation (or create it) and
        // send the message
        if (v == sendButton) {
            sendButtonClicked();
        }

        //When the Layer logo bar is clicked, randomly change the color and store it in the
        // conversation's metadata
        if (v == topBar) {
            topBarClicked();
        }
    }


    //================================================================================
    // TextWatcher methods
    //================================================================================

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    //After the user has changed some text, we notify other participants that they are typing
        if (activeConversation != null)
            activeConversation.send(TypingIndicator.STARTED);
    }

    public void afterTextChanged(Editable s) {

    }

    //================================================================================
    // LayerTypingIndicatorListener methods
    //================================================================================

    @Override
    public void onTypingIndicator(LayerClient layerClient, Conversation conversation, String
            userID, TypingIndicator indicator) {

        //Only show the typing indicator for the active (displayed) converation
        if (conversation != activeConversation)
            return;

        switch (indicator) {
            case STARTED:
                // This user started typing, so add them to the typing list if they are not
                // already on it.
                if (!typingUsers.contains(getUserById(userID)))
                    typingUsers.add(getUserById(userID));
                break;

            case FINISHED:
                // This user isn't typing anymore, so remove them from the list.
                typingUsers.remove(getUserById(userID));
                break;
        }

        //Format the text to display in the conversation view
        if (typingUsers.size() == 0) {

            //No one is typing, so clear the text
            typingIndicator.setVisibility(View.GONE);
            typingIndicator.setText("");

        } else if (typingUsers.size() == 1) {

            //Name the one user that is typing (and make sure the text is grammatically correct)
            typingIndicator.setText(typingUsers.get(0).getName() + " is typing");
            typingIndicator.setVisibility(View.VISIBLE);

        } else if (typingUsers.size() > 1) {

            //Name all the users that are typing (and make sure the text is grammatically correct)
            String users = "";
            for (int i = 0; i < typingUsers.size(); i++) {
                users += typingUsers.get(i).getName();
                if (i < typingUsers.size() - 1)
                    users += ", ";
            }

            typingIndicator.setText(users + " are typing");
            typingIndicator.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onBeforeSync(LayerClient layerClient, SyncType syncType) {
        Log.v(TAG, "Sync starting");
    }

    @Override
    public void onSyncProgress(LayerClient layerClient, SyncType syncType, int i) {
        Log.v(TAG, "Sync is "  + i + "% Complete");

    }

    @Override
    public void onAfterSync(LayerClient layerClient, SyncType syncType) {
        Log.v(TAG, "Sync complete");

    }

    //Captures any errors with syncing
    public void onSyncError(LayerClient layerClient, List<LayerException> layerExceptions) {
        for(LayerException e : layerExceptions){
            Log.v(TAG, "onSyncError: " + e.toString());
        }
    }



    //================================================================================
    // LayerChangeEventListener methods
    //================================================================================

    @Override
    public void onChangeEvent(LayerChangeEvent layerChangeEvent) {
        //You can choose to handle changes to conversations or messages however you'd like:
        List<LayerChange> changes = layerChangeEvent.getChanges();
        for (int i = 0; i < changes.size(); i++) {
            LayerChange change = changes.get(i);
            if (change.getObjectType() == LayerObject.Type.CONVERSATION) {

                Conversation conversation = (Conversation) change.getObject();
                Log.v(TAG, "Conversation " + conversation.getId() + " attribute " +
                        change.getAttributeName() + " was changed from " + change.getOldValue() +
                        " to " + change.getNewValue());

                switch (change.getChangeType()) {
                    case INSERT:
                        break;

                    case UPDATE:
                        break;

                    case DELETE:
                        break;
                }

            } else if (change.getObjectType() == LayerObject.Type.MESSAGE) {

                Message message = (Message) change.getObject();
                Log.v(TAG, "Message " + message.getId() + " attribute " + change
                        .getAttributeName() + " was changed from " + change.getOldValue() + " to " +
                        "" + change.getNewValue());

                switch (change.getChangeType()) {
                    case INSERT:
                        break;

                    case UPDATE:
                        break;

                    case DELETE:
                        break;
                }
            }
        }

        //If we don't have an active conversation, grab the oldest one
        if (activeConversation == null)
            activeConversation = getConversation(conversationId);


        //If anything in the conversation changes, re-draw it in the GUI
        drawConversation();

        //Check the meta-data for color changes
        getTopBarMetaData();
    }

    @Override
    public Loader<List<Event>> onCreateLoader(int id, Bundle args) {
        if(id == 10){
            return new EventsDataLoader(chatActivity, new EventsTable(CalendarDatabaseHelper.getInstance(chatActivity).getReadableDatabase()), EventsTable.COLUMN_ID + "=?", new String[]{eventId}, null, null, null);
        }else
            return new InvitesEventsDataLoader(chatActivity, new InvitesEventsTable(CalendarDatabaseHelper.getInstance(chatActivity).getReadableDatabase()), InvitesEventsTable.COLUMN_ID + "=?", new String[]{eventId}, null, null, null);
        }

    @Override
    public void onLoadFinished(Loader<List<Event>> loader, List<Event> data) {
        System.out.println("load fifnis " + data);

        if(!data.isEmpty()){
            Event event = data.get(0);
            participants = event.getPeople();
            if(chatActivity != null && chatActivity.getSupportActionBar() != null) {
                chatActivity.setToolbar();
                Toolbar toolbar = (Toolbar) chatActivity.findViewById(R.id.toolbar);
                chatActivity.setSupportActionBar(toolbar);
                chatActivity.getSupportActionBar().setTitle(event.getName());
            }
            drawConversation();
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Event>> loader) {

    }


}