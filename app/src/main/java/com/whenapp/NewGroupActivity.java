package com.whenapp;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.whenapp.adapters.UsersAdapter;
import com.whenapp.connection.ApiHelper;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.FriendsDataLoader;
import com.whenapp.database.FriendsTable;
import com.whenapp.model.ContactToken;
import com.whenapp.model.SessionManager;
import com.whenapp.model.User;
import com.whenapp.model.UserContactToken;
import com.whenapp.model.UsersGroup;
import com.whenapp.views.PersonCompletionView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


public class NewGroupActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<List<User>> {

    UsersAdapter usersAdapter;
    PersonCompletionView completionView;

    EditText name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);

        getSupportLoaderManager().initLoader(27, null, this);

        if (getParent() == null) {
            setResult(RESULT_CANCELED);
        } else {
            getParent().setResult(RESULT_CANCELED);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        name = (EditText) findViewById(R.id.group_name);

        RecyclerView usersRecyclerView = (RecyclerView) findViewById(R.id.users_recycler_view);
        completionView = (PersonCompletionView) findViewById(R.id.person_completion_view);

        List<User> users = new ArrayList<>();
        final List<ContactToken> contactTokens = new ArrayList<>();


        PersonCompletionView.CompletionAdapter userCompletionAdapter = new PersonCompletionView.CompletionAdapter(this, R.layout.user_layout, contactTokens);

        char[] splitChar = {',', ';', ' '};
        completionView.setSplitChar(splitChar);
        completionView.setAdapter(userCompletionAdapter);
        completionView.allowDuplicates(false);

        usersAdapter = new UsersAdapter(users, new UsersAdapter.OnUserClickedListener() {
            @Override
            public void onUserClicked(User user) {
                completionView.addObject(new UserContactToken(user, user.getName(), user.getImage()));
            }
        });

        usersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        usersRecyclerView.setAdapter(usersAdapter);
        usersRecyclerView.setItemAnimator(new DefaultItemAnimator());



        PackageManager p = getPackageManager();
        ComponentName componentName = new ComponentName(this, MainActivity.class); // activity which is first time open in manifiest file which is declare as <category android:name="android.intent.category.LAUNCHER" />
        p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_done) {
            List<User> users = new ArrayList<>();
            for(Object o :completionView.getObjects()){
                users.addAll(((ContactToken) o).getUsers());
            }

            List<String> emails = new ArrayList<>();
            for (User u : users) {
                emails.add(u.getEmail());
            }

            if(!name.getText().toString().isEmpty() && !users.isEmpty()){
                try {
                    ApiHelper.getInstance(this).createGroup(name.getText().toString().trim(), emails, new ApiHelper.OnCreateGroupListener() {
                        @Override
                        public void onGroupCreated(UsersGroup usersGroup) {
                            getGATracker().send(new HitBuilders.EventBuilder("UserGroup", "GroupCreated").build());

                            if (getParent() == null) {
                                setResult(RESULT_OK);
                            } else {
                                getParent().setResult(RESULT_OK);
                            }
                            supportFinishAfterTransition();
                        }

                        @Override
                        public void onFail(String errorMessage) {
                            Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (id == android.R.id.home) {
            supportFinishAfterTransition();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<List<User>> onCreateLoader(int id, Bundle args) {
        SQLiteDatabase database = CalendarDatabaseHelper.getInstance(this).getWritableDatabase();
        FriendsTable usersTable = new FriendsTable(database);
        FriendsDataLoader udl = new FriendsDataLoader(this, usersTable, null, null, null, null, null);
        return udl;
    }

    @Override
    public void onLoadFinished(Loader<List<User>> loader, List<User> data) {
        List<User> friends = new ArrayList<>();
        List<ContactToken> contactTokens = new ArrayList<>();

        User me = SessionManager.getInstance(this).getUser();
        if(data.contains(me)) data.remove(me);

        for (User u : data) {
                friends.add(u);
                contactTokens.add(new UserContactToken(u, u.getName(), u.getImage()));

        }

        usersAdapter.setUsers(friends);

        PersonCompletionView.CompletionAdapter userCompletionAdapter = new PersonCompletionView.CompletionAdapter(this, R.layout.user_layout, contactTokens);
        completionView.setAdapter(userCompletionAdapter);

    }

    @Override
    public void onLoaderReset(Loader<List<User>> loader) {

    }
}
