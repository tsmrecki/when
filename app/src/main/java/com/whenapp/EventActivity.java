package com.whenapp;

import android.app.Activity;
import android.app.Dialog;
import android.app.SharedElementCallback;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.view.menu.ActionMenuItem;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.transition.TransitionSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.whenapp.adapters.ChooseMultiDayTerminsAdapter;
import com.whenapp.adapters.ChooseSingleDayTerminsAdapter;
import com.whenapp.adapters.ChooseTerminsAdapter;
import com.whenapp.adapters.UsersInvitesAdapter;
import com.whenapp.common.ImageChooser;
import com.whenapp.common.ImageCoder;
import com.whenapp.common.Utils;
import com.whenapp.connection.ApiHelper;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.EventsDataLoader;
import com.whenapp.database.EventsTable;
import com.whenapp.database.InvitationsTable;
import com.whenapp.database.InvitesEventsDataLoader;
import com.whenapp.database.InvitesEventsTable;
import com.whenapp.database.MyInvitationsDataLoader;
import com.whenapp.database.MyInvitationsTable;
import com.whenapp.layer.ChatActivity;
import com.whenapp.model.AllDayTermin;
import com.whenapp.model.Event;
import com.whenapp.model.Invitation;
import com.whenapp.model.InviteTermin;
import com.whenapp.model.ReminderManager;
import com.whenapp.model.SessionManager;
import com.whenapp.model.Termin;
import com.whenapp.model.User;
import com.whenapp.views.AutoWrapLinearLayoutManager;
import com.whenapp.views.DateTimePickerDialog;
import com.whenapp.views.DialogBuilder;
import com.whenapp.views.DividerItemDecoration;
import com.whenapp.views.SwipeDismissRecyclerViewTouchListener;
import com.whenapp.views.TimePickerDialog;

import org.json.JSONException;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;


public class EventActivity extends BaseActivity implements OnMapReadyCallback {

    public static int EVENT_ACCEPTED_RESULT = 100;
    public static final String INVITATION = "invitation";
    public static final String EVENT = "event";
    public static final String OPENED_FROM_INVITES = "opened_from_invites";

    public static final String EVENT_ID = "com.whenapp.event_id";
    public static final String INVITATION_ID = "com.whenapp.invitation_id";

    private long invitationId = -1;
    private long eventId = -1;

    private Event event = null;
    private Invitation invitation = null;

    ChooseTerminsAdapter terminsAdapter;
    UsersInvitesAdapter usersAdapter;

    private SessionManager session;
    ImageView eventPhoto;
    Uri imageUri = null;

    boolean openedFromInvites = false;

    TextView eventDescription;
    ImageButton showEventMap;
    TextView eventNumPeople;
    TextView eventNumPeopleConfirmed;
    LinearLayout personsMore;
    TextView eventLocation;
    Button addNewPerson;
    Button addNewTermin;
    CheckBox cantAttend;
    CheckBox canAddPeople;
    CheckBox canAddTermins;
    RecyclerView people;
    RecyclerView termins;
    TextView eventInvitee;
    TextView eventName;
    View imageGradient;


    boolean calendarShown = false;
    boolean showChat = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        if (getParent() == null)
            setResult(RESULT_CANCELED);
        else getParent().setResult(RESULT_CANCELED);

        session = SessionManager.getInstance(this);

        if (getIntent().hasExtra(INVITATION)) {
            openedFromInvites = true;
            invitation = getIntent().getParcelableExtra(INVITATION);
            event = getIntent().getParcelableExtra(EVENT);
        } else if (getIntent().hasExtra(EVENT)) {
            event = getIntent().getParcelableExtra(EVENT);
        }else if (getIntent().hasExtra(INVITATION_ID)) {
            openedFromInvites = true;
            invitationId = getIntent().getLongExtra(INVITATION_ID, -1);
            eventId = getIntent().getLongExtra(EVENT_ID, -1);
            getSupportLoaderManager().initLoader(100, null, setupInvitationsLoader());
        }else if (getIntent().hasExtra(EVENT_ID)) {
            eventId = getIntent().getLongExtra(EVENT_ID, -1);
            getSupportLoaderManager().initLoader(101, null, setupEventsLoader());
        }

        if(getIntent().hasExtra("show_chat")) showChat = true;


        if (getIntent().hasExtra(OPENED_FROM_INVITES)) {
            openedFromInvites = getIntent().getBooleanExtra(OPENED_FROM_INVITES, false);
        }

        if(savedInstanceState != null){
            if(savedInstanceState.containsKey("imageUri"))
                imageUri = savedInstanceState.getParcelable("imageUri");
        }

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        getSupportActionBar().hide();

        toolbar.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!getSupportActionBar().isShowing())
                    getSupportActionBar().show();
            }
        }, 600);


        if(savedInstanceState == null)
            imageChooser = new ImageChooser(this, 200);
        else
            imageChooser = new ImageChooser(this, savedInstanceState);


        final ScrollView scrollView = (ScrollView) findViewById(R.id.scroll_view);

        View firstItem = findViewById(R.id.first_item);
        setupTitlebar(eventPhoto, firstItem, scrollView);

        setupLoliTransition(toolbar);

        eventPhoto = (ImageView) findViewById(R.id.event_photo);

        imageGradient = findViewById(R.id.image_gradient);


        eventDescription = (TextView) findViewById(R.id.event_description);
        showEventMap = (ImageButton) findViewById(R.id.show_event_map);
        eventNumPeople = (TextView) findViewById(R.id.event_num_people);
        eventNumPeopleConfirmed = (TextView) findViewById(R.id.event_num_people_confirmed);

        personsMore = (LinearLayout) findViewById(R.id.event_persons_more);
        final ImageButton showPersonsMore = (ImageButton) findViewById(R.id.show_persons_more);

        showPersonsMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (personsMore.getVisibility() == View.GONE)
                    personsMore.setVisibility(View.VISIBLE);
                else personsMore.setVisibility(View.GONE);
            }
        });

        eventLocation = (TextView) findViewById(R.id.event_location);
        addNewPerson = (Button) findViewById(R.id.event_add_people);


        people = (RecyclerView) findViewById(R.id.people_recycler_view);
        people.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        people.setItemAnimator(new DefaultItemAnimator());

        termins = (RecyclerView) findViewById(R.id.event_termins_recycler_view);
        termins.addItemDecoration(new DividerItemDecoration(this, RecyclerView.VERTICAL));
        termins.setLayoutManager(new AutoWrapLinearLayoutManager(this, AutoWrapLinearLayoutManager.VERTICAL, false));


        addNewTermin = (Button) findViewById(R.id.event_add_new_termin);

        View.OnClickListener newTerminClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(getSupportFragmentManager().findFragmentByTag("calendar_view") != null) return;

                DateTimePickerDialog dialog = new DateTimePickerDialog();
                dialog.setOnDateTimeSelectedListener(new DateTimePickerDialog.OnDateTimeSelectedListener() {
                    @Override
                    public void onDateTimeSelected(boolean allDay, int year, int monthOfYear, int dayOfMonth, int hourOfDay, int minute) {
                        List<User> users = new ArrayList<>();
                        users.add(session.getUser());
                        terminsAdapter.addTermin(new InviteTermin(new Termin(year, monthOfYear, dayOfMonth, hourOfDay, minute), users));
                    }
                }, new DateTimePickerDialog.OnDatesSelectedListener() {
                    @Override
                    public void onDatesSelected(List<Date> selectedDates) {
                        Calendar c = Calendar.getInstance();
                        List<User> users = new ArrayList<>();
                        users.add(session.getUser());
                        for (Date d : selectedDates) {
                            c.setTime(d);
                            terminsAdapter.addTermin(new InviteTermin(new AllDayTermin(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)), users));
                        }
                    }
                });

                if (!event.isSingleDay()) dialog.setAllDaySwitchEnabled(false);

                dialog.show(getSupportFragmentManager(), "calendar_view");
            }
        };


        addNewTermin.setOnClickListener(newTerminClickListener);
        cantAttend = (CheckBox) findViewById(R.id.cant_attend_event);
        canAddPeople = (CheckBox) findViewById(R.id.all_can_add_friends);
        canAddTermins = (CheckBox) findViewById(R.id.all_can_add_termins);
        cantAttend.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    DialogBuilder.showYesNoDialog(EventActivity.this, getString(R.string.you_sure_to_exit_event), new DialogBuilder.OnYesNoDialogResponse() {
                        @Override
                        public void onResponse(boolean pressedYes) {
                            if (!pressedYes) cantAttend.setChecked(false);
                            else {
                                if (!openedFromInvites) {
                                    try {
                                        ApiHelper.getInstance(getApplicationContext()).exitEvent(event.getId(), new ApiHelper.OnEventExitListener() {
                                            @Override
                                            public void onEventExit() {
                                                getGATracker().send(new HitBuilders.EventBuilder("Event", "Exit").setLabel(event.getName()).build());
                                                setResult();
                                                supportFinishAfterTransition();
                                            }

                                            @Override
                                            public void onFail(String errorText) {
                                                Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        ApiHelper.getInstance(getApplicationContext()).respondToInvitation(event.getId(), invitation.getId(), false, null, null, null, null, new ApiHelper.OnEventRespondListener() {
                                            @Override
                                            public void onResponded(Event event) {
                                                getGATracker().send(new HitBuilders.EventBuilder("Event", "Decline").setLabel(event.getName()).build());
                                                setResult();
                                                supportFinishAfterTransition();
                                            }

                                            @Override
                                            public void onFail(String errorMessage) {
                                                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });


        if(event != null)
            fillData();

        refreshEvent();
    }


    private void fillData() {

        initUsersRecView();
        initTerminsRecycerView();

        eventName.setText(event.getName());
        eventInvitee.setText(String.format(getString(R.string.invited_by), event.getInvitee().getName()));

        if (event.getImageUrl() == null || event.getImageUrl().isEmpty()) {
            imageGradient.setVisibility(View.GONE);
        }

        if(imageUri == null)
        ImageLoader.getInstance().displayImage(event.getImageUrl(), eventPhoto, new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).showImageOnFail(R.drawable.app_background_1).showImageForEmptyUri(R.drawable.app_background_1).build());
        else{
            eventPhoto.setImageURI(imageUri);
        }

        eventLocation.setText(event.getPlaceName());
        eventDescription.setText(event.getDescription());
        if (event.getDescription() == null || event.getDescription().isEmpty())
            eventDescription.setVisibility(View.GONE);

        if (event.getLatitude() != 0 && event.getLongitude() != 0) {
            setupMap();

            final FrameLayout map = (FrameLayout) findViewById(R.id.event_map);
            showEventMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (map.getVisibility() == View.GONE) map.setVisibility(View.VISIBLE);
                    else map.setVisibility(View.GONE);
                }
            });
            map.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String uri = String.format(Locale.ENGLISH, "geo:%f,%f", event.getLatitude(), event.getLongitude());
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(intent);
                }
            });

        } else if (event.getPlaceName() != null && !event.getPlaceName().isEmpty()) {
            showEventMap.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_grey600_24dp));
            showEventMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + event.getPlaceName());
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    startActivity(mapIntent);
                }
            });
        } else {
            showEventMap.setVisibility(View.GONE);
            findViewById(R.id.location_layout).setVisibility(View.GONE);
        }

        eventNumPeople.setText(String.format(getString(R.string.event_num_people), event.getTotalNumUsers()));
        eventNumPeopleConfirmed.setText(String.format(getString(R.string.event_num_confirmed), event.getNumUsersResponded()));



        addNewPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addContacts = new Intent(EventActivity.this, AddContactsActivity.class);
                startActivityForResult(addContacts, 333);
            }
        });


        if (event.getInvitee().equals(SessionManager.getInstance(this).getUser())) {
            cantAttend.setVisibility(View.GONE);
        }



        if (event.getInvitee().equals(session.getUser())) {
            if (event.getCanInvite() == Event.CanInvite.ONLY_AUTHOR)
                canAddPeople.setVisibility(View.GONE);
            else canAddPeople.setVisibility(View.GONE);

            if (event.getCanSuggestTime() == Event.CanSuggestTime.ONLY_AUTHOR)
                canAddTermins.setVisibility(View.GONE);
            else canAddTermins.setVisibility(View.GONE);
        } else{
            canAddPeople.setVisibility(View.GONE);
            canAddTermins.setVisibility(View.GONE);

            if (event.getCanInvite() == Event.CanInvite.ALL)
                addNewPerson.setVisibility(View.VISIBLE);
            else addNewPerson.setVisibility(View.GONE);

            if (event.getCanSuggestTime() == Event.CanSuggestTime.ALL)
                addNewTermin.setVisibility(View.VISIBLE);
            else addNewTermin.setVisibility(View.GONE);
        }


        if (event.isAccepted()) {
            addNewPerson.setVisibility(View.GONE);
            addNewTermin.setVisibility(View.GONE);
            cantAttend.setVisibility(View.GONE);

            findViewById(R.id.termins_layout).setVisibility(View.GONE);

            Button reminderButton = setupReminderButton();

            reminderButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogBuilder.showRemindInFrontDialog(EventActivity.this, event.getProminentTermin().getTermin().getStart().getTimeInMillis(), new DialogBuilder.OnRemindInFrontChoosenListener() {
                        @Override
                        public void onRemindInFrontChoosen(ReminderManager.ReminderInFront inFront) {

                            boolean enabled = inFront != ReminderManager.ReminderInFront.off;
                            ReminderManager.getInstance(EventActivity.this).setReminderForEvent(eventId, enabled, inFront);

                            if(enabled) Utils.setEventAlarm(EventActivity.this, event.getProminentTermin().getTermin().getStart().getTimeInMillis(), inFront, event.getId(), event.getName(), event.getDescription(), event.getImageUrl());
                            else Utils.removeEventAlarm(EventActivity.this, event.getId());

                            setupReminderButton();
                        }
                    });
                }
            });

            View finishedLayout = findViewById(R.id.finished_event_layout);
            finishedLayout.setVisibility(View.VISIBLE);

            TextView startDate = (TextView) finishedLayout.findViewById(R.id.event_start_date);
            TextView startTime = (TextView) finishedLayout.findViewById(R.id.event_start_time);
            TextView duration = (TextView) findViewById(R.id.event_duration);
            TextView endDateTime = (TextView) findViewById(R.id.event_end_date_time);

            InviteTermin t = event.getProminentTermin();
            if (t != null) {
                Termin termin = t.getTermin();

                DateFormat date = DateFormat.getDateInstance();
                startDate.setText(date.format(termin.getStart().getTime()));

                DateFormat time = DateFormat.getTimeInstance(DateFormat.SHORT);
                startTime.setText(time.format(termin.getStart().getTime()));


                if (event.isSingleDay()) {
                    if (termin.isAllDay()) {
                        endDateTime.setText(date.format(termin.getEnd().getTime()));
                        startTime.setVisibility(View.GONE);
                        long milisDiff = termin.getEnd().getTimeInMillis() - termin.getStart().getTimeInMillis();
                        long secs = milisDiff / 1000;
                        long hours = secs / 60 / 60;
                        long days = hours / 24;

                        duration.setText(days + " days");

                        if (event.isSingleDay()) {
                            duration.setText("All day");
                            endDateTime.setVisibility(View.GONE);
                        }

                    } else {
                        DateFormat dateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
                        endDateTime.setText("to " + dateTime.format(termin.getEnd().getTime()));

                        duration.setVisibility(View.GONE);
                    }
                }else{
                    if (termin.isAllDay()) {
                        endDateTime.setText(date.format(termin.getEnd().getTime()));
                        startTime.setVisibility(View.GONE);
                        long milisDiff = termin.getEnd().getTimeInMillis() - termin.getStart().getTimeInMillis();
                        long secs = milisDiff / 1000;
                        long hours = secs / 60 / 60;
                        long days = hours / 24;

                        duration.setText(days + " days");
                        if(days == 0){
                            duration.setText("All day");
                            endDateTime.setVisibility(View.GONE);
                        }
                    } else {
                        DateFormat dateTime = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
                        endDateTime.setText("to " + dateTime.format(termin.getEnd().getTime()));

                        duration.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    private Button setupReminderButton() {
        Button reminderButton = (Button) findViewById(R.id.reminder_button);
        if(ReminderManager.getInstance(this).isReminderForEventEnabled(event.getId())) {
            reminderButton.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_alarm_on_green_36dp), null, null);
            reminderButton.setTextColor(getResources().getColor(R.color.prominent_green));

            reminderButton.setText(getString(R.string.reminder_in_front, ReminderManager.getReminderInFrontPretty(ReminderManager.getInstance(EventActivity.this).remindsInFront(eventId), EventActivity.this)));
        }
        else {
            reminderButton.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_alarm_grey_36dp), null, null);
            reminderButton.setTextColor(Color.parseColor("#505050"));
            reminderButton.setText(getString(R.string.reminder_disabled));
        }
        return reminderButton;
    }

    private void setupLoliTransition(Toolbar toolbar) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            TransitionSet tran = new TransitionSet();
            tran.excludeTarget(toolbar, true);
            tran.excludeTarget(android.R.id.navigationBarBackground, true);
            tran.addTransition(new Slide(Gravity.BOTTOM));
            getWindow().setEnterTransition(tran);
            setEnterSharedElementCallback(new SharedElementCallback() {

                @Override
                public void onSharedElementStart(List<String> sharedElementNames, List<View> sharedElements, List<View> sharedElementSnapshots) {
                    getSupportActionBar().hide();
                    super.onSharedElementStart(sharedElementNames, sharedElements, sharedElementSnapshots);
                }

                @Override
                public void onSharedElementEnd(List<String> sharedElementNames, List<View> sharedElements, List<View> sharedElementSnapshots) {
                    super.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots);
                    getSupportActionBar().show();
                }
            });
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TransitionSet tran = new TransitionSet();
            tran.excludeTarget(toolbar, true);
            tran.excludeTarget(android.R.id.navigationBarBackground, true);
            tran.addTransition(new Slide(Gravity.BOTTOM));
            getWindow().setReturnTransition(tran);
        }
    }

    private void initUsersRecView() {

        usersAdapter = new UsersInvitesAdapter(session.getUser().equals(event.getInvitee()), event.getPeople(), event.getInvitations(), new UsersInvitesAdapter.OnUserClickedListener() {
            @Override
            public void onUserClicked(User user) {
                Intent profile = new Intent(EventActivity.this, ProfileActivity.class);
                profile.putExtra("user", user);
                startActivity(profile);
            }

            @Override
            public void onUserRemoved(Invitation invitation) {
                ApiHelper.getInstance(getApplicationContext()).removeInvitationFromEvent(event.getId(), invitation, new ApiHelper.OnRemoveInvitationListener() {
                    @Override
                    public void onInvitationRemoved(Invitation invitation) {
                        Toast.makeText(getApplicationContext(), "User " + invitation.getCallee() + " removed from the event.", Toast.LENGTH_SHORT).show();
                        usersAdapter.removeUser(invitation.getCallee());
                        setResult();
                    }

                    @Override
                    public void onFail(String errorMessage) {
                        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        usersAdapter.setUsersResponded(event.getParticipants());
        people.setAdapter(usersAdapter);
    }

    private void initTerminsRecycerView() {


        //terminsAdapter.setTermins(event.getInviteTerminList());

        User me = session.getUser();

        List<InviteTermin> myTermins = new ArrayList<>();
        for (InviteTermin it : event.getInviteTerminList()) {
            if (it.getUsersAccepted().contains(me)) myTermins.add(it);
        }

        if (event.isSingleDay())
            terminsAdapter = new ChooseSingleDayTerminsAdapter(event.getInviteTerminList(), myTermins);
        else
            terminsAdapter = new ChooseMultiDayTerminsAdapter(event.getInviteTerminList(), myTermins);

        //terminsAdapter.setSelectedTermins(myTermins);

        termins.setAdapter(terminsAdapter);


        terminsAdapter.setOnTerminClickedListener(new ChooseTerminsAdapter.OnTerminClickedListener() {
            @Override
            public void onTerminClicked(int position, InviteTermin termin, View view, boolean editable) {
                DialogBuilder.showPeopleListDialog(EventActivity.this, termin.getUsersAccepted());
            }

            @Override
            public void onTerminChecked(final int position, final InviteTermin termin, boolean isChecked, boolean editable) {

            }

            @Override
            public void onTerminEndEdited(final int position, final InviteTermin termin, View view, boolean editable) {
                if (!event.isSingleDay()) {
                    DateTimePickerDialog dialog = new DateTimePickerDialog();
                    dialog.setOnDateTimeSelectedListener(new DateTimePickerDialog.OnDateTimeSelectedListener() {
                        @Override
                        public void onDateTimeSelected(boolean allDay, int year, int monthOfYear, int dayOfMonth, int hourOfDay, int minute) {
                            Calendar cNew = Calendar.getInstance();
                            cNew.set(Calendar.YEAR, year);
                            cNew.set(Calendar.MONTH, monthOfYear);
                            cNew.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            cNew.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            cNew.set(Calendar.MINUTE, minute);

                            if (cNew.before(termin.getTermin().getStart())) {
                                Toast.makeText(getApplicationContext(), getString(R.string.cant_set_end_time_before_start), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            termin.getTermin().setEndYear(year);
                            termin.getTermin().setEndMonth(monthOfYear);
                            termin.getTermin().setEndDay(dayOfMonth);
                            termin.getTermin().setEndHour(hourOfDay);
                            termin.getTermin().setEndMinute(minute);
                            terminsAdapter.updateTermin(position, termin);
                        }
                    }, new DateTimePickerDialog.OnDatesSelectedListener() {
                        @Override
                        public void onDatesSelected(List<Date> selectedDates) {

                        }
                    });

                    dialog.setSelectableAfter(termin.getTermin().getStart().getTime());
                    if (!event.isSingleDay()) dialog.setAllDaySwitchEnabled(false);
                    dialog.show(getSupportFragmentManager(), "calendar_view");
                } else {
                    TimePickerDialog dialog = new TimePickerDialog();
                    dialog.setOnTimePickedListener(new TimePickerDialog.OnTimePickedListener() {
                        @Override
                        public void onTimePicked(int hourOfDay, int minute) {
                            if (hourOfDay < termin.getTermin().getHour() || (hourOfDay == termin.getTermin().getHour() && minute < termin.getTermin().getMinute())) {
                                Toast.makeText(getApplicationContext(), getString(R.string.cant_set_end_time_before_start), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            termin.getTermin().setEndHour(hourOfDay);
                            termin.getTermin().setEndMinute(minute);
                            terminsAdapter.updateTermin(position, termin);
                        }
                    });
                    dialog.show(getSupportFragmentManager(), "time_picker_view");
                }
            }
        });



        SwipeDismissRecyclerViewTouchListener swipeListener = new SwipeDismissRecyclerViewTouchListener(termins, new SwipeDismissRecyclerViewTouchListener.DismissCallbacks() {
            @Override
            public boolean canDismiss(int position) {
                return terminsAdapter.isTerminRemovable(terminsAdapter.getTermin(position));
            }

            @Override
            public void onDismiss(RecyclerView recyclerView, int[] reverseSortedPositions) {
                for (int position : reverseSortedPositions) {
                    terminsAdapter.removeTermin(position);
                }
            }
        });

        termins.setOnTouchListener(swipeListener);
        termins.setOnScrollListener(swipeListener.makeScrollListener());


    }

    private boolean customTitleSet = false;
    private boolean subtitleVisible = true;

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    private void setupTitlebar(final ImageView image, final View firstItem, final ScrollView scrollView) {

        // Calculate ActionBar height
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            actionBarHeight += 28 * getResources().getDisplayMetrics().density;
        }else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            actionBarHeight += 35 * getResources().getDisplayMetrics().density;
        } else {
            actionBarHeight += 10 * getResources().getDisplayMetrics().density;
        }
        final View imageParallaxHolder = findViewById(R.id.image_parallax_holder);

        eventName = (TextView) findViewById(R.id.event_name);
        eventInvitee = (TextView) findViewById(R.id.event_invitee);

        final View titleHolderTop = findViewById(R.id.title_holder_top);
        final View titleBackground = findViewById(R.id.title_holder);
        final LinearLayout titleSubtitleHolder = (LinearLayout) findViewById(R.id.title_subtitle_holder);
        final float titleOffset = 48 * getResources().getDisplayMetrics().density;
        final float movementSpace = 260 * getResources().getDisplayMetrics().density - actionBarHeight;
        final float headerSize = 260 * getResources().getDisplayMetrics().density;

        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scroll = scrollView.getScrollY();
                float percentageMovement = Math.min(1, (float) scroll / (movementSpace));

                titleHolderTop.setTranslationY(-(movementSpace) * percentageMovement);

                //eventInvitee.setAlpha(Math.max(0, (1 - percentageMovement * 2)));


                /*
                if(percentageMovement >= 0.5){
                    eventInvitee.setHeight((int)(( 1 - (percentageMovement*2 - 1)) * 18*3));
                }else{
                    eventInvitee.setHeight( 18*3);
                }
*/


                eventName.setShadowLayer((1 - percentageMovement) * 4, 1, 1, Color.BLACK);


                //eventName.setTranslationY(eventInvitee.getHeight() * percentageMovement);


                titleSubtitleHolder.setTranslationX(titleOffset * percentageMovement);

                titleBackground.setAlpha(percentageMovement);


                imageParallaxHolder.setTranslationY(-scroll / 2);

                if (scroll >= 20 && subtitleVisible) {
                    eventInvitee.setVisibility(View.GONE);
                    subtitleVisible = false;
                } else if (scroll < 20 && !subtitleVisible) {
                    eventInvitee.setVisibility(View.VISIBLE);
                    subtitleVisible = true;
                }

            }
        });
    }


    private void setResult() {
        Intent result = new Intent();

        if (getParent() == null) {
            setResult(Activity.RESULT_OK, result);
        } else {
            getParent().setResult(Activity.RESULT_OK, result);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(event == null){
            getMenuInflater().inflate(R.menu.menu_event, menu);
        }
        else if (!event.isAccepted()) {
            if (!event.getInvitee().equals(session.getUser())) {
                getMenuInflater().inflate(R.menu.menu_event, menu);
            } else {
                getMenuInflater().inflate(R.menu.menu_event_author, menu);
            }
        } else {
            if (!event.getInvitee().equals(session.getUser())) {
                getMenuInflater().inflate(R.menu.menu_event_finished, menu);
            } else {
                getMenuInflater().inflate(R.menu.menu_event_finished_author, menu);
            }
        }
        return true;
    }


    ImageChooser imageChooser;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            supportFinishAfterTransition();
            return true;
        } else if (id == R.id.action_delete_event) {

            DialogBuilder.showYesNoDialog(this, "Are you sure you want to delete this event?", new DialogBuilder.OnYesNoDialogResponse() {
                @Override
                public void onResponse(boolean pressedYes) {
                    if (pressedYes) {
                        ApiHelper.getInstance(getApplicationContext()).deleteEvent(event.getId(), new ApiHelper.OnDeleteEventListener() {
                            @Override
                            public void onEventDeleted() {
                                setResult();
                                supportFinishAfterTransition();
                            }

                            @Override
                            public void onFail(String errorMessage) {
                                Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            });
        }

         else if(id == R.id.action_chat) {
            openChat();
        }

        else if (id == R.id.action_image) {
            startActivityForResult(imageChooser.buildChooseImageIntent(), 200);
        } else if (id == R.id.action_done) {
            if (openedFromInvites) {
                if(terminsAdapter.getMyTermins().isEmpty() && terminsAdapter.getSelectedTermins().isEmpty()){
                    Toast.makeText(getApplicationContext(), getString(R.string.you_havent_voted_or_added_termin), Toast.LENGTH_LONG).show();
                    return true;
                }
                try {
                    final Dialog d = DialogBuilder.showLoadingDialog(EventActivity.this, getString(R.string.wait_till_we_save));
                    List<Termin> myTermins = new ArrayList<>();
                    for(InviteTermin it : terminsAdapter.getMyTermins()) myTermins.add(it.getTermin());
                    ApiHelper.getInstance(this).respondToInvitation(event.getId(), invitation.getId(), true, myTermins, terminsAdapter.getNewlySelectedTermins(), terminsAdapter.getNewlyUnselectedTermins(), usersAdapter.getMyAddedUsers(), new ApiHelper.OnEventRespondListener() {
                        @Override
                        public void onResponded(Event event) {
                            setResult();
                            d.dismiss();
                            supportFinishAfterTransition();
                        }

                        @Override
                        public void onFail(String errorMessage) {
                            d.dismiss();
                            Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {

                try {
                    final Dialog d = DialogBuilder.showLoadingDialog(EventActivity.this, getString(R.string.wait_till_we_save));
                    List<Termin> myTermins = new ArrayList<>();
                    for(InviteTermin it : terminsAdapter.getMyTermins()) myTermins.add(it.getTermin());

                    ApiHelper.getInstance(this).editEvent(event.getId(), myTermins, terminsAdapter.getNewlySelectedTermins(), terminsAdapter.getNewlyUnselectedTermins(), usersAdapter.getMyAddedUsers(), new ApiHelper.OnEventRespondListener() {
                        @Override
                        public void onResponded(Event event) {
                            getGATracker().send(new HitBuilders.EventBuilder("Event", "Edit").setLabel(event.getName()).build());
                            setResult();
                            d.dismiss();
                            supportFinishAfterTransition();
                        }

                        @Override
                        public void onFail(String errorMessage) {
                            d.dismiss();
                            Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openChat() {
        Intent chatActivity = new Intent(EventActivity.this, ChatActivity.class);
        chatActivity.putParcelableArrayListExtra("participants", (ArrayList<? extends Parcelable>) usersAdapter.getUsers());
        chatActivity.putExtra("chat_name", event.getName());
        chatActivity.putExtra("event_id", eventId);
        chatActivity.putExtra("invitation_id", invitationId);
        chatActivity.putExtra("event", event);

        startActivity(chatActivity);
    }

    private void addTermins() {
        final Dialog loader = DialogBuilder.showLoadingDialog(this, getString(R.string.wait_till_we_save));

        List<InviteTermin> inviteTermins = terminsAdapter.getMyTermins();
        List<Termin> termins = new ArrayList<>();
        for (InviteTermin it : inviteTermins) {
            termins.add(it.getTermin());
        }
        try {
            ApiHelper.getInstance(getApplicationContext()).addTerminsToEvent(event.getId(), termins, new ApiHelper.OnTerminsAddedListener() {
                @Override
                public void onTerminsAdded() {
                    loader.dismiss();
                    setResult();
                    supportFinishAfterTransition();
                }

                @Override
                public void onFail(String errorMessage) {
                    loader.dismiss();
                    Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        imageChooser.saveInstance(outState);
        outState.putParcelable("imageUri", imageUri);
    }

    private void refreshEvent() {
        long id = eventId;
        if(event != null) id = event.getId();

        final View progress = findViewById(R.id.event_loading_progress);
        progress.setVisibility(View.VISIBLE);
        final long finalId = id;
        ApiHelper.getInstance(EventActivity.this).getEvent(id, new ApiHelper.OnGetEventListener() {
            @Override
            public void onEvent(Event eventNew) {

                //if this is an event
                if (!openedFromInvites) {
                    EventsTable et = new EventsTable(CalendarDatabaseHelper.getInstance(EventActivity.this).getWritableDatabase());
                    EventsDataLoader eventsDataLoader = new EventsDataLoader(getApplicationContext(), et, null, null, null, null, null);
                    eventsDataLoader.insert(eventNew);
                } else {
                    // if this is a invite
                    InvitesEventsTable iet = new InvitesEventsTable(CalendarDatabaseHelper.getInstance(EventActivity.this).getWritableDatabase());
                    InvitesEventsDataLoader invitesEventsDataLoader = new InvitesEventsDataLoader(getApplicationContext(), iet, null, null, null, null, null);
                    invitesEventsDataLoader.insert(eventNew);

                    if(invitation == null)
                    ApiHelper.getInstance(EventActivity.this).getInvitation(finalId, invitationId, new ApiHelper.OnGetInvitationListener() {
                        @Override
                        public void onInvitation(Invitation invitationNew) {
                            InvitationsTable iet = new InvitationsTable(CalendarDatabaseHelper.getInstance(EventActivity.this).getWritableDatabase());
                            MyInvitationsDataLoader invitesEventsDataLoader = new MyInvitationsDataLoader(getApplicationContext(), iet, null, null, null, null, null);
                            invitesEventsDataLoader.insert(invitationNew);
                            invitation = invitationNew;
                        }

                        @Override
                        public void onFail(String error) {

                        }
                    });

                }

                event = eventNew;

                fillData();
                progress.setVisibility(View.GONE);

                if(showChat) openChat();
            }

            @Override
            public void onFail(String error) {
                progress.setVisibility(View.GONE);
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 200) {
            if (resultCode == RESULT_OK) {
                setResult();
                findViewById(R.id.image_loader).setVisibility(View.VISIBLE);
               imageUri = imageChooser.onActivityResult(requestCode, resultCode, data);
                try {
                    String base64 = new ImageCoder.EncodeAsync(imageUri, this).execute().get();
                    try {
                        ApiHelper.getInstance(this).changeEventImage(event.getId(), base64, new ApiHelper.OnChangeEventImageListener() {
                            @Override
                            public void onImageChanged(Event event) {
                                ImageLoader.getInstance().displayImage(event.getImageUrl(), eventPhoto);

                                findViewById(R.id.image_loader).setVisibility(View.GONE);
                            }

                            @Override
                            public void onFail(String errorMessage) {
                                findViewById(R.id.image_loader).setVisibility(View.GONE);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }
        if (requestCode == 333) {
            if (resultCode == RESULT_OK) {
                List<User> users = data.getParcelableArrayListExtra("contacts");
                for(User u : users)
                    usersAdapter.addUser(u);

                /*
                if(!users.isEmpty()) setResult();
                for (User u : users) {
                    if(u.getEmail().contains("@")) {
                        try {
                            ApiHelper.getInstance(this).inviteUserToEvent(event.getId(), u.getEmail(), new ApiHelper.OnInviteUserToEventListener() {
                                @Override
                                public void onInvite(Invitation invitation) {
                                    usersAdapter.addInvitation(invitation);
                                    setResult();
                                }

                                @Override
                                public void onFail(String errorMessage) {
                                    Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_SHORT).show();
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                */

                //usersAdapter.addUsers(users);
            }
        }
    }

    public void setupMap() {
                if (!isFinishing()) {
                    SupportMapFragment mapFragment = SupportMapFragment.newInstance();
                    getSupportFragmentManager().beginTransaction().replace(R.id.event_map, mapFragment).commitAllowingStateLoss();
                    mapFragment.getMapAsync(EventActivity.this);
                }

        /*
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        */

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        // Other supported types include: MAP_TYPE_NORMAL,
        // MAP_TYPE_TERRAIN, MAP_TYPE_HYBRID and MAP_TYPE_NONE
        if (googleMap.getMapType() != GoogleMap.MAP_TYPE_NORMAL)
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(event.getLatitude(), event.getLongitude()), 12));

        googleMap.addMarker(new MarkerOptions().position(new LatLng(event.getLatitude(), event.getLongitude())));
    }

    @Override
    public void onBackPressed() {
        if(!terminsAdapter.getMyTermins().isEmpty() || !terminsAdapter.getNewlySelectedTermins().isEmpty() || !terminsAdapter.getNewlyUnselectedTermins().isEmpty() || !usersAdapter.getMyAddedUsers().isEmpty()){
            DialogBuilder.showYesNoDialog(this, getString(R.string.you_made_event_changes_ask_save), new DialogBuilder.OnYesNoDialogResponse() {
                @Override
                public void onResponse(boolean pressedYes) {
                    if (pressedYes) {
                        supportFinishAfterTransition();
                    }
                }
            });
        }else
            super.onBackPressed();
    }

    SQLiteDatabase database;

    private LoaderManager.LoaderCallbacks<List<Invitation>> invitationsDataLoader;
    private LoaderManager.LoaderCallbacks<List<Event>> eventsDataLoader;

    private LoaderManager.LoaderCallbacks<List<Invitation>> setupInvitationsLoader(){

        final Dialog loaderPopup = DialogBuilder.showLoadingDialog(this, getString(R.string.wait_till_we_load));

        if(database == null) database = CalendarDatabaseHelper.getInstance(this).getWritableDatabase();

        invitationsDataLoader = new LoaderManager.LoaderCallbacks<List<Invitation>>() {
            @Override
            public Loader<List<Invitation>> onCreateLoader(int id, Bundle args) {
                return new MyInvitationsDataLoader(getApplicationContext(), new MyInvitationsTable(database), MyInvitationsTable.COLUMN_ID + "=?", new String[]{String.valueOf(invitationId)}, null, null, null);
            }

            @Override
            public void onLoadFinished(Loader<List<Invitation>> loader, List<Invitation> data) {

                if(!data.isEmpty()){
                    Invitation inv = data.get(0);
                    invitation = inv;
                    event = inv.getEvent();
                    fillData();
                    initTerminsRecycerView();
                    initUsersRecView();
                }

                loaderPopup.dismiss();
            }

            @Override
            public void onLoaderReset(Loader<List<Invitation>> loader) {

            }

        };

        return invitationsDataLoader;

    }

    private LoaderManager.LoaderCallbacks<List<Event>> setupEventsLoader(){
        final Dialog loaderPopup = DialogBuilder.showLoadingDialog(this, getString(R.string.wait_till_we_load));


        if(database == null) database = CalendarDatabaseHelper.getInstance(this).getWritableDatabase();


        eventsDataLoader = new LoaderManager.LoaderCallbacks<List<Event>>() {
            @Override
            public Loader<List<Event>> onCreateLoader(int id, Bundle args) {

                EventsDataLoader loader = new EventsDataLoader(getApplicationContext(), new EventsTable(database), EventsTable.COLUMN_ID + "=?", new String[]{String.valueOf(eventId)}, null, null, null);
                return loader;
            }

            @Override
            public void onLoadFinished(Loader<List<Event>> loader, List<Event> data) {
                if (!data.isEmpty()) {
                    Event e = data.get(0);
                    event = e;
                    fillData();
                }

                loaderPopup.dismiss();
            }

            @Override
            public void onLoaderReset(Loader<List<Event>> loader) {

            }
        };

        return eventsDataLoader;

    }




}
