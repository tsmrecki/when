package com.whenapp.model;

import android.os.Parcel;

/**
 * Created by tomislav on 17/04/15.
 */
public class AllDayTermin extends Termin implements android.os.Parcelable {

    public AllDayTermin(int year, int month, int day) {
        super(year, month, day, 0, 0);
        allDay = true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    private AllDayTermin(Parcel in) {
        super(in);
    }

    public static final Creator<AllDayTermin> CREATOR = new Creator<AllDayTermin>() {
        public AllDayTermin createFromParcel(Parcel source) {
            return new AllDayTermin(source);
        }

        public AllDayTermin[] newArray(int size) {
            return new AllDayTermin[size];
        }
    };
}
