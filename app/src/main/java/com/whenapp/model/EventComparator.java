package com.whenapp.model;

import java.util.Comparator;

/**
 * Created by tomislav on 01/05/15.
 */
public class EventComparator implements Comparator<Event> {

    boolean reverse = false;

    public EventComparator(boolean reverse){
        this.reverse = reverse;
    }

    @Override
    public int compare(Event first, Event second) {
        if(first.getProminentTermin() == null && second.getProminentTermin() == null) return 0;
        else if(first.getProminentTermin() == null) return (reverse ? -1 : 1);
        else if(second.getProminentTermin() == null) return reverse ? 1 : -1;
        else if(first.getProminentTermin().getTermin().getStart().before(second.getProminentTermin().getTermin().getStart()))
            return reverse ? 1 : -1;
        else if(first.getProminentTermin().getTermin().getStart().after(second.getProminentTermin().getTermin().getStart()))
            return reverse ? -1 : 1;
        else return 0;
    }
}
