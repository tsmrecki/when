package com.whenapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Tomislav on 28.3.2015..
 */
public class UsersGroup implements Parcelable {

    private long id;
    private String name;
    private List<User> users = new ArrayList<>();

    public UsersGroup(String name, List<User> users) {
        id = new Random().nextInt(200000);
        this.name = name;
        this.users = users;
    }

    public UsersGroup(JSONObject userGroup) {
        if(userGroup == null) return;

        id = userGroup.optLong("id");
        name = userGroup.optString("displayName", "Group");
        JSONArray friendsJ = userGroup.optJSONArray("friendsList");
        if (friendsJ != null) {
            for (int i = 0; i < friendsJ.length(); i++) {
                users.add(new User(friendsJ.optJSONObject(i)));
            }
        }
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserNames() {
        StringBuilder sb = new StringBuilder();
        for (User u : users)
            sb.append(u.getName()).append(", ");
        String users = sb.toString();
        if(!users.isEmpty())
            return users.substring(0, users.length() - 2);
        else
            return "Group is empty";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeTypedList(users);
    }

    private UsersGroup(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        in.readTypedList(users, User.CREATOR);
    }

    public static final Parcelable.Creator<UsersGroup> CREATOR = new Parcelable.Creator<UsersGroup>() {
        public UsersGroup createFromParcel(Parcel source) {
            return new UsersGroup(source);
        }

        public UsersGroup[] newArray(int size) {
            return new UsersGroup[size];
        }
    };
}
