package com.whenapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

/**
 * Created by Tomislav on 8.4.2015..
 */
public class InviteTermin implements Parcelable {

    private long id = 0;
    private Termin termin;
    private User author;
    private List<User> usersAccepted = new ArrayList<>();

    public InviteTermin(Termin termin, List<User> usersAccepted) {
        this.termin = termin;
        this.usersAccepted = usersAccepted;
    }

    public InviteTermin(JSONObject inviteTermin) {
        if(inviteTermin == null) return;

        id = inviteTermin.optLong("id");

        author = new User(inviteTermin.optJSONObject("author"));

        JSONArray usersJ = inviteTermin.optJSONArray("votes");
        for (int i = 0; i < usersJ.length(); i++) {
            usersAccepted.add(new User(usersJ.optJSONObject(i)));
        }

        long startM = inviteTermin.optLong("start_time", 0);
        Calendar start = Calendar.getInstance();
        start.setTimeInMillis(startM * 1000);

        long endM = inviteTermin.optLong("end_time", startM + 1);
        Calendar end = Calendar.getInstance();
        end.setTimeInMillis(endM * 1000);

        termin = new Termin(start, end);

        if(inviteTermin.optBoolean("is_all_day", false)) termin.setAllDay(true);

    }


    public Termin getTermin() {
        return termin;
    }

    public void setTermin(Termin termin) {
        this.termin = termin;
    }

    public List<User> getUsersAccepted() {
        return usersAccepted;
    }

    public void setUsersAccepted(List<User> usersAccepted) {
        this.usersAccepted = usersAccepted;
    }

    public String getUserNames() {
        StringBuilder sb = new StringBuilder();
        for (User u : usersAccepted)
            sb.append(u.getName()).append(", ");
        return sb.toString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String toString() {
        return termin.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeParcelable(this.termin, 0);
        dest.writeParcelable(this.author, 0);
        dest.writeTypedList(usersAccepted);
    }

    private InviteTermin(Parcel in) {
        this.id = in.readLong();
        this.termin = in.readParcelable(Termin.class.getClassLoader());
        this.author = in.readParcelable(User.class.getClassLoader());
        in.readTypedList(usersAccepted, User.CREATOR);
    }

    public static final Parcelable.Creator<InviteTermin> CREATOR = new Parcelable.Creator<InviteTermin>() {
        public InviteTermin createFromParcel(Parcel source) {
            return new InviteTermin(source);
        }

        public InviteTermin[] newArray(int size) {
            return new InviteTermin[size];
        }
    };
}
