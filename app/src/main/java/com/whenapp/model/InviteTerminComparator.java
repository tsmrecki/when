package com.whenapp.model;

import java.util.Comparator;

/**
 * Created by tomislav on 01/05/15.
 */
public class InviteTerminComparator implements Comparator<InviteTermin> {
    @Override
    public int compare(InviteTermin lhs, InviteTermin rhs) {
        if(lhs.getTermin().getStart().before(rhs.getTermin().getStart()))
            return -1;
        else if(lhs.getTermin().getStart().after(rhs.getTermin().getStart()))
            return 1;
        else return 0;
    }
}
