package com.whenapp.model;

import android.os.Parcel;

import java.util.ArrayList;

/**
 * Created by tomislav on 01/05/15.
 */
public class UserContactToken extends ContactToken implements android.os.Parcelable {

    private String tokenEmail;

    public UserContactToken(User user, String tokenName, String image) {
        super(tokenName, image);
        users = new ArrayList<>();
        users.add(user);
        tokenEmail = user.getEmail();
    }

    public String getTokenEmail() {
        return tokenEmail;
    }

    public void setTokenEmail(String tokenEmail) {
        this.tokenEmail = tokenEmail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.tokenEmail);
    }

    private UserContactToken(Parcel in) {
        super(in);
        this.tokenEmail = in.readString();
    }

    public static final Creator<UserContactToken> CREATOR = new Creator<UserContactToken>() {
        public UserContactToken createFromParcel(Parcel source) {
            return new UserContactToken(source);
        }

        public UserContactToken[] newArray(int size) {
            return new UserContactToken[size];
        }
    };
}
