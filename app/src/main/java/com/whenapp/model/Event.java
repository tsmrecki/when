package com.whenapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.whenapp.connection.ApiHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Tomislav on 18.3.2015..
 */
public class Event implements Parcelable {

    private String name;
    private String description;
    private String placeName;
    private double latitude;
    private double longitude;
    private long id = 0;

    private int numUsersResponded = 0;
    private List<User> participants = new ArrayList<>();
    //private List<User> invitedUsers = new ArrayList<>();
    private List<Invitation> invitations = new ArrayList<>();


    private User invitee = new User("", "");
    private List<InviteTermin> inviteTerminList = new ArrayList<>();
    InviteTermin prominentTermin = null;

    private String imageUrl = null;
    private boolean isAccepted = false;
    private boolean isSingleDay = true;

    private String dateQuery = "";

    private CanInvite canInvite = CanInvite.ONLY_AUTHOR;
    private CanSuggestTime canSuggestTime = CanSuggestTime.ALL;


    public Event() {
        id = new Random().nextLong();
    }

    public Event(String name) {
        id = new Random().nextLong();
        this.name = name;
    }

    public Event(JSONObject event){
        if(event == null) return;

        id = event.optLong("id");
        name = event.optString("name");
        if(event.optString("image_identifier") != null && !event.optString("image_identifier").equals("null") && !event.optString("image_identifier").isEmpty())
        imageUrl = ApiHelper.BASE_PATH + event.optString("image_identifier");
        invitee = new User(event.optJSONObject("author"));
        description = event.optString("description", "");

        JSONArray proposedTermins = event.optJSONArray("proposed_terms");
        if(proposedTermins != null)
        for(int i = 0; i<proposedTermins.length(); i++){
            inviteTerminList.add(new InviteTermin(proposedTermins.optJSONObject(i)));
        }

        JSONArray peopleJ = event.optJSONArray("participants");
        if(peopleJ != null)
        for (int i = 0; i < peopleJ.length(); i++) {
            participants.add(new User(peopleJ.optJSONObject(i)));
        }
        numUsersResponded = participants.size();



        JSONArray invitedUsersJ = event.optJSONArray("invitations");
        if(invitedUsersJ != null)
        for (int i = 0; i < invitedUsersJ.length(); i++) {
            invitations.add(new Invitation(invitedUsersJ.optJSONObject(i)));
            //invitedUsers.add(new User(invitedUsersJ.optJSONObject(i).optJSONObject("callee")));
        }


        JSONObject location = event.optJSONObject("location");
        if(location != null){
            placeName = location.optString("name");
            latitude = location.optDouble("latitude", 0);
            longitude = location.optDouble("longitude", 0);
        }

        isSingleDay = event.optBoolean("is_single_day", false);
        isAccepted = event.optBoolean("is_accepted", false);

        canInvite = CanInvite.valueOf(event.optString("add_user_privilege", CanInvite.ONLY_AUTHOR.toString()));
        canSuggestTime = CanSuggestTime.valueOf(event.optString("add_time_suggestions_privilege", CanSuggestTime.ALL.toString()));

        prominentTermin = new InviteTermin(event.optJSONObject("current_best_suggestion"));

        if(prominentTermin.getId() == 0 && !inviteTerminList.isEmpty()) prominentTermin = inviteTerminList.get(0);

        if(!isAccepted) {
            StringBuilder dqb = new StringBuilder();
            for (InviteTermin it : inviteTerminList) {
                Termin t = it.getTermin();
                dqb.append('x').append(t.getYear()).append('-').append(t.getMonth()).append('-').append(t.getDay()).append('x').append(',');
            }
            String dqbS = dqb.toString();
            if (!dqbS.isEmpty()) {
                dateQuery = dqbS.substring(0, dqbS.length() - 1);
            }
        }else{
            dateQuery = "x" + prominentTermin.getTermin().getYear() + "-" + prominentTermin.getTermin().getMonth() + "-" + prominentTermin.getTermin().getDay() + "x";
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public void setParticipants(List<User> participants) {
        this.participants = participants;
    }

    public void addUser(User user) {
        this.participants.add(user);
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isSingleDay() {
        return isSingleDay;
    }

    public void setSingleDay(boolean isSingleDay) {
        this.isSingleDay = isSingleDay;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String toString() {
        return "id=" + id + ", name=" +name + ", users " + participants.toString() + ", termins " + inviteTerminList.toString();
    }

    public User getInvitee() {
        return invitee;
    }

    public void setInvitee(User invitee) {
        this.invitee = invitee;
    }

    public List<Invitation> getInvitations() {
        return invitations;
    }

    public void setInvitations(List<Invitation> invitations) {
        if(invitations != null)
            this.invitations = invitations;
    }

    public List<InviteTermin> getInviteTerminList() {
        return inviteTerminList;
    }

    public void setInviteTerminList(List<InviteTermin> inviteTerminList) {
        if(inviteTerminList != null)
            this.inviteTerminList = inviteTerminList;
    }

    public String getTime() {
        if(!inviteTerminList.isEmpty())
            return inviteTerminList.get(0).getTermin().getStartTime();
        else return "...";
    }

    public String getDate() {
        if (!inviteTerminList.isEmpty()) {
            DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
            return df.format(inviteTerminList.get(0).getTermin().getStart().getTime());
        }
        else return "";
    }


    public String getProminentDate() {
        if (prominentTermin != null) {
            DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
            return df.format(prominentTermin.getTermin().getStart().getTime());
        }
        else return "";
    }

    public String getDateQuery() {
        return dateQuery;
    }

    public void setDateQuery(String dateQuery) {
        this.dateQuery = dateQuery;
    }

    /*
    public List<User> getInvitedUsers() {
        return invitedUsers;
    }


    public void setInvitedUsers(List<User> invitedUsers) {
        this.invitedUsers = invitedUsers;
    }
    */

    public CanInvite getCanInvite() {
        return canInvite;
    }

    public void setCanInvite(CanInvite canInvite) {
        this.canInvite = canInvite;
    }

    public CanSuggestTime getCanSuggestTime() {
        return canSuggestTime;
    }

    public void setCanSuggestTime(CanSuggestTime canSuggestTime) {
        this.canSuggestTime = canSuggestTime;
    }

    public InviteTermin getProminentTermin() {
            return prominentTermin;
    }

    public void setProminentTermin(InviteTermin prominentTermin) {
        this.prominentTermin = prominentTermin;
    }

    public int getNumUsersResponded() {
        return participants.size();
    }

    public int getTotalNumUsers() {
        return 1 + invitations.size();
    }

    public List<User> getPeople(){
        List<User> allUsers = new ArrayList<>();
        allUsers.addAll(participants);
        for(Invitation i : invitations){
            if(!allUsers.contains(i.getCallee()))
                allUsers.add(i.getCallee());
        }
        //allUsers.addAll(invitedUsers);
        return allUsers;
    }

    public enum CanInvite{
        ONLY_AUTHOR, ALL
    }

    public enum CanSuggestTime{
        ONLY_AUTHOR, ALL
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;

        Event event = (Event) o;

        return id == event.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.placeName);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeLong(this.id);
        dest.writeInt(this.numUsersResponded);
        dest.writeTypedList(participants);
        dest.writeTypedList(invitations);
        dest.writeParcelable(this.invitee, 0);
        dest.writeTypedList(inviteTerminList);
        dest.writeParcelable(this.prominentTermin, 0);
        dest.writeString(this.imageUrl);
        dest.writeByte(isAccepted ? (byte) 1 : (byte) 0);
        dest.writeByte(isSingleDay ? (byte) 1 : (byte) 0);
        dest.writeString(this.dateQuery);
        dest.writeInt(this.canInvite == null ? -1 : this.canInvite.ordinal());
        dest.writeInt(this.canSuggestTime == null ? -1 : this.canSuggestTime.ordinal());
    }

    private Event(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
        this.placeName = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.id = in.readLong();
        this.numUsersResponded = in.readInt();
        in.readTypedList(participants, User.CREATOR);
        in.readTypedList(invitations, Invitation.CREATOR);
        this.invitee = in.readParcelable(User.class.getClassLoader());
        in.readTypedList(inviteTerminList, InviteTermin.CREATOR);
        this.prominentTermin = in.readParcelable(InviteTermin.class.getClassLoader());
        this.imageUrl = in.readString();
        this.isAccepted = in.readByte() != 0;
        this.isSingleDay = in.readByte() != 0;
        this.dateQuery = in.readString();
        int tmpCanInvite = in.readInt();
        this.canInvite = tmpCanInvite == -1 ? null : CanInvite.values()[tmpCanInvite];
        int tmpCanSuggestTime = in.readInt();
        this.canSuggestTime = tmpCanSuggestTime == -1 ? null : CanSuggestTime.values()[tmpCanSuggestTime];
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}
