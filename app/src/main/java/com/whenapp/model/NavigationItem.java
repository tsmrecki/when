package com.whenapp.model;

/**
 * Created by tomislav on 18/04/15.
 */
public class NavigationItem {

    public String text;
    public int resourceId;
    public int selectedResourceId;

    public NavigationItem(String text, int resourceId, int selectedResourceId) {
        this.text = text;
        this.resourceId = resourceId;
        this.selectedResourceId = selectedResourceId;
    }

}
