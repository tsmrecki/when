package com.whenapp.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.whenapp.R;

/**
 * Created by tomislav on 02/05/15.
 */
public class ReminderManager {

    private static ReminderManager instance;

    private static final String SHARED_PREF_NAME = "reminder_manager";
    private SharedPreferences sessionPrefs;


    public static ReminderManager getInstance(Context context) {
        if(instance == null) instance = new ReminderManager(context);
        return instance;
    }

    private ReminderManager(Context context){
        if(context != null) {
            sessionPrefs = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        }
    }


    public void registerForChanges(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener){
        sessionPrefs.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    public void unregisterForChanges(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        sessionPrefs.unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    public void setReminderForEvent(long eventId, boolean enabled, ReminderInFront inFront) {
        if (enabled) {
            sessionPrefs.edit().putBoolean("reminder_for_" + String.valueOf(eventId), enabled).apply();
            sessionPrefs.edit().putString("reminder_in_front_" + String.valueOf(eventId), inFront.toString()).apply();
        } else {
            if(sessionPrefs.contains("reminder_for_" + String.valueOf(eventId)))
                sessionPrefs.edit().remove("reminder_for_" + String.valueOf(eventId)).apply();
            if (sessionPrefs.contains("reminder_in_front_" + String.valueOf(eventId))) {
                sessionPrefs.edit().remove("reminder_in_front_" + String.valueOf(eventId)).apply();
            }
        }
    }

    public boolean isReminderForEventEnabled(long eventId) {
        return sessionPrefs.getBoolean("reminder_for_" + String.valueOf(eventId), false);
    }

    public ReminderInFront remindsInFront(long eventId) {
        return ReminderInFront.valueOf(sessionPrefs.getString("reminder_in_front_" + String.valueOf(eventId), "ten_minutes"));
    }


    public enum ReminderInFront {
        off, ten_minutes, half_hour, hour, two_hours, day
    }

    public static String getReminderInFrontPretty(ReminderInFront inFront, Context context) {
        switch (inFront) {
            case off:
                return context.getString(R.string.off);
            case ten_minutes:
                return context.getString(R.string.ten_minutes);
            case half_hour:
                return context.getString(R.string.thirty_minutes);
            case hour:
                return context.getString(R.string.one_hour);
            case two_hours:
                return  context.getString(R.string.two_hours);
            case day:
                return context.getString(R.string.one_day);
            default: return "";
        }
    }

    public static String getReminderTextPretty(ReminderInFront inFront, Context context) {
        switch (inFront) {
            case ten_minutes:
                return context.getString(R.string.event_coming_up_in_minutes, 10);
            case half_hour:
                return context.getString(R.string.event_coming_up_in_minutes, 30);
            case hour:
                return context.getString(R.string.event_coming_up_in_hour);
            case two_hours:
                return context.getString(R.string.event_coming_up_in_two_hours);
            case day:
                return context.getString(R.string.event_coming_up_tomorrow);
            default:
                return "";
        }
    }


}
