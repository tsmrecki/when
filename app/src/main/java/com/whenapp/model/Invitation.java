package com.whenapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by tomislav on 04/05/15.
 */
public class Invitation implements Parcelable {

    long id;
    Event event;
    User caller;
    User callee;
    Status status;


    public Invitation(long id, Event event, User caller, User callee) {
        this.id = id;
        this.event = event;
        this.caller = caller;
        this.callee = callee;
    }


    public Invitation(JSONObject invitation){
        if(invitation == null) return;

        id = invitation.optLong("id");
        event = new Event(invitation.optJSONObject("event"));
        caller = new User(invitation.optJSONObject("caller"));
        callee = new User(invitation.optJSONObject("callee"));

        status = Status.valueOf(invitation.optString("status", "REJECTED"));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public User getCaller() {
        return caller;
    }

    public void setCaller(User caller) {
        this.caller = caller;
    }

    public User getCallee() {
        return callee;
    }

    public void setCallee(User callee) {
        this.callee = callee;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
            return "invitation " + id;
    }

    public enum Status {
        WAITING, REJECTED, ACCEPTED
    }

    public enum Response{
        DECLINED, ACCEPTED, ALREADY_ACCEPTED
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        //dest.writeParcelable(this.event, 0);
        dest.writeParcelable(this.caller, 0);
        dest.writeParcelable(this.callee, 0);
        dest.writeInt(this.status == null ? -1 : this.status.ordinal());
    }

    private Invitation(Parcel in) {
        this.id = in.readLong();
        //this.event = in.readParcelable(Event.class.getClassLoader());
        this.caller = in.readParcelable(User.class.getClassLoader());
        this.callee = in.readParcelable(User.class.getClassLoader());
        int tmpStatus = in.readInt();
        this.status = tmpStatus == -1 ? null : Status.values()[tmpStatus];
    }

    public static final Creator<Invitation> CREATOR = new Creator<Invitation>() {
        public Invitation createFromParcel(Parcel source) {
            return new Invitation(source);
        }

        public Invitation[] newArray(int size) {
            return new Invitation[size];
        }
    };
}
