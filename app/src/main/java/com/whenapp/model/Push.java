package com.whenapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by tomislav on 07/05/15.
 */
public class Push implements Parcelable {

    public static final String PUSH  = "com.whenapp.push";

    public long id;
    public Type type;

    public enum Type {
        INVITATION, EVENT, OTHER
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
    }

    public Push() {
    }

    private Push(Parcel in) {
        this.id = in.readLong();
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : Type.values()[tmpType];
    }

    public static final Parcelable.Creator<Push> CREATOR = new Parcelable.Creator<Push>() {
        public Push createFromParcel(Parcel source) {
            return new Push(source);
        }

        public Push[] newArray(int size) {
            return new Push[size];
        }
    };
}
