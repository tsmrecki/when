package com.whenapp.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.whenapp.connection.ApiHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Tomislav on 18.3.2015..
 */
public class User implements Parcelable {

    private long id = 0;
    private String name = "default";
    private String image = "";
    private String email = "";

    private Uri imageUri;

    private boolean isFriend = false;

    private List<User> friends = new ArrayList<>();
    private List<User> facebookFriends = new ArrayList<>();
    private List<UsersGroup> groups = new ArrayList<>();
    private List<Invitation> invitations = new ArrayList<>();
    private List<Event> events = new ArrayList<>();

    private int numOfCreatedEvents = 0;
    private int numOfAttendedEvents = 0;
    private int numOfFriends = 0;


    public User(String name, String image) {
        id = new Random().nextInt(100000);
        this.name = name;
        this.image = image;
    }

    public User(JSONObject user){
        if(user == null) return;

        id = user.optLong("id");
        email = user.optString("email", "default mail");

        name = user.optString("email");

        JSONObject additionalInfo = user.optJSONObject("additional_info");

        if(additionalInfo != null) {

            if(additionalInfo.optString("image_identifier") != null && !additionalInfo.optString("image_identifier").isEmpty() && !additionalInfo.optString("image_identifier").equals("null")){
                image = ApiHelper.BASE_PATH + additionalInfo.optString("image_identifier");
            }

            name = additionalInfo.optString("username", "default name");

            JSONArray friendsJ = additionalInfo.optJSONArray("friends");
            if (friendsJ != null)
                for (int i = 0; i < friendsJ.length(); i++) {
                    friends.add(new User(friendsJ.optJSONObject(i)));
                }
            numOfFriends = friends.size();

            JSONArray userGroupsJ = additionalInfo.optJSONArray("custom_lists");
            if (userGroupsJ != null)
                for (int i = 0; i < userGroupsJ.length(); i++) {
                    groups.add(new UsersGroup(userGroupsJ.optJSONObject(i)));
                }

            JSONArray createdEventsJ = additionalInfo.optJSONArray("created_events");
            if(createdEventsJ != null) numOfCreatedEvents = createdEventsJ.length();


            JSONArray facebookFriendsJ = additionalInfo.optJSONArray("facebook_friends");
            if (facebookFriendsJ != null) {
                for (int i = 0; i < facebookFriendsJ.length(); i++) {
                    facebookFriends.add(new User(facebookFriendsJ.optJSONObject(i)));
                }
            }
        }

        JSONArray invitationsJ = user.optJSONArray("received_invitations");
        if(invitationsJ != null)
        for(int i = 0; i<invitationsJ.length(); i++){
            invitations.add(new Invitation(invitationsJ.optJSONObject(i)));
        }

        JSONArray eventsJ = user.optJSONArray("actual_events");
        if (eventsJ != null) {
            for (int i = 0; i < eventsJ.length(); i++) {
                events.add(new Event(eventsJ.optJSONObject(i)));
            }
            numOfAttendedEvents = eventsJ.length();
        }



    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
        numOfFriends = friends.size();
    }

    public List<User> getFacebookFriends() {
        return facebookFriends;
    }

    public void setFacebookFriends(List<User> facebookFriends) {
        this.facebookFriends = facebookFriends;
    }

    public List<UsersGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<UsersGroup> groups) {
        this.groups = groups;
    }

    public List<Invitation> getInvitations() {
        return invitations;
    }

    public void setInvitations(List<Invitation> invitations) {
        this.invitations = invitations;
    }

    public boolean isFriend() {
        return isFriend;
    }

    public void setIsFriend(boolean isFriend) {
        this.isFriend = isFriend;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;
        return email.equals(user.email);

    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    public int getNumOfCreatedEvents() {
        return numOfCreatedEvents;
    }

    public int getNumOfAttendedEvents() {
        return numOfAttendedEvents;
    }

    public void setNumOfCreatedEvents(int numOfCreatedEvents) {
        this.numOfCreatedEvents = numOfCreatedEvents;
    }

    public void setNumOfAttendedEvents(int numOfAttendedEvents) {
        this.numOfAttendedEvents = numOfAttendedEvents;
    }

    public int getNumOfFriends() {
        return numOfFriends;
    }

    public void setNumOfFriends(int numOfFriends) {
        this.numOfFriends = numOfFriends;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.image);
        dest.writeString(this.email);
        dest.writeParcelable(this.imageUri, 0);
        dest.writeByte(isFriend ? (byte) 1 : (byte) 0);
        dest.writeTypedList(friends);
        dest.writeTypedList(facebookFriends);
        dest.writeTypedList(groups);
        dest.writeList(this.invitations);
        dest.writeTypedList(events);
        dest.writeInt(this.numOfCreatedEvents);
        dest.writeInt(this.numOfAttendedEvents);
        dest.writeInt(this.numOfFriends);
    }

    private User(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.image = in.readString();
        this.email = in.readString();
        this.imageUri = in.readParcelable(Uri.class.getClassLoader());
        this.isFriend = in.readByte() != 0;
        in.readTypedList(friends, User.CREATOR);
        in.readTypedList(facebookFriends, User.CREATOR);
        in.readTypedList(groups, UsersGroup.CREATOR);
        this.invitations = new ArrayList<Invitation>();
        in.readList(this.invitations, Invitation.class.getClassLoader());
        in.readTypedList(events, Event.CREATOR);
        this.numOfCreatedEvents = in.readInt();
        this.numOfAttendedEvents = in.readInt();
        this.numOfFriends = in.readInt();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
