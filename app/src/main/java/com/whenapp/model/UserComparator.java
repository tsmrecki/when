package com.whenapp.model;

import java.util.Comparator;

/**
 * Created by tomislav on 01/05/15.
 */
public class UserComparator implements Comparator<User> {
    @Override
    public int compare(User lhs, User rhs) {
        return lhs.getName().compareToIgnoreCase(rhs.getName());
    }
}
