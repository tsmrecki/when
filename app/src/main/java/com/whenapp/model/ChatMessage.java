package com.whenapp.model;

import com.layer.sdk.messaging.Message;

/**
 * Created by tomislav on 18/02/16.
 */
public class ChatMessage {

    public Message message;
    public User sender;
    public boolean isSent = false;
    public int messageStatusResource = 0;
}
