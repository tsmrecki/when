package com.whenapp.model;

import android.os.Parcel;

import java.util.ArrayList;

/**
 * Created by tomislav on 01/05/15.
 */
public class GroupContactToken extends ContactToken implements android.os.Parcelable {


    public GroupContactToken(UsersGroup usersGroup, String tokenName, String image) {
        super(tokenName, image);
        users = new ArrayList<>();
        for(User u : usersGroup.getUsers())
            users.add(u);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(users);
        dest.writeString(this.tokenName);
        dest.writeString(this.image);
    }

    private GroupContactToken(Parcel in) {
        super();
        in.readTypedList(users, User.CREATOR);
        this.tokenName = in.readString();
        this.image = in.readString();
    }

    public static final Creator<GroupContactToken> CREATOR = new Creator<GroupContactToken>() {
        public GroupContactToken createFromParcel(Parcel source) {
            return new GroupContactToken(source);
        }

        public GroupContactToken[] newArray(int size) {
            return new GroupContactToken[size];
        }
    };
}
