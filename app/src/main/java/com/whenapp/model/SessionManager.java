package com.whenapp.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by tomislav on 02/05/15.
 */
public class SessionManager {

    private static SessionManager instance;

    private static final String SHARED_PREF_NAME = "session_manager";
    private SharedPreferences sessionPrefs;
    private SharedPreferences lifetimeDataPreferences;

    public static final String INVITATIONS_NUMBER = "num_of_invitations";


    public static SessionManager getInstance(Context context) {
        if(instance == null) instance = new SessionManager(context);
        return instance;
    }

    private SessionManager(Context context){
        if(context != null) {
            sessionPrefs = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
            lifetimeDataPreferences = context.getSharedPreferences("lifetime_prefs", Context.MODE_PRIVATE);
        }
    }


    public void registerForChanges(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener){
        sessionPrefs.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    public void unregisterForChanges(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        sessionPrefs.unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }


    public void setAuthToken(String authToken){
        sessionPrefs.edit().putString("auth_token", authToken).apply();
    }

    public String getAuthToken(){
        return sessionPrefs.getString("auth_token", null);
    }


    public void setUser(User user){
        SharedPreferences.Editor editor = sessionPrefs.edit();
        editor.putString("user_name", user.getName());
        editor.putString("user_email", user.getEmail());
        editor.putString("user_image", user.getImage());
        editor.putLong("user_id", user.getId());
        editor.putInt("user_attended_events", user.getNumOfAttendedEvents());
        editor.putInt("user_created_events", user.getNumOfCreatedEvents());
        editor.putInt("user_num_friends", user.getNumOfFriends());
        editor.apply();
    }

    public User getUser(){
        User u = new User(sessionPrefs.getString("user_name", ""), sessionPrefs.getString("user_image", ""));
        u.setEmail(sessionPrefs.getString("user_email", ""));
        u.setId(sessionPrefs.getLong("user_id", 0));
        u.setNumOfAttendedEvents(sessionPrefs.getInt("user_attended_events", 0));
        u.setNumOfCreatedEvents(sessionPrefs.getInt("user_created_events", 0));
        u.setNumOfFriends(sessionPrefs.getInt("user_num_friends", 0));
        return u;
    }

    public void setIsLoggedIn(boolean isLoggedIn){
        sessionPrefs.edit().putBoolean("is_logged_in", isLoggedIn).apply();
    }



    public boolean isLoggedIn(){
        return sessionPrefs.getBoolean("is_logged_in", false);
    }


    public enum SocialLoginType {
        facebook
    }

    public boolean isSocialLoggedIn(SocialLoginType type) {
        return sessionPrefs.getBoolean("is_logged_in_" + type.toString(), false);
    }

    public void setIsSocialLoggedIn(SocialLoginType type, boolean isLoggedIn) {
        sessionPrefs.edit().putBoolean("is_logged_in_" + type.toString(), isLoggedIn).apply();
    }


    public void setNumOfInvitations(int number) {
        sessionPrefs.edit().putInt(INVITATIONS_NUMBER, number).apply();
    }

    public int getNumOfInvitations(){
        return sessionPrefs.getInt(INVITATIONS_NUMBER, 0);
    }

    public void logout(){
        sessionPrefs.edit().clear().commit();
    }

    public void setPassedTutorial(boolean passedTutorial){
        sessionPrefs.edit().putBoolean("passed_tutorial", passedTutorial).apply();
    }

    public boolean isPassedTutorial(){
        return sessionPrefs.getBoolean("passed_tutorial", false);
    }


    public boolean hasRatedApp() {
        return lifetimeDataPreferences.getBoolean("hasRatedApp", false);
    }

    public void setHasRatedApp() {
        lifetimeDataPreferences.edit().putBoolean("hasRatedApp", true).apply();
    }

    public void setLastTimeAskedForRating(long time) {
        lifetimeDataPreferences.edit().putLong("lastTimeAskedRating", time).apply();
    }

    public long getLastTimeAskedForRating() {
        return lifetimeDataPreferences.getLong("lastTimeAskedRating", 0);
    }

}
