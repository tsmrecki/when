package com.whenapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Tomislav on 27.3.2015..
 */
public class Day implements Parcelable {

    private List<Event> approvedEvents;
    private List<Event> pendingEvents;
    private Date date;

    public Day(Date date) {
        this.date = date;
        approvedEvents = new ArrayList<>();
        pendingEvents = new ArrayList<>();
    }

    public Day(Date date, List<Event> approvedEvents, List<Event> pendingEvents) {
        this.date = date;
        this.pendingEvents = pendingEvents;
        this.approvedEvents = approvedEvents;
    }

    public int getPendingEventsCount() {
        return pendingEvents.size();
    }

    public int getApprovedEventsCount() {
        return approvedEvents.size();
    }

    public List<Event> getApprovedEvents() {
        return approvedEvents;
    }

    public void setApprovedEvents(List<Event> approvedEvents) {
        this.approvedEvents = approvedEvents;
    }

    public List<Event> getPendingEvents() {
        return pendingEvents;
    }

    public void setPendingEvents(List<Event> pendingEvents) {
        this.pendingEvents = pendingEvents;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void addApprovedEvents(List<Event> approvedEvents) {
        for(Event e : approvedEvents) if(!this.approvedEvents.contains(e)) this.approvedEvents.add(e);
    }

    public void addPendingEvents(List<Event> pendingEvents) {
        for(Event e : pendingEvents) if(!this.pendingEvents.contains(e)) this.pendingEvents.add(e);
    }


    @Override
    public String toString() {
        return date.toString() + " approved " + approvedEvents + " " + " pending " + pendingEvents;
    }

    public Day copy() {
        List<Event> approved = new ArrayList<>();
        approved.addAll(this.approvedEvents);
        List<Event> pending = new ArrayList<>();
        pending.addAll(this.pendingEvents);
        Day d = new Day((Date) date.clone(), approved, pending);
        return d;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(approvedEvents);
        dest.writeTypedList(pendingEvents);
        dest.writeLong(date != null ? date.getTime() : -1);
    }

    private Day(Parcel in) {
        in.readTypedList(approvedEvents, Event.CREATOR);
        in.readTypedList(pendingEvents, Event.CREATOR);
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
    }

    public static final Parcelable.Creator<Day> CREATOR = new Parcelable.Creator<Day>() {
        public Day createFromParcel(Parcel source) {
            return new Day(source);
        }

        public Day[] newArray(int size) {
            return new Day[size];
        }
    };
}
