package com.whenapp.model;

import java.util.Comparator;

/**
 * Created by tomislav on 01/05/15.
 */
public class TerminComparator implements Comparator<Termin> {
    @Override
    public int compare(Termin lhs, Termin rhs) {
        if(lhs.getStart().before(rhs.getStart()))
            return -1;
        else if(lhs.getStart().after(rhs.getStart()))
            return 1;
        else return 0;
    }
}
