package com.whenapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tomislav on 01/05/15.
 */
public class ContactToken implements Parcelable {

    protected List<User> users;
    protected String tokenName;
    protected String image;

    public ContactToken(String tokenName, String image) {
        this.tokenName = tokenName;
        this.image = image;
    }

    public ContactToken() {

    }

    public String getTokenName() {
        return tokenName;
    }

    public void setTokenName(String tokenName) {
        this.tokenName = tokenName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String toString(){
        return tokenName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContactToken)) return false;

        ContactToken that = (ContactToken) o;

        if (!users.equals(that.users)) return false;
        if (!tokenName.equals(that.tokenName)) return false;
        return !(image != null ? !image.equals(that.image) : that.image != null);

    }

    @Override
    public int hashCode() {
        int result = users.hashCode();
        result = 31 * result + tokenName.hashCode();
        result = 31 * result + (image != null ? image.hashCode() : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(users);
        dest.writeString(this.tokenName);
        dest.writeString(this.image);
    }

    protected ContactToken(Parcel in) {
        in.readTypedList(users, User.CREATOR);
        this.tokenName = in.readString();
        this.image = in.readString();
    }

    public static final Creator<ContactToken> CREATOR = new Creator<ContactToken>() {
        public ContactToken createFromParcel(Parcel source) {
            return new ContactToken(source);
        }

        public ContactToken[] newArray(int size) {
            return new ContactToken[size];
        }
    };
}
