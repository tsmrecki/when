package com.whenapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Tomislav on 25.3.2015..
 */
public class Termin implements Parcelable {

    Calendar start = Calendar.getInstance();
    Calendar end = Calendar.getInstance();

    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;

    private int endYear;
    private int endMonth;
    private int endDay;
    private int endHour;
    private int endMinute;

    protected boolean allDay = false;

    public Termin(int year, int month, int day, int hour, int minute) {
        start = Calendar.getInstance();
        start.set(Calendar.YEAR, year);
        start.set(Calendar.MONTH, month);
        start.set(Calendar.DAY_OF_MONTH, day);
        start.set(Calendar.HOUR_OF_DAY, hour);
        start.set(Calendar.MINUTE, minute);

        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;

        end = (Calendar) start.clone();
        end.add(Calendar.HOUR_OF_DAY, 1);

        endYear = end.get(Calendar.YEAR);
        endMonth = end.get(Calendar.MONTH);

        endDay = end.get(Calendar.DAY_OF_MONTH);
        endHour = end.get(Calendar.HOUR_OF_DAY);
        endMinute = end.get(Calendar.MINUTE);
    }

    public Termin(Calendar start, Calendar end){
        setStart(start);
        setEnd(end);
    }


    public void setEndTime(int year, int month, int day, int hour, int minute) {
        setEndYear(year);
        setEndMonth(month);
        setEndDay(day);
        setEndHour(hour);
        setEndMinute(minute);
    }

    public void setStart(Calendar start){
        this.start = start;
        year = start.get(Calendar.YEAR);
        month = start.get(Calendar.MONTH);
        day = start.get(Calendar.DAY_OF_MONTH);
        hour = start.get(Calendar.HOUR_OF_DAY);
        minute = start.get(Calendar.MINUTE);
    }
    
    
    public Calendar getStart() {
        return start;
    }

    
    public void setEnd(Calendar end){
        this.end = end;
        endYear = end.get(Calendar.YEAR);
        endMonth = end.get(Calendar.MONTH);
        endDay = end.get(Calendar.DAY_OF_MONTH);
        endHour = end.get(Calendar.HOUR_OF_DAY);
        endMinute = end.get(Calendar.MINUTE);
    }

    public Calendar getEnd() {
        return end;
    }

    public String getStartTime() {
        DateFormat df = DateFormat.getTimeInstance(DateFormat.SHORT);
        return df.format(start.getTime());
    }

    public String getEndTime() {
        DateFormat df = DateFormat.getTimeInstance(DateFormat.SHORT);
        return df.format(end.getTime());
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
        start.set(Calendar.YEAR, year);
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
        start.set(Calendar.MONTH, year);
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
        start.set(Calendar.DAY_OF_MONTH, year);
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
        start.set(Calendar.HOUR_OF_DAY, year);
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
        start.set(Calendar.MINUTE, year);
    }

    public boolean isAllDay() {
        return allDay;
    }

    public void setAllDay(boolean allDay) {
        this.allDay = allDay;
        if(allDay) {
            start.set(Calendar.HOUR_OF_DAY, 0);
            start.set(Calendar.MINUTE, 0);
        }
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
        end.set(Calendar.YEAR, endYear);
    }

    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
        end.set(Calendar.DAY_OF_MONTH, endDay);
    }

    public int getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(int endMonth) {
        this.endMonth = endMonth;
        end.set(Calendar.MONTH, endMonth);
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
        end.set(Calendar.HOUR_OF_DAY, endHour);
    }

    public int getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(int endMinute) {
        this.endMinute = endMinute;
        end.set(Calendar.MINUTE, endMinute);
    }

    public String getStartWeekDay() {
        return start.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
    }

    public String getEndWeekDay() {
        return end.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
    }

    public String toString() {
        return start.getTime().toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Termin)) return false;

        Termin termin = (Termin) o;

        if (year != termin.year) return false;
        if (month != termin.month) return false;
        if (day != termin.day) return false;
        if (hour != termin.hour) return false;
        if (minute != termin.minute) return false;
        if (endYear != termin.endYear) return false;
        if (endMonth != termin.endMonth) return false;
        if (endDay != termin.endDay) return false;
        if (endHour != termin.endHour) return false;
        if (endMinute != termin.endMinute) return false;
        return allDay == termin.allDay;

    }

    @Override
    public int hashCode() {
        int result = year;
        result = 31 * result + month;
        result = 31 * result + day;
        result = 31 * result + hour;
        result = 31 * result + minute;
        result = 31 * result + endYear;
        result = 31 * result + endMonth;
        result = 31 * result + endDay;
        result = 31 * result + endHour;
        result = 31 * result + endMinute;
        result = 31 * result + (allDay ? 1 : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.start);
        dest.writeSerializable(this.end);
        dest.writeInt(this.year);
        dest.writeInt(this.month);
        dest.writeInt(this.day);
        dest.writeInt(this.hour);
        dest.writeInt(this.minute);
        dest.writeInt(this.endYear);
        dest.writeInt(this.endMonth);
        dest.writeInt(this.endDay);
        dest.writeInt(this.endHour);
        dest.writeInt(this.endMinute);
        dest.writeByte(allDay ? (byte) 1 : (byte) 0);
    }

    protected Termin(Parcel in) {
        this.start = (Calendar) in.readSerializable();
        this.end = (Calendar) in.readSerializable();
        this.year = in.readInt();
        this.month = in.readInt();
        this.day = in.readInt();
        this.hour = in.readInt();
        this.minute = in.readInt();
        this.endYear = in.readInt();
        this.endMonth = in.readInt();
        this.endDay = in.readInt();
        this.endHour = in.readInt();
        this.endMinute = in.readInt();
        this.allDay = in.readByte() != 0;
    }

    public static final Creator<Termin> CREATOR = new Creator<Termin>() {
        public Termin createFromParcel(Parcel source) {
            return new Termin(source);
        }

        public Termin[] newArray(int size) {
            return new Termin[size];
        }
    };
}
