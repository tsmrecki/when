package com.whenapp.model;

import java.util.Comparator;

/**
 * Created by tomislav on 01/05/15.
 */
public class UserInvitesComparator implements Comparator<Invitation> {
    @Override
    public int compare(Invitation lhs, Invitation rhs) {
        if(lhs.getStatus() == Invitation.Status.ACCEPTED && (rhs.getStatus() == Invitation.Status.WAITING))
            return -1;
        else if(lhs.getStatus() == Invitation.Status.WAITING && rhs.getStatus() == Invitation.Status.ACCEPTED)
            return 1;
        else return  0;
    }
}
