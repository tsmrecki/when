package com.whenapp;

import android.app.IntentService;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

import com.whenapp.common.Utils;
import com.whenapp.connection.ApiHelper;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.EventsDataLoader;
import com.whenapp.database.EventsTable;
import com.whenapp.database.InvitesEventsDataLoader;
import com.whenapp.database.InvitesEventsTable;
import com.whenapp.database.MyInvitationsDataLoader;
import com.whenapp.database.MyInvitationsTable;
import com.whenapp.model.Event;
import com.whenapp.model.Invitation;
import com.whenapp.model.ReminderManager;
import com.whenapp.model.SessionManager;
import com.whenapp.model.User;

import java.util.Calendar;

/**
 * Created by tomislav on 02/05/15.
 */
public class RefreshService extends IntentService {

    public static final String REFRESH_TYPE = "refresh_type";
    public static final String SUBTYPE = "subtype";

    EventsDataLoader edl;
    InvitesEventsDataLoader invitesEventsDataLoader;
    MyInvitationsDataLoader iedl;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public RefreshService(String name) {
        super(name);
    }

    public RefreshService() {
        super("RefreshService");

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RefreshType type = RefreshType.all;
        if(intent.getStringExtra("refresh_type") != null)
                type = RefreshType.valueOf(intent.getStringExtra("refresh_type"));

        if(type == RefreshType.all) {

            final SQLiteDatabase database = CalendarDatabaseHelper.getInstance(getApplicationContext()).getWritableDatabase();
            edl = new EventsDataLoader(getApplicationContext(), new EventsTable(database), null, null, null, null, null);
            iedl = new MyInvitationsDataLoader(getApplicationContext(), new MyInvitationsTable(database), null, null, null, null, null);
            invitesEventsDataLoader = new InvitesEventsDataLoader(getApplicationContext(), new InvitesEventsTable(database), null, null, null, null, null);

            ApiHelper.getInstance(getApplicationContext()).getUser(SessionManager.getInstance(getApplicationContext()).getUser().getId(), new ApiHelper.OnGetUserListener() {
                @Override
                public void onGetUser(User me) {

                    int totalInvites = 0;
                    for (Invitation invitation : me.getInvitations()) {
                        if (invitation.getStatus() == Invitation.Status.WAITING) totalInvites++;
                        ApiHelper.getInstance(getApplicationContext()).getEvent(invitation.getEvent().getId(), new ApiHelper.OnGetEventListener() {
                            @Override
                            public void onEvent(Event event) {
                                invitesEventsDataLoader.insert(event);
                            }

                            @Override
                            public void onFail(String error) {
                            }
                        });
                        //events.add(invitation.getEvent());
                    }
                    SessionManager.getInstance(getApplicationContext()).setNumOfInvitations(totalInvites);

                    iedl.deleteAll();
                    iedl.insert(me.getInvitations());

                    edl.deleteAll();
                    edl.insert(me.getEvents());

                }

                @Override
                public void onFail(String errorMessage) {
                }
            });
        }

        else if(type == RefreshType.invitation){
            final SQLiteDatabase database = CalendarDatabaseHelper.getInstance(getApplicationContext()).getWritableDatabase();
            invitesEventsDataLoader = new InvitesEventsDataLoader(getApplicationContext(), new InvitesEventsTable(database), null, null, null, null, null);
            iedl = new MyInvitationsDataLoader(getApplicationContext(), new MyInvitationsTable(database), null, null, null, null, null);

            final long eventId = intent.getLongExtra("eventId", 0);
            long invitationId = intent.getLongExtra("invitationId", 0);

            ApiHelper.getInstance(getApplicationContext()).getInvitation(eventId, invitationId, new ApiHelper.OnGetInvitationListener() {
                @Override
                public void onInvitation(Invitation invitation) {
                    iedl.insert(invitation);
                    ApiHelper.getInstance(getApplicationContext()).getEvent(eventId, new ApiHelper.OnGetEventListener() {
                        @Override
                        public void onEvent(Event event) {
                            invitesEventsDataLoader.insert(event);
                        }

                        @Override
                        public void onFail(String error) {

                        }
                    });
                }

                @Override
                public void onFail(String error) {

                }
            });


        }else if (type == RefreshType.event) {
            final SQLiteDatabase database = CalendarDatabaseHelper.getInstance(getApplicationContext()).getWritableDatabase();
            edl = new EventsDataLoader(getApplicationContext(), new EventsTable(database), null, null, null, null, null);

            final long eventId = intent.getLongExtra("eventId", 0);
            final String subtype = intent.getStringExtra(SUBTYPE);

            ApiHelper.getInstance(getApplicationContext()).getEvent(eventId, new ApiHelper.OnGetEventListener() {
                @Override
                public void onEvent(Event event) {
                    edl.insert(event);

                    if (subtype.equals("EventTimeDecided")) {
                        Utils.setEventAlarm(getApplicationContext(), event.getProminentTermin().getTermin().getStart().getTimeInMillis(), ReminderManager.ReminderInFront.ten_minutes, event.getId(), event.getName(), event.getDescription(), event.getImageUrl());
                    }
                }

                @Override
                public void onFail(String error) {

                }
            });
        }
    }

    public enum RefreshType {
        invitation, event, all
    }
}
