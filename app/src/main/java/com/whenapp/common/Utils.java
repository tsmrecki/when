package com.whenapp.common;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.whenapp.OnReminderReceiver;
import com.whenapp.model.ReminderManager;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by tomislav on 18/09/15.
 */
public class Utils {

    public static void hideKeyboardOnFocusLost(final EditText... editTexts) {
        final InputMethodManager inputMethodManager =(InputMethodManager) editTexts[0].getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);

        for(final EditText e : editTexts)
            e.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        inputMethodManager.hideSoftInputFromWindow(e.getWindowToken(), 0);
                    }
                }
            });
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    public static void setEventAlarm(Context context, long eventTime, ReminderManager.ReminderInFront reminderInFront, Long eventId, String eventName, String description, String imageUrl){

        AlarmManager mgr = (AlarmManager)context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context.getApplicationContext(), OnReminderReceiver.class);
        i.putExtra("title", eventName);
        i.putExtra("description", description);
        i.putExtra("image", imageUrl);
        i.putExtra("event_id", eventId);

        PendingIntent pi = PendingIntent.getBroadcast(context.getApplicationContext(), eventId.intValue(), i, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar myCal = Calendar.getInstance();

        myCal.setTimeInMillis(eventTime - getMillis(reminderInFront));
        mgr.set(AlarmManager.RTC_WAKEUP, myCal.getTimeInMillis(), pi);

        ReminderManager.getInstance(context.getApplicationContext()).setReminderForEvent(eventId, true, reminderInFront);
    }

    public static void removeEventAlarm(Context context, Long eventId) {
        Intent i = new Intent(context.getApplicationContext(), OnReminderReceiver.class);

        i.putExtra("event_id", eventId);

        PendingIntent pi = PendingIntent.getBroadcast(context.getApplicationContext(), eventId.intValue(), i, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.cancel(pi);

        ReminderManager.getInstance(context).setReminderForEvent(eventId, false, null);
    }


    public static long getMillis(ReminderManager.ReminderInFront reminderInFront) {
        switch (reminderInFront) {
            case off:
                return -10000000;
            case ten_minutes:
                return 10 * 60 * 1000;
            case half_hour:
                return 30 * 60 * 1000;
            case hour:
                return 60 * 60 * 1000;
            case two_hours:
                return 120 * 60 * 1000;
            case day:
                return 24 * 60 * 60 * 1000;
            default:
                return 10 * 60 * 1000;
        }
    }
}
