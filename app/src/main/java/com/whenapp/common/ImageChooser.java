package com.whenapp.common;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by tomislav on 16/04/15.
 */
public class ImageChooser {

    private Context context;
    private int YOUR_SELECT_PICTURE_REQUEST_CODE;
    Uri outputFileUri;

    private String fname;

    public ImageChooser(Context context, int YOUR_SELECT_PICTURE_REQUEST_CODE) {
        this.context = context;
        this.YOUR_SELECT_PICTURE_REQUEST_CODE = YOUR_SELECT_PICTURE_REQUEST_CODE;
    }


    public Intent buildChooseImageIntent() {

        // Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "When" + File.separator);
        root.mkdirs();
        fname = "When_" + Calendar.getInstance().getTime().toString().replace(':', '_').replace('/', '_').replace(' ', '_') + ".jpg";
        final File sdImageMainDirectory = new File(root, fname);

        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = context.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_PICK);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        return chooserIntent;
    }


    public Uri onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == YOUR_SELECT_PICTURE_REQUEST_CODE) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                Uri selectedImageUri;
                if (isCamera) {
                    selectedImageUri = outputFileUri;
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                }
                return selectedImageUri;
            }
        }
        return null;
    }

    public void saveInstance(Bundle outState){
        outState.putString("file_name", fname);
        outState.putInt("request_code", YOUR_SELECT_PICTURE_REQUEST_CODE);
    }

    public ImageChooser(Context context, Bundle savedInstanceState){
        this.context = context;
        this.YOUR_SELECT_PICTURE_REQUEST_CODE = savedInstanceState.getInt("request_code");
        String fileName = savedInstanceState.getString("file_name");

        if(fileName == null) return;
        // Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "When" + File.separator);
        final File sdImageMainDirectory = new File(root, fileName);


        outputFileUri = Uri.fromFile(sdImageMainDirectory);

    }

}
