package com.whenapp.common;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Base64;

import com.crashlytics.android.Crashlytics;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Image tools Encoding the Bitmap file into base64 String, and decoding from
 * base64 String to Bitmap file
 * 
 * @author Tomislav
 * 
 */
public class ImageCoder {

	public static int IMAGE_MAX_SIZE = 500000; // 0.5MB
	public static int IMAGE_MAX_WH = 900;

	/**
	 * Encodes Bitmap file into a base64 String
	 * 
	 * @param image
	 * @return String representative of Bitmap file
	 */
	public static String encodeTobase64(Bitmap image) {

        int img_height = image.getHeight();
        int img_width = image.getWidth();
        float scaleFactor = Math.max((float)img_height / IMAGE_MAX_WH, (float)img_width
                / IMAGE_MAX_WH);

        if(img_height > IMAGE_MAX_WH || img_width > IMAGE_MAX_WH) {
            image = Bitmap.createScaledBitmap(image, (int) (img_width / scaleFactor), (int) (img_height / scaleFactor), true);
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 90, stream); //compress to which format you want.
        byte [] byte_arr = stream.toByteArray();

        return Base64.encodeToString(byte_arr, Base64.DEFAULT);
	}

	/**
	 * Encodes Bitmap file into a base64 String
	 *
	 * @param image
	 * @return String representative of Bitmap file
	 */
	public static String encodeTobase64Rotate(Bitmap image, int rotationInDegrees) {

		int img_height = image.getHeight();
		int img_width = image.getWidth();
		float scaleFactor = Math.max((float) img_height / IMAGE_MAX_WH, (float) img_width
				/ IMAGE_MAX_WH);

		if(img_height > IMAGE_MAX_WH || img_width > IMAGE_MAX_WH) {
			image = Bitmap.createScaledBitmap(image, (int) (img_width / scaleFactor), (int) (img_height / scaleFactor), true);
		}


		Matrix matrix = new Matrix();

			matrix.preRotate(rotationInDegrees);
			image = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);


		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.JPEG, 90, stream); //compress to which format you want.
		byte [] byte_arr = stream.toByteArray();

		return Base64.encodeToString(byte_arr, Base64.DEFAULT);
	}


    public static String encodeTobase64(Bitmap image, int quality) {
        int img_height = image.getHeight();
        int img_width = image.getWidth();
        float scaleFactor = Math.max((float)img_height / IMAGE_MAX_WH,(float)img_width
                / IMAGE_MAX_WH);

        if(img_height > IMAGE_MAX_WH || img_width > IMAGE_MAX_WH) {
            image = Bitmap.createScaledBitmap(image, (int) (img_width / scaleFactor), (int) (img_height / scaleFactor), true);
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, quality, stream); //compress to which format you want.
        byte [] byte_arr = stream.toByteArray();
        return Base64.encodeToString(byte_arr, Base64.DEFAULT);
    }

	/**
	 * Decodes Bitmap file from String input
	 * 
	 * @param input
	 * @return Bitmap file
	 */
	public static Bitmap decodeBase64(String input) throws OutOfMemoryError {
		byte[] decodedByte = Base64.decode(input, Base64.DEFAULT);
		BitmapFactory.Options opt = new BitmapFactory.Options();
		return BitmapFactory.decodeByteArray(decodedByte, 0,
				decodedByte.length, opt);

	}

	/**
	 * Decodes Bitmap file from String input
	 * 
	 * @param input
	 * @return Bitmap file
	 */
	public static Bitmap decodeBase64(String input, int scale)
			throws OutOfMemoryError {
		byte[] decodedByte = Base64.decode(input, Base64.DEFAULT);
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = scale;
		return BitmapFactory.decodeByteArray(decodedByte, 0,
				decodedByte.length, options);

	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public static class EncodeAsync extends AsyncTask<Void, Void, String> {

		private Uri uri;
		private Activity context;
		public EncodeAsync(Uri imageUri, Activity context){
			uri = imageUri;
			this.context = context;
		}
		@Override
		protected String doInBackground(Void... params) {

            int degree;
			int orientation = 0;
			orientation = getOrientation(context, uri);
            degree = orientation;
            if(orientation == -1){
                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(getRealPathFromURI(context, uri));
                    orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                    degree = exifToDegrees(orientation);
                } catch (Exception e) {
                    Crashlytics.logException(e);
                    e.printStackTrace();
                }
            }

			Bitmap bitmap = null;
			try {
				bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);

			} catch (IOException e) {
				e.printStackTrace();
			}



			if(degree == 0)
				return encodeTobase64(bitmap);
			else
				return encodeTobase64Rotate(bitmap, degree);
		}
	}

	private static int exifToDegrees(int exifOrientation) {
		if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
		else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
		else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
		return 0;
	}



    /*
	public static String getRealPathFromURI(Context context, Uri uri)
	{
		String fileName="unknown";//default fileName
		Uri filePathUri = uri;
		if (uri.getScheme().compareTo("content")==0)
		{
			Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
			if (cursor.moveToFirst())
			{
				int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//Instead of "MediaStore.Images.Media.DATA" can be used "_data"
				filePathUri = Uri.parse(cursor.getString(column_index));
				fileName = filePathUri.getPath();
			}
            cursor.close();
		}
		else if (uri.getScheme().compareTo("file")==0)
		{
			fileName = filePathUri.getLastPathSegment();
		}
		else
		{
			fileName = fileName+"_"+filePathUri.getLastPathSegment();
		}

		return fileName;
	}

*/


	private static String getRealPathFromURI(final Activity context, Uri contentURI) {
		String result = "";
		Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
		if (cursor == null) { // Source is Dropbox or other similar local file path
			result = contentURI.getPath();
		} else {
			cursor.moveToFirst();
			try {
                int idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				result = cursor.getString(idx);
			} catch (IllegalStateException | IllegalArgumentException iex) {
                Crashlytics.logException(iex);
			}
			cursor.close();
		}
		return result;
	}


    public static int getOrientation(Context context, Uri photoUri) {
        /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[] { MediaStore.Images.ImageColumns.ORIENTATION }, null, null, null);


        if (cursor == null || cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        Integer rotation = cursor.getInt(0);
        cursor.close();
        return rotation;
    }

}
