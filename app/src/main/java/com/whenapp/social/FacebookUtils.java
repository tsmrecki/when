package com.whenapp.social;

import android.content.Context;
import android.content.SharedPreferences;

import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.entities.User;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnLogoutListener;
import com.sromku.simple.fb.listeners.OnNewPermissionsListener;
import com.whenapp.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by tomislav on 22/10/15.
 */
public class FacebookUtils {

    private static Permission[] permissions = new Permission[] {
            Permission.EMAIL,
            Permission.USER_FRIENDS
    };

    public static void setupConfig(Context context) {
        SharedPreferences fbPrefs = context.getSharedPreferences("facebook", Context.MODE_PRIVATE);
        boolean isFbConfigured = fbPrefs.getBoolean("isbasicconfigured", false);

        if(!isFbConfigured) {
            SimpleFacebookConfiguration config = new SimpleFacebookConfiguration.Builder()
                    .setAppId(context.getString(R.string.facebook_app_id))
                    .setNamespace("appwhen")
                    .setPermissions(permissions)
                    .build();

            SimpleFacebook.setConfiguration(config);

            fbPrefs.edit().putBoolean("isbasicconfigured", true).apply();
       }
    }


    public static void login(Context context, final SimpleFacebook facebook, final OnFacebookLoginListener onFacebookLoginListener) {

        if(facebook.isLogin()) facebook.logout(new OnLogoutListener() {
            @Override
            public void onLogout() {

            }
        });

        facebook.login(new OnLoginListener() {
            @Override
            public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
                onFacebookLoginListener.onFacebookLogin(accessToken);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onException(Throwable throwable) {
                onFacebookLoginListener.onFacebookLoginFailed(throwable.getMessage());
            }

            @Override
            public void onFail(String reason) {
                onFacebookLoginListener.onFacebookLoginFailed(reason);
            }
        });
    }


    public interface OnFacebookLoginListener {
        void onFacebookLogin(String accessToken);
        void onFacebookLoginFailed(String errorMessage);
    }

}
