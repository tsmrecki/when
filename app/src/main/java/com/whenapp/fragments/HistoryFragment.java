package com.whenapp.fragments;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.whenapp.CreateEventActivity;
import com.whenapp.EventActivity;
import com.whenapp.MainActivity;
import com.whenapp.R;
import com.whenapp.adapters.EventsAdapter;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.EventsDataLoader;
import com.whenapp.database.EventsTable;
import com.whenapp.model.Event;
import com.whenapp.model.SessionManager;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by Tomislav on 18.3.2015..
 */
public class HistoryFragment extends Fragment implements MonthFragment.OnDateClickedListener,
        LoaderManager.LoaderCallbacks<List<Event>> {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private TextView dateView;
    private ProgressBar progress;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static HistoryFragment newInstance(int sectionNumber) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public HistoryFragment() {
    }


    public static final String ACCOUNT_TYPE = "when.com";
    // Account
    // Instance fields
    Account mAccount;
    EventsAdapter eventsAdapter;
    EventsDataLoader edv;
    View calendar;

    float movement = 0;
    float initMovement = 0;
        boolean fabVisible = false;

    SessionManager sessionManager;

    private String dateQuery = "";

    Calendar c;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setRetainInstance(true);
        eventsAdapter = new EventsAdapter(getActivity());
        eventsAdapter.setHistory(true);

        eventsAdapter.setOnEventClickedListener(new EventsAdapter.OnEventClickedListener() {
            @Override
            public void onEventClicked(View view, int position) {

                Intent intent = new Intent(getActivity(), EventActivity.class);
                intent.putExtra("event", eventsAdapter.getEvent(position));

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ImageView image = (ImageView) view.findViewById(R.id.event_photo);
                    TextView eventName = (TextView) view.findViewById(R.id.event_name);
                    TextView eventTime = (TextView) view.findViewById(R.id.event_time_info);
                    TextView eventSummary = (TextView) view.findViewById(R.id.event_summary);
                    RecyclerView people = (RecyclerView) view.findViewById(R.id.people_recycler_view);
                    View imageGradient = view.findViewById(R.id.image_gradient);

                    if (image != null) {

                        getActivity().startActivityForResult(intent, MainActivity.GLOBAL_REFRESH, ActivityOptions.makeSceneTransitionAnimation(getActivity(), new Pair<View, String>(image, "eventImage"), new Pair<View, String>(eventName, "eventName"), new Pair<View, String>(eventTime, "eventTime"), new Pair<View, String>(people, "people"), new Pair<View, String>(eventSummary, "eventInvitee"), new Pair<View, String>(imageGradient, "imageGradient")).toBundle());
                    } else {
                        getActivity().startActivityForResult(intent, MainActivity.GLOBAL_REFRESH, ActivityOptions.makeSceneTransitionAnimation(getActivity(), new Pair<View, String>(eventName, "eventName"), new Pair<View, String>(eventTime, "eventTime"), new Pair<View, String>(people, "people"), new Pair<View, String>(eventSummary, "eventInvitee")).toBundle());
                    }
                } else {
                    getActivity().startActivityForResult(intent, MainActivity.GLOBAL_REFRESH);
                }
            }
        });

        setHasOptionsMenu(true);

        c = Calendar.getInstance();
        dateQuery = "";
        eventsAdapter.setQuery(dateQuery);

    }

    View emptyEventsCard;
    TextView emptyEventsText;
    Button emptyEventsCreateEvents;


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_events, container, false);
        dateView = (TextView) rootView.findViewById(R.id.date_view);
        dateView.setText("All events");
        //setupCalendar(rootView);

        progress = (ProgressBar) rootView.findViewById(R.id.progress);

        edv = (EventsDataLoader) getLoaderManager().initLoader(20, null, this);

        //mAccount = CreateSyncAccount(getActivity());

        emptyEventsCard = rootView.findViewById(R.id.empty_events_message);
        emptyEventsText = (TextView) emptyEventsCard.findViewById(R.id.no_events_message);
        emptyEventsCreateEvents = (Button) emptyEventsCard.findViewById(R.id.create_event);

        DateFormat df = DateFormat.getDateInstance();
        emptyEventsText.setText(getString(R.string.no_events_message, df.format(c.getTime())));


        AccountManager accountManager =
                (AccountManager) getActivity().getSystemService(
                        Context.ACCOUNT_SERVICE);


            // Pass the settings flags by inserting them in a bundle
            Bundle settingsBundle = new Bundle();
            settingsBundle.putBoolean(
                    ContentResolver.SYNC_EXTRAS_MANUAL, true);
            settingsBundle.putBoolean(
                    ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        /*
         * Request the sync for the default account, authority, and
         * manual sync settings
         */
        //ContentResolver.requestSync(mAccount, EventsContentProvider.AUTHORITY, settingsBundle);

        calendar = rootView.findViewById(R.id.calendar);


        final float density = getResources().getDisplayMetrics().density;
        RecyclerView eventsRecyclerView = (RecyclerView) rootView.findViewById(R.id.events_recycler_view);
        eventsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()) {
            @Override
            protected int getExtraLayoutSpace(RecyclerView.State state) {
                return (int) (300 * density);
            }

            @Override
            public int getPaddingTop() {
                return (int) (8 * density);
            }
        });
        eventsRecyclerView.setAdapter(eventsAdapter);
        eventsRecyclerView.setItemAnimator(new DefaultItemAnimator());


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Transition tran = TransitionInflater.from(getActivity()).inflateTransition(R.transition.event_open_transition);
            getActivity().getWindow().setExitTransition(tran);
        }



        final FloatingActionsMenu fabMenu = (FloatingActionsMenu) rootView.findViewById(R.id.fab_menu);

        FloatingActionButton createSingleDayEvent = (FloatingActionButton) rootView.findViewById(R.id.create_single_day_event);
        createSingleDayEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createEvent = new Intent(getActivity(), CreateEventActivity.class);
                createEvent.putExtra(CreateEventActivity.IS_SINGLE_DAY, true);
                startActivityForResult(createEvent, MainActivity.GLOBAL_REFRESH);
                fabMenu.collapse();
            }
        });

        FloatingActionButton createMultiDayEvent = (FloatingActionButton) rootView.findViewById(R.id.create_multi_day_event);
        createMultiDayEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createEvent = new Intent(getActivity(), CreateEventActivity.class);
                createEvent.putExtra(CreateEventActivity.IS_SINGLE_DAY, false);
                startActivityForResult(createEvent, MainActivity.GLOBAL_REFRESH);
                fabMenu.collapse();
            }
        });

        final View fabBack = rootView.findViewById(R.id.fab_back);

        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                fabBack.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fabBack.setVisibility(View.VISIBLE);
                    }
                },200);
            }

            @Override
            public void onMenuCollapsed() {
                fabBack.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fabBack.setVisibility(View.GONE);
                    }
                },200);
            }
        });

        emptyEventsCreateEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fabMenu.expand();
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_calendar:

                final int height = calendar.getHeight();
                Animator animatorHide = ObjectAnimator.ofFloat(calendar, "translationY", 0, -height);
                animatorHide.setDuration(300);
                animatorHide.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        calendar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                final Animator animationShow = ObjectAnimator.ofFloat(calendar, "translationY", -height, 0);
                animationShow.setDuration(300);
                animationShow.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                if (calendar.getVisibility() == View.VISIBLE) {
                    animatorHide.start();
                } else {
                    calendar.setVisibility(View.VISIBLE);
                    calendar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            animationShow.start();
                        }
                    }, 200);
                }
                return true;
            case R.id.action_show_all:
                dateQuery = "";
                dateView.setText("All events");
                eventsAdapter.setQuery("");
                if(eventsAdapter.getVisibleCount() > 0)
                    emptyEventsCard.setVisibility(View.GONE);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    TextView invitesCountView;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.history, menu);

    }


    private void setupCalendar(View rootView) {
        CalendarFragment calendarFragment = SingleSelectionCalendarFragment.newInstance(this, null);
        FragmentTransaction t = getFragmentManager().beginTransaction();
        t.replace(R.id.calendar, calendarFragment);
        t.commit();

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDateClicked(Date date) {
        dateView.setText(DateFormat.getDateInstance(DateFormat.LONG).format(date));

        c.setTime(date);
        dateQuery = "x" + c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH) + "-" + c.get(Calendar.DAY_OF_MONTH) + "x";

        eventsAdapter.setQuery(dateQuery);

        DateFormat df = DateFormat.getDateInstance();

        if(eventsAdapter.getVisibleCount() == 0) {
            emptyEventsText.setText(getString(R.string.no_events_message, df.format(c.getTime())));
            emptyEventsCard.setVisibility(View.VISIBLE);
        }
        else emptyEventsCard.setVisibility(View.GONE);
    }

    SQLiteDatabase database;

    @Override
    public Loader<List<Event>> onCreateLoader(int id, Bundle args) {
        database = CalendarDatabaseHelper.getInstance(getActivity()).getWritableDatabase();
        EventsTable usersTable = new EventsTable(database);

        EventsDataLoader eventsDataLoader = new EventsDataLoader(getActivity(), usersTable, null, null, null, null, null);
        return eventsDataLoader;
    }

    @Override
    public void onLoadFinished(Loader<List<Event>> loader, List<Event> data) {

        if(progress != null) progress.setVisibility(View.GONE);
        eventsAdapter.setEvents(data, dateQuery);

    }

    @Override
    public void onLoaderReset(Loader<List<Event>> loader) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
