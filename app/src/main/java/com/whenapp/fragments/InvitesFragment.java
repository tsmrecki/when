package com.whenapp.fragments;


import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.whenapp.EventActivity;
import com.whenapp.MainActivity;
import com.whenapp.R;
import com.whenapp.adapters.InvitesAdapter;
import com.whenapp.connection.ApiHelper;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.InvitesEventsDataLoader;
import com.whenapp.database.InvitesEventsTable;
import com.whenapp.database.MyInvitationsDataLoader;
import com.whenapp.database.MyInvitationsTable;
import com.whenapp.model.Event;
import com.whenapp.model.Invitation;
import com.whenapp.model.SessionManager;
import com.whenapp.model.User;
import com.whenapp.views.DialogBuilder;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InvitesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InvitesFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Invitation>> {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SECTION_NUMBER = "section_number";

    public static int GLOBAL_INVITES_LOADER_ID = 38;

    InvitesAdapter invitesAdapter;
    View emptyMessage;
    View progress;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment InvitesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InvitesFragment newInstance(int sectionNumber) {
        InvitesFragment fragment = new InvitesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public InvitesFragment() {
        // Required empty public constructor
    }


    MyInvitationsDataLoader dataLoader;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (getArguments() != null) {
        }
        dataLoader = (MyInvitationsDataLoader) getLoaderManager().initLoader(GLOBAL_INVITES_LOADER_ID, null, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.invitations, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_events:
                ((MainActivity) getActivity()).getNavigationDrawerFragment().selectItem(0);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_invites, container, false);

        RecyclerView invitesRecyclerView = (RecyclerView) rootView.findViewById(R.id.invites_recycler_view);
        invitesAdapter = new InvitesAdapter(getActivity(), onInviteClickListener);
        invitesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()){
            @Override
            public int getPaddingTop() {
                return (int)(getResources().getDisplayMetrics().density * 8);
            }
        });

        invitesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        invitesRecyclerView.setAdapter(invitesAdapter);

        emptyMessage = rootView.findViewById(R.id.empty_invitations_message);
        progress = rootView.findViewById(R.id.progress);

        refreshInvites();

        return rootView;
    }


    InvitesAdapter.InviteViewHolder.OnInviteOptionsClicked onInviteClickListener = new InvitesAdapter.InviteViewHolder.OnInviteOptionsClicked() {
        @Override
        public void onInviteResponded(View view, final int position, boolean accepted) {
            final Invitation invitation = invitesAdapter.getInvitation(position);
            if (accepted) {
                Intent intent = new Intent(getActivity(), EventActivity.class);
                intent.putExtra(EventActivity.INVITATION, invitesAdapter.getInvitation(position));
                intent.putExtra(EventActivity.EVENT, invitesAdapter.getInvitation(position).getEvent());
                intent.putExtra(EventActivity.OPENED_FROM_INVITES, true);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ImageView image = (ImageView) view.findViewById(R.id.event_photo);
                    View imageGradient = view.findViewById(R.id.image_gradient);
                    TextView eventName = (TextView) view.findViewById(R.id.event_name);
                    TextView eventTime = (TextView) view.findViewById(R.id.event_time_info);
                    TextView eventSummary = (TextView) view.findViewById(R.id.event_invitee);
                    RecyclerView people = (RecyclerView) view.findViewById(R.id.people_recycler_view);

                    if (image != null) {
                        getActivity().startActivityForResult(intent, MainActivity.GLOBAL_REFRESH, ActivityOptions.makeSceneTransitionAnimation(getActivity(), new Pair<View, String>(image, "eventImage"), new Pair<View, String>(eventName, "eventName"), new Pair<View, String>(eventTime, "eventTime"), new Pair<View, String>(people, "people"), new Pair<View, String>(eventSummary, "eventInvitee"), new Pair<View, String>(imageGradient, "imageGradient")).toBundle());
                    } else {
                        getActivity().startActivityForResult(intent, MainActivity.GLOBAL_REFRESH, ActivityOptions.makeSceneTransitionAnimation(getActivity(), new Pair<View, String>(eventName, "eventName"), new Pair<View, String>(eventTime, "eventTime"), new Pair<View, String>(people, "people"), new Pair<View, String>(eventSummary, "eventInvitee")).toBundle());
                    }
                } else {
                    startActivityForResult(intent, MainActivity.GLOBAL_REFRESH);
                }
            } else {
                DialogBuilder.showYesNoDialog(getActivity(), getString(R.string.you_sure_to_exit_event), new DialogBuilder.OnYesNoDialogResponse() {
                    @Override
                    public void onResponse(boolean pressedYes) {
                        if(pressedYes){
                            try {
                                ApiHelper.getInstance(getActivity()).respondToInvitation(invitation.getEvent().getId(), invitation.getId(), false, null, null, null, null, new ApiHelper.OnEventRespondListener() {
                                    @Override
                                    public void onResponded(Event event) {
                                        invitesAdapter.removeInvitation(position);
                                    }

                                    @Override
                                    public void onFail(String errorMessage) {
                                        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
            }
        }
    };


    private void refreshInvites() {
    progress.setVisibility(View.VISIBLE);
        final InvitesEventsDataLoader iedl = new InvitesEventsDataLoader(getActivity(), new InvitesEventsTable(CalendarDatabaseHelper.getInstance(getActivity()).getWritableDatabase()), null, null, null, null, null);
        ApiHelper.getInstance(getActivity()).getUser(SessionManager.getInstance(getActivity()).getUser().getId(), new ApiHelper.OnGetUserListener() {
            @Override
            public void onGetUser(User me) {

                List<Invitation> invites = new ArrayList<>();
                for (Invitation i : me.getInvitations()) {
                    if(i.getStatus() == Invitation.Status.ACCEPTED || i.getStatus() == Invitation.Status.REJECTED) continue;

                    ApiHelper.getInstance(getActivity()).getEvent(i.getEvent().getId(), new ApiHelper.OnGetEventListener() {
                        @Override
                        public void onEvent(Event event) {
                            iedl.insert(event);
                        }

                        @Override
                        public void onFail(String error) {

                        }
                    });
                    if(i.getStatus() == Invitation.Status.WAITING) invites.add(i);
                }

                if(dataLoader != null){
                    dataLoader.deleteAll();
                    dataLoader.insert(me.getInvitations());
                }

                    if(invites.isEmpty()) emptyMessage.setVisibility(View.VISIBLE);
                    else emptyMessage.setVisibility(View.GONE);


                SessionManager.getInstance(getActivity()).setNumOfInvitations(invites.size());

                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFail(String errorMessage) {
                progress.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    SQLiteDatabase database;

    @Override
    public Loader<List<Invitation>> onCreateLoader(int id, Bundle args) {
        database = CalendarDatabaseHelper.getInstance(getActivity()).getWritableDatabase();
        MyInvitationsTable table = new MyInvitationsTable(database);
        return new MyInvitationsDataLoader(getActivity(), table, MyInvitationsTable.COLUMN_STATUS + "=?", new String[]{Invitation.Status.WAITING.toString()}, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<List<Invitation>> loader, List<Invitation> data) {

        invitesAdapter.setInvitations(data);

    }

    @Override
    public void onLoaderReset(Loader<List<Invitation>> loader) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MainActivity.GLOBAL_REFRESH && resultCode == Activity.RESULT_OK)
            refreshInvites();
    }
}
