package com.whenapp.fragments;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.SharedElementCallback;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.whenapp.AddFriendsActivity;
import com.whenapp.BaseActivity;
import com.whenapp.R;
import com.whenapp.adapters.FacebookContactsAdapter;
import com.whenapp.adapters.UserGroupsAdapter;
import com.whenapp.adapters.UsersAdapter;
import com.whenapp.common.Utils;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.FriendsDataLoader;
import com.whenapp.database.FriendsTable;
import com.whenapp.database.UserGroupsDataLoader;
import com.whenapp.database.UsersDataLoader;
import com.whenapp.database.UsersGroupsTable;
import com.whenapp.database.UsersTable;
import com.whenapp.model.ContactToken;
import com.whenapp.model.GroupContactToken;
import com.whenapp.model.SessionManager;
import com.whenapp.model.User;
import com.whenapp.model.UserContactToken;
import com.whenapp.model.UsersGroup;
import com.whenapp.views.PersonCompletionView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ContactsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactsFragment extends Fragment implements UsersAdapter.OnUserClickedListener, UserGroupsAdapter.OnUserGroupClickedListener, LoaderManager.LoaderCallbacks<List<User>>,FacebookContactsAdapter.OnFacebookContactClickedListener {

    List<User> users;
    List<UsersGroup> usersGroups;
    List<ContactToken> selectedObjects;

    PersonCompletionView completionView;
    List<ContactToken> completion = new ArrayList<>();

    LoadPhoneContacts loadPhoneContacts;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ContactsFragment.
     */
    public static ContactsFragment newInstance(List<User> users, List<UsersGroup> groups, List<ContactToken> selectedContacts) {
        ContactsFragment fragment = new ContactsFragment();
        fragment.setContacts(users, groups, selectedContacts);
        return fragment;
    }

    public ContactsFragment() {
        // Required empty public constructor
    }


    PersonCompletionView.CompletionAdapter userAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
        getLoaderManager().initLoader(31, null, this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_contacts, container, false);



        for (UsersGroup ug : usersGroups) {
            completion.add(new GroupContactToken(ug, ug.getName(), "http://cdn.hellogiggles.com/wp-content/uploads/2014/06/01/Friends-007.jpg"));
        }

        userAdapter = new PersonCompletionView.CompletionAdapter(getActivity(), R.layout.user_layout, completion);
        completionView = (PersonCompletionView) rootView.findViewById(R.id.person_completion_view);
        char[] splitChar = {',', ';', ' '};
        completionView.setSplitChar(splitChar);
        completionView.setAdapter(userAdapter);
        completionView.allowDuplicates(false);
        for(ContactToken o : selectedObjects) completionView.addObject(o);


        ViewPager contactsPager = (ViewPager) rootView.findViewById(R.id.contacts_view_pager);
        contactsPager.setAdapter(new ContactsPagerAdapter(getActivity(), getChildFragmentManager(), users, usersGroups, this, this, this));
        contactsPager.setOffscreenPageLimit(2);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        tabs.setViewPager(contactsPager);

        setEnterSharedElementCallback(new SharedElementCallback() {
            @Override
            public void onSharedElementEnd(List<String> sharedElementNames, List<View> sharedElements, List<View> sharedElementSnapshots) {
                super.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots);
                completionView.requestFocus();
            }
        });

        loadPhoneContacts = new LoadPhoneContacts();
        if(ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED )
        loadPhoneContacts.execute();

        return rootView;
    }


    @Override
    public void onDestroy() {
        if(loadPhoneContacts != null)
            loadPhoneContacts.cancel(true);
        loadPhoneContacts = null;
        super.onDestroy();
    }

    public List<ContactToken> getSelectedContacts(){
        return completionView.getObjects();
    }

    public void setContacts(List<User> users, List<UsersGroup> usersGroups, List<ContactToken> selectedContacts) {
        this.users = users;
        this.usersGroups = usersGroups;
        this.selectedObjects = selectedContacts;

        User me = SessionManager.getInstance(getActivity()).getUser();
        if(this.users.contains(me)) this.users.remove(me);
    }

    @Override
    public void onUserClicked(User user) {
        completionView.addObject(new UserContactToken(user, user.getName(), user.getImage()));
    }

    @Override
    public void onUserGroupClicked(UsersGroup usersGroup) {
        completionView.addObject(new GroupContactToken(usersGroup, usersGroup.getName(), ""));
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_create_event, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == R.id.action_done){
            getActivity().onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<List<User>> onCreateLoader(int id, Bundle args) {
        return new FriendsDataLoader(getActivity(), new FriendsTable(CalendarDatabaseHelper.getInstance(getActivity()).getWritableDatabase()), null, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<List<User>> loader, List<User> data) {

        User me = SessionManager.getInstance(getActivity()).getUser();
        if(data.contains(me)) data.remove(me);

        Set<User> dataSet = new LinkedHashSet<>(data);

        Set<ContactToken> token = new LinkedHashSet<>(completion);
        completion = new ArrayList<>(token);

        for (User u : dataSet) {
            completion.add(new UserContactToken(u, u.getName(), u.getImage()));
        }

        userAdapter.addAll(completion);
        //PersonCompletionView.CompletionAdapter userAdapter = new PersonCompletionView.CompletionAdapter(getActivity(), R.layout.user_layout, completion);
        //completionView.setAdapter(userAdapter);
    }

    @Override
    public void onLoaderReset(Loader<List<User>> loader) {

    }

    @Override
    public void onContactClicked(User user) {
        completionView.addObject(new UserContactToken(user, user.getName(), user.getImage()));
    }

    @Override
    public void onContactAddToFriendsClicked(User user) {
            //not used here
    }

    @Override
    public void onContactRemoveFromFriendsClicked(User user) {
        //not used here
    }

    public class LoadPhoneContacts extends AsyncTask<Void, Void, List<User>> {

        @Override
        protected List<User> doInBackground(Void... params) {
            return AddFriendsActivity.getPhoneContacts(getActivity());
        }

        @Override
        protected void onPostExecute(List<User> users) {
            if(getActivity() == null) return;

            Set<User> dataSet = new LinkedHashSet<>(users);

            List<ContactToken> newTokens = new ArrayList<>();
            for(User user : dataSet){
                newTokens.add(new UserContactToken(user, user.getName(), user.getImage()));
            }

            //PersonCompletionView.CompletionAdapter completionAdapter = new PersonCompletionView.CompletionAdapter(getActivity(), R.layout.user_layout, completion);
            userAdapter.addAll(newTokens);
            //completionView.setAdapter(completionAdapter);
        }
    }


    /**
     *
     */
    public static class ContactsPagerAdapter extends FragmentPagerAdapter {
        Context context;
        List<User> users;
        List<UsersGroup> usersGroups;
        UsersAdapter.OnUserClickedListener onUserClickedListener;
        FacebookContactsAdapter.OnFacebookContactClickedListener onFacebookContactClickedListener;
        UserGroupsAdapter.OnUserGroupClickedListener onUserGroupClickedListener;

        public ContactsPagerAdapter(Context context, FragmentManager fm, List<User> users, List<UsersGroup> usersGroups, UsersAdapter.OnUserClickedListener onUserClickedListener, UserGroupsAdapter.OnUserGroupClickedListener onUserGroupClickedListener, FacebookContactsAdapter.OnFacebookContactClickedListener onFacebookContactClickedListener) {
            super(fm);
            this.context = context;
            this.usersGroups = usersGroups;
            this.users = users;
            this.onUserClickedListener = onUserClickedListener;
            this.onUserGroupClickedListener = onUserGroupClickedListener;
            this.onFacebookContactClickedListener = onFacebookContactClickedListener;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                FriendsFragment.FriendsListFragment ff = FriendsFragment.FriendsListFragment.newInstance(users, onUserClickedListener);
                ff.setAddFABVisible(false);
                return ff;
            }else if(position == 1) {
                return UserGroupsListFragment.newInstance(usersGroups, onUserGroupClickedListener);
            }
            else
                return FacebookContactsListFragment.newInstance(false, onFacebookContactClickedListener);
        }

        @Override
        public int getCount() {
            if(SessionManager.getInstance(context).isSocialLoggedIn(SessionManager.SocialLoginType.facebook))
            return 3;
            else return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return context.getResources().getString(R.string.favorites);
                case 1:
                    return context.getResources().getString(R.string.groups);
                case 2:
                    return context.getResources().getString(R.string.facebook_contacts);
                default:
                    return "";
            }
        }
    }


    public static class PersonsListFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<User>>{

        private List<User> users;
        private UsersAdapter.OnUserClickedListener onUserClickedListener;
        UsersAdapter usersAdapter;

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment PersonsListFragment.
         */
        public static PersonsListFragment newInstance(List<User> users, UsersAdapter.OnUserClickedListener onUserClickedListener) {
            PersonsListFragment fragment = new PersonsListFragment();
            fragment.setUsers(users);
            fragment.setOnUserClickedListener(onUserClickedListener);
            return fragment;
        }

        public PersonsListFragment() {
            // Required empty public constructor
        }

        public void setUsers(List<User> users) {
            this.users = users;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            getLoaderManager().initLoader(25, null, this);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View rootView = inflater.inflate(R.layout.fragment_event_user_list, container, false);
            RecyclerView usersRecyclerView = (RecyclerView) rootView.findViewById(R.id.users_recycler_view);
            usersAdapter = new UsersAdapter(users, onUserClickedListener);
            usersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()){
                @Override
                public int getPaddingBottom() {
                    return (int) (64 * getResources().getDisplayMetrics().density);
                }
            });
            usersRecyclerView.setAdapter(usersAdapter);
            usersRecyclerView.setItemAnimator(new DefaultItemAnimator());
            return rootView;
        }

        public void setOnUserClickedListener(UsersAdapter.OnUserClickedListener onUserClickedListener) {
            this.onUserClickedListener = onUserClickedListener;
        }

        @Override
        public Loader<List<User>> onCreateLoader(int id, Bundle args) {
            SQLiteDatabase database = CalendarDatabaseHelper.getInstance(getActivity()).getWritableDatabase();
            UsersTable usersTable = new UsersTable(database);
            UsersDataLoader udl = new UsersDataLoader(getActivity(), usersTable, null, null, null, null, null);
            return udl;
        }

        @Override
        public void onLoadFinished(Loader<List<User>> loader, List<User> data) {
            users = data;
            usersAdapter.setUsers(data);


        }

        @Override
        public void onLoaderReset(Loader<List<User>> loader) {

        }
    }


    public static class UserGroupsListFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<UsersGroup>> {

        private List<UsersGroup> usersGroups;
        private UserGroupsAdapter.OnUserGroupClickedListener onUserGroupClickedListener;
        UserGroupsAdapter userGroupsAdapter;
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment PersonsListFragment.
         */
        public static UserGroupsListFragment newInstance(List<UsersGroup> usersGroups, UserGroupsAdapter.OnUserGroupClickedListener onUserGroupClickedListener) {
            UserGroupsListFragment fragment = new UserGroupsListFragment();
            fragment.setUsersGroups(usersGroups);
            fragment.setOnUserGroupClickedListener(onUserGroupClickedListener);
            return fragment;
        }

        public UserGroupsListFragment() {
            // Required empty public constructor
        }

        public void setUsersGroups(List<UsersGroup> usersGroups) {
            this.usersGroups = usersGroups;
        }

        public void setOnUserGroupClickedListener(UserGroupsAdapter.OnUserGroupClickedListener onUserGroupClickedListener) {
            this.onUserGroupClickedListener = onUserGroupClickedListener;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            getLoaderManager().initLoader(26, null, this);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View rootView = inflater.inflate(R.layout.fragment_event_user_groups_list, container, false);
            RecyclerView usersRecyclerView = (RecyclerView) rootView.findViewById(R.id.user_groups_recycler_view);
            userGroupsAdapter = new UserGroupsAdapter(usersGroups, onUserGroupClickedListener);
            usersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()){
                @Override
                public int getPaddingBottom() {
                    return (int) (64 * getResources().getDisplayMetrics().density);
                }
            });

            usersRecyclerView.setAdapter(userGroupsAdapter);
            usersRecyclerView.setItemAnimator(new DefaultItemAnimator());


            return rootView;
        }


        @Override
        public Loader<List<UsersGroup>> onCreateLoader(int id, Bundle args) {
            SQLiteDatabase database = CalendarDatabaseHelper.getInstance(getActivity()).getWritableDatabase();
            UsersGroupsTable usersTable = new UsersGroupsTable(database);
            return new UserGroupsDataLoader(getActivity(), usersTable, null, null, null, null, null);
        }

        @Override
        public void onLoadFinished(Loader<List<UsersGroup>> loader, List<UsersGroup> data) {
            usersGroups = data;
            userGroupsAdapter.setGroups(data);
        }

        @Override
        public void onLoaderReset(Loader<List<UsersGroup>> loader) {

        }

    }

}
