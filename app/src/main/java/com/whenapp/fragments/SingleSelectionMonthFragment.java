package com.whenapp.fragments;

import android.os.Bundle;

import com.whenapp.adapters.MonthAdapter;
import com.whenapp.adapters.SingleSelectionMonthAdapter;

import java.util.Date;

/**
 * Created by tomislav on 16/04/15.
 */
public class SingleSelectionMonthFragment extends MonthFragment {

    public static SingleSelectionMonthFragment newInstance(int month, int year, OnDateClickedListener onDateClickedListener) {
        SingleSelectionMonthFragment fragment = new SingleSelectionMonthFragment();
        Bundle args = new Bundle();
        args.putInt("month", month);
        args.putInt("year", year);
        fragment.setArguments(args);
        fragment.setOnDateClickedListener(onDateClickedListener);
        return fragment;
    }

    @Override
    public MonthAdapter getMonthAdapter(int month, int year, final OnDateClickedListener onDateClickedListener) {
        return new SingleSelectionMonthAdapter(month, year, new MonthAdapter.DateViewHolder.OnDateViewHolderClickedListener() {
            @Override
            public void onDateViewHolderClicked(int day, Date date) {
                onDateClickedListener.onDateClicked(date);

            }
        });
    }


}
