package com.whenapp.fragments;


import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.analytics.HitBuilders;
import com.whenapp.AddFriendsActivity;
import com.whenapp.BaseActivity;
import com.whenapp.MainActivity;
import com.whenapp.NewGroupActivity;
import com.whenapp.ProfileActivity;
import com.whenapp.R;
import com.whenapp.UsersGroupActivity;
import com.whenapp.adapters.FacebookContactsAdapter;
import com.whenapp.adapters.UserGroupsAdapter;
import com.whenapp.adapters.UsersAdapter;
import com.whenapp.connection.ApiHelper;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.FriendsDataLoader;
import com.whenapp.database.FriendsTable;
import com.whenapp.database.UserGroupsDataLoader;
import com.whenapp.database.UsersGroupsTable;
import com.whenapp.model.SessionManager;
import com.whenapp.model.User;
import com.whenapp.model.UsersGroup;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FriendsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FriendsFragment extends Fragment implements SearchView.OnQueryTextListener, UsersAdapter.OnUserClickedListener, UserGroupsAdapter.OnUserGroupClickedListener, ViewPager.OnPageChangeListener, FacebookContactsAdapter.OnFacebookContactClickedListener{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SECTION_NUMBER = "section_number";


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FriendsFragment.
     */
    public static FriendsFragment newInstance(int sectionNumber) {
        FriendsFragment fragment = new FriendsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public FriendsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_friends, container, false);

        List<User> users = new ArrayList<>();
        List<UsersGroup> usersGroups = new ArrayList<>();

        ViewPager contactsPager = (ViewPager) rootView.findViewById(R.id.friends_view_pager);
        contactsPager.setAdapter(new ContactsPagerAdapter(getActivity(), getChildFragmentManager(), users, usersGroups, this, this, this));
        contactsPager.setOnPageChangeListener(this);
        contactsPager.setOffscreenPageLimit(2);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.tabs);
        tabs.setVisibility(View.VISIBLE);
        tabs.setViewPager(contactsPager);

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int actionId = item.getItemId();


        if(actionId == R.id.action_search){
            ((BaseActivity)getActivity()).getSupportActionBar().setCustomView(searchView);
            ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);
            return true;
        }else if (actionId == R.id.action_invite) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey, let's meet for a cup of coffee! I'm using When? app to schedule my meetings, download it from Google Play Store: goo.gl/70t5M8");
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Invite friends via..."));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment f : getChildFragmentManager().getFragments()) {
            f.onActivityResult(requestCode, resultCode, data);
        }
    }

    SearchView searchView;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.friends, menu);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(this);

        // text color
        AutoCompleteTextView searchText = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchText.setTextColor(Color.parseColor("#404040"));
        searchText.setHintTextColor(Color.parseColor("#AAAAAA"));
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        if (getChildFragmentManager() != null && getChildFragmentManager().getFragments() != null) {
            ((Searchable) getChildFragmentManager().getFragments().get(0)).setFilterQuery(s);
            ((Searchable) getChildFragmentManager().getFragments().get(1)).setFilterQuery(s);
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Get the intent, verify the action and get the query
        Intent intent = getActivity().getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            ((Searchable) getChildFragmentManager().getFragments().get(0)).setFilterQuery(query);
            ((Searchable) getChildFragmentManager().getFragments().get(1)).setFilterQuery(query);

        }
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public void onUserClicked(User user) {
        Intent userIntent = new Intent(getActivity(), ProfileActivity.class);
        userIntent.putExtra("user", user);
        startActivity(userIntent);
    }

    @Override
    public void onUserGroupClicked(UsersGroup usersGroup) {
        Intent i = new Intent(getActivity(), UsersGroupActivity.class);
        i.putExtra(UsersGroupActivity.USERS_GROUP, usersGroup);
        startActivityForResult(i, 320);
    }

    int pageSelected = 0;

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        pageSelected = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onContactClicked(User user) {
        Intent userIntent = new Intent(getActivity(), ProfileActivity.class);
        userIntent.putExtra("user", user);
        startActivity(userIntent);
    }

    @Override
    public void onContactAddToFriendsClicked(final User user) {
        try {
            ApiHelper.getInstance(getActivity()).addFriend(user.getId(), user.getEmail(), new ApiHelper.OnAddFriendListener() {
                @Override
                public void onFriendAdded() {
                    ((Refresh)getChildFragmentManager().getFragments().get(0)).refresh();
                    Toast.makeText(getContext(), user.getName() + " is added to favorites.", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFail(String errorMessage) {
                    Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onContactRemoveFromFriendsClicked(final User user) {
        try {
            ApiHelper.getInstance(getActivity()).removeFriend(SessionManager.getInstance(getActivity()).getUser().getId(), user.getId(), new ApiHelper.OnRemoveFriendListener() {
                @Override
                public void onFriendRemoved() {
                    ((Refresh)getChildFragmentManager().getFragments().get(0)).refresh();
                    Toast.makeText(getContext(), user.getName() + " is removed from favorites.", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFail(String errorMessage) {
                    Toast.makeText(getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static class ContactsPagerAdapter extends FragmentPagerAdapter {
        Context context;
        List<User> users;
        List<UsersGroup> usersGroups;
        UsersAdapter.OnUserClickedListener onUserClickedListener;
        UserGroupsAdapter.OnUserGroupClickedListener onGroupClickListener;
        FacebookContactsAdapter.OnFacebookContactClickedListener onFacebookContactClickedListener;

        public ContactsPagerAdapter(Context context, FragmentManager fm, List<User> users, List<UsersGroup> usersGroups, UsersAdapter.OnUserClickedListener onUserClickedListener, UserGroupsAdapter.OnUserGroupClickedListener onGroupClickListener, FacebookContactsAdapter.OnFacebookContactClickedListener onFacebookContactClickedListener) {
            super(fm);
            this.context = context;
            this.usersGroups = usersGroups;
            this.users = users;
            this.onUserClickedListener = onUserClickedListener;
            this.onGroupClickListener = onGroupClickListener;
            this.onFacebookContactClickedListener = onFacebookContactClickedListener;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0)
                return FriendsListFragment.newInstance(users, onUserClickedListener);
            else if(position == 1)
                return UserGroupsListFragment.newInstance(usersGroups, onGroupClickListener);
            else
                return FacebookContactsListFragment.newInstance(true, onFacebookContactClickedListener);
        }

        @Override
        public int getCount() {
            if(SessionManager.getInstance(context).isSocialLoggedIn(SessionManager.SocialLoginType.facebook))
                return 3;
            else return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return context.getResources().getString(R.string.favorites);
                case 1:
                    return context.getResources().getString(R.string.groups);
                case 2:
                    return context.getResources().getString(R.string.facebook_contacts);
                default:
                    return "";
            }
        }
    }

    public interface Searchable {
        public void setFilterQuery(String query);
    }

    public static class FriendsListFragment extends Fragment implements Searchable, LoaderManager.LoaderCallbacks<List<User>>, Refresh {

        private List<User> users = new ArrayList<>();
        private UsersAdapter.OnUserClickedListener onUserClickedListener;
        UsersAdapter usersAdapter;
        private boolean fabVisible = true;
        FriendsDataLoader udl;

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment PersonsListFragment.
         */
        public static FriendsListFragment newInstance(List<User> users, UsersAdapter.OnUserClickedListener onUserClickedListener) {
            FriendsListFragment fragment = new FriendsListFragment();
            fragment.setUsers(users);
            fragment.setOnUserClickedListener(onUserClickedListener);
            return fragment;
        }

        public FriendsListFragment() {
            // Required empty public constructor
        }

        public void setAddFABVisible(boolean visible) {
            fabVisible = visible;
        }

        public void setUsers(List<User> users) {
            this.users = users;

            User me = SessionManager.getInstance(getActivity()).getUser();
            if(this.users.contains(me)) this.users.remove(me);
            if(usersAdapter != null)
                usersAdapter.setUsers(this.users);
        }


        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            udl = (FriendsDataLoader) getLoaderManager().initLoader(31, null, this);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View rootView = inflater.inflate(R.layout.fragment_user_list, container, false);
            RecyclerView usersRecyclerView = (RecyclerView) rootView.findViewById(R.id.users_recycler_view);
            usersAdapter = new UsersAdapter(users, onUserClickedListener);
            usersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()){
                @Override
                public int getPaddingBottom() {
                    return (int) (64 * getResources().getDisplayMetrics().density);
                }
            });
            usersRecyclerView.setAdapter(usersAdapter);
            usersRecyclerView.setItemAnimator(new DefaultItemAnimator());

            FloatingActionButton addFriend = (FloatingActionButton) rootView.findViewById(R.id.add_friend);
            addFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent addFriends = new Intent(getActivity(), AddFriendsActivity.class);
                    getActivity().startActivityForResult(addFriends, 333);
                }
            });

            if(!fabVisible) addFriend.setVisibility(View.GONE);

            return rootView;
        }

        public void setOnUserClickedListener(UsersAdapter.OnUserClickedListener onUserClickedListener) {
            this.onUserClickedListener = onUserClickedListener;
        }

        public void setFilterQuery(String query) {
            usersAdapter.setFilter(query);
        }

        @Override
        public Loader<List<User>> onCreateLoader(int id, Bundle args) {
            SQLiteDatabase database = CalendarDatabaseHelper.getInstance(getActivity()).getWritableDatabase();
            FriendsTable usersTable = new FriendsTable(database);
            return new FriendsDataLoader(getActivity(), usersTable, null, null, null, null, null);
        }

        @Override
        public void onLoadFinished(Loader<List<User>> loader, List<User> data) {
            List<User> friends = new ArrayList<>();
            for(User u : data){
                friends.add(u);
            }

            User me = SessionManager.getInstance(getActivity()).getUser();
            if(friends.contains(me)) friends.remove(me);

            users = friends;
            usersAdapter.setUsers(friends);

        }

        @Override
        public void onLoaderReset(Loader<List<User>> loader) {

        }


        int totalFriends = 0;
        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);


            if(requestCode == 333) {
                if (resultCode == Activity.RESULT_OK) {
                    totalFriends = 0;
                    final List<User> users =  data.getParcelableArrayListExtra("contacts");

                    for(final User u : users){
                        try {
                            ApiHelper.getInstance(getActivity()).addFriend(SessionManager.getInstance(getActivity()).getUser().getId(), u.getEmail(), new ApiHelper.OnAddFriendListener() {
                                @Override
                                public void onFriendAdded() {
                                    ((BaseActivity) getActivity()).getGATracker().send(new HitBuilders.EventBuilder("UserGroup", "UserAdded").setLabel(u.getEmail()).build());
                                    udl.insert(u);
                                    totalFriends++;
                                    if(totalFriends == users.size()) refreshFriends();
                                }

                                @Override
                                public void onFail(String errorMessage) {
                                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }
            }
        }


        private void refreshFriends() {
            ApiHelper.getInstance(getActivity()).getUser(SessionManager.getInstance(getActivity()).getUser().getId(), new ApiHelper.OnGetUserListener() {
                @Override
                public void onGetUser(User me) {
                    udl.deleteAll();
                    if(me.getFriends().contains(me))
                        me.getFriends().remove(me);
                    udl.insert(me.getFriends());
                }

                @Override
                public void onFail(String errorMessage) {

                }
            });
        }

        @Override
        public void refresh() {
            udl = (FriendsDataLoader) getLoaderManager().restartLoader(31, null, this);
        }
    }


    public static class UserGroupsListFragment extends Fragment implements Searchable, LoaderManager.LoaderCallbacks<List<UsersGroup>> {

        private List<UsersGroup> usersGroups;
        private UserGroupsAdapter.OnUserGroupClickedListener onUserGroupClickedListener;
        UserGroupsAdapter userGroupsAdapter;

        UserGroupsDataLoader userGroupsDataLoader;

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment PersonsListFragment.
         */
        public static UserGroupsListFragment newInstance(List<UsersGroup> usersGroups, UserGroupsAdapter.OnUserGroupClickedListener onUserGroupClickedListener) {
            UserGroupsListFragment fragment = new UserGroupsListFragment();
            fragment.setUsersGroups(usersGroups);
            fragment.setOnUserGroupClickedListener(onUserGroupClickedListener);
            return fragment;
        }

        public UserGroupsListFragment() {
            // Required empty public constructor
        }

        public void setUsersGroups(List<UsersGroup> usersGroups) {
            this.usersGroups = usersGroups;
        }

        public void setOnUserGroupClickedListener(UserGroupsAdapter.OnUserGroupClickedListener onUserGroupClickedListener) {
            this.onUserGroupClickedListener = onUserGroupClickedListener;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            userGroupsDataLoader = (UserGroupsDataLoader) getLoaderManager().initLoader(32, null, this);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View rootView = inflater.inflate(R.layout.fragment_user_groups_list, container, false);
            RecyclerView usersRecyclerView = (RecyclerView) rootView.findViewById(R.id.user_groups_recycler_view);
            userGroupsAdapter = new UserGroupsAdapter(usersGroups, onUserGroupClickedListener);
            usersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()){
                @Override
                public int getPaddingBottom() {
                    return (int) (64 * getResources().getDisplayMetrics().density);
                }
            });
            usersRecyclerView.setAdapter(userGroupsAdapter);
            usersRecyclerView.setItemAnimator(new DefaultItemAnimator());

            FloatingActionButton addGroup = (FloatingActionButton) rootView.findViewById(R.id.add_group);
            addGroup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent t = new Intent(getActivity(), NewGroupActivity.class);
                    getParentFragment().startActivityForResult(t, 30);
                }
            });
            return rootView;
        }

        @Override
        public void setFilterQuery(String query) {
            userGroupsAdapter.setFilter(query);
        }


        @Override
        public Loader<List<UsersGroup>> onCreateLoader(int id, Bundle args) {
            SQLiteDatabase database = CalendarDatabaseHelper.getInstance(getActivity()).getWritableDatabase();
            UsersGroupsTable usersTable = new UsersGroupsTable(database);
            return new UserGroupsDataLoader(getActivity(), usersTable, null, null, null, null, null);
        }

        @Override
        public void onLoadFinished(Loader<List<UsersGroup>> loader, List<UsersGroup> data) {
            usersGroups = data;
            userGroupsAdapter.setGroups(data);
        }

        @Override
        public void onLoaderReset(Loader<List<UsersGroup>> loader) {

        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if((requestCode == 30 || requestCode == 320) && resultCode == Activity.RESULT_OK){
                ((BaseActivity) getActivity()).getGATracker().send(new HitBuilders.EventBuilder("UserGroup", "GroupCreated").build());
                refreshGroups();
            }
        }

        private void refreshGroups() {
            ApiHelper.getInstance(getActivity()).getUser(SessionManager.getInstance(getActivity()).getUser().getId(), new ApiHelper.OnGetUserListener() {
                @Override
                public void onGetUser(User me) {
                    userGroupsDataLoader.deleteAll();
                    userGroupsDataLoader.insert(me.getGroups());
                }

                @Override
                public void onFail(String errorMessage) {

                }
            });
        }
    }
}
