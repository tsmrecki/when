package com.whenapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whenapp.R;
import com.whenapp.model.User;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileEventsStatsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileEventsStatsFragment extends Fragment {

     private User user;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileEventsStatsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileEventsStatsFragment newInstance(User user) {
        ProfileEventsStatsFragment fragment = new ProfileEventsStatsFragment();
        fragment.setUser(user);
        return fragment;
    }

    public ProfileEventsStatsFragment() {
        // Required empty public constructor
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View rootView =  inflater.inflate(R.layout.fragment_profile_events_stats, container, false);


        TextView attendedEvents = (TextView) rootView.findViewById(R.id.events_attended_num);
        TextView createdEvents = (TextView) rootView.findViewById(R.id.events_created_num);
        TextView friendsNumber = (TextView) rootView.findViewById(R.id.friends_num);

        if(user != null) {
            attendedEvents.setText(String.valueOf(user.getNumOfAttendedEvents()));
            createdEvents.setText(String.valueOf(user.getNumOfCreatedEvents()));
            friendsNumber.setText(String.valueOf(user.getNumOfFriends()));
        }

        return rootView;
    }
}
