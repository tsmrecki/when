package com.whenapp.fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.whenapp.R;
import com.whenapp.adapters.MonthAdapter;
import com.whenapp.adapters.WeekDaysAdapter;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.EventsContentProvider;
import com.whenapp.database.EventsDataLoader;
import com.whenapp.database.EventsTable;
import com.whenapp.database.InvitationsTable;
import com.whenapp.database.TerminsTable;
import com.whenapp.database.UsersTable;
import com.whenapp.model.Day;
import com.whenapp.model.Event;
import com.whenapp.model.InviteTermin;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.whenapp.fragments.MonthFragment.OnDateClickedListener} interface
 * to handle interaction events.
 * Use the {@link MonthFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public abstract class MonthFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Event>>{

    private int month;
    private int year;
    private Date selectableAfterDate = new Date(0);
    MonthAdapter adapter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param month Month of the year (1 - 12)
     * @return A new instance of fragment MonthFragment.
     */
    public static MonthFragment newInstance(boolean multipleSelection, int month, int year, OnDateClickedListener onDateClickedListener) {
        MonthFragment fragment;
        if (!multipleSelection)
            fragment = new SingleSelectionMonthFragment();
        else
            fragment = new MultiSelectionMonthFragment();

        Bundle args = new Bundle();
        args.putInt("month", month);
        args.putInt("year", year);
        args.putBoolean("multiple_selection", multipleSelection);
        fragment.setArguments(args);
        fragment.setOnDateClickedListener(onDateClickedListener);
        return fragment;
    }

    public MonthFragment() {
        // Required empty public constructor
    }

    public MonthFragment setSelectableAfterDate(Date selectableAfterDate) {
        this.selectableAfterDate = selectableAfterDate;
        return this;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null) {
            month = getArguments().getInt("month");
            year = Calendar.getInstance().get(Calendar.YEAR) + getArguments().getInt("year");
        }

        getLoaderManager().initLoader(20, null, this);
}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_month, container, false);

        RecyclerView weekDaysRecyclerView = (RecyclerView) rootView.findViewById(R.id.week_days_recycler_view);
        WeekDaysAdapter weekDaysAdapter = new WeekDaysAdapter();
        weekDaysRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 7));
        weekDaysRecyclerView.setAdapter(weekDaysAdapter);
        weekDaysRecyclerView.setHasFixedSize(true);

        RecyclerView monthRecyclerView = (RecyclerView) rootView.findViewById(R.id.month_recycler_view);
        monthRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 7));
        monthRecyclerView.setItemAnimator(new DefaultItemAnimator());
        monthRecyclerView.setHasFixedSize(true);

        adapter = getMonthAdapter(month, year, onDateClickedListener);
        adapter.setSelectableAfterDate(selectableAfterDate);
        monthRecyclerView.setAdapter(adapter);

        return rootView;
    }

    public abstract MonthAdapter getMonthAdapter(int month, int year, OnDateClickedListener onDateClickedListener);


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnDateClickedListener {
        public void onDateClicked(Date date);
    }

    private OnDateClickedListener onDateClickedListener;

    public void setOnDateClickedListener(OnDateClickedListener onDateClickedListener) {
        this.onDateClickedListener = onDateClickedListener;
    }


    @Override
    public Loader<List<Event>> onCreateLoader(int id, Bundle args) {

        return new EventsDataLoader(getActivity(), new EventsTable(CalendarDatabaseHelper.getInstance(getActivity()).getWritableDatabase()), null, null, null, null, null, year, month-1, 1);
        /*
        String[] projection = EventsTable.getAllColumns();
        CursorLoader cursorLoader = new CursorLoader(getActivity(),
                EventsContentProvider.CONTENT_URI, projection, null, null, null);

        return cursorLoader;
        */
    }


    @Override
    public void onLoadFinished(Loader<List<Event>> loader, List<Event> data) {

        SQLiteDatabase database = CalendarDatabaseHelper.getInstance(getActivity()).getWritableDatabase();
        UsersTable usersTable = new UsersTable(database);
        TerminsTable terminsTable = new TerminsTable(database);
        InvitationsTable invitationsTable = new InvitationsTable(database);

        List<Day> days = new ArrayList<>();
        /*
        if (data.moveToFirst())
            do {
                Event event = EventsTable.generateObjectFromCursor(data, usersTable, terminsTable, invitationsTable);
                List<Event> app = new ArrayList<>();
                List<Event> notApp = new ArrayList<>();
                if(event.isAccepted()) app.add(event);
                else notApp.add(event);
                if (event.isAccepted()) {
                    Day day = new Day(event.getProminentTermin().getTermin().getStart().getTime(), app, notApp);
                    days.add(day);
                }
                else if(event.getInviteTerminList().size() > 0) {
                    int currentDay = 0;
                    for(InviteTermin it : event.getInviteTerminList()) {

                        if(currentDay == it.getTermin().getDay()) continue;
                        Day day = new Day(it.getTermin().getStart().getTime(), app, notApp);
                        days.add(day);
                        currentDay = it.getTermin().getDay();
                    }

                }
            }
            while (data.moveToNext());
            */

        for (Event event : data) {
            List<Event> app = new ArrayList<>();
            List<Event> notApp = new ArrayList<>();
            if(event.isAccepted()) app.add(event);
            else notApp.add(event);
            if (event.isAccepted()) {
                Day day = new Day(event.getProminentTermin().getTermin().getStart().getTime(), app, notApp);
                days.add(day);
            }
            else if(event.getInviteTerminList().size() > 0) {
                int currentDay = 0;
                for(InviteTermin it : event.getInviteTerminList()) {

                    if(currentDay == it.getTermin().getDay()) continue;
                    Day day = new Day(it.getTermin().getStart().getTime(), app, notApp);
                    days.add(day);
                    currentDay = it.getTermin().getDay();
                }

            }
        }

        adapter.setDays(days);

    }

    @Override
    public void onLoaderReset(Loader<List<Event>> loader) {

    }
}
