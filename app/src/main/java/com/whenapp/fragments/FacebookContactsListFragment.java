package com.whenapp.fragments;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.whenapp.R;
import com.whenapp.adapters.FacebookContactsAdapter;
import com.whenapp.connection.ApiHelper;
import com.whenapp.database.CalendarDatabaseHelper;
import com.whenapp.database.FacebookContactsLoader;
import com.whenapp.database.FacebookContactsTable;
import com.whenapp.database.FriendsDataLoader;
import com.whenapp.database.FriendsTable;
import com.whenapp.model.SessionManager;
import com.whenapp.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomislav on 22/10/15.
 */
public class FacebookContactsListFragment extends Fragment implements FriendsFragment.Searchable, LoaderManager.LoaderCallbacks<List<User>> {

    private FacebookContactsAdapter.OnFacebookContactClickedListener onFacebookContactClickedListener;
    FacebookContactsAdapter usersAdapter;
    FriendsDataLoader friendsDataLoader;
    FacebookContactsLoader facebookContactsLoader;
    
    private boolean addToFriendsOptionEnabled = true;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PersonsListFragment.
     */
    public static FacebookContactsListFragment newInstance(boolean addToFriendsOptionaEnabled, FacebookContactsAdapter.OnFacebookContactClickedListener onUserClickedListener) {
        FacebookContactsListFragment fragment = new FacebookContactsListFragment();
        fragment.setOnFacebookContactClickedListener(onUserClickedListener);
        fragment.setAddToFriendsOptionEnabled(addToFriendsOptionaEnabled);
        return fragment;
    }

    public FacebookContactsListFragment() {
        // Required empty public constructor
    }

    public void setAddToFriendsOptionEnabled(boolean addToFriendsOptionEnabled) {
        this.addToFriendsOptionEnabled = addToFriendsOptionEnabled;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        usersAdapter = new FacebookContactsAdapter(addToFriendsOptionEnabled, new FacebookContactsAdapter.OnFacebookContactClickedListener() {
            @Override
            public void onContactClicked(User user) {
                onFacebookContactClickedListener.onContactClicked(user);
            }

            @Override
            public void onContactAddToFriendsClicked(User user) {
                friendsDataLoader.insert(user);
                onFacebookContactClickedListener.onContactAddToFriendsClicked(user);
            }

            @Override
            public void onContactRemoveFromFriendsClicked(User user) {
                friendsDataLoader.delete(user);
                onFacebookContactClickedListener.onContactRemoveFromFriendsClicked(user);
            }
        });
        friendsDataLoader = (FriendsDataLoader) getLoaderManager().initLoader(31, null, this);
        facebookContactsLoader = (FacebookContactsLoader) getLoaderManager().initLoader(35, null, this);
        //refreshFacebookFriends();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user_list, container, false);
        RecyclerView usersRecyclerView = (RecyclerView) rootView.findViewById(R.id.users_recycler_view);
        usersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()) {
            @Override
            public int getPaddingBottom() {
                return (int) (64 * getResources().getDisplayMetrics().density);
            }
        });
        usersRecyclerView.setAdapter(usersAdapter);
        usersRecyclerView.setItemAnimator(new DefaultItemAnimator());

        FloatingActionButton addFriend = (FloatingActionButton) rootView.findViewById(R.id.add_friend);
        addFriend.setVisibility(View.GONE);

        return rootView;
    }

    public void setOnFacebookContactClickedListener(FacebookContactsAdapter.OnFacebookContactClickedListener onFacebookContactClickedListener) {
        this.onFacebookContactClickedListener = onFacebookContactClickedListener;
    }

    public void setFilterQuery(String query) {
        usersAdapter.setFilter(query);
    }

    @Override
    public Loader<List<User>> onCreateLoader(int id, Bundle args) {
        SQLiteDatabase database = CalendarDatabaseHelper.getInstance(getActivity()).getWritableDatabase();
        if(id == 35) {
            FacebookContactsTable usersTable = new FacebookContactsTable(database);
            return new FacebookContactsLoader(getActivity(), usersTable, null, null, null, null, null);
        }else {
            FriendsTable friendsTable = new FriendsTable(database);
            return new FriendsDataLoader(getActivity(), friendsTable, null, null, null, null, null);
        }
    }

    @Override
    public void onLoadFinished(Loader<List<User>> loader, List<User> data) {
            List<User> users = new ArrayList<>();
            for (User u : data) {
                users.add(u);
            }

            User me = SessionManager.getInstance(getActivity()).getUser();
            if (users.contains(me)) users.remove(me);


            if(loader.getId() == 35) {
                if (usersAdapter != null)
                    usersAdapter.setUsers(users);
            }

            if (loader.getId() == 31) {
                if (usersAdapter != null)
                    usersAdapter.setFavorites(users);
            }
    }

    @Override
    public void onLoaderReset(Loader<List<User>> loader) {

    }


    private void refreshFacebookFriends() {

        ApiHelper.getInstance(getActivity()).getUser(SessionManager.getInstance(getActivity()).getUser().getId(), new ApiHelper.OnGetUserListener() {
            @Override
            public void onGetUser(User me) {
                //facebookContactsLoader.deleteAll();
                if(me.getFriends().contains(me))
                    me.getFriends().remove(me);
                facebookContactsLoader.insert(me.getFriends());
            }

            @Override
            public void onFail(String errorMessage) {

            }
        });
    }
}
