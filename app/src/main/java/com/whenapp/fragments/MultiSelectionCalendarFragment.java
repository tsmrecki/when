package com.whenapp.fragments;

import android.support.v4.app.FragmentManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by tomislav on 16/04/15.
 */
public class MultiSelectionCalendarFragment extends CalendarFragment {

    private List<Date> datesSelected = new ArrayList<>();
    private MultiSelectionMonthFragment.OnDatesSelectedListener onDatesSelectedListener;

    public static MultiSelectionCalendarFragment newInstance(MonthFragment.OnDateClickedListener onDateClickedListener, OnMonthSelectedListener onMonthSelectedListener, MultiSelectionMonthFragment.OnDatesSelectedListener onDatesSelectedListener) {
        MultiSelectionCalendarFragment fragment = new MultiSelectionCalendarFragment();
        fragment.setOnMonthSelectedListener(onMonthSelectedListener);
        fragment.setOnDateClickedListener(onDateClickedListener);
        fragment.setOnDatesSelectedListener(onDatesSelectedListener);
        return fragment;
    }

    public void setOnDatesSelectedListener(MultiSelectionMonthFragment.OnDatesSelectedListener onDatesSelectedListener) {
        this.onDatesSelectedListener = onDatesSelectedListener;
    }

    @Override
    public CalendarPagerAdapter getCalendarAdapter(FragmentManager fragmentManager) {
        return new MultiSelectionCalendarAdapter(fragmentManager, new MonthFragment.OnDateClickedListener() {
            @Override
            public void onDateClicked(Date date) {
                datesSelected.add(date);
                onDateClickedListener.onDateClicked(date);
            }
        }, new MultiSelectionMonthFragment.OnDatesSelectedListener() {
            @Override
            public void onDatesSelected(List<Date> datesSelected) {
                onDatesSelectedListener.onDatesSelected(datesSelected);
            }
        });
    }

    public List<Date> getSelectedDates() {
        return datesSelected;
    }
}
