package com.whenapp.fragments;

import android.support.v4.app.FragmentManager;

import java.util.Date;

/**
 * Created by tomislav on 16/04/15.
 */
public class SingleSelectionCalendarFragment extends CalendarFragment {

    private Date dateSelected;

    public static SingleSelectionCalendarFragment newInstance(MonthFragment.OnDateClickedListener onDateClickedListener, OnMonthSelectedListener onMonthSelectedListener) {
        SingleSelectionCalendarFragment fragment = new SingleSelectionCalendarFragment();
        fragment.setOnMonthSelectedListener(onMonthSelectedListener);
        fragment.setOnDateClickedListener(onDateClickedListener);
        return fragment;
    }

    @Override
    public CalendarPagerAdapter getCalendarAdapter(FragmentManager fragmentManager) {
        return new SingleSelectionCalendarAdapter(fragmentManager, new MonthFragment.OnDateClickedListener() {
            @Override
            public void onDateClicked(Date date) {
                dateSelected = date;
                onDateClickedListener.onDateClicked(date);
            }
        });
    }

    public Date getSelectedDate() {
        return dateSelected;
    }
}
