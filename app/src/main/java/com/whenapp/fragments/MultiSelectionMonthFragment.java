package com.whenapp.fragments;

import android.os.Bundle;

import com.whenapp.adapters.MonthAdapter;
import com.whenapp.adapters.MultipleSelectionMonthAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by tomislav on 16/04/15.
 */
public class MultiSelectionMonthFragment extends MonthFragment {

    public static MultiSelectionMonthFragment newInstance(int month, int year, OnDateClickedListener onDateClickedListener, OnDatesSelectedListener onDatesSelectedListener) {
        MultiSelectionMonthFragment fragment = new MultiSelectionMonthFragment();
        Bundle args = new Bundle();
        args.putInt("month", month);
        args.putInt("year", year);
        fragment.setArguments(args);

        fragment.setOnDateClickedListener(onDateClickedListener);
        fragment.setOnDatesSelectedListener(onDatesSelectedListener);
        return fragment;
    }

    private List<Date> datesSelected = new ArrayList<>();

    public interface OnDatesSelectedListener {
        public void onDatesSelected(List<Date> datesSelected);
    }

    private OnDatesSelectedListener onDatesSelectedListener;

    public void setOnDatesSelectedListener(OnDatesSelectedListener onDatesSelectedListener) {
        this.onDatesSelectedListener = onDatesSelectedListener;
    }


    @Override
    public MonthAdapter getMonthAdapter(int month, int year, final OnDateClickedListener onDateClickedListener) {
        return new MultipleSelectionMonthAdapter(month, year, new MonthAdapter.DateViewHolder.OnDateViewHolderClickedListener() {

            @Override
            public void onDateViewHolderClicked(int day, Date date) {
                onDateClickedListener.onDateClicked(date);
                datesSelected.add(date);
                if (onDatesSelectedListener != null)
                    onDatesSelectedListener.onDatesSelected(datesSelected);
            }
        });
    }
}
