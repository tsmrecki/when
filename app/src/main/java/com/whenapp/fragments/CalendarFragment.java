package com.whenapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.whenapp.R;
import com.whenapp.model.Day;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MonthFragment.OnDateClickedListener} interface
 * to handle interaction events.
 * Use the {@link CalendarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public abstract class CalendarFragment extends Fragment {

    private Date selectableAfterDate = new Date(0);
    protected MonthFragment.OnDateClickedListener onDateClickedListener;

    public static CalendarFragment newInstance(boolean multipleSelection, OnMonthSelectedListener onMonthSelectedListener, MonthFragment.OnDateClickedListener onDateClickedListener) {
        if (!multipleSelection)
            return SingleSelectionCalendarFragment.newInstance(onDateClickedListener, onMonthSelectedListener);
        else
            return MultiSelectionCalendarFragment.newInstance(onDateClickedListener, onMonthSelectedListener, new MultiSelectionMonthFragment.OnDatesSelectedListener() {
                @Override
                public void onDatesSelected(List<Date> datesSelected) {

                }
            });
    }

    public CalendarFragment() {
        // Required empty public constructor
    }

    public CalendarFragment setSelectableAfterDate(Date selectableAfterDate) {
        this.selectableAfterDate = selectableAfterDate;
        return this;
    }

    public void setDays(List<Day> days){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);

        final TextView monthYearView = (TextView) rootView.findViewById(R.id.month_year_view);

        ViewPager calendarViewPager = (ViewPager) rootView.findViewById(R.id.calendar_viewpager);
        CalendarPagerAdapter calendarPagerAdapter = getCalendarAdapter(getChildFragmentManager());
        calendarPagerAdapter.setSelectableAfterDate(selectableAfterDate);
        calendarViewPager.setAdapter(calendarPagerAdapter);


        calendarViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Calendar c = Calendar.getInstance();
                c.add(Calendar.YEAR, position / 12 - 30);
                c.set(Calendar.DAY_OF_MONTH, 1);
                c.set(Calendar.MONTH, position % 12);
                monthYearView.setText(new SimpleDateFormat("MMMM, yyyy", Locale.getDefault()).format(c.getTime()));

                if (onMonthSelectedListener != null) {
                    onMonthSelectedListener.onMonthSelected(c.get(Calendar.YEAR), c.get(Calendar.MONTH));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        calendarViewPager.setCurrentItem(30 * 12 + Calendar.getInstance().get(Calendar.MONTH));


        return rootView;
    }

    public interface OnMonthSelectedListener {
        void onMonthSelected(int year, int month);
    }
    private OnMonthSelectedListener onMonthSelectedListener;

    public void setOnMonthSelectedListener(OnMonthSelectedListener onMonthSelectedListener) {
        this.onMonthSelectedListener = onMonthSelectedListener;
    }

    public abstract CalendarPagerAdapter getCalendarAdapter(FragmentManager fragmentManager);

    public void setOnDateClickedListener(MonthFragment.OnDateClickedListener onDateClickedListener) {
        this.onDateClickedListener = onDateClickedListener;
    }

    static abstract class CalendarPagerAdapter extends FragmentStatePagerAdapter {

        protected MonthFragment.OnDateClickedListener onDateClickedListener;

        public CalendarPagerAdapter(FragmentManager fm, MonthFragment.OnDateClickedListener onDateClickedListener) {
            super(fm);
            this.onDateClickedListener = onDateClickedListener;
        }

        @Override
        public int getCount() {
            return 60 * 12;
        }

        protected Date selectableAfterDate = new Date(0);

        public void setSelectableAfterDate(Date selectableAfterDate) {
            this.selectableAfterDate = selectableAfterDate;
        }

    }

    static class SingleSelectionCalendarAdapter extends CalendarPagerAdapter {

        public SingleSelectionCalendarAdapter(FragmentManager fm, MonthFragment.OnDateClickedListener onDateClickedListener) {
            super(fm, onDateClickedListener);
        }

        @Override
        public Fragment getItem(int position) {
            return SingleSelectionMonthFragment.newInstance(position % 12, position / 12 - 30, onDateClickedListener).setSelectableAfterDate(selectableAfterDate);
        }
    }

    static class MultiSelectionCalendarAdapter extends CalendarPagerAdapter {

        private MultiSelectionMonthFragment.OnDatesSelectedListener onDatesSelectedListener;

        public MultiSelectionCalendarAdapter(FragmentManager fm, MonthFragment.OnDateClickedListener onDateClickedListener, MultiSelectionMonthFragment.OnDatesSelectedListener onDatesSelectedListener) {
            super(fm, onDateClickedListener);
            this.onDatesSelectedListener = onDatesSelectedListener;
        }

        @Override
        public Fragment getItem(int position) {
            return MultiSelectionMonthFragment.newInstance(position % 12, position / 12 - 30, onDateClickedListener, onDatesSelectedListener).setSelectableAfterDate(selectableAfterDate);
        }
    }

}
